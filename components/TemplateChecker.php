<?php

namespace app\components;

use app\models\AppliedAutoRegistr;
use app\models\AutoRegistr;
use yii\base\Component;
use yii\helpers\ArrayHelper;

/**
 * Class TemplateChecker
 * @package app\components
 * Класс отвечает за анализ и подсчет примененных записей текущего шаблона аккаунта ($model)
 *
 * @property int $appliedCount Кол-во примененных записей
 * @property int $remainCount Кол-во записей, которые должны быть применены
 * @property int $autoRegistrsCount Обшие кол-во записей шаблона
 */
class TemplateChecker extends Component
{
    /** @var \app\models\DispatchRegist */
    public $model;


    /**
     * Возвращает кол-во примененных записей
     * @return int
     */
    public function getAppliedCount()
    {
        if ($this->model->templateProcessing) {
            return $this->model->getAppliedAutoRegistrs()->count();
        }

        return 0;
    }

    /**
     * Возвращает кол-во записей, которые должны быть применены
     * @return int
     */
    public function getRemainCount()
    {
        if ($this->model->templateProcessing) {
            $appliedCount = $this->getAppliedCount();
            $total = $this->getAutoRegistrsCount();

            return $total - $appliedCount;
        }

        return 0;
    }

    /**
     * Возвращает кол-во записей шаблона
     * @return int
     */
    public function getAutoRegistrsCount()
    {
        if ($this->model->templateProcessing) {
            $autoRegistrsCount = AutoRegistr::find()->where(['template_id' => $this->model->template_id])->count();
            return $autoRegistrsCount;
        }

        return 0;
    }


    /**
     * Возвращает примененные записи
     * @return \app\models\AutoRegistr[]
     */
    public function getAppliedAutoRegists()
    {
        if ($this->model->templateProcessing) {
            $appliedPks = array_values(ArrayHelper::map($this->model->getAppliedAutoRegistrs()->all(), 'id', 'auto_registr_id'));

            $autoRegists = AutoRegistr::findAll($appliedPks);

            return $autoRegists;
        }

        return [];
    }

    /**
     * Возвращает кол-во записей, которые должны быть применены
     * @return \app\models\AutoRegistr[]
     */
    public function getRemainAutoRegistrs()
    {
        if ($this->model->templateProcessing) {
            $appliedPks = array_values(ArrayHelper::map($this->getAppliedAutoRegists(), 'id', 'id'));

            $remain = AutoRegistr::find()->where(['template_id' => $this->model->template_id]);

            foreach ($appliedPks as $pk)
            {
                $remain->andFilterWhere(['!=', 'id', $pk]);
            }

            return $remain->all();
        }

        return [];
    }

    /**
     * Возвращает записи шаблона
     * @return \app\models\AutoRegistr[]
     */
    public function getAutoRegistrs()
    {
        if ($this->model->templateProcessing) {
            $autoRegistrs = AutoRegistr::find()->where(['template_id' => $this->model->template_id])->all();
            return $autoRegistrs;
        }

        return [];
    }


    /**
     * @return int
     */
    public function getAppliedErrorsCount()
    {
        if($this->model->templateProcessing)
        {
            return $this->model->getAppliedAutoRegistrs()->andWhere(['status' => AppliedAutoRegistr::STATUS_ERROR])->count();
        }

        return 0;
    }

    /**
     * @return boolean
     */
    public function hasAppliedErrors()
    {
        if($this->model->templateProcessing)
        {
            return $this->getAppliedErrorsCount() > 0 ? true : false;
        }

        return false;
    }

    /**
     * Закончена ли шаблонизация
     * @return bool
     */
    public function isTemplateProcessingCompleted()
    {
        return $this->getRemainCount() == 0 ? true : false;
    }
}