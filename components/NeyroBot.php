<?php

namespace app\components;

use yii\base\Component;
use yii\helpers\ArrayHelper;

/**
 * Class NeyroBot
 * @package app\components
 */
class NeyroBot extends Component
{
    const URL = 'http://neyro.teo-bot.ru/api/default/one';
    const URL_GROUP = 'http://neyro.teo-bot.ru/api/vk/newgroup';
    const URL_DELGROUP = 'http://neyro.teo-bot.ru/api/vk/delgroup';
    const URL_EXPORT = 'http://neyro.teo-bot.ru/api/vk/export';

    /**
     * @param array $data
     * @return mixed
     */
    public static function sendInfo($data)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, self::URL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $output = curl_exec($ch);
        curl_close($ch);

        return $output;
    }

    /**
     * @param array $data
     * @return mixed
     */
    public static function sendParser($data)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, self::URL_GROUP);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $output = curl_exec($ch);
        curl_close($ch);

        return $output;
    }

    /**
     * @param int $id
     * @param array $params
     * @return mixed
     */
    public static function exportParser($id, $params = [])
    {
        $ch = curl_init();

        $params = ArrayHelper::merge(['id' => $id], $params);

        $params = http_build_query($params);

        curl_setopt($ch, CURLOPT_URL, self::URL_EXPORT.'?'.$params);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
        $output = curl_exec($ch);
        curl_close($ch);

        return $output;
    }

    public static function deleteParser($id)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, self::URL_DELGROUP);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, ['outer_tracking_group_id' => $id]);
        $output = curl_exec($ch);
        curl_close($ch);

        return $output;
    }

}

?>