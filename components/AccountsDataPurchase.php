<?php

namespace app\components;

use app\components\helpers\FunctionHelper;
use app\models\AccountingReport;
use app\models\Companies;
use app\models\DataRecipient;
use app\models\Settings;
use app\modules\api\controllers\ClientController;
use Yii;
use yii\base\Component;

/**
 * Class AccountsDataPurchase
 * @package app\components
 * Класс служит для осуществления покупок в разделе "Магазин"
 */
class AccountsDataPurchase extends Component
{
    /**
     * @var \app\models\Shop
     */
    public $model;

    /**
     * @var int
     */
    public $companyId;

    /**
     * @var int
     */
    public $amount;

    /**
     * @var string
     */
    public $purchasedAccounts;

    /**
     * @param \app\models\Shop $model
     * @param int $companyId
     * @param int $amount
     * @return self
     */
    public static function makePurchase($model, $companyId, $amount)
    {
        $purchase = new AccountsDataPurchase([
            'model' => $model,
            'companyId' => $companyId,
            'amount' => $amount,
        ]);

        $accounts = explode("\n", $purchase->model->accounts_data);

        if($amount < $purchase->model->accountsRemain)
        {
            $accountsPurchased = [];
            $accountsSaved = [];
            $count = 1;
            foreach ($accounts as $account)
            {
                if($count <= $amount)
                {
                    $accountsPurchased[] = $account;
                } else {
                    $accountsSaved[] = $account;
                }
                $count++;
            }
        } else {
            $accountsPurchased = $accounts;
            $accountsSaved = [];
        }

        $purchase->model->accounts_data = implode("\n", $accountsSaved);
//        $accountsPurchased = implode("\n", $accountsPurchased);
        $purchase->model->save(false);

        $accountsPurchasedText = implode("\n", $accountsPurchased);

        $purchase->purchasedAccounts = $accountsPurchased;

        $vk_id = Settings::find()->where(['key' => 'akk_notify'])->one()->value;
        $proxy = Settings::find()->where(['key' => 'proxy_server'])->one()->value;
        $token = Settings::find()->where(['key' => 'vk_access_token'])->one()->value;

        $userName = Yii::$app->user->identity->name;
        $userLogin = Yii::$app->user->identity->login;

        $price = $purchase->model->price * $amount;

        $accountReport = new AccountingReport([
            'company_id' => $purchase->companyId,
            'operation_type' => AccountingReport::TYPE_SHOP_PAYED,
            'amount' => $price,
            'description' => "Покупка товара \"{$purchase->model->name}\" ({$amount} ед.). $accountsPurchasedText",
        ]);

        if($accountReport->save(false))
        {
            $company = Companies::findOne(FunctionHelper::getCompanyId());
            $company->general_balance -= $price;
            $company->save();
        }

        ClientController::sendTelMessage('247187885', "Были куплены аккаунты «{$purchase->model->name}» ({$amount} шт.) пользователем «{$userName}» ({$userLogin})\nАккаунты\n{$accountsPurchasedText}");

        return $purchase;
    }
}