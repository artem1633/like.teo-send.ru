<?php

namespace app\modules\api\controllers;

use app\components\helpers\FunctionHelper;
use app\components\helpers\TagHelper;
use app\models\AccountingReport;
use app\models\Automation;
use app\models\Bots;
use app\models\BotsAnswers;
use app\models\BotsDialogs;
use app\models\Companies;
use app\models\CompanySettings;
use app\models\DailyReport;
use app\models\Dispatch;
use app\models\DispatchChats;
use app\models\DispatchConnect;
use app\models\DispatchInstagram;
use app\models\DispatchRegist;
use app\models\DispatchStatus;
use app\models\Logs;
use app\models\Message;
use app\models\MessagesStatistics;
use app\models\Proxy;
use app\models\ReferalRedirects;
use app\models\Settings;
use app\models\TemplateMessages;
use app\models\UniqueMessage;
use app\models\Users;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\rest\ActiveController;
use yii\web\Response;
use Yii;

/**
 * Default controller for the `api` module
 */
class LikeController extends ActiveController
{
    public $modelClass = 'app\models\Api';

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'only' => ['mission','mission-instagram'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }



    /**
     * Отправляет отчет о новый пользователях,
     * - сумму пополнений
     * - кол-во сообщений
     * за сутки
     */
    public function actionDailyReport()
    {
        $hours24 = time() - 86400;

        $hours24DateTime = date('Y-m-d H:i:s', $hours24);
        $hours24Date = date('Y-m-d', $hours24);


        $users = Users::find()->disableCompanyRequire()->where(['data_cr' => date('Y-m-d')])->count();
        $payments = AccountingReport::find()->where(['between', 'date_report', date('Y-m-d').' 00:00:00', date('Y-m-d').' 23:59:59'])->andWhere(['operation_type' => AccountingReport::TYPE_INCOME_BALANCE])->sum('amount');

        self::sendTelMessage('247187885', "ОТЧЕТ ЗА ДЕНЬ:
            Кол-во зарегистрированных пользователей: {$users}
            Сумма пополнений: {$payments}
        ");
    }


    public function actionTest()
    {
        self::sendTelMessage(['247187885','314811066'], 'ntcn' );
        return 'ok';
    }



    /**
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionMissionunread()//Проверяем все миссии на ограничения
    {
        $missionAll = Dispatch::find()
            ->where(['=', 'status', 'job'])
            ->all();
        /** @var Dispatch $mission */
        foreach ($missionAll as $mission) {
            $i++;

            $my_arr[$i]['acc'] = $mission->id;
            $my_arr[$i]['acc_status_start'] = $mission->status;
            $my_arr[$i]['company_id'] = $mission->company_id;

            if (!$mission->company_id) continue;

            /** @var DispatchRegist $data */
            /** @var CompanySettings $setting */

            // кол-во mission в день
            $setting = CompanySettings::find()->where(['company_id' => $mission->company_id])->one();
            $acc_message_count = $setting->messages_daily_limit;
            $my_arr[$i]['acc_message_count'] = $acc_message_count;
            $my_arr[$i]['action_mission'] = $mission->action_mission;


            //  Проверяем не перевышан лимит сообщения в день (статус становиться limit_exceeded при отправке)
            $d = date("Y-m-d H:i:s", strtotime($mission->last_mission_time . " + 24 hours"));
            $now = date("Y-m-d H:i:s");
            $hour = round((strtotime($d) - strtotime($now)) / (60 * 60));
            $my_arr[$i]['hour'] = $hour;

            $my_arr[$i]['hour_status'] = 'false';
            if ($hour <= 0) {
                $mission->action_mission = 0;
                $mission->status_job = "ok";
                $my_arr[$i]['hour_status'] = 'true';
            }


            // Проверяем еще не прошел интервал (статус становиться interval_not_end при отправке)
            $date_2 = date("Y-m-d H:i:s");
            $diff = strtotime($date_2) - strtotime($mission->last_mission_time);
            $diff = round($setting->send_interval - $diff / 60);
            $my_arr[$i]['diff'] = $diff;
            $my_arr[$i]['date_2'] = $date_2;
            $my_arr[$i]['last_mission_time'] = $mission->last_mission_time;
            $my_arr[$i]['send_interval'] = $setting->send_interval;

            $my_arr[$i]['diff_count_status'] = 'false';
            if ($diff <= 0 && $mission->action_mission < $acc_message_count) {
                $mission->status_job = 'ok';
                $my_arr[$i]['diff_count_status'] = 'true';
            }

            if ($mission->action_mission >= $acc_message_count) {
                $mission->status_job = "limit_exceeded";
            }


            $mission->update();
        }

        echo "<pre>";
        print_r($my_arr);
        echo "</pre>";

    }


    /**
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionAkkunread()//Проверяем все акк на ограничения
    {
        $i = 0;

        $akkAll = DispatchRegist::find()
            ->where(['!=', 'status', DispatchRegist::STATUS_SLEEPING])
            ->andWhere(['!=', 'status', 'account_blocked'])
            ->all();
        /** @var DispatchRegist $akk */
        foreach ($akkAll as $akk) {
            $i++;

            $my_arr[$i]['acc'] = $akk->id;
            $my_arr[$i]['acc_status_start'] = $akk->status;

            if (!$akk->company_id) continue;

            /** @var DispatchRegist $data */
            /** @var CompanySettings $setting */
            $setting = CompanySettings::find()->where(['company_id' => $akk->company_id])->one();
            // кол-во сообщения в день
            $acc_message_count = Settings::findByKey('allowed_message_per_account')->value;
            $my_arr[$i]['acc_message_count'] = $acc_message_count;
            $my_arr[$i]['sended_message_count'] = $akk->sended_message_count;


            //  Проверяем не перевышан лимит сообщения в день (статус становиться limit_exceeded при отправке)
            $d = date("Y-m-d H:i:s", strtotime($akk->last_dispatch_time . " + 24 hours"));
            $now = date("Y-m-d H:i:s");
            $hour = round((strtotime($d) - strtotime($now)) / (60 * 60));
            $my_arr[$i]['hour'] = $hour;

            $my_arr[$i]['hour_status'] = 'false';
            if ($hour < 0) {
                $akk->sended_message_count = 0;
                $akk->status = "ok";
                $my_arr[$i]['hour_status'] = 'true';
            }


            // Проверяем еще не прошел интервал (статус становиться interval_not_end при отправке)
            $date_2 = date("Y-m-d H:i:s");
            $diff = strtotime($date_2) - strtotime($akk->last_dispatch_time);
            $diff = round(Settings::findByKey('interval_in_minutes')->value - $diff / 60);
            $my_arr[$i]['diff'] = $diff;

            $my_arr[$i]['diff_count_status'] = 'false';
            if ($diff < 0 && $akk->sended_message_count < $acc_message_count) {
                $akk->status = 'ok';
                $my_arr[$i]['diff_count_status'] = 'true';
            }

            if ($akk->sended_message_count >= $acc_message_count) {
                $akk->status = "limit_exceeded";
            }

            $proxy = Proxy::findOne(['id' => $akk->proxy]);

            if (!$proxy) {
                $akk->makeBlock('Прокси не подключен');
                $akk->update();
                continue;
            }
            $url = 'https://api.vk.com/method/account.setOnline';
            $params = array(
                'access_token' => $akk->token,
                'v' => '5.80'
            );

            // В $result вернется id отправленного сообщения
            $result = file_get_contents($url, false, stream_context_create(array(
                'http' => array(
                    'proxy' => "tcp://{$proxy->ip_adress}:{$proxy->port}",
                    'request_fulluri' => true,
                    'method' => 'POST',
                    'header' => 'Content-type: application/x-www-form-urlencoded',
                    'content' => http_build_query($params)
                )
            )));
            $result = json_decode($result, true);

            //Если проверка показада ошибку и ранее ошибки не было то нужно поставить статус и отправить уведомление
            if ($result['error'] and $akk->status != DispatchStatus::STATUS_ACCOUNT_BLOCKED) {
                //Если проблемма в том что акк заморозили
                if ($result['error']["error_msg"] = 'User authorization failed: invalid access_token (2).') {
                    $my_arr[$i]['error'] = $result['error']["error_msg"];
                    $akk->makeBlock('Заморожен');
                    if (!$akk->error_notif) {
                        $company = Users::find()->where(['company_id' => $akk->company_id])->one();
                        $connect = DispatchConnect::find()->where(['dispatch_regist_id' => $akk->id])->one();
                        if ($company->vk_id) {
                            $akk->error_notif = true;
                            $text = "Аккаунт \"{$akk->username}\" не доступен по причине \"{$result['error']["error_msg"]}\"
                            Для проверки перейдите http://demo.teo-send.ru/dispatch-regist/show-akk?block={$connect->dispatch_id}";
                            // $this->send($company->vk_id, $text, $setingToken->value, $setingProxy->value);
                            self::sendTelMessage(['247187885','314811066'], $text );
                        }
                    }
                    //Если поблемма не в том что акк заморозили
                } else {
                    $my_arr[$i]['error'] = $result['error']["error_msg"];
                    $akk->makeBlock($result['error']["error_msg"]);
                    if (!$akk->error_notif) {
                        $company = Users::find()->where(['company_id' => $akk->company_id])->one();
                        $connect = DispatchConnect::find()->where(['dispatch_regist_id' => $akk->id])->one();
                        if ($company->vk_id) {
                            $akk->error_notif = true;
                            $text = "Аккаунт \"{$akk->username}\" не доступен по причине \"{$result['error']["error_msg"]}\"
                            Для проверки перейдите http://demo.teo-send.ru/dispatch-regist/show-akk?block={$connect->dispatch_id} ";
                            //$this->send($company->vk_id, $text, $setingToken->value, $setingProxy->value);
                            self::sendTelMessage(['247187885','314811066'], $text );
                        }
                    }
                }
                //Если ранее акк был заблокирована а теперь нет
            } elseif (!$result['error'] and $akk->status == DispatchStatus::STATUS_ACCOUNT_BLOCKED) {
                // $my_arr[$i]['error'] = $result['error']["error_msg"];
                $akk->status = 'ok';
                $akk->coment = $result['error']["error_msg"];
                $akk->error_notif = false;
            }
            $my_arr[$i]['acc_status_finish'] = $akk->status;
            //Проверка на заполнение полей username и account_url
            if ($akk->status == 'ok' && !$akk->username && !$akk->account_url) {
                DispatchRegist::setVkInfo($akk);
                Yii::warning("Данные обновлены. Логин: $akk->login", __METHOD__);
            }
            $akk->update();
        }

        echo "<pre>";
        print_r($my_arr);
        echo "</pre>";

    }

    /**
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionAkkunreadins()//Проверяем все акк на ограничения
    {
        $i = 0;

        $date = date('Y-m-d  H:m:s');
        $new_date = date('Y-m-d H:m:s', strtotime("-1 hours", strtotime($date)));
        $akkAll = DispatchInstagram::find()
            ->where(['!=', 'status', DispatchRegist::STATUS_SLEEPING])
            ->andWhere(['!=', 'status', 'account_blocked'])
            ->andWhere(['or',['<=', 'data', $new_date],['is','data',new \yii\db\Expression('null')]])
            ->orderBy(['rand()' => SORT_DESC])
            ->limit(300)
            ->all();

        $bloc = 0;
        foreach ($akkAll as $akk) {
//            $proxy = file_get_contents('https://dashboard.airsocks.in/partner?u=artem.kovalskiy.93@mail.ru&p=nOrlcE');
//            $proxy = json_decode($proxy);
//            foreach ($proxy->data as $item) {
//                $proxyAll[] = $item->hostIp.':'.$item->ports->http;
//            }
//            $proxy = $proxyAll[rand(0, count($proxyAll))];
//
//            /** @var DispatchInstagram $akk */
//            $url = 'https://www.instagram.com/'.$akk->login.'/?__a=1';
//
//            $result = file_get_contents($url, false, stream_context_create([
//                'https' => [
//                    'proxy' => 'tcp://'.$proxy,
//                    'request_fulluri' => true,
//                    'method' => 'GET',
//                    'content' => ['__a' => 1]
//                ]
//            ]));
//
//            var_dump($url);
//            var_dump($result);
//            var_dump($proxy);
////            exit();
//            if (!$result) {
//                $bloc ++;
//                $hourAgo = time() - 3600;
//                $hourAgoDate = date('Y-m-d H:i:s', $hourAgo);
//                if ($bloc >= 5) {
//                    $text = "Много блоков" ;
//                    self::sendTelMessage(['247187885','314811066'], $text );
//                    return 'много блоков';
//                }
//                //Если проблемма в том что акк заморозили
//                $akk->makeBlock('ban');
//                $a = serialize($result);
//                $text = "Аккаунт https://www.instagram.com/{$akk->login} не доступен по причине \"ban\" all - {$akk->all_sended_message_count}" . '  a:'.$a ;
//                self::sendTelMessage(['247187885','314811066'], $text, 1 );
//                //Если поблемма не в том что акк заморозили
//                continue;
//            }


            if (!$akk->company_id) continue;
//
            /** @var DispatchRegist $data */
            /** @var CompanySettings $setting */
            $setting = CompanySettings::find()->where(['company_id' => $akk->company_id])->one();
            // кол-во сообщения в день
            $acc_message_count = Settings::findByKey('allowed_message_per_account')->value;
            $my_arr[$i]['acc_message_count'] = $acc_message_count;
            $my_arr[$i]['sended_message_count'] = $akk->sended_message_count;


            //  Проверяем не перевышан лимит сообщения в день (статус становиться limit_exceeded при отправке)
            $d = date("Y-m-d H:i:s", strtotime($akk->last_dispatch_time . " + 24 hours"));
            $now = date("Y-m-d H:i:s");
            $hour = round((strtotime($d) - strtotime($now)) / (60 * 60));
            $my_arr[$i]['hour'] = $hour;

            $my_arr[$i]['hour_status'] = 'false';
            if ($hour < 0) {
                $akk->sended_message_count = 0;
                $akk->status = "ok";
                $my_arr[$i]['hour_status'] = 'true';
            }


            // Проверяем еще не прошел интервал (статус становиться interval_not_end при отправке)
            $date_2 = date("Y-m-d H:i:s");
            $diff = strtotime($date_2) - strtotime($akk->last_dispatch_time);
            $diff = round(Settings::findByKey('interval_in_minutes')->value - $diff / 60);
            $my_arr[$i]['diff'] = $diff;

            $my_arr[$i]['diff_count_status'] = 'false';
            if ($diff < 0 && $akk->sended_message_count < $acc_message_count) {
                $akk->status = 'ok';
                $my_arr[$i]['diff_count_status'] = 'true';
            }

            if ($akk->sended_message_count >= $acc_message_count) {
                $akk->status = "limit_exceeded";
            }

            $akk->data = date('Y-m-d H:m:s');
            $akk->save(false);

        }



        echo "<pre>";
        print_r($my_arr);
        echo "</pre>";

    }

    /**
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionBaninst()//Проверяем все акк на ограничения
    {
        $i = 0;

        $date = date('Y-m-d  H:m:s');
        $new_date = date('Y-m-d H:m:s', strtotime("-1 hours", strtotime($date)));
        $akkAll = DispatchInstagram::find()
            ->where(['!=', 'status', DispatchRegist::STATUS_SLEEPING])
            ->andWhere(['!=', 'status', 'account_blocked'])
            ->andWhere(['or',['<=', 'data', $new_date],['is','data',new \yii\db\Expression('null')]])
            ->orderBy(['rand()' => SORT_DESC])
            ->limit(100)
            ->all();

        $bloc = 0;
        foreach ($akkAll as $akk) {
            $proxy = file_get_contents('https://dashboard.airsocks.in/partner?u=artem.kovalskiy.93@mail.ru&p=nOrlcE');
            $proxy = json_decode($proxy);
            foreach ($proxy->data as $item) {
                $proxyAll[] = $item->hostIp.':'.$item->ports->http;
            }
            $proxy = $proxyAll[rand(0, count($proxyAll))];

            /** @var DispatchInstagram $akk */
            $url = 'https://www.instagram.com/'.$akk->login.'/?__a=1';

            $result = file_get_contents($url, false, stream_context_create([
                'https' => [
                    'proxy' => 'tcp://'.$proxy,
                    'request_fulluri' => true,
                    'method' => 'GET',
                    'content' => ['__a' => 1]
                ]
            ]));

            var_dump($url);
            var_dump($result);
            var_dump($proxy);
//            exit();
            if (!$result) {
                $bloc ++;
                $hourAgo = time() - 3600;
                $hourAgoDate = date('Y-m-d H:i:s', $hourAgo);
                if ($bloc >= 5) {
                    $text = "Много блоков" ;
                    self::sendTelMessage(['247187885','314811066'], $text );
                    return 'много блоков';
                }
                //Если проблемма в том что акк заморозили
                $akk->makeBlock('ban');
                $a = serialize($result);
                $text = "Аккаунт https://www.instagram.com/{$akk->login} не доступен по причине \"ban\" all - {$akk->all_sended_message_count}" . '  a:'.$a ;
                self::sendTelMessage(['247187885','314811066'], $text, 1 );
                //Если поблемма не в том что акк заморозили
                continue;
            }

        }

    }

    /**
     * Принимает информацию о новом реферальном посещении
     * @return array
     */
    public function actionSendRefer()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $headers = Yii::$app->response->headers;
        $headers->add('Access-Control-Allow-Origin', '*');

        $refer = new ReferalRedirects([
            'ip' => $_SERVER['REMOTE_ADDR'],
            'refer_company_id' => $_GET['ref'],
            'user_agent' => Yii::$app->request->userAgent,
        ]);

        return ['result' => $refer->save()];
    }

    /**
     * Принимает информацию о новом реферальном посещении
     * @return array
     */
    public function actionLog()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $result = $request->post('result');

//        $a = serialize($result);
//        self::sendTelMessage(['247187885','314811066'], "ОТЧЕТ  {$a}");
        $acc = 0;
        $eracc = 0;
        $post = 0;
        $mis = 0;
        for ($i = 0; $i < count($result); $i++){
            $acc ++;

            if (!$result[$i]['acc']['posts']) {
//                $a = serialize($result[$i]['acc']['comment']);
//                self::sendTelMessage(['247187885','314811066'], "{$_SERVER['REMOTE_ADDR']} Error acc  {$a}", 1);
                $eracc++;
                continue;
            }
            $account = DispatchInstagram::findOne($result[$i]['acc']['id']);

//            $a = serialize($result[$i]['acc']);
//            self::sendTelMessage('247187885', "ОТЧЕТ 1  {$a}");
//            $a = serialize($result[$i]['acc']['id']);
//            self::sendTelMessage('247187885', "ОТЧЕТ 2  {$result[$i]['acc']['id']}");


            for ($i2 = 0; $i2 < count($result[$i]['acc']['posts']); $i2++){
                $mis++;
                $mission = Dispatch::findOne($result[$i]['acc']['posts'][$i2][0]);

                if ($result[$i]['acc']['posts'][$i2][1] == 'false') {
                    if ($result[$i]['acc']['posts'][$i2][2] == 'error page') {
                        $mission->status = 'wait';
                        $mission->save(false);

                        $a = serialize($result[$i]['acc']['posts'][$i2]);
                        continue;
//                        self::sendTelMessage(['247187885','314811066'], "Error page {$mission->id} {$a}", 1);
                    } else {
//                        $a = serialize($result[$i]['acc']['posts'][$i2]);
//                        self::sendTelMessage(['247187885','314811066'], "{$_SERVER['REMOTE_ADDR']}  Ошибка в посте {$mission->id} {$a}", 1);
                        continue;
                    }
                    continue;
                }
                $post ++;

//                self::sendTelMessage('247187885', "ОТЧЕТ 3  мисия {$result[$i]['acc']['posts'][$i2][0]} la {$result[$i]['acc']['posts'][$i2][2]}");
                $dispatchStatus = new DispatchStatus([
                    'company_id' => $mission->company_id,
                    'account_id' => $account->login,
                    'send_account_id' => $account->id,
                    'link' => $mission->link,
                    'dispatch_id' => $mission->id,
                    'photo' => '',
                    'name' => $account->login,
                    'status' => $mission->type,
                    'data' => date("Y-m-d H:i:s"),
                ]);
                $dispatchStatus->save(false);

                $account->sended_message_count++;
                $account->all_sended_message_count++;

                $mission->action_mission = $mission->action_mission + 1;
                $mission->all_action_mission = $mission->all_action_mission + 1;


                // кол-во mission в день
                if ($account->sended_message_count >= 20) {
                    $account->status = "limit_exceeded";
                }

//                $dispatchStatus->save(false);
//                if ($result[$i]['acc']['posts'][$i2][3]) {
                $mission->like_count = $result[$i]['acc']['posts'][$i2][2];
//                }

                if ($mission->mission <= $mission->all_action_mission and $mission->status != "finish") {
                    $fullUrl = Url::toRoute(['/dispatch/view', 'id' => $mission->id], true);
//                    $text = "Рассылка {$mission->name} завершена, можете посмотреть статистику по ссылки {$fullUrl}";
//                    $userAll = ArrayHelper::getColumn(DispatchChats::find()->where(['dispatchId' => $mission->id])->all(), 'chatId');
//                    ClientController::sendTelMessage($userAll, $text);
                    $mission->status = "finish";
                    $mission->save();
                }

                $account->save(false);
                $mission->save(false);

                //отчет отправки в реальном времени
                $date = date('Y-m-d');
                $rep = DailyReport::find()
                    ->andfilterWhere(['like', 'company_id', $mission->company_id])
                    ->andfilterWhere(['like', 'dispatch_id', $mission->id])
                    ->andfilterWhere(['like', 'account_id', $account->id])
                    ->andfilterWhere(['=', 'type', DailyReport::TYPE_SENT])
                    ->andfilterWhere(['like', 'date_event', $date])
                    ->one();
                if ($rep) {
                    $rep->sended_message_count = $rep->sended_message_count + 1;
                    $rep->save(false);
                } else {
                    $report = new DailyReport([
                        'company_id' => $mission->company_id,
                        'dispatch_id' => $mission->id,
                        'account_id' => $account->id,
                        'sended_message_count' => 1,
                        'type' => DailyReport::TYPE_SENT,
                    ]);
                    $report->save();
                }
//
//                $a = serialize($result[$i]['acc']['posts'][$i2]);
//                self::sendTelMessage('247187885', "ОТЧЕТ 3  {$a}");
//                $a = serialize($result[$i]['acc']['posts'][$i2][0]);
//                self::sendTelMessage('247187885', "ОТЧЕТ 3  {$result[$i]['acc']['posts'][$i2][0]}");
            }
        }
        self::sendTelMessage(['247187885','314811066','402469716'], "{$_SERVER['REMOTE_ADDR']} Acc {$acc} ErrAcc {$eracc}  мисий {$mis} лайков {$post}");
        return ['result' =>'ok'];
    }




    /**
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionMissionInstagram()
    {
        $i = 0;
        $max_send = 0;

        // Взять сообщение статусом В очериди
        $missionsAll = Dispatch::find()->where(['status' => 'job','status_job' => 'ok','platform' => 'instagram'])->groupBy('link')->limit(3)->all();

        if (!$missionsAll) {
            return  'нету мисий';
        }

        $arrStatus = [];
        /** @var Dispatch $mission */
        $iLim = 0; //Интервал между отправками
        $i++;
        $max_send = 0;
        $like = 0;

        $proxy = file_get_contents('https://dashboard.airsocks.in/partner?u=artem.kovalskiy.93@mail.ru&p=nOrlcE');
        $proxy = json_decode($proxy);
        $send_proxy = false;

        foreach ($proxy->data as $item) {
            $proxyList[] = $item->hostIp.':'.$item->ports->http;
        }
//        $proxyList = ["91.218.246.96:42312","91.218.246.96:42314",
//            "37.203.246.51:42313","37.203.246.51:42315",
//            "37.203.246.52:42311","37.203.246.52:42314",
//            "91.218.246.73:42314","91.218.246.73:42315",
//            "158.255.3.44:42314","158.255.3.44:42315"];
        $misCount = 0;
        $payCount = 20;
        foreach ($missionsAll as $mission) {
            if ($mission->mission <= $mission->all_action_mission and $mission->status != "finish") {
                $fullUrl = Url::toRoute(['/dispatch/view', 'id' => $mission->id], true);
                $text = "Рассылка {$mission->name} завершена, можете посмотреть статистику по ссылки {$fullUrl}";
                $userAll = ArrayHelper::getColumn(DispatchChats::find()->where(['dispatchId' => $mission->id])->all(), 'chatId');
                ClientController::sendTelMessage($userAll, $text);
                $mission->status = "finish";
                $mission->save();
            }
            /** @var CompanySettings $setting */
            $setting = CompanySettings::find()->where(['company_id' => $mission->company_id])->one();

            $balance = $setting->companyModel->bonus_balance > 0 ? $setting->companyModel->bonus_balance : $setting->companyModel->general_balance;
            // Стоимость за одну мисию
            if ($balance < $setting->getPriceByMessage()) {
                $fullUrl = Url::toRoute(['/dispatch/view', 'id' => $mission->id], true);
                $text = "Рассылка {$mission->name} остановленна, у вас не достаточно средств {$fullUrl}";
                $userAll = ArrayHelper::getColumn(DispatchChats::find()->where(['dispatchId' => $mission->id])->all(), 'chatId');
                self::sendTelMessage($userAll, $text);
                $mission->status = 'wait';
                $mission->save(false);
                echo 'что то не так с balans';
                continue;
            }

            if ($setting->payed()) {
                $pay = $payCount + $setting->getPriceByMessage();
                $accountReport = new AccountingReport([
                    'company_id' => $mission->company_id,
                    'operation_type' => AccountingReport::TYPE_DISPATCH_PAYED,
                    'amount' => $pay,
                    'description' => 'Списание средств за накрутку "' . $mission->name . '". ввыполнено ' . $pay . ' действие',
                ]);
                $accountReport->save();
            }


            if ($setting->messages_daily_limit <= $mission->action_mission) {
                $mission->status_job = "limit_exceeded";
                $my_arr[$i]['limit_exceeded'] = 'true';
            }

            $missions[] = [
                'id' => $mission->id,
                'link' => $mission->link,
                'type' => $mission->type,
            ];

            $mission->status_job = "interval_not_end";
            $mission->last_mission_time = date("Y-m-d H:i:s");;
            $mission->save(false);


        }
        /** @var DispatchRegist $item */
        $accounts = DispatchInstagram::find()
            ->leftJoin('dispatch_status', 'dispatch_instagram.id != dispatch_status.send_account_id')
            ->where(['dispatch_instagram.status' => 'ok'])
            ->andWhere(['not in','dispatch_status.link', ArrayHelper::getColumn($missions, 'link')])
            ->limit($payCount)
            ->all();
//        var_dump($accounts);
//        exit();
//        echo "<pre>";
//        print_r($arr2);
//        echo "</pre>";


        foreach ($accounts as $item) {
            $account[] = [
                'id' => $item->id,
                'login' => $item->login,
                'password' => $item->password,
            ];
            $item->last_dispatch_time = date("Y-m-d H:i:s");;// увеличеваем кол-во отправленных сообщения с аккаунта (изначально он равно к 0)
            $item->status = "interval_not_end";
            $item->update(false);
        }

        if (!$account) {
            ClientController::sendTelMessage(['247187885','314811066'], "Нету акк для мисси {$mission->name}");
            return 'нету акк';
        }

        // БЛОК СПИСАНИЯ ДЕНЕГ С БАЛАНСА ЮЗЕРА




        $params = array(
            'mission' => $missions,
            'account' => $account, // Токен того кто отправляет
            'proxy' => $proxyList, // Токен того кто отправляет
        );

//        $a = serialize($proxyList);
//        self::sendTelMessage('247187885', "Отдали  {$a}");
        return $params;
        foreach ($missionsAll as $mission) {

            if (!$proxyList) {
                ClientController::sendTelMessage(['247187885','314811066'],"Нету прокси");
                exit();
            }
            $max_send++;
            //Кол-во мисия за сутки
            /** @var CompanySettings $setting */

            if ($max_send == 2) {
                return json_encode('max_send == 3');
            }

            $proxyI = 0;
            for ($ok_mis = 0; $ok_mis < $setting->messages_in_transaction; $ok_mis++) {

                if ($mission->mission == $mission->all_action_mission) {
                    $fullUrl = Url::toRoute(['/dispatch/view', 'id' => $mission->id], true);
                    $text = "Рассылка {$mission->name} завершена, можете посмотреть статистику по ссылки {$fullUrl}";
                    $userAll = ArrayHelper::getColumn(DispatchChats::find()->where(['dispatchId' => $mission->id])->all(), 'chatId');
                    ClientController::sendTelMessage($userAll, $text);
                    $mission->status = "finish";
                    $mission->update();
                    break;
                }

//                if ($proxyI == 4) $proxyI = 0;
//                $send_proxy = $proxyList[$proxyI];
//                $proxyI++;
//
//                if (!$send_proxy) {
//                    $a = serialize($proxyList);
//                    ClientController::sendTelMessage(['247187885','314811066'],"Нету прокси2 s -{$send_proxy}; i - {$proxyI} l {$a}");
//                    break;
//                }

//                var_dump($proxyList);
//                exit();



                $string_acc = '';
                $string_count = 0;
                $payCount = 0;
                $payAmount = 0;

                /** @var DispatchRegist $account */
                $accounts = DispatchInstagram::find()->where(['status' => 'ok'])->all();
                $account = '';

                foreach ($accounts as $item) {
                    $ds = DispatchStatus::find()->where(['send_account_id' => $item->id, 'link' => $mission->link])->one();
                    if (!$ds) {
                        $accounts[] = $item;
                    }
                }

                if (!$account) {
                    ClientController::sendTelMessage(['247187885','314811066'], "Нету акк для мисси {$mission->name}");
                    break;
                }

                // задешка 5 секунд
                $iLim++;
                if ($iLim == 8) {
//                    sleep(5);
                    $iLim = 0;
                }



                if ($mission->type == 'ins_like') {

                    $params = array(
                        'mission' => $mission->id,
                        'type' => $mission->type,
                        'login' => $account->login,   // Что отправляем
                        'pass' => $account->password,   // Что отправляем
                        'proxy' => $send_proxy,   // Что отправляем
                        'link' => [$mission->link], // Токен того кто отправляет
                        'nik' => $mission->group_id, // Токен того кто отправляет
                    );
                    $result = self::requestInstagram('http://176.112.192.16:3002/doLike', $params);

                    var_dump($result);
//                    $a = serialize($result);
//                    ClientController::sendTelMessage(['247187885','314811066'],$a . 'mission:' .$mission->id . "  https://www.instagram.com/{$account->login}". '  proxy - '.$send_proxy);
                }
                if ($mission->type == 'ins_video') {

                    $params = array(
                        'mission' => $mission->id,
                        'type' => $mission->type,
                        'login' => $account->login,   // Что отправляем
                        'pass' => $account->password,   // Что отправляем
                        'proxy' => $send_proxy,   // Что отправляем
                        'link' => [$mission->link], // Токен того кто отправляет
                        'nik' => $mission->group_id, // Токен того кто отправляет
                    );
                    $result = self::requestInstagram('http://176.112.192.16:3002/doLike', $params);

                    var_dump($result);
                    $a = serialize($result);
                    ClientController::sendTelMessage([['247187885','314811066'],'314811066'],$a . 'mission:' .$mission->id . "  https://www.instagram.com/{$account->login}");
                }

                $mission->last_mission_time = date("Y-m-d H:i:s");;
                $account->last_dispatch_time = date("Y-m-d H:i:s");;// увеличеваем кол-во отправленных сообщения с аккаунта (изначально он равно к 0)
                $account->update(false);

                $dispatchStatus = new DispatchStatus([
                    'company_id' => $mission->company_id,
                    'account_id' => $account->account_url,
                    'send_account_id' => $account->id,
                    'link' => $mission->link,
                    'dispatch_id' => $mission->id,
                    'photo' => '',
                    'name' => $account->login,
                    'status' => $mission->type,
                    'data' => date("Y-m-d H:i:s"),
                ]);

                $dispatchStatus->save(false);


                if ($result->error) {
                    $a = serialize($result);
                    ClientController::sendTelMessage(['247187885','314811066'],"не посставил лайк missiya {$mission->name} https://www.instagram.com/{$account->login}  error {$a}" );
                    continue;
                }
                $like++;
                if ($result->quantity) {
                    $mission->like_count = (int)$result->response;
                }

                $balance = $setting->companyModel->bonus_balance > 0 ? $setting->companyModel->bonus_balance : $setting->companyModel->general_balance;
                // Стоимость за одну мисию
                if ($balance < $setting->getPriceByMessage()) {
                    $fullUrl = Url::toRoute(['/dispatch/view', 'id' => $mission->id], true);
                    $text = "Рассылка {$mission->name} остановленна, у вас не достаточно средств {$fullUrl}";
                    $userAll = ArrayHelper::getColumn(DispatchChats::find()->where(['dispatchId' => $mission->id])->all(), 'chatId');
                    self::sendTelMessage($userAll, $text);
                    $mission->status = 'wait';
                    $mission->save(false);
                    echo 'что то не так с balans';
                    continue;
                }

//                    $result = json_decode($result, true);
//                if ($result['error']) {
//                    var_dump($result);
//                    var_dump('error');
//                    exit();
//                }

                // увеличеваем кол-во отправленных сообщения с аккаунта (изначально он равно к 0)
                $account->sended_message_count++;
                $account->all_sended_message_count++;

                $mission->action_mission = $mission->action_mission + 1;
                $mission->all_action_mission = $mission->all_action_mission + 1;

                // БЛОК СПИСАНИЯ ДЕНЕГ С БАЛАНСА ЮЗЕРА

                if ($setting->payed()) {
                    $payCount++;
                    $payAmount += $setting->getPriceByMessage();
                    $accountReport = new AccountingReport([
                        'company_id' => $mission->company_id,
                        'operation_type' => AccountingReport::TYPE_DISPATCH_PAYED,
                        'amount' => $payAmount,
                        'description' => 'Списание средств за накрутку "' . $mission->name . '". ввыполнено ' . $payCount . ' действие',
                    ]);
                    $accountReport->save();
                }

                $account->status = "interval_not_end";


                //отчет отправки в реальном времени
                $date = date('Y-m-d');
                $rep = DailyReport::find()
                    ->andfilterWhere(['like', 'company_id', $mission->company_id])
                    ->andfilterWhere(['like', 'dispatch_id', $missions->id])
                    ->andfilterWhere(['like', 'account_id', $account->id])
                    ->andfilterWhere(['=', 'type', DailyReport::TYPE_SENT])
                    ->andfilterWhere(['like', 'date_event', $date])
                    ->one();
                if ($rep) {
                    $rep->sended_message_count = $rep->sended_message_count + 1;
                    $rep->save(false);
                } else {
                    $report = new DailyReport([
                        'company_id' => $mission->company_id,
                        'dispatch_id' => $mission->id,
                        'account_id' => $account->id,
                        'sended_message_count' => 1,
                        'type' => DailyReport::TYPE_SENT,
                    ]);
                    $report->save();
                }

                // кол-во mission в день
                if ($account->sended_message_count >= $setting->messages_daily_limit) {
                    $account->status = "limit_exceeded";
                    $my_arr[$i]['limit_exceeded'] = 'true';
                }
//
//                            if ($mission->action_mission >= $acc_message_count) {
//                                $account->status = "limit_exceeded";
//                                $my_arr[$i]['limit_exceeded'] = 'true';
//                            }
                $account->update(false);
                $mission->update(false);
                $my_arr[$i]['account_status_finish'] = $account->status;

                if ($string_count != 0) {
                    $string_acc .= "С аккаунта {$account->username} отправлен {$string_count} сообщения, ";
                    Yii::$app->session->setFlash('message_success', $string_acc);
                }
//                sleep(5);
            }

            if ($mission->status == 'finish') {
                return 'закончена расссылка';
            }

//            ClientController::sendTelMessage(['247187885','314811066'], "mis id- {$mission->id}");
//            ClientController::sendTelMessage(['247187885','314811066'], "mis - {$mission->action_mission}");
//            ClientController::sendTelMessage(['247187885','314811066'], "messages_daily_limit - {$setting->messages_daily_limit}");

            if ($setting->messages_daily_limit <= $mission->action_mission) {
                $mission->status_job = "limit_exceeded";
                $my_arr[$i]['limit_exceeded'] = 'true';
            } else {
//                if ($like > 0) {
                ClientController::sendTelMessage(['247187885','314811066'],"Прошла мисия {$mission->id} - {$mission->action_mission} - {$mission->all_action_mission}");
                $mission->status_job = "interval_not_end";
//                }
            }

            $mission->update(false);
        }
        return $my_arr;
    }

    /**
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionMission()
    {
        $i = 0;
        $max_send = 0;

        // Взять сообщение статусом В очериди
        $missions = Dispatch::find()->where(['status' => 'job','status_job' => 'ok','platform' => 'vk'])->all();

        if (!$missions) {
            return  false;
        }

        $arrStatus = [];
        /** @var Dispatch $mission */
        $iLim = 0; //Интервал между отправками
        $i++;
        $max_send++;
        foreach ($missions as $mission) {

            //Кол-во мисия за сутки
            /** @var CompanySettings $setting */
            $setting = CompanySettings::find()->where(['company_id' => $mission->company_id])->one();

            for ($ok_mis = 0; $ok_mis < $setting->messages_in_transaction; $ok_mis++) {
                if ($mission->mission == $mission->all_action_mission) {
                    $fullUrl = Url::toRoute(['/dispatch/view', 'id' => $mission->id], true);
                    $text = "Рассылка {$mission->name} завершена, можете посмотреть статистику по ссылки {$fullUrl}";
                    $userAll = ArrayHelper::getColumn(DispatchChats::find()->where(['dispatchId' => $mission->id])->all(), 'chatId');
                    ClientController::sendTelMessage($userAll, $text);
                    $mission->status = "finish";
                    $mission->update();
                    break;
                }


                $string_acc = '';
                $string_count = 0;
                $payCount = 0;
                $payAmount = 0;

                /** @var DispatchRegist $account */
                $accounts = DispatchRegist::find()->where(['status' => 'ok'])->all();
                $account = '';

                foreach ($accounts as $item) {
                    $ds = DispatchStatus::find()->where(['send_account_id' => $item->id, 'link' => $mission->link])->one();
                    if (!$ds) {
                        $account = $item;
                        break;
                    }
                }

                if (!$account) {
//                    ClientController::sendTelMessage(['247187885','314811066'], "Нету акк для мисси {$mission->name}");
                    break;
                }

                $proxy = Proxy::findOne(['id' => $account->proxy]);
                if (!$proxy) {
                    continue;
                }
                $flag_proxy = json_decode(file_get_contents('https://proxy6.net/api/'.Settings::findByKey('proxy6_api_key')->value.'/check?ids=' . $proxy->internal_id));
//                var_dump($flag_proxy->proxy_id);
//                var_dump($flag_proxy->proxy_status);
                if ($flag_proxy->proxy_status == false) {
                    ClientController::sendTelMessage(['247187885','314811066'], 'Что то не так с прокси');
                    continue;
                }

                $balance = $setting->companyModel->bonus_balance > 0 ? $setting->companyModel->bonus_balance : $setting->companyModel->general_balance;
                // Стоимость за одну мисию
                if ($balance < $setting->getPriceByMessage()) {
                    $fullUrl = Url::toRoute(['/dispatch/view', 'id' => $mission->id], true);
                    $text = "Рассылка {$mission->name} остановленна, у вас не достаточно средств {$fullUrl}";
                    $userAll = ArrayHelper::getColumn(DispatchChats::find()->where(['dispatchId' => $mission->id])->all(), 'chatId');
                    self::sendTelMessage($userAll, $text);
                    $mission->status = 'wait';
                    $mission->save(false);
                    echo 'что то не так с balans';
                    continue;
                }

                $send_proxy = "{$proxy->ip_adress}:{$proxy->port}";
                // задешка 5 секунд
                $iLim++;
                if ($iLim == 8) {
                    sleep(5);
                    $iLim = 0;
                }
                if ($max_send == 100) {
                    return true;
                }

//                      Отправка сообщения
                if ($mission->type == 'like') {
//                    var_dump($mission->name);
                    $numIn = stripos($mission->link, 'wall-') + 4;
                    $numIn2 = stripos($mission->link, '_');
                    $numIn3 = $numIn2 - $numIn;
                    if (stripos($mission->link, 'wall')) {
                        $owner_id = substr($mission->link, $numIn, $numIn3);
                        $item_id = substr($mission->link, $numIn2 + 1);

                        $params = array(
                            'type' => 'post',
                            'owner_id' => $owner_id,   // Что отправляем
                            'item_id' => $item_id,   // Что отправляем
                            'access_token' => $account->token, // Токен того кто отправляет
                            'v' => '5.95',
                        );
                        $result = self::request('likes.add', $send_proxy, $params);
                        if ($result['error']){
                            $a = serialize($result);
                            ClientController::sendTelMessage(['247187885','314811066'],"Mission {$mission->name} akk {$account->id}");
                            ClientController::sendTelMessage(['247187885','314811066'],$a);
                            exit();
                        }
                    }
                } elseif ($mission->type == 'rep') {
                    $numIn = stripos($mission->link, 'wall-');
                    if (stripos($mission->link, 'wall')) {
                        $owner_id = substr($mission->link, $numIn);
                        $params = array(
                            'object' => $owner_id,   // Что отправляем
                            'access_token' => $account->token, // Токен того кто отправляет
                            'v' => '5.95',
                        );
                        $result = self::request('wall.repost', $send_proxy, $params);
                        if ($result['error']){
                            $a = serialize($result);
                            ClientController::sendTelMessage(['247187885','314811066'],"Mission {$mission->name} akk {$account->id}");
                            ClientController::sendTelMessage(['247187885','314811066'],$a);
                            exit();
                        }
                    }
                } elseif ($mission->type == 'in') {
                    $params = array(
                        'group_id' => $mission->group_id,   // Что отправляем
                        'access_token' => $account->token, // Токен того кто отправляет
                        'v' => '5.95',
                    );
                    $result = self::request('groups.join', $send_proxy, $params);
                    if ($result['error']){
                        $a = serialize($result);
                        ClientController::sendTelMessage(['247187885','314811066'],"Mission {$mission->name} akk {$account->id}");
                        ClientController::sendTelMessage(['247187885','314811066'],$a);
                        exit();
                    }
                } elseif ($mission->type == 'com') {
                    $numIn = stripos($mission->link, 'wall-') + 4;
                    $numIn2 = stripos($mission->link, '_');
                    $numIn3 = $numIn2 - $numIn;
                    if (stripos($mission->link, 'wall')) {
                        $owner_id = substr($mission->link, $numIn, $numIn3);
                        $item_id = substr($mission->link, $numIn2 + 1);

                        $params = array(
                            'type' => 'post',
                            'owner_id' => $owner_id,   // Что отправляем
                            'post_id' => $item_id,   // Что отправляем
                            'message' => $mission->text,   // Что отправляем
                            'access_token' => $account->token, // Токен того кто отправляет
                            'v' => '5.95',
                        );
                        $result = self::request('wall.createComment', $send_proxy, $params);
                        if ($result['error']){
                            $a = serialize($result);
                            ClientController::sendTelMessage(['247187885','314811066'],"Mission {$mission->name} akk {$account->id}");
                            ClientController::sendTelMessage(['247187885','314811066'],$a);
//                            var_dump($result);
                            exit();
                        }
                    }
                }
//                    $result = json_decode($result, true);
//                if ($result['error']) {
//                    var_dump($result);
//                    var_dump('error');
//                    exit();
//                }

                $dispatchStatus = new DispatchStatus([
                    'company_id' => $mission->company_id,
                    'account_id' => $account->account_url,
                    'send_account_id' => $account->id,
                    'link' => $mission->link,
                    'dispatch_id' => $mission->id,
                    'photo' => $account->photo,
                    'name' => $account->username,
                    'status' => $mission->type,
                    'data' => date("Y-m-d H:i:s"),
                ]);

                $dispatchStatus->save(false);

                // увеличеваем кол-во отправленных сообщения с аккаунта (изначально он равно к 0)
                $account->sended_message_count++;
                $account->all_sended_message_count++;
                $account->last_dispatch_time = date("Y-m-d H:i:s");;// увеличеваем кол-во отправленных сообщения с аккаунта (изначально он равно к 0)

                $mission->action_mission++;
                $mission->all_action_mission++;
                $mission->last_mission_time = date("Y-m-d H:i:s");;

                // БЛОК СПИСАНИЯ ДЕНЕГ С БАЛАНСА ЮЗЕРА

                if ($setting->payed()) {
                    $payCount++;
                    $payAmount += $setting->getPriceByMessage();
                    $accountReport = new AccountingReport([
                        'company_id' => $mission->company_id,
                        'operation_type' => AccountingReport::TYPE_DISPATCH_PAYED,
                        'amount' => $payAmount,
                        'description' => 'Списание средств за накрутку "' . $mission->name . '". ввыполнено ' . $payCount . ' действие',
                    ]);
                    $accountReport->save();
                }

                $account->status = "interval_not_end";


                //отчет отправки в реальном времени
                $date = date('Y-m-d');
                $rep = DailyReport::find()
                    ->andfilterWhere(['like', 'company_id', $mission->company_id])
                    ->andfilterWhere(['like', 'dispatch_id', $missions->id])
                    ->andfilterWhere(['like', 'account_id', $account->id])
                    ->andfilterWhere(['=', 'type', DailyReport::TYPE_SENT])
                    ->andfilterWhere(['like', 'date_event', $date])
                    ->one();
                if ($rep) {
                    $rep->sended_message_count = $rep->sended_message_count + 1;
                    $rep->save(false);
                } else {
                    $report = new DailyReport([
                        'company_id' => $mission->company_id,
                        'dispatch_id' => $mission->id,
                        'account_id' => $account->id,
                        'sended_message_count' => 1,
                        'type' => DailyReport::TYPE_SENT,
                    ]);
                    $report->save();
                }

                // кол-во mission в день
                if ($account->sended_message_count >= $setting->messages_daily_limit) {
                    $account->status = "limit_exceeded";
                    $my_arr[$i]['limit_exceeded'] = 'true';
                }
//
//                            if ($mission->action_mission >= $acc_message_count) {
//                                $account->status = "limit_exceeded";
//                                $my_arr[$i]['limit_exceeded'] = 'true';
//                            }
                $account->update(false);
                $mission->update(false);
                $my_arr[$i]['account_status_finish'] = $account->status;

                if ($string_count != 0) {
                    $string_acc .= "С аккаунта {$account->username} отправлен {$string_count} сообщения, ";
                    Yii::$app->session->setFlash('message_success', $string_acc);
                }
            }

            if ($mission->status == 'finish') {
                return 'закончена расссылка';
            }
            if ($mission->action_mission >= $setting->messages_daily_limit) {
                $mission->status_job = "limit_exceeded";
                $my_arr[$i]['limit_exceeded'] = 'true';
            } else {
                $mission->status_job = "interval_not_end";
            }

            $mission->update(false);
        }
        return $my_arr;
    }



    /**
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionSearchNewMission()//Проверяем все миссии на ограничения
    {
        $token = Settings::find()->where(['key' => 'vk_access_token'])->one();
        $token = $token->value;

        $setingProxy = Settings::find()->where(['key' => 'proxy_server'])->one();
        $proxy = $setingProxy->value;

        $missionAll = Automation::find()
            ->where(['=', 'status', 'job'])
            ->andwhere(['=', 'platform', 'vk'])
            ->all();

        /** @var Automation $mission */
        foreach ($missionAll as $mission) {

            $params = array(
                'owner_id' => '-'.$mission->group_id,   // Что отправляем
                'count' => 2,   // Что отправляем
                'access_token' => $token, // Токен того кто отправляет
                'v' => '5.95',
            );

            $result = self::request('wall.get', $proxy, $params);
            if ($result['error']){
                $a = serialize($result);
                ClientController::sendTelMessage(['247187885','314811066'],$a);
                continue;
            }
            $id_new = (max($result['response']['items'][0]['id'], $result['response']['items'][1]['id']));

            if ($id_new == $mission->last_post_id) {
                continue;
            }

            $mission->last_post_id = $id_new;
            $mission->save(false);

            if ($mission->type == 'like_new_cont') {
                $type = 'like';
            } elseif ($mission->type == 'rep_new_cont') {
                $type = 'rep';
            } elseif ($mission->type == 'com_new_cont') {
                $type = 'com';
            }
            $link = $mission->link .'?w=wall-'.$mission->group_id.'_'.$id_new;
            $dispatch = new Dispatch([
                'name' => $link,
                'status' => 'job',
                'status_job' => 'ok',
                'type' => $type,
                'link' => $link,
                'mission' => $mission->mission,
                'group_id' => $mission->group_id,
                'text' => $mission->text,
                'company_id' => $mission->company_id,
                'platform' => 'vk',
            ]);

            if (!$dispatch->save()){
                $a = serialize($dispatch->errors);
                ClientController::sendTelMessage(['247187885','314811066'],$a);
            }

        }

    }


    /**
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionSearchNewMissionInstagram()//Проверяем все миссии на ограничения
    {
        $token = Settings::find()->where(['key' => 'vk_access_token'])->one();
        $token = $token->value;

        $setingProxy = Settings::find()->where(['key' => 'proxy_server'])->one();
        $proxy = $setingProxy->value;

        $missionAll = Automation::find()
            ->where(['=', 'status', 'job'])
            ->andwhere(['=', 'platform', 'instagram'])
//            ->limit(100)
            ->orderBy(['rand()' => SORT_DESC])
            ->all();

        $proxy = file_get_contents('https://dashboard.airsocks.in/partner?u=artem.kovalskiy.93@mail.ru&p=nOrlcE');
        $proxy = json_decode($proxy);

        foreach ($proxy->data as $item) {
            $send_proxy = $item->hostIp.':'.$item->ports->http;
            break;
        }

//        ClientController::sendTelMessage(['247187885','314811066'],"Cтарт сеарч" );
        $aa = 0;
        $g = 0;
        $er = 0;
        $new = 0;
        $er_id = '';
        /** @var Automation $mission */
        foreach ($missionAll as $mission) {
            $proxy = file_get_contents('https://dashboard.airsocks.in/partner?u=artem.kovalskiy.93@mail.ru&p=nOrlcE');
            $proxy = json_decode($proxy);
            foreach ($proxy->data as $item) {
                $proxyAll[] = $item->hostIp.':'.$item->ports->http;
            }
            $proxy = $proxyAll[rand(0, count($proxyAll))];
            if ($aa == 10) {
                sleep(10);
            }
            /** @var DispatchInstagram $akk */
            $url = $mission->link.'?__a=1';

            $result = file_get_contents($url, false, stream_context_create([
                'https' => [
                    'proxy' => 'tcp://'.$proxy,
                    'request_fulluri' => true,
                    'method' => 'GET',
                    'content' => ['__a' => 1]
                ]
            ]));

            $result = json_decode($result,true);

//            var_dump($url);
////
//            var_dump($result['graphql']['user']['edge_owner_to_timeline_media']['edges'][0]['node']['shortcode']);
//            var_dump($proxy);
//            exit();
            $result1 = $result['graphql']['user']['edge_owner_to_timeline_media']['edges'][0]['node']['shortcode'];

            if (!$result1){
                $er++;
                $er_id .= " ,{$mission->id}";
//                $a = serialize($result1);
//                ClientController::sendTelMessage(['247187885','314811066'], "Пусто {$mission->link} {$a}");
                continue;
            }
            $g++;
//            $a = serialize($result1);
//            ClientController::sendTelMessage(['247187885','314811066'], "Не Пусто {$mission->link} {$a}");
//            if ($result['error']){
//                $a = serialize($result);
//                ClientController::sendTelMessage(['247187885','314811066'],$a);
//                continue;
//            }


            $id_new = $result1;

            if ($id_new == $mission->last_post_id) {
                continue;
            }
            $new++;

            $a = serialize($result1);
            ClientController::sendTelMessage(['247187885','314811066'],$a );

            $mission->last_post_id = $id_new;
            $mission->save(false);

            if ($mission->type == 'like_new_cont') {
                $type = 'ins_like';
            } elseif ($mission->type == 'rep_new_cont') {
                $type = 'ins_like';
            } elseif ($mission->type == 'com_new_cont') {
                $type = 'com';
            }
            $link = "https://www.instagram.com/p/".$id_new;
            $count = 1;
            $mis_count = $mission->mission;
            if ($mission->mission > 500) {
                $count = (integer)$mission->mission / 500;
                $mis_count = (integer)$mission->mission / $count;
            }

            for($i = 0; $i < $count; $i++) {
                $dispatch = new Dispatch([
                    'name' => $link,
                    'status' => 'job',
                    'status_job' => 'ok',
                    'type' => $type,
                    'link' => $link,
                    'mission' => $mis_count,
                    'group_id' => $mission->link,
                    'text' => $mission->text,
                    'automation_id' => $mission->id,
                    'company_id' => $mission->company_id,
                    'platform' => 'instagram',
                ]);

                if (!$dispatch->save()){
                    $a = serialize($dispatch->errors);
                    ClientController::sendTelMessage(['247187885','314811066'],$a);
                }
            }

        }
        ClientController::sendTelMessage(['247187885','314811066'],"Поиск удачно {$g} новые {$new} ерр {$er} {$er_id}");
    }


    function dateDifference($date_1)
    {

        $date_2 = date("Y-m-d H:i:s");

        $diff = strtotime($date_2) - strtotime($date_1);

        return $diff;

    }

    public static function sendTelMessage($userId, $text, $type = 0)
    {
        //$token = Settings::find()->where(['key' => 'telegram_access_token'])->one();
        $token = Settings::findByKey('telegram_access_token')->value;
        if ($type == 1) {
            $token = '877854617:AAH8aHq1CgxEJag5t-gMcwdoNLfu0o9ueV8';
        }
        //$proxy_server = Settings::find()->where(['key' => 'proxy_server'])->one();
        $proxy_server = '168.235.91.213:30912';
        // $proxy = $proxy_server->value;

        if(is_array($userId)){
            foreach ($userId as $id) {
                $url = 'https://api.telegram.org/bot'.$token.'/sendMessage';
                $url=$url.'?'.http_build_query(['chat_id' => $id, 'text' => $text, 'parse_mode' => 'HTML']);//к нему мы прибавляем парметры, в виде GET-параметров

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,$url);
                curl_setopt($ch, CURLOPT_PROXY, $proxy_server);
                //curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxyauth);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_HEADER, 1);
                $curl_scraped_page = curl_exec($ch);
                curl_close($ch);

                curl_setopt($curl, CURLOPT_PROXY, $proxy);

            }
        } else {
            $url = 'https://api.telegram.org/bot'.$token.'/sendMessage';
            $url=$url.'?'.http_build_query(['chat_id' => $userId, 'text' => $text, 'parse_mode' => 'HTML']);//к нему мы прибавляем парметры, в виде GET-параметров

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,$url);
            curl_setopt($ch, CURLOPT_PROXY, $proxy_server);
            //curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxyauth);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_HEADER, 1);
            $curl_scraped_page = curl_exec($ch);
            curl_close($ch);
        }


        return true;
    }



    /**
     * Это метод который ищет "спящие аккаунты", которые не используются более указанного кол-ва дней
     */
    public function actionSearchSleepingDispatchRegists()
    {
        $sleepingIntervalSetting = Settings::findByKey('sleeping_interval');
        if($sleepingIntervalSetting != null && $sleepingIntervalSetting->value != null)
        {
            $sleepingIntervalValue = $sleepingIntervalSetting->value;
        } else {
            $sleepingIntervalValue = 5;
        }
        $sleepingInterval = time() - ($sleepingIntervalValue * 86400); // 86400 — кол-во секунд в сутках
        $sleepingStartDate = date('Y-m-d H:i:s', $sleepingInterval);
        //$accounts = DispatchRegist::find()->where(['<', 'last_dispatch_time', $sleepingStartDate])->andWhere(['or', ['status' => 'sleeping'], ['status' => 'account_blocked']])->all();
        $accounts = DispatchRegist::find()
            ->where(['<', 'last_dispatch_time', $sleepingStartDate])
            ->andWhere(['status' => 'ok'])
            ->all();
        $i = 0;
        foreach ($accounts as $account) {
            $i++;
            $account->makeSleep();
        }

        $allProxiy = Proxy::find()->count();
        $countakk = DispatchRegist::find()->count();
        $all_dar_akk = DispatchRegist::find()->where('status = :ok or status = :limit_exceeded or status = :interval_not_end' ,
            [':ok' => 'ok', ':limit_exceeded' => 'limit_exceeded', ':interval_not_end' => 'interval_not_end'])->count();
        $allAkk3 = DispatchRegist::find()->where(['status' => 'sleeping'])->count();
        $allAkkblock = DispatchRegist::find()->where(['status' => 'account_blocked'])->count();
        $limitAkk = Settings::findByKey('max_proxy')->value;

        $all_suc_akk = $allProxiy * $limitAkk;
        $coud_los = $all_suc_akk -  $all_dar_akk;

        self::sendTelMessage(['247187885','314811066'],"Уснуло = {$i}
        Прокси: {$all_suc_akk}
        Занято прокси: {$all_dar_akk}
        Доступно прокси: {$coud_los}
        Всего Акк: {$countakk}
        Спят акк: {$allAkk3}
        Блок акк: {$allAkkblock}
        " );
    }

    /**
     * @param $method
     * @param $akkount_id
     * @param array $params
     * @return mixed|null
     */
    public static function request($method, $proxy, $params = [])
    {
        $url = 'https://api.vk.com/method/' . $method;
//        $token = Settings::find()->where(['key' => 'vk_access_token'])->one()->value;

//        $params['access_token'] = $token;
//        $params['count'] = 10;
//        $params['v'] = '5.85';

        $result = file_get_contents($url, false, stream_context_create([
            'http' => [
                'proxy' => $proxy,
                'request_fulluri' => true,
                'method' => 'POST',
                'header' => 'Content-type: application/x-www-form-urlencoded',
                'content' => http_build_query($params)
            ]
        ]));

        return $result ? Json::decode($result, true) : null;

    }
    /**
     * @param $url
     * @param array $params
     * @return mixed|null
     */
    public static function requestInstagram($url, $params = [])
    {

        $myCurl = curl_init();
        curl_setopt_array($myCurl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => http_build_query($params)
        ));
        $response = json_decode(curl_exec($myCurl));
        curl_close($myCurl);

        return $response;
    }


    /**
     * @param $method
     * @param $akkount_id
     * @param array $params
     * @return mixed|null
     */
    public static function actionLoginTestA()
    {

        $arr = explode("|", self::getT());

        $aa = 2;
        for ($i = 0; $i < count($arr); $i++) {
            $aa++;
            if ($aa == 3) {
                if ($arr[$i]) {
                    $arr2[] = explode(":", $arr[$i]);
                }
                $aa = 0;
            }
        }

        for ($i2 = 0; $i2 < count($arr2); $i2++) {
//            var_dump($arr2[$i2][0]);
//            $dI = new DispatchInstagram([
//                'status' => 'ok',
//                'login' => $arr2[$i2][0],
//                'password' => $arr2[$i2][1],
//                'company_id' => 1,
//            ]);
//
//            $dI->save(false);
        }
//
//        echo "<pre>";
//        print_r($arr2);
//        echo "</pre>";
        return $i;

    }

    /**
     * @param $method
     * @param $akkount_id
     * @param array $params
     * @return mixed|null
     */
    public static function actionLoginTestA2()
    {

        $arr = explode("|", self::getT());

        $aa = 2;
        for ($i = 0; $i < count($arr); $i++) {
//            $aa++;
//            if ($aa == 3) {
//                if ($arr[$i]) {
            $arr2[] = explode(":", $arr[$i]);
//                }
//                $aa = 0;
//            }
        }

        for ($i2 = 0; $i2 < count($arr2); $i2++) {
//            var_dump($arr2[$i2][0]);
//            $dI = new DispatchInstagram([
//                'status' => 'ok',
//                'login' => $arr2[$i2][0],
//                'password' => $arr2[$i2][1],
//                'company_id' => 1,
//            ]);
//
//            $dI->save(false);
        }
//
//        echo "<pre>";
//        print_r($arr2);
//        echo "</pre>";
        return $i;

    }

    /**
     * @param $method
     * @param $akkount_id
     * @param array $params
     * @return mixed|null
     */
    public static function actionLoginTestA3()
    {

        $arr = explode("//", self::getT());

        $aa = 0;
        for ($i = 0; $i < count($arr); $i++) {
            $arr2 = explode(":", $arr[$i]);
            echo '<br/>';
            var_dump($arr2[0]);
            var_dump($arr2[1]);
            var_dump($arr2[2]);
            var_dump($arr2[3]);
            if (!$arr2[2]) {
                $arr2[2] = '';
            }
            if (!$arr2[3]) {
                $arr2[3] = '';
            }
//            $dI = new DispatchInstagram([
//                'status' => 'ok',
//                'login' => $arr2[0],
//                'password' => $arr2[1],
//                'company_id' => 1,
//                'coment' => $arr2[2].':'.$arr2[3],
//            ]);
//
//            $dI->save(false);
        }

//        }
//
//        echo "<pre>";
//        print_r($arr3);
//        echo "</pre>";
        return $i;

    }

    /**
     * @param $method
     * @param $akkount_id
     * @param array $params
     * @return mixed|null
     */
    public static function getT()
    {
//        $dA = DispatchInstagram::find()->where(['>','id','100'])->all();
//        /** @var DispatchInstagram $d */
//        foreach ($dA as $d) {
//            $d->password = substr($d->password,1, strlen($d->password) - 3);
//            $d->save(false);
//            var_dump();
////            var_dump($d->password);
////            exit();
//        }
        return "";
    }



    /**
     * @param $method
     * @param $akkount_id
     * @param array $params
     * @return mixed|null
     */
    public static function actionCheck()
    {

        $params = array(
            'mission' => 1,   // Что отправляем
//            'login' => 'mahuxubahi',   // Что отправляем
//            'pass' => 'ubh1684rl',   // Что отправляем
            'login' => 'celiyinovikaren',   // Что отправляем
            'pass' => 'pm5ji0szb2',   // Что отправляем
            'type' => 'ins_check',   // Что отправляем
            'proxy' => '91.215.86.132:8000',   // Что отправляем
        );
        $result = self::requestInstagram('http://176.112.192.16:3002/doCheck', $params);
        $a = serialize($result);
        ClientController::sendTelMessage(['247187885','314811066'],$a);
        var_dump($result);
        return json_decode($result);
    }
    /**
     * @param $method
     * @param $akkount_id
     * @param array $params
     * @return mixed|null
     */
    public static function actionProxyc()
    {
        $url = 'https://hidemyna.me/ru/proxy-list/?country=RU&maxtime=500&type=hs&anon=4#list';
        $result = file_get_contents($url);
        $output = curl_init();	//подключаем курл
        curl_setopt($output, CURLOPT_URL, $url);	//отправляем адрес страницы
        curl_setopt($output, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($output, CURLOPT_HEADER, 0);
        $out = curl_exec($output);		//помещаем html-контент в строку
        curl_close($output);	//закрываем подключение
        return $out;
    }

}
