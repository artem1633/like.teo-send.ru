<?php

namespace app\modules\api\controllers;


use app\models\AccountingReport;
use app\models\Parser;
use app\models\Settings;
use app\models\Telegram;
use yii\helpers\ArrayHelper;
use yii\rest\ActiveController;
use yii\web\Response;
use Yii;

/**
 * Default controller for the `api` module
 */
class ParserController extends ActiveController
{
    public $modelClass = 'app\models\Api';

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'only' => ['update'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }




    /**
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionNewinfo()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        // return $request;
        $find = Parser::findOne($request->post('id'));
        $count = $request->post('count');
        $status = $request->post('status');
        $progress = $request->post('progress');
        if ($find->status != $request->post('status')) {
            $userAll = ArrayHelper::getColumn(Telegram::find()->where(['company_id' => $find->company_id])->all(), 'telegram_id');
            if ($userAll) {
                ClientController::sendTelMessage($userAll, "Парсер {$find->name} выполнен");
            }
        }
        if ($find->type == 0) {
            $userAll = ArrayHelper::getColumn(Telegram::find()->where(['company_id' => $find->company_id])->all(), 'telegram_id');
            if ($userAll) {
                ClientController::sendTelMessage($userAll, "В парсере {$find->name} найден новый вступивший пользователь");
            }
        }
        $find->count = $count;
        $find->status = $status;
        $find->progress = $progress;
        if (!$find->save(false)){
            return $find->error;
        }


        return true;

    }

    /**
     *
     */
    public function actionPayment()
    {
        $observePrice = Settings::findByKey('parser_observe_price')->value;

        $sevenDaysSec = 604800;
        $now = time();
        $datetime = date('Y-m-d H:i:s', $now - $sevenDaysSec);

        /** @var Parser[] $parsers */
        $parsers = Parser::find()->where(['type' => 0])->andWhere(['or', ['<', 'last_observe_pay_datetime', $datetime], ['last_observe_pay_datetime' => null]])->all();

        foreach ($parsers as $parser)
        {
            $company = $parser->company;

            if($company->general_balance >= $observePrice){
                $company->general_balance -= $observePrice;
                $company->save(false);

                $report = new AccountingReport([
                    'operation_type' => AccountingReport::TYPE_PARSER_OBSERVE,
                    'company_id' => $company->id,
                    'amount' => $observePrice,
                    'description' => "Продление слежения парсера «{$parser->name}»"
                ]);
                $report->save(false);

                $parser->last_observe_pay_datetime = date('Y-m-d H:i:s');
                $parser->save(false);
            } else {

            }
        }
    }


}
