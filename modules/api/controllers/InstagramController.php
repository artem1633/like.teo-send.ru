<?php

namespace app\modules\api\controllers;

use app\components\helpers\FunctionHelper;
use app\components\helpers\TagHelper;
use app\models\AccountingReport;
use app\models\Automation;
use app\models\Bots;
use app\models\BotsAnswers;
use app\models\BotsDialogs;
use app\models\CompanySettings;
use app\models\DailyReport;
use app\models\Dispatch;
use app\models\DispatchChats;
use app\models\DispatchConnect;
use app\models\DispatchRegist;
use app\models\DispatchStatus;
use app\models\Logs;
use app\models\Message;
use app\models\MessagesStatistics;
use app\models\Proxy;
use app\models\ReferalRedirects;
use app\models\Settings;
use app\models\TemplateMessages;
use app\models\UniqueMessage;
use app\models\Users;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\rest\ActiveController;
use yii\web\Response;
use Yii;

/**
 * Default controller for the `api` module
 */
class InstagramController extends ActiveController
{
    public $modelClass = 'app\models\Api';

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'only' => ['mission'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }



    /**
     * Отправляет отчет о новый пользователях,
     * - сумму пополнений
     * - кол-во сообщений
     * за сутки
     */
    public function actionMission()
    {
        $mission = Dispatch::find()->where(['platform' => Dispatch::PLATFORM_INSTAGRAM])->all();
        /** @var Dispatch $item */
        foreach ($mission as $item) {
            $result['items'][] = [
                'login' => $item->login,
                'pass' => $item->pass,
                'proxy' => '91.215.86.132:8000',
            ];
        }


        return $result;
    }
}
