<?php

namespace app\modules\api\controllers;

use app\components\helpers\FunctionHelper;
use app\components\helpers\TagHelper;
use app\models\AccountingReport;
use app\models\Bots;
use app\models\BotsAnswers;
use app\models\BotsDialogs;
use app\models\CompanySettings;
use app\models\DailyReport;
use app\models\Dispatch;
use app\models\DispatchChats;
use app\models\DispatchConnect;
use app\models\DispatchRegist;
use app\models\DispatchStatus;
use app\models\Logs;
use app\models\Message;
use app\models\MessagesStatistics;
use app\models\Proxy;
use app\models\ReferalRedirects;
use app\models\Settings;
use app\models\TemplateMessages;
use app\models\UniqueMessage;
use app\models\Users;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\rest\ActiveController;
use yii\web\Response;
use Yii;

/**
 * Default controller for the `api` module
 */
class ClientController extends ActiveController
{
    public $modelClass = 'app\models\Api';

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'only' => ['testmsg','new-message', 'send','chatsls',
                    'sendsls', 'status', 'history', 'proxy', 'test',
                    'akkunread', 'getdialog', 'getbotdialog', 'akkread',
                    'send-message', 'sendMessage', 'newsend-message', 'newsendMessage',
                    'login','logout2',
                    'newrequest'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }



    /**
     * Отправляет отчет о новый пользователях,
     * - сумму пополнений
     * - кол-во сообщений
     * за сутки
     */
    public function actionDailyReport()
    {
        $hours24 = time() - 86400;

        $hours24DateTime = date('Y-m-d H:i:s', $hours24);
        $hours24Date = date('Y-m-d', $hours24);


        $users = Users::find()->disableCompanyRequire()->where(['data_cr' => date('Y-m-d')])->count();
        $payments = AccountingReport::find()->where(['between', 'date_report', date('Y-m-d').' 00:00:00', date('Y-m-d').' 23:59:59'])->andWhere(['operation_type' => AccountingReport::TYPE_INCOME_BALANCE])->sum('amount');

        self::sendTelMessage('247187885', "ОТЧЕТ ЗА ДЕНЬ:
            Кол-во зарегистрированных пользователей: {$users}
            Сумма пополнений: {$payments}
        ");
    }


    public function actionTest()
    {
        //$userVk = '252341158,13390257,333043290';
        $text = 'тест';
        //$token = '89a237d21dfc53cd066408833a8c2a82538b6f89c11c3d70c8b778a512d4705a02509187ce1040707ecc7';
        $token = Settings::find()->where(['key' => 'vk_access_token'])->one();

        $url = 'https://api.vk.com/method/messages.send';
        $params = array(
            //  'user_id' => 252341158,13390257,333043290,    // Кому отправляем

            //  'group_id' => 101360637,
            'user_id' => 252341158,
            //'group_id' => 101360637,
            'message' => $text,   // Что отправляем
            'access_token' => $token->value, // Токен того кто отправляет
            'v' => '5.37',
        );

        // В $result вернется id отправленного сообщения
        $result = file_get_contents($url, false, stream_context_create(array(
            'http' => array(
                'method' => 'POST',
                'header' => 'Content-type: application/x-www-form-urlencoded',
                'content' => http_build_query($params)
            )
        )));
        $result = json_decode($result, true);


        // foreach ($result['response']['items'] as $i) {
        //     $message = DispatchStatus::find()->where(['account_id' => $i['message']['user_id']])->all();
        //     if ($message) {
        //         $message->status = DispatchStatus::STATUS_UNREAD_MESSAGE;
        //         $message->update();
        //     }
        //     var_dump($message->id);
        //     var_dump($i['message']['user_id']);

        // }
        return $result;
    }

//    //Проверка прочитаных сообщений
//    public function actionAkkread()
//    {
//
//        $akkAll = DispatchRegist::find()->where('status != :status', ['status' => 'account_blocked'])->all();
//
//        $i = 0;
//        /** @var DispatchRegist $akk */
//        foreach ($akkAll as $akk) {
//
//            $i++;//для логов
//
//            $my_arr[$i]['akk'] = $akk->id;
//            $my_arr[$i]['akk_status'] = $akk->status;
//
//            $proxy = Proxy::findOne(['id' => $akk->proxy]);
//
//
//            $mesAll = DispatchStatus::find()
//                ->where(
//                    [
//                        'send' => true,
//                        'read' => 'no',
//                        'send_account_id' => $akk->id
//                    ])->all();
//
//            /** @var DispatchStatus $mes */
//            foreach ($mesAll as $mes) {
//                $his = getHistory($mes->account_id, $akk->token, "tcp://{$proxy->ip_adress}:{$proxy->port}");
//                if ($his['response']['items'][0]['read_state']) {
//                    $mes->read = 'yes';
//                    $mes->update();
//
//                    //Должно записывать что новый человек прочитал сообщение
//                    $report = new DailyReport([
//                        'company_id' => $akk->company_id,
//                        'account_id' => $akk->id,
//                        'sended_message_count' => 1,
//                        'type' => DailyReport::TYPE_READ,
//                        'dispatch_id' => $mes->dispatch_id,
//                    ]);
//                    $report->save();
//
//                }
//            }
//        }
//        echo "<pre>";
//        print_r($my_arr);
//        echo "</pre>";
//    }

//    /**
//     * @throws \Throwable
//     * @throws \yii\db\StaleObjectException
//     */
//    public function actionGetdialog()//проверяем диалоги на не прочитанные сообщения
//    {
//
//        /** @var Settings $setingToken */
//        $setingToken = Settings::find()->where(['key' => 'vk_access_token'])->one();
//        $setingProxy = Settings::find()->where(['key' => 'proxy_server'])->one();
//
//        $akkAll = DispatchRegist::find()->where('status = :status || status = :status_limit || status = :interval_not_end',
//            ['status' => 'ok', ':status_limit' => 'limit_exceeded', ':interval_not_end' => 'interval_not_end'])->all();
//
//        $i = 0;
//        /** @var DispatchRegist $akk */
//        foreach ($akkAll as $akk) {
//            $connect = DispatchConnect::find()->where(['dispatch_regist_id' => $akk->id])->one();
//            $i++;//для логов
//            $i2 = 0;//для логов
//            $akkPush = false; // отправка уведомлений
//            $my_arr[$i]['akk'] = $akk->id;
//            $my_arr[$i]['akk_status'] = $akk->status;
//
//            $proxy = Proxy::findOne(['id' => $akk->proxy]);
//
//            $result = $this->getDialog($akk->token, "tcp://{$proxy->ip_adress}:{$proxy->port}");
//
//            if ($result['error']) {
//                $my_arr[$i]['error'] = $result['error']["error_msg"];
//                continue;
//            }
//
//            foreach ($result['response']['items'] as $item) {
//                $i2++;
//                $my_arr[$i][$i2]['message_user_id'] = $item['message']['user_id'];
//                $my_arr[$i][$i2]['message_user_id_status'] = 'false';
//
//                /** @var DispatchStatus $message */
//                $message = DispatchStatus::find()
//                    ->where('send = :send and new_message = :new_message and account_id = :account_id',
//                        [
//                            'send' => true,
//                            'new_message' => false,
//                            'account_id' => $item['message']['user_id'],
//                            //'status' => 4,
//                        ])->orderBy('id DESC')->one();
//
//                $my_arr[$i][$i2]['body'] = $item['message']['body'];
//                if ($message) {//пользователь только отправил нам уведомления
//                    if ($message->status == 4) {
//                        continue;
//                    }
//                    $dispatch = Dispatch::find()->where(['id' => $message->dispatch_id])->one();
//
//
//                    $message->new_message = true;
//                    $message->read = 'yes';
//                    $message->update();
//
//                    //подкачиваем историю на случай если было отправленно больше чем 1 не прочитанное сообщение
//                    $results2 = getHistory($item['message']['user_id'], $akk->token, $proxy);
//
//                    foreach ($results2['response']['items'] as $result2) {
//                        if (!$result2['read_state']) {
//                            /** @var Message $mesHistory */
//                            if($result['from_id'] != $akk->id)
//                            {
//                                $mesHistory = new Message();
//                                $mesHistory->date_time = date("Y-m-d H:i:s");
//                                $mesHistory->company_id = $akk->company_id;
//                                $mesHistory->dispatch_id = $message->dispatch_id;
//                                $mesHistory->dispatch_registr_id = $akk->id;
//                                $mesHistory->dispatch_status_id = $message->id;
//                                if ($dispatch->text_data) $mesHistory->database_id = $dispatch->text_data;
//                                $mesHistory->text = $result2['body'];
//                                $mesHistory->from = Message::FROM_CLIENT;
//                                $mesHistory->save();
//
//                                $messagesCount = Message::find()->where([
//                                    'company_id' => $akk->company_id,
//                                    'dispatch_id' => $message->dispatch_id,
//                                    'dispatch_registr_id' => $akk->id,
//                                    'dispatch_status_id' => $message->id
//                                ])->count();
//
//                                $maxStoredMessages = 10;
//
//                                if($messagesCount > $maxStoredMessages){
//                                    $deleteCount = $messagesCount - $maxStoredMessages;
//                                    $deleteMessages = Message::find()->where([
//                                        'company_id' => $akk->company_id,
//                                        'dispatch_id' => $message->dispatch_id,
//                                        'dispatch_registr_id' => $akk->id,
//                                        'dispatch_status_id' => $message->id
//                                    ])->limit($deleteCount)->all();
//
//                                    foreach ($deleteMessages as $msg){
//                                        $msg->delete();
//                                    }
//                                }
//                            }
//
//                            $partnerUrl = Settings::findByKey('partner_domain')->value;
//
//                            //if (!$akkPush) {//проверяем отправляли уведомления
//                            $text = "
//                            __________________________________________
//                            У пользователя №{$message->send_account_id} новое не прочитаное сообщение
//                                      Рассылка: {$dispatch->name}
//                                      Акк: https://vk.com/id{$item['message']['user_id']}
//                                      Текст: {$result2['body']}
//
//                                      {$partnerUrl}/dispatch-status/dialog?id={$message->id}";
//                            $userAll = DispatchChats::find()->where(['dispatchId' => $message->dispatch_id])->all();
//                            foreach ($userAll as $user) {
//                                if ($user) {
//                                    $this->send($user->chatId, $text, $setingToken->value, $setingProxy->value);
//                                }
//                            }
//                        }
//                    }
//
//                    $my_arr[$i][$i2]['message_user_id_status'] = 'true';
//                } else {
////                    $message = DispatchStatus::find()
////                        ->where(
////                            ['account_id' => $item['message']['user_id'],'company_id' => $akk->company_id,
////                                'new_message' => false]
////                        )->one();
////                    if (!$message) {
////                        continue;
////                    }
////
////                    $connect = DispatchConnect::find()->where(['dispatch_regist_id' => $akk->id])->one();
////                    /** @var DispatchStatus $message */
////                    //$message = new DispatchStatus();
////                    $message->account_id = $item['message']['user_id'];
////                    $message->send_account_id = $akk->id;
////                    $message->new_message = true;
////                    $message->read = 'yes';
////                    $message->send = true;
////                    //$message->company_id = $akk->company_id;
////                    if ($connect) $message->dispatch_id = $connect->dispatch_id;
////                    $message->save();
////                    if ($message->errors) {
////                        $my_arr[$i][$i2]['new_error'] = $message->errors;
////                        continue;
////                    }
////
////                    $company = Users::find()->where(['company_id' => $akk->company_id])->one();
////                    if ($company->vk_id) {
////                        $akk->error_notif = true;
////                        $text = "У пользователя №{$message->send_account_id} БЕЗ РАССЫЛКИ новое не прочитаное сообщение
////                              http://demo.teo-send.ru/dispatch-regist/show?unread={$message->send_account_id}
////                              ";
////                        $this->send($company->vk_id, $text, $setingToken->value, $setingProxy->value);
////                    }
////                    $my_arr[$i][$i2]['message_user_id_status'] = 'true';
//
//                }
//            }
//        }
//        echo "<pre>";
//        print_r($my_arr);
//        echo "</pre>";
//    }



    /**
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionAkkunread()//Проверяем все акк на ограничения
    {
        $i = 0;

        $setingProxy = Settings::find()->where(['key' => 'proxy_server'])->one();

        $groupLimit = Settings::find()->where(['key' => 'group_limit_send'])->one();


        $akkAll = DispatchRegist::find()
            ->where(['!=', 'status', DispatchRegist::STATUS_SLEEPING])
            ->andWhere(['!=', 'status', 'account_blocked'])
            ->all();
        /** @var DispatchRegist $akk */
        foreach ($akkAll as $akk) {
            $i++;

            $my_arr[$i]['acc'] = $akk->id;
            $my_arr[$i]['acc_status_start'] = $akk->status;

            if (!$akk->company_id) continue;

            /** @var DispatchRegist $data */
            /** @var CompanySettings $setting */
            $setting = CompanySettings::find()->where(['company_id' => $akk->company_id])->one();
            // кол-во сообщения в день
            $acc_message_count = $setting->messages_daily_limit;
            $my_arr[$i]['acc_message_count'] = $acc_message_count;
            $my_arr[$i]['sended_message_count'] = $akk->sended_message_count;


            //  Проверяем не перевышан лимит сообщения в день (статус становиться limit_exceeded при отправке)
            $d = date("Y-m-d H:i:s", strtotime($akk->last_dispatch_time . " +$setting->reloadTime hours"));
            $now = date("Y-m-d H:i:s");
            $hour = round((strtotime($d) - strtotime($now)) / (60 * 60));
            $my_arr[$i]['hour'] = $hour;

            $my_arr[$i]['hour_status'] = 'false';
            if ($hour < 0) {
                $akk->sended_message_count = 0;
                $akk->status = "ok";
                $my_arr[$i]['hour_status'] = 'true';
            }


            // Проверяем еще не прошел интервал (статус становиться interval_not_end при отправке)
            $date_2 = date("Y-m-d H:i:s");
            $diff = strtotime($date_2) - strtotime($akk->last_dispatch_time);
            $diff = round($setting->send_interval - $diff / 60);
            $my_arr[$i]['diff'] = $diff;

            $my_arr[$i]['diff_count_status'] = 'false';
            if ($diff < 0 && $akk->sended_message_count < $acc_message_count) {
                $akk->status = 'ok';
                $my_arr[$i]['diff_count_status'] = 'true';
            }

            if ($akk->sended_message_count >= $acc_message_count) {
                $akk->status = "limit_exceeded";
            }

            $proxy = Proxy::findOne(['id' => $akk->proxy]);

            if (!$proxy) {
//                    $akk->status = DispatchStatus::STATUS_ACCOUNT_BLOCKED;
//                    $akk->coment = 'Прокси не подключен';
                $akk->makeBlock('Прокси не подключен');
                $akk->update();
                continue;
            }
            $url = 'https://api.vk.com/method/account.setOnline';
            $params = array(
                'access_token' => $akk->token,
                'v' => '5.80'
            );

            // В $result вернется id отправленного сообщения
            $result = file_get_contents($url, false, stream_context_create(array(
                'http' => array(
                    'proxy' => "tcp://{$proxy->ip_adress}:{$proxy->port}",
                    'request_fulluri' => true,
                    'method' => 'POST',
                    'header' => 'Content-type: application/x-www-form-urlencoded',
                    'content' => http_build_query($params)
                )
            )));
            $result = json_decode($result, true);

            //Если проверка показада ошибку и ранее ошибки не было то нужно поставить статус и отправить уведомление
            if ($result['error'] and $akk->status != DispatchStatus::STATUS_ACCOUNT_BLOCKED) {
                //Если проблемма в том что акк заморозили
                if ($result['error']["error_msg"] = 'User authorization failed: invalid access_token (2).') {
                    $my_arr[$i]['error'] = $result['error']["error_msg"];
                    $akk->makeBlock('Заморожен');
                    if (!$akk->error_notif) {
                        $company = Users::find()->where(['company_id' => $akk->company_id])->one();
                        $connect = DispatchConnect::find()->where(['dispatch_regist_id' => $akk->id])->one();
                        if ($company->vk_id) {
                            $akk->error_notif = true;
                            $text = "Аккаунт \"{$akk->username}\" не доступен по причине \"{$result['error']["error_msg"]}\"
                            Для проверки перейдите http://demo.teo-send.ru/dispatch-regist/show-akk?block={$connect->dispatch_id}";
                            // $this->send($company->vk_id, $text, $setingToken->value, $setingProxy->value);
                            self::sendTelMessage('247187885', $text );
                        }
                    }
                    //Если поблемма не в том что акк заморозили
                } else {
                    $my_arr[$i]['error'] = $result['error']["error_msg"];
//                    $akk->status = DispatchStatus::STATUS_ACCOUNT_BLOCKED;
//                    $akk->blocking_date = date('Y-m-d'); //Запись даты блокировки аккаунта
//                    $akk->coment = $result['error']["error_msg"];
                    $akk->makeBlock($result['error']["error_msg"]);
                    if (!$akk->error_notif) {
                        $company = Users::find()->where(['company_id' => $akk->company_id])->one();
                        $connect = DispatchConnect::find()->where(['dispatch_regist_id' => $akk->id])->one();
                        if ($company->vk_id) {
                            $akk->error_notif = true;
                            $text = "Аккаунт \"{$akk->username}\" не доступен по причине \"{$result['error']["error_msg"]}\"
                            Для проверки перейдите http://demo.teo-send.ru/dispatch-regist/show-akk?block={$connect->dispatch_id} ";
                            //$this->send($company->vk_id, $text, $setingToken->value, $setingProxy->value);
                            self::sendTelMessage('247187885', $text );
                        }
                    }
                }
                //Если ранее акк был заблокирована а теперь нет
            } elseif (!$result['error'] and $akk->status == DispatchStatus::STATUS_ACCOUNT_BLOCKED) {
                // $my_arr[$i]['error'] = $result['error']["error_msg"];
                $akk->status = 'ok';
                $akk->coment = $result['error']["error_msg"];
                $akk->error_notif = false;
            }
            $my_arr[$i]['acc_status_finish'] = $akk->status;
            //Проверка на заполнение полей username и account_url
            if ($akk->status == 'ok' && !$akk->username && !$akk->account_url) {
                DispatchRegist::setVkInfo($akk);
                Yii::warning("Данные обновлены. Логин: $akk->login", __METHOD__);
            }
            $akk->update();
        }

        echo "<pre>";
        print_r($my_arr);
        echo "</pre>";

    }

    /**
     * Принимает информацию о новом реферальном посещении
     * @return array
     */
    public function actionSendRefer()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $headers = Yii::$app->response->headers;
        $headers->add('Access-Control-Allow-Origin', '*');

        $refer = new ReferalRedirects([
            'ip' => $_SERVER['REMOTE_ADDR'],
            'refer_company_id' => $_GET['ref'],
            'user_agent' => Yii::$app->request->userAgent,
        ]);

        return ['result' => $refer->save()];
    }




    /**
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionSend()
    {
        $i = 0;
        $max_send = 0;

        $interval_min = Settings::find()->where(['key' => 'interval_in_minutes'])->one();

        // Взять сообщение статусом В очериди
        $messages = Dispatch::find()->where(['status' => 'job', 'type' => 0])->all();

        Yii::warning($messages, __METHOD__);

        if ($messages) {
            $arrStatus = [];
            /** @var Dispatch $message */
            foreach ($messages as $message) {

                Yii::warning(Json::encode($message), __METHOD__);

                $string_acc = '';
                $string_count = 0;

                Yii::warning( $message->id, __METHOD__);

                $connects = DispatchConnect::find()->where(['dispatch_id' => $message->id])->all();

                Yii::warning($connects, __METHOD__);

                foreach ($connects as $connect) {
                    $iLim = 0; //Интервал между отправками
                    $i++;
                    $my_arr[$i]['dispatch_id'] = $connect->dispatch_id;
                    $my_arr[$i]['dispatch_regist_id'] = $connect->dispatch_regist_id;
                    $string_count = 0;
                    $payCount = 0;
                    $payAmount = 0;
                    $account = DispatchRegist::find()->where(['id' => $connect->dispatch_regist_id, 'status' => 'ok'])->one();

                    //VarDumper::dump($account, 10, true); die();

                    if ($account) {
                        $my_arr[$i]['account'] = $account->id;
                        $my_arr[$i]['account_status_start'] = $account->status;
                        $my_arr[$i]['account_name'] = $account->username;

                        /** @var DispatchRegist $data */
                        $setting = CompanySettings::find()->where(['company_id' => $account->company_id])->one();
                        // кол-во сообщения за раз
                        $count_message = $setting->messages_in_transaction;
                        // кол-во сообщения в день
                        $acc_message_count = $setting->messages_daily_limit;

                        // Получаем клиенты кто в очереди кому надо отправить в колчестве кол-во сообщения за раз
                        $clients = DispatchStatus::find()->where(['send' => false, 'dispatch_id' => $message->id])->orderBy(['id' => SORT_ASC])->limit($count_message)->all();

                        //VarDumper::dump($clients, 10, true); die();

                        // если нет таких, то статус сообщения становиться отправлено (finish)
                        if (!$clients) {

                            $fullUrl = Url::toRoute(['/dispatch/view', 'id' => $message->id], true);
                            $text = "Рассылка {$message->name} завершена, можете посмотреть статистику по ссылки {$fullUrl}";
                            $userAll = ArrayHelper::getColumn(DispatchChats::find()->where(['dispatchId' => $message->id])->all(), 'chatId');
                            self::sendTelMessage($userAll, $text);
                            $message->status = "finish";
                            $message->update();
                        } else {
                            $user1 = null;
                            // Отправляем сообщения каждому
                            foreach ($clients as $user) {
                                if ($user->send){
                                    continue;
                                }
                                $key = array_search($user->id, $arrStatus);
                                if ($key) {
                                    // echo  "Дубль <br/>";
                                    // $this->send('', 'dubl', '', '');
                                    continue;
                                }
                                if  ($user->id == $user1) {
                                    continue;
                                }
                                $arrStatus[] = $user->id;
                                $user1 = $user->id;
                                //VarDumper::dump($user, 10, true); die();

                                $proxy = Proxy::findOne(['id' => $account->proxy]);



                                if (!$proxy) {
                                    continue;
                                }
                                $flag_proxy = json_decode(file_get_contents('https://proxy6.net/api/882221fcfc-2a04d5d9ca-854e8ec5e4/check?ids='.$proxy->internal_id));
                                var_dump( $flag_proxy->proxy_id);
                                var_dump( $flag_proxy->proxy_status);
                                if ($flag_proxy->proxy_status == false) {
                                    continue;
                                }
                                $companySetting = CompanySettings::findOne(['company_id' => $account->company_id]);
                                $priceByMessage = $companySetting->getPriceByMessage();
                                $balance = $companySetting->companyModel->bonus_balance > 0 ? $companySetting->companyModel->bonus_balance : $companySetting->companyModel->general_balance;
                                if ($balance < $priceByMessage) {
                                    $fullUrl = Url::toRoute(['/dispatch/view', 'id' => $message->id], true);
                                    $text = "Рассылка {$message->name} остановленна, у вас не достаточно средств {$fullUrl}";
                                    $userAll = ArrayHelper::getColumn(DispatchChats::find()->where(['dispatchId' => $message->id])->all(), 'chatId');
                                    self::sendTelMessage($userAll, $text);
                                    $message->status = 'wait';
                                    $message->save(false);
                                    continue;
                                }
                                if ($proxy->status == 'yes') {
                                    $send_proxy = "{$proxy->ip_adress}:{$proxy->port}";
                                    // задешка 5 секунд
                                    $iLim++;
                                    $max_send++;
                                    if ($iLim == 8) {
                                        sleep(5);
                                        $iLim = 0;
                                    }
                                    if ($max_send == 100) {
                                        return true;
                                    }

                                    // Выбираем случайное сообщение
                                    for ($i = 0; $i < 20; $i++) {
                                        $num = 'text' . random_int(1, 4);
                                        $msg = $message[$num];
                                        if ($msg) {//Если сообщение не пустое - ввыходим из цикла
                                            break 1;
                                        }
                                    }

                                    //VarDumper::dump($msg, 10, true); die();

//                                     $msg = $this->replacingTags($msg, $user);//Обрабатываем теги выбранного сообщения

                                    $tags = TemplateMessages::find()->where(['or', ['company_id' => $account->company_id], ['company_id' => 1]])->all();
                                    $msg = TagHelper::handleTemplateMessages($msg, $tags);
                                    if(UniqueMessage::hasWithText($msg, $message->id) == false){
                                        $uniqueMessage = new UniqueMessage(['dispatch_id' => $message->id, 'text' => $msg]);
                                    } else {
                                        $uniqueMessage = UniqueMessage::findByText($msg);
                                    }
                                    $msg = TagHelper::handleModel($msg, [$account, $user]);



//                                    $statistic = new MessagesStatistics();
//                                    $statistic->content = $msg;
//                                    $statistic->dispatch_id = $message->id;
//                                    $statistic->dispatch_regist_id = $account->id;
//                                    $statistic->company_id = $account->company_id;
//                                    $statistic->save(false);

//                                    if ($msg != 'Недостаточно информации'){
//                                         Отправка сообщения
                                    $result = $this->sendMessage((int)trim($user->account_id), $msg, $account, $send_proxy);
                                    //echo 999;


                                    $string_count++;

//                                    $user->send = true;
//                                    $user->update();
                                    // сообщения успешно отправлено
                                    if ($result == true) {
                                        $mesHistory = new Message();
                                        $mesHistory->date_time = date("Y-m-d H:i:s");
                                        $mesHistory->company_id = $account->company_id;
                                        $mesHistory->dispatch_id = $message->id;
                                        $mesHistory->dispatch_registr_id = $account->id;
                                        $mesHistory->dispatch_status_id = $user->id;
                                        $mesHistory->text = $msg;
                                        $mesHistory->from = Message::FROM_ACCOUNT;
                                        $mesHistory->save(false);

                                        $uniqueMessage->repeat_count++;
                                        $uniqueMessage->save(false);
                                        $user->send = true;
                                        $user->data = date("Y-m-d H:i:s");
                                        $user->read = "no";
                                        $user->send_account_id = $account->id;
                                        $user->response_id = $result['response'];
                                        $user->update();

                                        Yii::warning('Сообщение успешно отправлено', __METHOD__);

                                        // увеличеваем кол-во отправленных сообщения с аккаунта (изначально он равно к 0)
                                        $account->sended_message_count++;
                                        $account->all_sended_message_count++;
                                        $account->last_dispatch_time = $user->data;

                                        // БЛОК СПИСАНИЯ ДЕНЕГ С БАЛАНСА ЮЗЕРА

                                        if ($companySetting->payed()) {
                                            $payCount++;
                                            $payAmount += $priceByMessage;
                                            $accountReport = new AccountingReport([
                                                'company_id' => $account->company_id,
                                                'operation_type' => AccountingReport::TYPE_DISPATCH_PAYED,
                                                'amount' => $payAmount,
                                                'description' => 'Списание средств за рассылку "' . $message->name . '". Отправлено ' . $payCount . ' сообщений',
                                            ]);
                                            $accountReport->save();
                                        }

                                        // сообщения не отправилось записаваем причину в логах
                                    } else {
                                        $my_arr[$i]['account'] = $user->account_id;
                                        // $account->status = DispatchStatus::STATUS_ACCOUNT_BLOCKED;
                                        //$account->blocking_date = date('Y-m-d'); //Запись даты блокировки аккаунта
                                        $account->coment = $result;

                                        $account->update();
                                        continue;
                                    }

                                    // считаем отправленный сообщения с аккаунта если он равно к кол-во сообщения за раз то статус меняем на (interval_not_end)
                                    $my_arr[$i]['string_count'] = $string_count;
                                    $my_arr[$i]['count_message'] = $count_message;
                                    $my_arr[$i]['interval_not_end'] = 'false';
                                    if ($string_count >= $count_message) {
                                        $account->status = "interval_not_end";
                                        $my_arr[$i]['interval_not_end'] = 'true';
                                    }

                                    // кол-во отправленых сообщения равно к кол-во сообщения в день то статус меняем на (limit_exceeded)
                                    $my_arr[$i]['sended_message_count'] = $account->sended_message_count;
                                    $my_arr[$i]['count_message'] = $acc_message_count;
                                    $my_arr[$i]['limit_exceeded'] = 'false';

                                    //отчет отправки в реальном времени
                                    $date = date('Y-m-d');
                                    $rep = DailyReport::find()
                                        ->andfilterWhere(['like','company_id',$account->company_id])
                                        ->andfilterWhere(['like','dispatch_id',$message->id])
                                        ->andfilterWhere(['like','account_id',$account->id])
                                        ->andfilterWhere(['=','type',DailyReport::TYPE_SENT])
                                        ->andfilterWhere(['like','date_event',$date])
                                        ->one();
                                    if ($rep) {
                                        $rep->sended_message_count = $rep->sended_message_count + 1;
                                        $rep->save(false);
                                    } else {
                                        $report = new DailyReport([
                                            'company_id' => $account->company_id,
                                            'dispatch_id' => $message->id,
                                            'account_id' => $account->id,
                                            'sended_message_count' => 1,
                                            'type' => DailyReport::TYPE_SENT,
                                        ]);
                                        $report->save();
                                    }

                                    if ($account->sended_message_count >= $acc_message_count) {
                                        $account->status = "limit_exceeded";
                                        $my_arr[$i]['limit_exceeded'] = 'true';

//                                        $report = new DailyReport([
//                                            'company_id' => $account->company_id,
//                                            'dispatch_id' => $message->id,
//                                            'account_id' => $account->id,
//                                            'sended_message_count' => $account->sended_message_count,
//                                            'type' => DailyReport::TYPE_SENT,
//                                        ]);
//                                        $report->save();


                                    }
                                    $account->update();
//                                    } else {
//                                        Yii::warning('Недостаточно информации для замены тегов. Отправка пропущена', __METHOD__);
//                                        $my_arr['convert_tags'] =  'Недостаточно информации для замены тегов. Отправка пропущена';
//                                    }
                                }
                            }

                            $my_arr[$i]['account_status_finish'] = $account->status;

                        }

                    } else {
                        Yii::$app->session->setFlash('message_fail', "Нет готовых аккаунтов для отправки сообщении");
                    }

                    if ($string_count != 0) {
                        $string_acc .= "С аккаунта {$account->username} отправлен {$string_count} сообщения, ";
                        Yii::$app->session->setFlash('message_success', $string_acc);
                    }
                }

            }
        } else {

            Yii::$app->session->setFlash('message_info', "Нет готовых сообщение для отправки");
        }

        VarDumper::dump($my_arr, 10, true);
    }


    /**
     * Заменяет теги на соответствующие значения.
     * @param string $msg Сообщение
     * @param mixed $user Модель dispatch_status получателя сообщения
     * @return string
     */
    private function replacingTags($msg, $user)
    {
        Yii::warning('Сообщение до обработки: ' . $msg, __METHOD__);
        Yii::warning($user, __METHOD__);

        $dispatch_model = Dispatch::find()->where(['id' => $user->dispatch_id])->one();
        $account_model = DispatchRegist::find()->where(['id' => $user->send_account_id])->one();

        //VarDumper::dump($dispatch_model, 10, true); die();

        $sender = $account_model->username; //Имя отправителя
        $recipient = $user->name; //Имя получателя
//        $base = '--//--'; //Имя базы
        $city = $user->city; //Город получателя
        $dispatch_name = $dispatch_model->name; //Имя рассылки

        if (strpos($msg, '%}') > 0) {
            if (!$sender || !$recipient || !$city || !$dispatch_name){
                //VarDumper::dump('Недостаточно информации для подстановки!', 10, true); die();
                return 'Недостаточно информации';
            }
            $msg = str_replace(Dispatch::TAG_MESSAGE_SENDER, $sender, $msg);
            $msg = str_replace(Dispatch::TAG_MESSAGE_RECIPIENT, $recipient, $msg);
//            $msg = str_replace(Dispatch::TAG_BASE_NAME, $base, $msg);
            $msg = str_replace(Dispatch::TAG_CITY_RECIPIENT, $city, $msg);
            $msg = str_replace(Dispatch::TAG_DISPATCH_NAME, $dispatch_name, $msg);
            Yii::warning('Сообщение после обработки: ' . $msg, __METHOD__);

            //VarDumper::dump($msg, 10, true); die();

        }

        Yii::warning('Сообщение не содержит тегов', __METHOD__);

        return $msg;
    }

    function dateDifference($date_1)
    {

        $date_2 = date("Y-m-d H:i:s");

        $diff = strtotime($date_2) - strtotime($date_1);

        return $diff;

    }

    public static function sendTelMessage($userId, $text)
    {
        //$token = Settings::find()->where(['key' => 'telegram_access_token'])->one();
        $token = Settings::findByKey('telegram_access_token')->value;
        //$proxy_server = Settings::find()->where(['key' => 'proxy_server'])->one();
        $proxy_server = '186.65.114.167:9875';
        // $proxy = $proxy_server->value;

        if(is_array($userId)){
            foreach ($userId as $id) {
                $url = 'https://api.telegram.org/bot'.$token.'/sendMessage';
                $url=$url.'?'.http_build_query(['chat_id' => $id, 'text' => $text, 'parse_mode' => 'HTML']);//к нему мы прибавляем парметры, в виде GET-параметров

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,$url);
                curl_setopt($ch, CURLOPT_PROXY, $proxy_server);
                //curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxyauth);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_HEADER, 1);
                $curl_scraped_page = curl_exec($ch);
                curl_close($ch);

                curl_setopt($curl, CURLOPT_PROXY, $proxy);

            }
        } else {
            $url = 'https://api.telegram.org/bot'.$token.'/sendMessage';
            $url=$url.'?'.http_build_query(['chat_id' => $userId, 'text' => $text, 'parse_mode' => 'HTML']);//к нему мы прибавляем парметры, в виде GET-параметров

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,$url);
            curl_setopt($ch, CURLOPT_PROXY, $proxy_server);
            //curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxyauth);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_HEADER, 1);
            $curl_scraped_page = curl_exec($ch);
            curl_close($ch);
        }


        return true;
    }


    /**
     * @param $token
     * @param $proxy
     * @return mixed
     */
    function getDialog($token, $proxy)
    {
        $url = 'https://api.vk.com/method/messages.getDialogs';
        $params = array(
            'unread' => '1',
            'preview_length' => '200',
            'access_token' => $token, // Токен того кто отправляет
            'v' => '5.80',
        );

        // В $result вернется id отправленного сообщения

        $result = file_get_contents($url, false, stream_context_create(array(
            'http' => array(
                'proxy' => $proxy,
                'request_fulluri' => true,
                'method' => 'POST',
                'header' => 'Content-type: application/x-www-form-urlencoded',
                'content' => http_build_query($params)
            )
        )));

        return json_decode($result, true);
    }



    /**
     * Это метод который ищет "спящие аккаунты", которые не используются более указанного кол-ва дней
     */
    public function actionSearchSleepingDispatchRegists()
    {
        $sleepingIntervalSetting = Settings::findByKey('sleeping_interval');
        if($sleepingIntervalSetting != null && $sleepingIntervalSetting->value != null)
        {
            $sleepingIntervalValue = $sleepingIntervalSetting->value;
        } else {
            $sleepingIntervalValue = 5;
        }
        $sleepingInterval = time() - ($sleepingIntervalValue * 86400); // 86400 — кол-во секунд в сутках
        $sleepingStartDate = date('Y-m-d H:i:s', $sleepingInterval);
        //$accounts = DispatchRegist::find()->where(['<', 'last_dispatch_time', $sleepingStartDate])->andWhere(['or', ['status' => 'sleeping'], ['status' => 'account_blocked']])->all();
        $accounts = DispatchRegist::find()
            ->where(['<', 'last_dispatch_time', $sleepingStartDate])
            ->andWhere(['status' => 'ok'])
            ->all();
        $i = 0;
        foreach ($accounts as $account) {
            $i++;
            $account->makeSleep();
        }

        $allProxiy = Proxy::find()->count();
        $countakk = DispatchRegist::find()->count();
        $all_dar_akk = DispatchRegist::find()->where('status = :ok or status = :limit_exceeded or status = :interval_not_end' ,
            [':ok' => 'ok', ':limit_exceeded' => 'limit_exceeded', ':interval_not_end' => 'interval_not_end'])->count();
        $allAkk3 = DispatchRegist::find()->where(['status' => 'sleeping'])->count();
        $allAkkblock = DispatchRegist::find()->where(['status' => 'account_blocked'])->count();
        $limitAkk = Settings::findByKey('max_proxy')->value;

        $all_suc_akk = $allProxiy * $limitAkk;
        $coud_los = $all_suc_akk -  $all_dar_akk;

        self::sendTelMessage('247187885',"Уснуло = {$i}
        Прокси: {$all_suc_akk}
        Занято прокси: {$all_dar_akk}
        Доступно прокси: {$coud_los}
        Всего Акк: {$countakk}
        Спят акк: {$allAkk3}
        Блок акк: {$allAkkblock}
        " );
    }

    /**
     * Проходит все аккаунты, у которых auto_view == 0 и оформляет их
     */
    public function actionApplyTemplates()
    {
        $dispatchRegists = DispatchRegist::find()->where(['auto_view' => 0])->limit(2)->all();

        /** @var \app\models\DispatchRegist $dispatchRegist */
        foreach ($dispatchRegists as $dispatchRegist)
        {
            $dispatchRegist->auto_view = 1;
            $dispatchRegist->makeAutoPage();
            $dispatchRegist->save(false);
        }
    }

    public function actionAkkInfo()
    {
        $setingProxy = Settings::find()->where(['key' => 'proxy_server'])->one();
        $setingToken = Settings::find()->where(['key' => 'vk_access_token'])->one();
        $allAkk = DispatchStatus::find()->where('photo is  null or name is null')->limit(150)->orderBy(['id' => SORT_DESC,])->all();
        $akks = ArrayHelper::map($allAkk, 'id', 'name');
        $list = '';
        /** @var DispatchStatus $item */
        foreach ($allAkk as $item) {

            if(stripos($item->default_account_id, 'vk.com/'))
            {
                if(stripos($item->default_account_id, 'vk.com/id'))
                {
                    $item->default_account_id = explode('vk.com/id', $item->default_account_id)[1];
                } else {
                    $item->default_account_id = explode('vk.com/', $item->default_account_id)[1];
                }
            }

            $list .= "{$item->default_account_id},";
        }
        $results = $this->getUser($list, 'photo_100,city,country,bdate,sex,domain', $setingToken->value, $setingProxy->value);
        $list = '';
        $i = 0;
        foreach ($results['response'] as $item2) {

            $users = DispatchStatus::find()
                ->where(['or', ['LIKE', 'default_account_id', strval($item2['id'])], ['LIKE', 'default_account_id', strval($item2['domain'])]])
                ->all();

            foreach ($users as $user) {

                if(isset($akks[$user->id]))
                {
                    unset($akks[$user->id]);
                }

                if ($user) {
                    $user->account_id = $item2['id'];
                    $user->name = "{$item2['first_name']}";
                    $user->photo = $item2['photo_100'];
                    $user->age = $item2['bdate'];
                    $user->gender = 'Ж';
                    if ($item2['sex'] == 2) {
                        $user->gender = 'М';
                    }
                    $user->city = $item2['country']['title'] . " " . $item2['city']['title'];
                    $user->save();
                    $i++;
                    $list .= "____{$i}
                  {$item2['first_name']}  {$item2['last_name']} ____
                  {$item2['country']['title']}  {$item2['city']['title']}____
                  {$item2['sex']}_____
                  {$user->gender}_____
                  {$item2['bdate']}_____
                  {$user->age}_____
                  
                  
                  
                  Akk {$item2['id']} Status";
                    if ($user->errors) {
                        $list .= "error";
                    } else {
                        $list .= "good";
                    }
                }
            }
        }
        return $list;
    }

    /**
     * Обновляет информацию у аккаутов, которые находятся в обрабатывающихся рассылках
     * @return string
     */
    public function actionAccountsUpdateInfo()
    {
        $globalList = '';
        $setingProxy = Settings::find()->where(['key' => 'proxy_server'])->one();
        $setingToken = Settings::find()->where(['key' => 'vk_access_token'])->one();
        $dispatches = Dispatch::find()->where(['status' => DispatchStatus::STATUS_HANDLE, 'type' => 0])->all();


        $counter = 0;
        foreach ($dispatches as $dispatch)
        {
            if($counter >= 10)
                break;

            $list = '';
            $clients = DispatchStatus::find()
                ->where('(account_id is  null or name is null) and (dispatch_id = :dispatch_id)', [':dispatch_id' => $dispatch->id])
                ->limit(100)
                ->all();

//            $accountsPks = array_values(ArrayHelper::map(DispatchConnect::find()->where(['dispatch_id' => $dispatch->id])->all(), 'id', 'dispatch_regist_id'));
//
//            $dispatchRegists = DispatchRegist::find()->where(['id' => $accountsPks, 'auto_view' => 0])->limit(2)->all();

//            foreach($dispatchRegists as $dispatchRegist)
//            {
////                $dispatchRegist->auto_view = 1;
//                $output = $dispatchRegist->makeAutoPage();
//                $dispatchRegist->save(false);
//
//                echo "<pre>";
//                print_r($output);
//                echo "</pre>";
//            }

            // $appliedCount = DispatchRegist::find()->where(['id' => $accountsPks, 'auto_view' => 1])->count();

            if(count($clients) == 0){
                $fullUrl = Url::toRoute(['/dispatch/view', 'id' => $dispatch->id], true);
                $text = "Рассылка {$dispatch->name} готова для запуска, можете запустить ее по ссылки {$fullUrl}";
                $userAll = ArrayHelper::getColumn(DispatchChats::find()->where(['dispatchId' => $dispatch->id])->all(), 'chatId');
                self::sendTelMessage($userAll, $text);
                $dispatch->status = DispatchStatus::STATUS_WAIT;
                $dispatch->save();
                continue;

            }

            foreach ($clients as $client)
            {
                if(stripos($client->default_account_id, 'vk.com/'))
                {
                    if(stripos($client->default_account_id, 'vk.com/id'))
                    {
                        $client->default_account_id = explode('vk.com/id', $client->default_account_id)[1];
                    } else {
                        $client->default_account_id = explode('vk.com/', $client->default_account_id)[1];
                    }
                }

                $list .= "{$client->default_account_id},";
                $list2 .= "{$client->id} - {$client->default_account_id},";
            }

            echo "<pre>";
            print_r($list2);
            echo "</pre>";
//            exit();

            $results = $this->getUser($list, 'photo_100,city,country,bdate,sex,domain, can_write_private_message', $setingToken->value, $setingProxy->value);
            $i = 0;
            //     echo "////////////////////////////";

            echo "<pre>";
            print_r($results);
            echo "</pre>";

//            exit;

            if(isset($results['error']))
            {
                echo '1';
                if($results['error']['error_code'] == '113')
                {
                    echo '2';
                    $id = $results['error']['request_params'][2]['value'];
                    echo "<br>$id<br>";
                    $id = trim(str_replace(',', '', $id));
                    $client = DispatchStatus::find()->where('`default_account_id` LIKE :id',[':id' => "%{$id}%"])->one();
                    var_dump($id);
                    echo DispatchStatus::find()->where('`default_account_id` LIKE :id',[':id' => "%{$id}%"])->createCommand()->getRawSql();;
                    if($client != null){
                        $client->delete();
                    }
                }
            }

            $i11 = 0;

            foreach ($results['response'] as $item)
            {
                $i22 = 0;
                $i11++;
                if($item['deactivated'] == 'deleted' || $item['deactivated'] == 'banned')
                {
                    $id = $item['domain'];
                    $client = DispatchStatus::find()->where('`default_account_id` LIKE :id',[':id' => "%{$id}%"])->one();
                    var_dump($id);
                    echo DispatchStatus::find()->where('`default_account_id` LIKE :id',[':id' => "%{$id}"])->createCommand()->getRawSql();;
                    if($client != null){
                        $client->delete();
                    }
                    $id = null;
                }
                /** @var DispatchStatus $users */
                $users = DispatchStatus::find()
                    ->where(['or', ['LIKE', 'default_account_id', strval($item['id'])], ['LIKE', 'default_account_id', strval($item['domain'])]])
                    ->andWhere(['dispatch_id' => $dispatch->id])

//                    ->where('(((`default_account_id` LIKE :id) or (`default_account_id` LIKE :domain)) and (`dispatch_id`= :dis))',
//                        [':id' => '%'.strval($item['id']), ':domain' => '%'.strval($item['domain']), ':dis' => $dispatch->id])
                    ->all();
                echo DispatchStatus::find()
                    ->where(['or', ['LIKE', 'default_account_id', strval($item['id'])], ['LIKE', 'default_account_id', strval($item['domain'])]])
                    ->andWhere(['dispatch_id' => $dispatch->id])

                    ->createCommand()->getRawSql();

                if (count($users) > 2){
                    foreach ($users as $a) {
                        $a->delete();
                        //self::sendTelMessage('247187885', 'Серьезный баг - '.$dispatch->name.'--'.$a->default_account_id );
                        break;
                    }
                    echo 'почему то нету пользователй';
                    continue;
                }

                if ($item['can_write_private_message'] == 0) {
                    foreach ($users as $a) {
                        //self::sendTelMessage('247187885', 'Нельзя отправлять, УДАЛЯЕМ -'.$dispatch->name.'--'.$a->default_account_id );
                        $a->delete();
                        break;
                    }
                    continue;
                }


                foreach ($users as $user) {
                    echo "<br/>I{$i11} {$item['id']}  - {$i22} - {$user->id} - $user->default_account_id";

                    if ($user) {

                        $user->account_id = $item['id'];
                        $user->name = "{$item['first_name']}";
                        $user->photo = $item['photo_100'];
                        $user->age = $item['bdate'];
                        $user->gender = 'Ж';
                        if ($item['sex'] == 2) {
                            $user->gender = 'М';
                        }
                        $user->city = $item['country']['title'] . " " . $item['city']['title'];

                        if (!$user->save()) {
                            echo serialize($user->getErrors());
                            continue;
                        }
                        $i++;
                        $list2 .= "__{$i}
                 {$item['first_name']}  {$item['last_name']} __
                 {$item['country']['title']}  {$item['city']['title']}__
                 {$item['sex']}___
                 {$user->gender}___
                 {$item['bdate']}___
                 {$user->age}___



                 Akk {$item['id']} Status";
                        if ($user->errors) {
                            $list2 .= "error";
                        } else {
                            $list2 .= "good";
                        }
                    }
                }
            }

            $globalList .= $list2;
            $counter++;
        }
        $dispatchRegists = DispatchRegist::find()->where('auto_view = 0 AND (status = :ok or status = :limit_exceeded)',
            [':ok' => 'ok', ':limit_exceeded' => 'limit_exceeded'])->one();
        if ($dispatchRegists) {
            $dispatchRegists->makeAutoPage();
        }
        return $globalList;
    }


    /**
     * @param $token
     * @param $proxy
     * @return mixed
     */
    static function getUser($users, $fields, $token, $proxy)
    {
        if (!$token){

            $com = 0;
            if (FunctionHelper::getCompanyId()) {
                $com  = FunctionHelper::getCompanyModel();
                if ($com) {
                    $com =  serialize($com);;
                }
            }
            self::sendTelMessage('247187885', "Запрос инфы без токена {$com}");

            $result = ['error'=>'Запрос без токена'];
            return $result;
        }
        if (!$proxy) {
            $setingProxy = Settings::find()->where(['key' => 'proxy_server'])->one();
            $proxy = $setingProxy->value;
        }
        $url = 'https://api.vk.com/method/users.get';
        $params = array(
            'user_ids' => $users,
            'fields' => $fields,
            'name_case' => 'Nom', // Токен того кто отправляет
            'access_token' => $token, // Токен того кто отправляет
            'v' => '5.85',
        );

        // В $result вернется id отправленного сообщения

        $result = file_get_contents($url, false, stream_context_create(array(
            'http' => array(
                'proxy' => $proxy,
                'request_fulluri' => true,
                'method' => 'POST',
                'header' => 'Content-type: application/x-www-form-urlencoded',
                'content' => http_build_query($params)
            )
        )));

        return json_decode($result, true);

    }

    /**
     * @param $method
     * @param $akkount_id
     * @param array $params
     * @return mixed|null
     */
    static function request($method, $proxy, $params = [])
    {
        $url = 'https://api.vk.com/method/' . $method;
        $token = Settings::find()->where(['key' => 'vk_access_token'])->one()->value;

        $params['access_token'] = $token;
//        $params['count'] = 10;
        $params['v'] = '5.85';

        $result = file_get_contents($url, false, stream_context_create([
            'http' => [
                'proxy' => $proxy,
                'request_fulluri' => true,
                'method' => 'POST',
                'header' => 'Content-type: application/x-www-form-urlencoded',
                'content' => http_build_query($params)
            ]
        ]));

        return $result ? Json::decode($result, true) : null;

    }


    public function actionSendMessage($userVk, $text, $token, $proxy)
    {
        $this->send($userVk, $text, $token, $proxy);
    }

    public function actionTestmsg($id){
//            $id = 4270;
        $user = DispatchStatus::find()->where(['id' => $id])->one();
//        VarDumper::dump($user, 10, true);
        $msg = 'Привет {%msgRecipient%}! Меня зовут {%msgSender%}. Имя рассылки {%dispatchName%}, твой город {%cityRecipient%}';
        $message = $this->replacingTags($msg, $user);
        return $message;
    }

    public function actionChatsls($id)
    {
        $clients = DispatchStatus::find()->where(['id' => $id])->one();
        if (!$clients) {
            return ['status' => false, 'errors' => 'no client'];
        }
        $account = DispatchRegist::find()->where(['id' => $clients->send_account_id])->one();
        if (!$account) {
            return ['status' => false, 'errors' => 'no account'];
        }


        //$results = getHistory($from_id,$token,$send_proxy);

        $myHistory = Message::find()->where([
            'dispatch_registr_id' =>  $account->id,
            'dispatch_status_id' =>  $clients->id,
        ])->all();


        return ['status' => true, 'errors' => null,'account'=>$account->id, 'history'=>$myHistory];
    }

    public function actionSendsls()
    {
        $clients = DispatchStatus::find()->where(['id' => $_POST['id']])->one();
        if (!$clients) {
            return ['status' => false, 'errors' => 'no client'];
        }
        $account = DispatchRegist::find()->where(['id' => $clients->send_account_id])->one();
        if (!$account) {
            return ['status' => false, 'errors' => 'no account'];
        }


        //$results = getHistory($from_id,$token,$send_proxy);

        $myHistory = Message::find()->where([
            'dispatch_registr_id' =>  $account->id,
            'dispatch_status_id' =>  $clients->id,
        ])->all();

        $from_id =  $clients->account_id;
        $token = $account->token;
        $message = $_POST['message'];
        $id = trim($_POST['id']);

        // Взять аккаунт доступен для отправки
        /** @var DispatchRegist $account */
        $proxy = Proxy::findOne(['id' => $account->proxy]);
        $send_proxy = "tcp://{$proxy->ip_adress}:{$proxy->port}";

        $result = send($from_id, $message, $token,$send_proxy);



        if ($result['response'] != null) {

            $dispatch = Dispatch::find()->where(['id' => $clients->dispatch_id])->one();
            /** @var DispatchStatus $status */

            $status = DispatchStatus::findOne(['id' => $id]);
            $status->new_message = false;
            $status->response_id = $result['response'];
            $status->data = date("Y-m-d H:i:s");
            $status->update();

            /** @var Message $mesHistory */
            $mesHistory = new Message();
            $mesHistory->date_time = date("Y-m-d H:i:s");
            $mesHistory->company_id = $account->company_id;
            $mesHistory->dispatch_id = $status->dispatch_id;
            $mesHistory->dispatch_registr_id = $account->id;
            $mesHistory->dispatch_status_id = $status->id;
            if ($dispatch->text_data) $mesHistory->database_id = $dispatch->text_data;
            $mesHistory->text = $message;
            $mesHistory->from = Message::FROM_ACCOUNT;
            $mesHistory->save();


            return ['status' => true, 'errors' => null,'info'=>'сообщение отправленно'];


        } else {
            return ['status' => false, 'errors' => 'Акк заблокирован', 'info'=>$result['response']];

        }




    }

    public function actionGentext()
    {
        $tags = TemplateMessages::find()->where(['or', ['company_id' => 150], ['company_id' => 1]])->all();
        $countCopy = 0;
        $copy = [];
        $ic = 0;
        for ($i =0; $i < 200; $i++){
            $ic++;
            $msg = '{hello} {dispatchStatus.name}, {my_name} {dispatchRegist.username}. {info} {info2} {finish}';

            $msg = TagHelper::handle($msg, [null, null], $tags);

            if (array_search($msg, $copy)) {
                $countCopy++;
                echo "<br/>{$ic}   ---- <font size='2' color='red'>$ic{$msg}</font>";
            } else {
                echo "<br/>{$ic}   ---- <font size='2' color='green'>{$msg}</font>";
            }
            $copy[] = $msg;
        }
        $procent = 100 - (($i / 100) * $countCopy);

//
//        echo "<pre>";
//        print_r($account);
//        echo "</pre>";
//
//
        echo "<br/>Процент уникального текста: {$procent}%";
        exit();
    }




//////////////////////Новый метод отправки


    public static function sendMessage($userVk, $text, $acc, $proxy)
    {
        define('COOKIE_FILE', 'cookie.txt');

        $data = self::login($acc->login, $acc->password, $proxy);
        if (!preg_match('~/logout~s', $data, $actionLogout)) {
            echo "Ошибка входа в аккаунт, {$acc->username} - {$acc->id} : <br><br>" . $data;
            return false;
        } else {

            self::newsendMessage($userVk, $text, $proxy);

            //выходим из акка возвращая результат
            self::logout2();
            return true;
        }
    }


    public static function newsendMessage($target, $message, $proxy)
    {

        //заходим на стрицу диалога
        $data = self::newrequest('https://m.vk.com/write' . $target . '?mvk_entrypoint=profile_page', 0, 'cookie.txt', 0, 1, $proxy);

        //ищем адрес отправки для поста
        //если его нет значит на страницу мы не зашли
        if (preg_match('~form" action="(.*?)"~s', $data, $actionMessage)) {

            //отправляем сообщение
            $data = self::newrequest('https://m.vk.com/' . $actionMessage[1], ["message" => $message], 'cookie.txt', 0, 0, $proxy);

            //снова загружаем страницу диалога сообщения
            $data = self::newrequest('https://m.vk.com/write' . $target . '?mvk_entrypoint=profile_page', 0, 'cookie.txt', 0, 1, $proxy);

//            //проверяем отправилось ли сообщение и выводим соответствующие результаты
//            if (!preg_match('~' . $message . '~s', $data)) {
////
//                echo '<span style="color: red; font-size: 16px">' . $target . ' ==> ERROR! </span> (Пользователь ограничил приём сообщений или появилась капча)<br>';
//                flush();
//                return false;
//            } else {
//                echo '<span style="color: green; font-size: 16px">' . $target . ' ==> DONE! </span><br> ';
//                flush();
//                return true;
//
//            }
            return true;
            //если не зашли на страницу значит id не правильное, сообщаем
        } else {

            return '<span>Пользователь{ ' . $target . ' }не найден, возможно ошибка в id( только число! )</span><br>';
//            flush();
            return false;
        }
        //спим некоторое время между каждым человеком чтобы не палится
//        sleep(2);
        return true;

    }



    function logout()
    {
        if (file_exists(COOKIE_FILE)) {
            unlink(COOKIE_FILE);
            echo 'Завершено. Выход из аккаунта';
        }

        if (file_exists('0')) {
            unlink('0');
            echo "del1";
        }
        if (file_exists( 'cookie.txt')) {
            echo "del2";
            @unlink( 'cookie.txt');
        }
    }


    public static function newrequest($url, $post = 0, $cookieRead = 0, $cookieWrite = 0, $follow = 0, $proxy)
    {

        $ch = curl_init();
        curl_setopt_array($ch, [
            CURLOPT_PROXYAUTH => CURLAUTH_NTLM,
            CURLOPT_PROXYTYPE => CURLPROXY_HTTP,
            CURLOPT_PROXY => $proxy,
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_USERAGENT => 'Opera/9.80 (Android; Opera Mini/7.5.33361/31.1350; U; en) Presto/2.8.119 Version/11.11',
            CURLOPT_FOLLOWLOCATION => $follow,
            CURLOPT_URL => $url,
        ]);

        if ($cookieWrite !== 0) {
            curl_setopt($ch, CURLOPT_COOKIEJAR, COOKIE_FILE);
        }

        if ($cookieRead !== 0) {
            curl_setopt($ch, CURLOPT_COOKIEFILE, COOKIE_FILE);
        }

        if ($post !== 0) {
            curl_setopt_array($ch, [
                CURLOPT_POST => 1,
                CURLOPT_POSTFIELDS => $post,
            ]);
        }

        if ($err_no = curl_errno($ch)) {
            return "Ошибка get($url) #$err_no: " . curl_error($ch);
        }
        //echo '<br/>' . $url;
        //собираем, записываем в переменную и закрываем
        $data = curl_exec($ch);
        curl_close($ch);

        //спим 1 секунду для каждего запроса что бы уменьшить количество ошибок
//        sleep(1);

        return $data;
    }


    public static function login($login, $password, $proxy)
    {
        //делаем запросс на m.vk.com и получаем страницу сайта в переменную
        $data = self::newrequest('https://m.vk.com/', 0, 0, 1, 1, $proxy);

        //находим адрес отправки post.
        //если есть логинимся
        //в противном случаи мы уже залогинены, возвращаем начальную страницу
        if (preg_match('~post" action="(.*?)"~s', $data, $actionLogin)) {
            return self::newrequest($actionLogin[1], ["email" => $login, "pass" => $password], 1, 1, 1, $proxy);
        } else {
            return $data;
        }

    }

    public static function logout2()
    {
        //удаляем куки
        if (file_exists(COOKIE_FILE)) {
            unlink(COOKIE_FILE);
            // echo 'Завершено. Выход из аккаунта';
        }

        return true;
    }


    /**
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionCheckakk()//возвращаем статус чтобы заного проверить все на новые ответы
    {

        $akkAll = DispatchRegist::find()->where('status = :status || status = :status_limit || status = :interval_not_end',
            ['status' => 'ok', ':status_limit' => 'limit_exceeded', ':interval_not_end' => 'interval_not_end'])
            ->andwhere(['status_read' => 1])->all();
        $akkCount = DispatchRegist::find()->where('status = :status || status = :status_limit || status = :interval_not_end',
            ['status' => 'ok', ':status_limit' => 'limit_exceeded', ':interval_not_end' => 'interval_not_end'])
            ->andwhere(['status_read' => 0])->count();
//        $i = 0;
        if ($akkCount > 1) {
            self::sendTelMessage('247187885', 'не все обработаные' );
            return 'не все прочитаные';
        }
//        $c = count($akkAll);

        //self::sendTelMessage('247187885', "Все были прочитаны {$akkCount} --- {$c}" );
        /** @var DispatchRegist $akk */
        foreach ($akkAll as $akk) {
            $akk->status_read = 0;
            if (!$akk->save()){
                echo serialize($akk);
//                flush();
                continue;
            }

        }
    }




    /**
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionGetdialog()//проверяем диалоги на не прочитанные сообщения
    {

        $akkAll = DispatchRegist::find()->where('status = :status || status = :status_limit || status = :interval_not_end',
            ['status' => 'ok', ':status_limit' => 'limit_exceeded', ':interval_not_end' => 'interval_not_end'])
            ->andwhere(['status_read' => false])->limit(30)->all();
        $i = 0;
        /** @var DispatchRegist $akk */
        foreach ($akkAll as $akk) {

            $proxy = Proxy::findOne(['id' => $akk->proxy]);
            echo "<br/> {$akk->login}";

            $result = null;

            $data = $this->login($akk->login, $akk->password,"{$proxy->ip_adress}:{$proxy->port}");
            if (!preg_match('~/logout~s', $data, $actionLogout)) {
                echo "Ошибка входа в аккаунт, попробуйте ещё раз после чего проверьте логин и пароль или смените прокси: <br><br>" . $data;
                // self::sendTelMessage('247187885', "{Читаем сообщения----не вошли}");
            } else {
                define('COOKIE_FILE', 'cookie.txt');
                $result = $this->readmessage("{$proxy->ip_adress}:{$proxy->port}");
//                $a = serialize($result);
//                self::sendTelMessage('247187885', "Читаем сообщения--{$akk->id}--{$a}");
                //выходим из акка возвращая результат
                $this->logout2();
            }


            echo "<pre>";
            print_r($result);
            echo "</pre>"

            ;
            $akk->status_read = 1;
            if (!$akk->save()){
                continue;
            }
            foreach ($result as $k => $v) {
//                self::sendTelMessage('247187885', "К = {$k} - {$akk->id} ");
                /** @var DispatchStatus $message */
                $message = DispatchStatus::find()
                    ->where(['account_id' => $k, 'send_account_id' => $akk->id])->orderBy('id DESC')->one();

                if ($message) {

                    $message->new_message = true;
                    $message->check_bot = false;
                    $message->read = 'yes';
                    if (!$message->save(false)){
                        echo serialize($message);
                        //flush();
                        continue;
                    }


                    if (!$v) {
                        $a12 = serialize($result);
//                        self::sendTelMessage('247187885', "message = {$message->id} текст - {$a12}");
                    }

                    /** @var Message $mesHistory */
                    $mesHistory = new Message();
                    $mesHistory->date_time = date("Y-m-d H:i:s");
                    $mesHistory->company_id = $akk->company_id;
                    $mesHistory->dispatch_id = $message->dispatch_id;
                    $mesHistory->dispatch_registr_id = $akk->id;
                    $mesHistory->dispatch_status_id = $message->id;
                    $mesHistory->text = $v;
                    $mesHistory->from = Message::FROM_CLIENT;

                    if (!$mesHistory->save(false)){
                        echo serialize($mesHistory);
                        //flush();
                        continue;
                    }
                    $messagesCount = Message::find()->where([
                        'company_id' => $akk->company_id,
                        'dispatch_id' => $message->dispatch_id,
                        'dispatch_registr_id' => $akk->id,
                        'dispatch_status_id' => $message->id
                    ])->count();

                    $maxStoredMessages = 10;

                    if ($messagesCount > $maxStoredMessages) {
                        $deleteCount = $messagesCount - $maxStoredMessages;
                        $deleteMessages = Message::find()->where([
                            'company_id' => $akk->company_id,
                            'dispatch_id' => $message->dispatch_id,
                            'dispatch_registr_id' => $akk->id,
                            'dispatch_status_id' => $message->id
                        ])->limit($deleteCount)->all();

                        foreach ($deleteMessages as $msg) {
                            $msg->delete();
                        }
                    }

                    $dispatch = Dispatch::findOne($message->dispatch_id);

                    $fullUrl = Url::toRoute(['/dispatch-status/dialog', 'id' => $message->id], true);

                    $text = "
                            __________________________________________
                            У пользователя №{$message->send_account_id} новое не прочитаное сообщение 
                                      Рассылка: {$dispatch->name}
                                      Акк: https://vk.com/id{$message->account_id}
                                      Текст: {$mesHistory->text}

                                      {$fullUrl}";

                    // $text = '123';

                    $userAll = ArrayHelper::getColumn(DispatchChats::find()->where(['dispatchId' => $message->dispatch_id])->all(), 'chatId');
                    var_dump($userAll);
                    self::sendTelMessage($userAll, $text);
                }
            }

        }
    }

    //поиск новых ответов сам метод перебора
    function ReadMessage($proxy)
    {
        $arr = [];

        for ($g = 0; $g != 220; $g += 20) {

            //ищим не прочитанные
            $dataIn = $this->newrequest('https://m.vk.com/mail?&offset=' . $g, 0, 1, 0, 1, $proxy);

            if (preg_match_all('~_lv(\d+)\"><\/b><span class=\"di_unread_cnt~s', $dataIn, $checkNoWriteUser)) {

                //запускаем цикл для сбора всех не прочитанных сообщений всех пользователей
                for ($i = 0; $i != count($checkNoWriteUser[1]); $i++) {
                    sleep(1);
//                echo $checkNoWriteUser[1][$i].'<br>';
//                flush();
                    //заходим на страници диалогов с непрочитанными
                    $dataIn = $this->newrequest('https://m.vk.com/write' . $checkNoWriteUser[1][$i] . '?mvk_entrypoint=profile_page', 0, 1, 0, 0, $proxy);
                    //sleep(1);
//                    flush();
                    //узнаём количество непрочитанных и берём ВСЕ выведенные сообщения
                    preg_match_all('~mi_unread~s', $dataIn, $checkAllUnreadMessage);
                    preg_match_all('~mi_text">(.*?)<\/div>~s', $dataIn, $checkAllMessageUser);
//
                    $messageArr = [];

                    //запускаем цикл по добавлению не прочитанных сообщений выбирая с конца смотря по количеству
                    for ($u = 0; $u != count($checkAllUnreadMessage[0]); $u++) {
//
                        sleep(1);
                        $messageArr[$u] = $checkAllMessageUser[1][$u];
//
                    }

                    //составляем асоциативный масив с id и сообщениями
                    //зарание развернув масив сообщений в нужную сторону и сложив всё в строку
                    $arr[$checkNoWriteUser[1][$i]] = implode("/n ", array_reverse($messageArr));

                }
            }
        }

//    array_merge ( $arr [ $tmpArr ] );
//        flush();
        return $arr;
        //спим некоторое время между каждым человеком чтобы не палится
//    sleep(2);

    }
}
