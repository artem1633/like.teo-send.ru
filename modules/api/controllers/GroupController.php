<?php

namespace app\modules\api\controllers;

use app\components\helpers\FunctionHelper;
use app\components\helpers\TagHelper;
use app\models\AccountingReport;
use app\models\Bots;
use app\models\BotsAnswers;
use app\models\BotsDialogs;
use app\models\CompanySettings;
use app\models\DailyReport;
use app\models\Dispatch;
use app\models\DispatchChats;
use app\models\DispatchConnect;
use app\models\DispatchRegist;
use app\models\DispatchStatus;
use app\models\Logs;
use app\models\Message;
use app\models\MessagesStatistics;
use app\models\Proxy;
use app\models\ReferalRedirects;
use app\models\Settings;
use app\models\TemplateMessages;
use app\models\UniqueMessage;
use app\models\Users;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\rest\ActiveController;
use yii\web\Response;
use Yii;

/**
 * Default controller for the `api` module
 */
class GroupController extends ActiveController
{
    public $modelClass = 'app\models\Api';

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'only' => ['testmsg','new-message', 'send','chatsls',
                    'sendsls', 'status', 'history', 'proxy', 'test',
                    'akkunread', 'getdialog', 'getbotdialog', 'akkread',
                    'send-message', 'sendMessage', 'newsend-message', 'newsendMessage',
                    'login','logout2',
                    'newrequest'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }



    /**
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionSend()
    {
        $i = 0;
        $max_send = 0;

        $interval_min = Settings::find()->where(['key' => 'interval_in_minutes'])->one();

        // Взять сообщение статусом В очериди
        $messages = Dispatch::find()->where(['status' => 'job', 'type' => 1])->all();

        Yii::warning($messages, __METHOD__);

        if ($messages) {
            $arrStatus = [];
            /** @var Dispatch $message */
            foreach ($messages as $message) {

                Yii::warning(Json::encode($message), __METHOD__);

                $string_acc = '';
                $string_count = 0;

                Yii::warning( $message->id, __METHOD__);

                $connects = DispatchConnect::find()->where(['dispatch_id' => $message->id])->all();

                Yii::warning($connects, __METHOD__);

                foreach ($connects as $connect) {
                    $iLim = 0; //Интервал между отправками
                    $i++;
                    $my_arr[$i]['dispatch_id'] = $connect->dispatch_id;
                    $my_arr[$i]['dispatch_regist_id'] = $connect->dispatch_regist_id;
                    $string_count = 0;
                    $payCount = 0;
                    $payAmount = 0;
                    $account = DispatchRegist::find()->where(['id' => $connect->dispatch_regist_id, 'status' => 'ok'])->one();

                    //VarDumper::dump($account, 10, true); die();

                    if ($account) {
                        $my_arr[$i]['account'] = $account->id;
                        $my_arr[$i]['account_status_start'] = $account->status;
                        $my_arr[$i]['account_name'] = $account->username;

                        /** @var DispatchRegist $data */
                        $setting = CompanySettings::find()->where(['company_id' => $account->company_id])->one();
                        // кол-во сообщения за раз
                        $count_message = $setting->messages_in_transaction;
                        // кол-во сообщения в день
                        $acc_message_count = $setting->messages_daily_limit;

                        // Получаем клиенты кто в очереди кому надо отправить в колчестве кол-во сообщения за раз
                        $clients = DispatchStatus::find()->where(['send' => false, 'dispatch_id' => $message->id])->orderBy(['id' => SORT_ASC])->limit($count_message)->all();

                        //VarDumper::dump($clients, 10, true); die();

                        // если нет таких, то статус сообщения становиться отправлено (finish)
                        if (!$clients) {

                            $fullUrl = Url::toRoute(['/dispatch/view', 'id' => $message->id], true);
                            $text = "Рассылка {$message->name} завершена, можете посмотреть статистику по ссылки {$fullUrl}";
                            $userAll = ArrayHelper::getColumn(DispatchChats::find()->where(['dispatchId' => $message->id])->all(), 'chatId');
                            ClientController::sendTelMessage($userAll, $text);
                            $message->status = "finish";
                            $message->update();
                        } else {
                            $user1 = null;
                            // Отправляем сообщения каждому
                            foreach ($clients as $user) {
                                if ($user->send){
                                    continue;
                                }
                                $key = array_search($user->id, $arrStatus);
                                if ($key) {
                                    // echo  "Дубль <br/>";
                                    // $this->send('', 'dubl', '', '');
                                    continue;
                                }
                                if  ($user->id == $user1) {
                                    continue;
                                }
                                $arrStatus[] = $user->id;
                                $user1 = $user->id;
                                //VarDumper::dump($user, 10, true); die();

                                $proxy = Proxy::findOne(['id' => $account->proxy]);
                                if (!$proxy) {
                                    continue;
                                }
                                $companySetting = CompanySettings::findOne(['company_id' => $account->company_id]);
                                $priceByMessage = $companySetting->getPriceByMessage();
                                $balance = $companySetting->companyModel->bonus_balance > 0 ? $companySetting->companyModel->bonus_balance : $companySetting->companyModel->general_balance;
                                if ($balance < $priceByMessage) {
                                    $fullUrl = Url::toRoute(['/dispatch/view', 'id' => $message->id], true);
                                    $text = "Рассылка {$message->name} остановленна, у вас не достаточно средств {$fullUrl}";
                                    $userAll = ArrayHelper::getColumn(DispatchChats::find()->where(['dispatchId' => $message->id])->all(), 'chatId');
                                    ClientController::sendTelMessage($userAll, $text);
                                    $message->status = 'wait';
                                    $message->save(false);
                                    continue;
                                }
                                if ($proxy->status == 'yes') {
                                    $send_proxy = "{$proxy->ip_adress}:{$proxy->port}";
                                    // задешка 5 секунд
                                    $iLim++;
                                    $max_send++;
                                    if ($iLim == 8) {
                                        sleep(5);
                                        $iLim = 0;
                                    }
                                    if ($max_send == 100) {
                                        return true;
                                    }

                                    // Выбираем случайное сообщение
                                    for ($i = 0; $i < 20; $i++) {
                                        $num = 'text' . random_int(1, 4);
                                        $msg = $message[$num];
                                        if ($msg) {//Если сообщение не пустое - ввыходим из цикла
                                            break 1;
                                        }
                                    }

                                    //VarDumper::dump($msg, 10, true); die();

//                                     $msg = $this->replacingTags($msg, $user);//Обрабатываем теги выбранного сообщения

                                    $tags = TemplateMessages::find()->where(['or', ['company_id' => $account->company_id], ['company_id' => 1]])->all();
                                    $msg = TagHelper::handleTemplateMessages($msg, $tags);
                                    if(UniqueMessage::hasWithText($msg, $message->id) == false){
                                        $uniqueMessage = new UniqueMessage(['dispatch_id' => $message->id, 'text' => $msg]);
                                    } else {
                                        $uniqueMessage = UniqueMessage::findByText($msg);
                                    }
                                    $msg = TagHelper::handleModel($msg, [$account, $user]);



//                                    $statistic = new MessagesStatistics();
//                                    $statistic->content = $msg;
//                                    $statistic->dispatch_id = $message->id;
//                                    $statistic->dispatch_regist_id = $account->id;
//                                    $statistic->company_id = $account->company_id;
//                                    $statistic->save(false);

//                                    if ($msg != 'Недостаточно информации'){
//                                         Отправка сообщения
                                    $result = $this->sendMessage((int)trim($user->account_id), $msg, $account, $send_proxy);
                                    //echo 999;


                                    $string_count++;

//                                    $user->send = true;
//                                    $user->update();
                                    // сообщения успешно отправлено
                                    if ($result == true) {
                                        $mesHistory = new Message();
                                        $mesHistory->date_time = date("Y-m-d H:i:s");
                                        $mesHistory->company_id = $account->company_id;
                                        $mesHistory->dispatch_id = $message->id;
                                        $mesHistory->dispatch_registr_id = $account->id;
                                        $mesHistory->dispatch_status_id = $user->id;
                                        $mesHistory->text = $msg;
                                        $mesHistory->from = Message::FROM_ACCOUNT;
                                        $mesHistory->save(false);

                                        $uniqueMessage->repeat_count++;
                                        $uniqueMessage->save(false);
                                        $user->send = true;
                                        $user->data = date("Y-m-d H:i:s");
                                        $user->read = "no";
                                        $user->send_account_id = $account->id;
                                        $user->response_id = $result['response'];
                                        $user->update();

                                        Yii::warning('Сообщение успешно отправлено', __METHOD__);

                                        // увеличеваем кол-во отправленных сообщения с аккаунта (изначально он равно к 0)
                                        $account->sended_message_count++;
                                        $account->all_sended_message_count++;
                                        $account->last_dispatch_time = $user->data;

                                        // БЛОК СПИСАНИЯ ДЕНЕГ С БАЛАНСА ЮЗЕРА

                                        if ($companySetting->payed()) {
                                            $payCount++;
                                            $payAmount += $priceByMessage;
                                            $accountReport = new AccountingReport([
                                                'company_id' => $account->company_id,
                                                'operation_type' => AccountingReport::TYPE_DISPATCH_PAYED,
                                                'amount' => $payAmount,
                                                'description' => 'Списание средств за рассылку "' . $message->name . '". Отправлено ' . $payCount . ' сообщений',
                                            ]);
                                            $accountReport->save();
                                        }

                                        // сообщения не отправилось записаваем причину в логах
                                    } else {
                                        $my_arr[$i]['account'] = $user->account_id;
                                        // $account->status = DispatchStatus::STATUS_ACCOUNT_BLOCKED;
                                        //$account->blocking_date = date('Y-m-d'); //Запись даты блокировки аккаунта
                                        $account->coment = $result;

                                        $account->update();
                                        continue;
                                    }

                                    // считаем отправленный сообщения с аккаунта если он равно к кол-во сообщения за раз то статус меняем на (interval_not_end)
                                    $my_arr[$i]['string_count'] = $string_count;
                                    $my_arr[$i]['count_message'] = $count_message;
                                    $my_arr[$i]['interval_not_end'] = 'false';
                                    if ($string_count >= $count_message) {
                                        $account->status = "interval_not_end";
                                        $my_arr[$i]['interval_not_end'] = 'true';
                                    }

                                    // кол-во отправленых сообщения равно к кол-во сообщения в день то статус меняем на (limit_exceeded)
                                    $my_arr[$i]['sended_message_count'] = $account->sended_message_count;
                                    $my_arr[$i]['count_message'] = $acc_message_count;
                                    $my_arr[$i]['limit_exceeded'] = 'false';

                                    //отчет отправки в реальном времени
                                    $date = date('Y-m-d');
                                    $rep = DailyReport::find()
                                        ->andfilterWhere(['like','company_id',$account->company_id])
                                        ->andfilterWhere(['like','dispatch_id',$message->id])
                                        ->andfilterWhere(['like','account_id',$account->id])
                                        ->andfilterWhere(['=','type',DailyReport::TYPE_SENT])
                                        ->andfilterWhere(['like','date_event',$date])
                                        ->one();
                                    if ($rep) {
                                        $rep->sended_message_count = $rep->sended_message_count + 1;
                                        $rep->save(false);
                                    } else {
                                        $report = new DailyReport([
                                            'company_id' => $account->company_id,
                                            'dispatch_id' => $message->id,
                                            'account_id' => $account->id,
                                            'sended_message_count' => 1,
                                            'type' => DailyReport::TYPE_SENT,
                                        ]);
                                        $report->save();
                                    }

                                    if ($account->sended_message_count >= $acc_message_count) {
                                        $account->status = "limit_exceeded";
                                        $my_arr[$i]['limit_exceeded'] = 'true';

//                                        $report = new DailyReport([
//                                            'company_id' => $account->company_id,
//                                            'dispatch_id' => $message->id,
//                                            'account_id' => $account->id,
//                                            'sended_message_count' => $account->sended_message_count,
//                                            'type' => DailyReport::TYPE_SENT,
//                                        ]);
//                                        $report->save();


                                    }
                                    $account->update();
//                                    } else {
//                                        Yii::warning('Недостаточно информации для замены тегов. Отправка пропущена', __METHOD__);
//                                        $my_arr['convert_tags'] =  'Недостаточно информации для замены тегов. Отправка пропущена';
//                                    }
                                }
                            }

                            $my_arr[$i]['account_status_finish'] = $account->status;

                        }

                    } else {
                        Yii::$app->session->setFlash('message_fail', "Нет готовых аккаунтов для отправки сообщении");
                    }

                    if ($string_count != 0) {
                        $string_acc .= "С аккаунта {$account->username} отправлен {$string_count} сообщения, ";
                        Yii::$app->session->setFlash('message_success', $string_acc);
                    }
                }

            }
        } else {

            Yii::$app->session->setFlash('message_info', "Нет готовых сообщение для отправки");
        }

        VarDumper::dump($my_arr, 10, true);
    }


    /**
     * Обновляет информацию у аккаутов, которые находятся в обрабатывающихся рассылках
     * @return string
     */
    public function actionAccountsUpdateInfo()
    {
        $globalList = '';
        $setingProxy = Settings::find()->where(['key' => 'proxy_server'])->one();
        $setingToken = Settings::find()->where(['key' => 'vk_access_token'])->one();
        $dispatches = Dispatch::find()->where(['status' => DispatchStatus::STATUS_HANDLE, 'type' => 1])->all();


        $counter = 0;
        foreach ($dispatches as $dispatch)
        {
            if($counter >= 10)
                break;

            $list = '';
            $clients = DispatchStatus::find()
                ->where('(account_id is  null or name is null) and (dispatch_id = :dispatch_id)', [':dispatch_id' => $dispatch->id])
                ->limit(100)
                ->all();


            if(count($clients) == 0){
                $fullUrl = Url::toRoute(['/dispatch/view', 'id' => $dispatch->id], true);
                $text = "Рассылка {$dispatch->name} готова для запуска, можете запустить ее по ссылки {$fullUrl}";
                $userAll = ArrayHelper::getColumn(DispatchChats::find()->where(['dispatchId' => $dispatch->id])->all(), 'chatId');
                ClientController::sendTelMessage($userAll, $text);
                $dispatch->status = DispatchStatus::STATUS_WAIT;
                $dispatch->save();
                continue;

            }

            foreach ($clients as $client)
            {
                if(stripos($client->default_account_id, 'vk.com/'))
                {
                    if(stripos($client->default_account_id, 'vk.com/id'))
                    {
                        $client->default_account_id = explode('vk.com/id', $client->default_account_id)[1];
                    } else {
                        $client->default_account_id = explode('vk.com/', $client->default_account_id)[1];
                    }
                }

                $list .= "{$client->default_account_id},";
                $list2 .= "{$client->id} - {$client->default_account_id},";
            }

            echo "<pre>";
            print_r($list2);
            echo "</pre>";
//            exit();

            $results = $this->getGroup($list, 'photo_100,city,country,domain,can_message', $setingToken->value, $setingProxy->value);
            $i = 0;
            //     echo "////////////////////////////";

//            echo "<pre>";
//            print_r($results);
//            echo "</pre>";

//            exit;

            if(isset($results['error']))
            {
                echo '1';
                if($results['error']['error_code'] == '113')
                {
                    echo '2';
                    $id = $results['error']['request_params'][2]['value'];
                    echo "<br>$id<br>";
                    $id = trim(str_replace(',', '', $id));
                    $client = DispatchStatus::find()->where('`default_account_id` LIKE :id',[':id' => "%{$id}%"])->one();
                    var_dump($id);
                    echo DispatchStatus::find()->where('`default_account_id` LIKE :id',[':id' => "%{$id}%"])->createCommand()->getRawSql();;
                    if($client != null){
                        $client->delete();
                    }
                }
            }

            $i11 = 0;

            foreach ($results['response'] as $item)
            {
                $i22 = 0;
                $i11++;
                if($item['deactivated'] == 'deleted' || $item['deactivated'] == 'banned')
                {
                    $id = $item['screen_name'];
                    $client = DispatchStatus::find()->where('`default_account_id` LIKE :id',[':id' => "%{$id}%"])->one();
                    var_dump($id);
                    echo DispatchStatus::find()->where('`default_account_id` LIKE :id',[':id' => "%{$id}"])->createCommand()->getRawSql();;
                    if($client != null){
                        $client->delete();
                    }
                    $id = null;
                }
                /** @var DispatchStatus $users */
                $users = DispatchStatus::find()
                    ->where(['or', ['LIKE', 'default_account_id', strval($item['id'])], ['LIKE', 'default_account_id', strval($item['screen_name'])]])
                    ->andWhere(['dispatch_id' => $dispatch->id])
                    ->all();
//                echo DispatchStatus::find()
//                    ->where(['or', ['LIKE', 'default_account_id', strval($item['id'])], ['LIKE', 'default_account_id', strval($item['screen_name'])]])
//                    ->andWhere(['dispatch_id' => $dispatch->id])
//
//                    ->createCommand()->getRawSql();

                if (count($users) > 2){
                    foreach ($users as $a) {
                        $a->delete();
                        //ClientController::sendTelMessage('247187885', 'Серьезный баг - '.$dispatch->name.'--'.$a->default_account_id );
                        break;
                    }
                    echo 'почему то нету пользователй';
                    continue;
                }

                if ($item['can_message'] == 0) {
                    foreach ($users as $a) {
                        //ClientController::sendTelMessage('247187885', 'Нельзя отправлять, УДАЛЯЕМ -'.$dispatch->name.'--'.$a->default_account_id );
                        $a->delete();
                        break;
                    }
                    continue;
                }


                foreach ($users as $user) {
                    echo "<br/>I{$i11} {$item['id']}  - {$i22} - {$user->id} - $user->default_account_id";

                    if ($user) {

                        $user->account_id = $item['id'];
                        $user->name = "{$item['name']}";
                        $user->photo = $item['photo_100'];
                        if (!$user->save()) {
                            echo serialize($user->getErrors());
                            continue;
                        }
                        $i++;

                        if ($user->errors) {
                            $list2 .= "error";
                        } else {
                            $list2 .= "good";
                        }
                    }
                }
            }

            $globalList .= $list2;
            $counter++;
        }
        $dispatchRegists = DispatchRegist::find()->where('auto_view = 0 AND (status = :ok or status = :limit_exceeded)',
            [':ok' => 'ok', ':limit_exceeded' => 'limit_exceeded'])->one();
        if ($dispatchRegists) {
            $dispatchRegists->makeAutoPage();
        }
        return $globalList;
    }


    /**
     * @param $token
     * @param $proxy
     * @return mixed
     */
    public static function getGroup($users, $fields, $token, $proxy)
    {
        if (!$token){
            $com = 0;
            if (FunctionHelper::getCompanyId()) {
                $com  = FunctionHelper::getCompanyModel();
                if ($com) {
                    $com =  serialize($com);;
                }
            }
            ClientController::sendTelMessage('247187885', "Запрос инфы без токена {$com}");

            $result = ['error'=>'Запрос без токена'];
            return $result;
        }
        if (!$proxy) {
            $setingProxy = Settings::find()->where(['key' => 'proxy_server'])->one();
            $proxy = $setingProxy->value;
        }
        $url = 'https://api.vk.com/method/groups.getById';
        $params = array(
            'group_ids' => $users,
            'fields' => $fields,
            'name_case' => 'Nom', // Токен того кто отправляет
            'access_token' => $token, // Токен того кто отправляет
            'v' => '5.95',
        );

        // В $result вернется id отправленного сообщения

        $result = file_get_contents($url, false, stream_context_create(array(
            'http' => array(
                'proxy' => $proxy,
                'request_fulluri' => true,
                'method' => 'POST',
                'header' => 'Content-type: application/x-www-form-urlencoded',
                'content' => http_build_query($params)
            )
        )));

        return json_decode($result, true);

    }

    /**
     * @param $method
     * @param $akkount_id
     * @param array $params
     * @return mixed|null
     */
    static function request($method, $proxy, $params = [])
    {
        $url = 'https://api.vk.com/method/' . $method;
        $token = Settings::find()->where(['key' => 'vk_access_token'])->one()->value;

        $params['access_token'] = $token;
//        $params['count'] = 10;
        $params['v'] = '5.85';

        $result = file_get_contents($url, false, stream_context_create([
            'http' => [
                'proxy' => $proxy,
                'request_fulluri' => true,
                'method' => 'POST',
                'header' => 'Content-type: application/x-www-form-urlencoded',
                'content' => http_build_query($params)
            ]
        ]));

        return $result ? Json::decode($result, true) : null;

    }


//////////////////////Новый метод отправки


    public static function sendMessage($userVk, $text, $acc, $proxy)
    {
        define('COOKIE_FILE2', 'cookie2.txt');

        $data = self::login($acc->login, $acc->password, $proxy);
        if (!preg_match('~/logout~s', $data, $actionLogout)) {
            echo "Ошибка входа в аккаунт, {$acc->username} - {$acc->id} : <br><br>" . $data;
            return false;
        } else {

            self::newsendMessage($userVk, $text, $proxy);

            //выходим из акка возвращая результат
            self::logout2();
            return true;
        }
    }


    public static function newsendMessage($target, $message, $proxy)
    {

        //заходим на стрицу диалога
        $data = self::newrequest('https://m.vk.com/write-' . $target . '?mvk_entrypoint=community_page', 0, 'cookie2.txt', 0, 1, $proxy);

        //ищем адрес отправки для поста
        //если его нет значит на страницу мы не зашли
        if (preg_match('~form" action="(.*?)"~s', $data, $actionMessage)) {

            //отправляем сообщение
            $data = self::newrequest('https://m.vk.com/' . $actionMessage[1], ["message" => $message], 'cookie2.txt', 0, 0, $proxy);

            //снова загружаем страницу диалога сообщения
            $data = self::newrequest('https://m.vk.com/write-' . $target . '?mvk_entrypoint=community_page', 0, 'cookie2.txt', 0, 1, $proxy);

//            //проверяем отправилось ли сообщение и выводим соответствующие результаты
//            if (!preg_match('~' . $message . '~s', $data)) {
////
//                echo '<span style="color: red; font-size: 16px">' . $target . ' ==> ERROR! </span> (Пользователь ограничил приём сообщений или появилась капча)<br>';
//                flush();
//                return false;
//            } else {
//                echo '<span style="color: green; font-size: 16px">' . $target . ' ==> DONE! </span><br> ';
//                flush();
//                return true;
//
//            }
            return true;
            //если не зашли на страницу значит id не правильное, сообщаем
        } else {

            return '<span>Пользователь{ ' . $target . ' }не найден, возможно ошибка в id( только число! )</span><br>';
            flush();
            return false;
        }
        //спим некоторое время между каждым человеком чтобы не палится
//        sleep(2);
        return true;

    }



    function logout()
    {
        if (file_exists(COOKIE_FILE2)) {
            unlink(COOKIE_FILE2);
            echo 'Завершено. Выход из аккаунта';
        }

        if (file_exists('0')) {
            unlink('0');
            echo "del1";
        }
        if (file_exists( 'cookie2.txt')) {
            echo "del2";
            @unlink( 'cookie2.txt');
        }
    }


    public static function newrequest($url, $post = 0, $cookieRead = 0, $cookieWrite = 0, $follow = 0, $proxy)
    {

        $ch = curl_init();
        curl_setopt_array($ch, [
            CURLOPT_PROXYAUTH => CURLAUTH_NTLM,
            CURLOPT_PROXYTYPE => CURLPROXY_HTTP,
            CURLOPT_PROXY => $proxy,
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_USERAGENT => 'Opera/9.80 (Android; Opera Mini/7.5.33361/31.1350; U; en) Presto/2.8.119 Version/11.11',
            CURLOPT_FOLLOWLOCATION => $follow,
            CURLOPT_URL => $url,
        ]);

        if ($cookieWrite !== 0) {
            curl_setopt($ch, CURLOPT_COOKIEJAR, COOKIE_FILE2);
        }

        if ($cookieRead !== 0) {
            curl_setopt($ch, CURLOPT_COOKIEFILE, COOKIE_FILE2);
        }

        if ($post !== 0) {
            curl_setopt_array($ch, [
                CURLOPT_POST => 1,
                CURLOPT_POSTFIELDS => $post,
            ]);
        }

        if ($err_no = curl_errno($ch)) {
            return "Ошибка get($url) #$err_no: " . curl_error($ch);
        }
        //echo '<br/>' . $url;
        //собираем, записываем в переменную и закрываем
        $data = curl_exec($ch);
        curl_close($ch);

        //спим 1 секунду для каждего запроса что бы уменьшить количество ошибок
//        sleep(1);

        return $data;
    }


    public static function login($login, $password, $proxy)
    {
        //делаем запросс на m.vk.com и получаем страницу сайта в переменную
        $data = self::newrequest('https://m.vk.com/', 0, 0, 1, 1, $proxy);

        //находим адрес отправки post.
        //если есть логинимся
        //в противном случаи мы уже залогинены, возвращаем начальную страницу
        if (preg_match('~post" action="(.*?)"~s', $data, $actionLogin)) {
            return self::newrequest($actionLogin[1], ["email" => $login, "pass" => $password], 1, 1, 1, $proxy);
        } else {
            return $data;
        }

    }

    public static function logout2()
    {
        //удаляем куки
        if (file_exists(COOKIE_FILE2)) {
            unlink(COOKIE_FILE2);
            // echo 'Завершено. Выход из аккаунта';
        }

        return true;
    }





    /**
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionGetdialog()//проверяем диалоги на не прочитанные сообщения
    {

        $akkAll = DispatchRegist::find()->where('status = :status || status = :status_limit || status = :interval_not_end',
            ['status' => 'ok', ':status_limit' => 'limit_exceeded', ':interval_not_end' => 'interval_not_end'])
            ->andwhere(['status_read' => false])->limit(30)->all();
        $i = 0;
        /** @var DispatchRegist $akk */
        foreach ($akkAll as $akk) {

            $proxy = Proxy::findOne(['id' => $akk->proxy]);
            echo "<br/> {$akk->login}";

            $result = null;

            $data = $this->login($akk->login, $akk->password,"{$proxy->ip_adress}:{$proxy->port}");
            if (!preg_match('~/logout~s', $data, $actionLogout)) {
                echo "Ошибка входа в аккаунт, попробуйте ещё раз после чего проверьте логин и пароль или смените прокси: <br><br>" . $data;
                // ClientController::sendTelMessage('247187885', "{Читаем сообщения----не вошли}");
            } else {
                define('COOKIE_FILE2', 'cookie2.txt');
                $result = $this->readmessage("{$proxy->ip_adress}:{$proxy->port}",$akk);
//                $a = serialize($result);
//                ClientController::sendTelMessage('247187885', "Читаем сообщения--{$akk->id}--{$a}");
                //выходим из акка возвращая результат
                $this->logout2();
            }


            echo "<pre>";
            print_r($result);
            echo "</pre>"

            ;
//            $akk->status_read = 1;
            if (!$akk->save()){
                continue;
            }
            foreach ($result as $k => $v) {
//                ClientController::sendTelMessage('247187885', "К = {$k} - {$akk->id} ");
                /** @var DispatchStatus $message */
                $message = DispatchStatus::find()
                    ->where(['account_id' => $k, 'send_account_id' => $akk->id])->orderBy('id DESC')->one();

                if ($message) {

                    $message->new_message = true;
                    $message->check_bot = false;
                    $message->read = 'yes';
                    if (!$message->save(false)){
                        echo serialize($message);
                        //flush();
                        continue;
                    }


//                    ClientController::sendTelMessage('247187885', "message = {$message->id} ");


                    /** @var Message $mesHistory */
                    $mesHistory = new Message();
                    $mesHistory->date_time = date("Y-m-d H:i:s");
                    $mesHistory->company_id = $akk->company_id;
                    $mesHistory->dispatch_id = $message->dispatch_id;
                    $mesHistory->dispatch_registr_id = $akk->id;
                    $mesHistory->dispatch_status_id = $message->id;
                    $mesHistory->text = $v;
                    $mesHistory->from = Message::FROM_CLIENT;

                    if (!$mesHistory->save(false)){
                        echo serialize($mesHistory);
                        //flush();
                        continue;
                    }
                    $messagesCount = Message::find()->where([
                        'company_id' => $akk->company_id,
                        'dispatch_id' => $message->dispatch_id,
                        'dispatch_registr_id' => $akk->id,
                        'dispatch_status_id' => $message->id
                    ])->count();

                    $maxStoredMessages = 10;

                    if ($messagesCount > $maxStoredMessages) {
                        $deleteCount = $messagesCount - $maxStoredMessages;
                        $deleteMessages = Message::find()->where([
                            'company_id' => $akk->company_id,
                            'dispatch_id' => $message->dispatch_id,
                            'dispatch_registr_id' => $akk->id,
                            'dispatch_status_id' => $message->id
                        ])->limit($deleteCount)->all();

                        foreach ($deleteMessages as $msg) {
                            $msg->delete();
                        }
                    }

                    $dispatch = Dispatch::findOne($message->dispatch_id);

                    $fullUrl = Url::toRoute(['/dispatch-status/dialog', 'id' => $message->id], true);

                    $text = "
                            __________________________________________
                            У пользователя №{$message->send_account_id} новое не прочитаное сообщение 
                                      Рассылка: {$dispatch->name}
                                      Акк: https://vk.com/id{$message->account_id}
                                      Текст: {$mesHistory->text}

                                      {$fullUrl}";

                    // $text = '123';

                    $userAll = ArrayHelper::getColumn(DispatchChats::find()->where(['dispatchId' => $message->dispatch_id])->all(), 'chatId');
                    var_dump($userAll);
                    ClientController::sendTelMessage($userAll, $text);
                }
            }

        }
    }

    //поиск новых ответов сам метод перебора
    //поиск новых ответов сам метод перебора
    function ReadMessage($proxy)
    {
        $arr = [];

        for ($g = 0; $g != 220; $g += 20) {

            //ищим не прочитанные
            $dataIn = $this->newrequest('https://m.vk.com/mail?&offset=' . $g, 0, 1, 0, 1, $proxy);
            $q1 = preg_match_all('~_lv-(\d+)\"><\/b><span class=\"di_unread_cnt~s', $dataIn, $checkNoWriteUser);
            var_dump($checkNoWriteUser);
            if (preg_match_all('~_lv-(\d+)\"><\/b><span class=\"di_unread_cnt~s', $dataIn, $checkNoWriteUser)) {

                //запускаем цикл для сбора всех не прочитанных сообщений всех пользователей
                for ($i = 0; $i != count($checkNoWriteUser[1]); $i++) {
                    sleep(1);
//                echo $checkNoWriteUser[1][$i].'<br>';
//                flush();
                    //заходим на страници диалогов с непрочитанными
                    $dataIn = $this->newrequest('https://m.vk.com/write-' . $checkNoWriteUser[1][$i] . '?mvk_entrypoint=community_page', 0, 1, 0, 0, $proxy);
                    //sleep(1);
//                    flush();
                    //узнаём количество непрочитанных и берём ВСЕ выведенные сообщения
                    preg_match_all('~mi_unread~s', $dataIn, $checkAllUnreadMessage);
                    preg_match_all('~mi_text">(.*?)<\/div>~s', $dataIn, $checkAllMessageUser);
//
                    $messageArr = [];

                    //запускаем цикл по добавлению не прочитанных сообщений выбирая с конца смотря по количеству
                    for ($u = 0; $u != count($checkAllUnreadMessage[0]); $u++) {
//
                        sleep(1);
                        $messageArr[$u] = $checkAllMessageUser[1][$u];
//
                    }

                    //составляем асоциативный масив с id и сообщениями
                    //зарание развернув масив сообщений в нужную сторону и сложив всё в строку
                    $arr[$checkNoWriteUser[1][$i]] = implode("/n ", array_reverse($messageArr));

                }
            }
        }

//    array_merge ( $arr [ $tmpArr ] );
//        flush();
        return $arr;
        //спим некоторое время между каждым человеком чтобы не палится
//    sleep(2);

    }
}
