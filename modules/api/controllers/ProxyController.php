<?php

namespace app\modules\api\controllers;

use app\components\helpers\FunctionHelper;
use app\models\Dispatch;
use app\models\DispatchRegist;
use app\models\Proxy;
use app\models\Settings;
use Yii;
use yii\rest\ActiveController;
use yii\web\Response;


/**
 * Proxy API Controller
 */
class ProxyController extends ActiveController
{
    public $modelClass = 'app\models\Api';

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    /**
     * Производит проверку аккаунтов на наличие прокси
     * Если у аккаунта нет прокси, то пытается привязать к нему свободный
     * Если свободного прокси нет, то покупает его.
     */
    public function actionCheckAccounts()
    {
        Proxy::synchronize();

        $accountsNoProxy = DispatchRegist::find()->where(['proxy' => null])->andWhere(['!=', 'status', 'account_blocked'])->andWhere(['!=', 'status', 'sleeping'])->all();

        // Присвоение аккаунтам прокси
        foreach ($accountsNoProxy as $account) {
            /** @var \app\models\DispatchRegist $account */
            /** @var \app\models\Proxy $proxy */
            $proxy = $account->getOneAvailableProxy();
            if ($proxy != null) {
                $account->proxy = $proxy->id;
                $account->save(false);
                $proxy->companyId = 1;
                $proxy->save(false);
            } else {
                $proxy = Yii::$app->proxy->buy();
                if ($proxy != null) {
                    if (isset($proxy->list[0])) {
                        $proxy = $proxy->list[0];
                        $proxy = Proxy::createFromApiInstance($proxy);
                        if ($proxy->save(false)) {
                            $account->proxy = $proxy->id;
                            $account->save(false);
                            $proxy->companyId = 1;
                            $proxy->save(false);
                        }
                    }
                }
            }
        }

        $accountsBlocked = DispatchRegist::find()->where(['status' => 'account_blocked'])->all();

        foreach ($accountsBlocked as $account)
        {
            $account->proxy = null;
            $account->save(false);
        }
    }

    /**
     * Проверяет прокси на срок истечения и уведомляет об этом Администратора через ВК
     */
    public function actionCheckProxy()
    {
        /** @var \app\models\Proxy[] $proxyList */
        $proxyList = Proxy::find()->all();
        $list = Proxy::synchronize();
        //echo $list;
        foreach ($proxyList as $proxy)
        {
            //Отправка уведомления
            $vk_id = Settings::find()->where(['key' => 'akk_notify'])->one()->value;
            $sendingProxy = Settings::find()->where(['key' => 'proxy_server'])->one()->value;
            $token = Settings::find()->where(['key' => 'vk_access_token'])->one()->value;
            $diff = $this->getExpiringProxyHours($proxy) - 3;
            //echo $diff.'------'.$proxy->ip_adress.'+++++++++';
            // Если прокси исчет совсем скоро и его нужно продлить
            if($diff < 5)
            {
                echo $diff.'------'.$proxy->ip_adress.'+++++++++';
                $result = Yii::$app->proxy->prolong($proxy->internal_id);

                if(isset($result->list[0]))
                {
                    $proxy->expire_date = $result->list[0]->date_end;
                    $proxy->save(false);
                }
            }

            if($diff < 7)
            {
                ClientController::sendTelMessage('247187885',  "Прокси {$proxy->fullIp} истечет через 1 день (через {$this->getExpiringProxyHours($proxy)} ч.)");




            }
//            if($this->isExpiringProxyDays($proxy, 2))
//            {
//                ClientController::request('messages.send', $sendingProxy, [
//                    'access_token' => $token,
//                    'user_id' => $vk_id,
//                    'message' => "Прокси {$proxy->fullIp} истечет через 2 дня (через {$this->getExpiringProxyHours($proxy)} ч.)",
//                ]);
//                return;
//            }
//            if($this->isExpiringProxyDays($proxy, 3))
//            {
//                ClientController::request('messages.send', $sendingProxy, [
//                    'access_token' => $token,
//                    'user_id' => $vk_id,
//                    'message' => "Прокси {$proxy->fullIp} истечет через 3 дня (через {$this->getExpiringProxyHours($proxy)} ч.)",
//                ]);
//                return;
//            }
        }
    }

//    public function actionTest()
//    {
//        $proxy = new Proxy(['expire_date' => '2018-11-23 13:44:00']);
//
//        return $this->getExpiringProxyHours($proxy);
//    }

    /**
     * Возвращает будет ли окончен срок действия прокси
     * через определнное кол-во суток ($days)
     * @param \app\models\Proxy $proxy
     * @param int $days
     * @return boolean
     */
    private function isExpiringProxyDays($proxy, $days)
    {
        $date_end = strtotime($proxy->expire_date);
        $now = strtotime(date('Y-m-d H:i:s'));

        $daysTime = 86400 * $days; // 86400 сек = 24 часа

        return (($date_end - $daysTime) - $now) > 0 ? false : true;
    }

    /**
     * Возвращает кол-во часов, через которое истечет прокси
     * @param \app\models\Proxy $proxy
     * @return int
     */
    private function getExpiringProxyHours($proxy)
    {
        $date_end = strtotime($proxy->expire_date);
        $now = strtotime(date('Y-m-d H:i:s'));

        $diff = $date_end - $now ;


        return round($diff / 3600);

    }
}
