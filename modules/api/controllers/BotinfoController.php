<?php

namespace app\modules\api\controllers;

use app\controllers\DispatchController;
use app\models\Alert;
use app\models\Bots;
use app\models\BotsAnswers;
use app\models\BotsDialogs;
use app\models\Chat;
use app\models\Companies;
use app\models\CompanyChat;
use app\models\Dispatch;
use app\models\DispatchChats;
use app\models\DispatchRegist;
use app\models\DispatchStatus;
use app\models\Message;
use app\models\Proxy;
use app\models\Questionary;
use app\models\ReferalRedirects;
use app\models\Resume;
use app\models\Settings;
use app\models\Telegram;
use app\models\Users;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\rest\ActiveController;
use yii\web\Response;
use Yii;

/**
 * Default controller for the `api` module
 */
class BotinfoController extends ActiveController
{
    public $modelClass = 'app\models\Api';

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'only' => ['bot-in','testpush','bot-update','send-refer'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }




    /**
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionBotIn()
    {

        $content = file_get_contents('php://input'); //интересная строка: означает, что мы поточно воспринимаем запрос, который к нам пришел. Подробнее после)
        $report = json_decode($content); //интересная строка: означает, что мы поточно воспринимаем запрос, который к нам пришел. Подробнее после)
        $result = json_decode($content,true); //декодируем апдейт json, пришедший с телеграмма


        // if ($result["callback_query"]) {
        //     $this->botCall($result);
        //     return true;
        // }

        $text = $result["message"]["text"]; //Текст сообщения
        $chat_id = $result["message"]["chat"]["id"]; //Уникальный идентификатор пользователя
        $username = $result["message"]["chat"]["username"]; //Уникальный идентификатор пользователя
        $name = $result["message"]["from"]["first_name"]; //Юзернейм пользователя

        // $file = file_put_contents('income.txt', $content); // создаем текстовый файл для отладки(по желанию)



        if ($text == '/start') {

            $this->getReq('sendMessage',[
                'chat_id'=> $chat_id,
                'text'=>" {$name} Привет! Меня создали для того чтобы я информарировал вас, о всех событиях
               Чтобы я начал работать введи свой код:",
                //'reply_markup' => json_encode($key),
                // 'parse_mode'=>'HTML',
            ]);

            return true;

        }


        if ($text != '/start') {
            /** @var Telegram $telegtamC */
            //проверяем может он уже подключен
            $telegtamC = Telegram::find()->where(['telegram_id' => $chat_id])->one();
            if (!$telegtamC) {
                //Проверяем подключаеться супер компания или просто пользователь
                $company = Companies::find()->where(['unique_code' => $text])->one();
                if ($company) {
                    $telegtamC = new Telegram();
                    $telegtamC->name = $name  . ' ' . $username;
                    $telegtamC->telegram_id = (string)$chat_id;
                    $telegtamC->company_id = $company->id;
                    if ($telegtamC->save()) {
                        $this->getReq('sendMessage', [
                            'chat_id' => $chat_id,
                            'text' => "{$name}, Поздравляю с успешным подключением. Жди новостей",
                            //'parse_mode'=>'HTML',
                        ]);
                        $a = "Подключен новый пользователь к боту";
                        ClientController::sendTelMessage('247187885', $a);
                        return true;
                    } else {
                        $a = serialize($telegtamC->getErrors());

                        $this->getReq('sendMessage',[
                            'chat_id'=> $chat_id,
                            'text'=>"{$name}, Что то пошло не так напишите администратору @technology_to_everyone {$a}",
                            'parse_mode'=>'HTML',
                        ]);

                        return true;
                    }
                } else {
                    $this->getReq('sendMessage',[
                        'chat_id'=> $chat_id,
                        'text'=>"{$name}, мы не нашли ваш ключ! Попробуйте еще раз",
                        //'parse_mode'=>'HTML',
                    ]);
                    return true;

                }

            } else {
                //раз он подключен значит это новое сообщение
                $chat = new CompanyChat();
                $chat->company_id = $telegtamC->company_id;
                $chat->telegram_name = $username;
                $chat->text = $text;
                if ($chat->save()) {
                    $company = Companies::findOne($telegtamC->company_id);
                    $company->new_message = 1;
                    $company->save(false);
                    $fullUrl = Url::toRoute(['/companies/view', 'id' => $company->id], true);
                    $text = "Новое сообщение от компании {$company->company_name} {$fullUrl}";
                    ClientController::sendTelMessage('247187885', $text);

                    return true;
                } else {
                    $a = serialize($chat->getErrors());
                    ClientController::sendTelMessage('247187885', " Что то пошло не так напишите администратору @technology_to_everyone {$a}");
                    return true;
                }

            }
        }
//            if (!$company) {
//                //если нету компании проверяем может подключаеться кто заполнял резюме
//                $resume = Resume::find()->where(['code' => $text])->one();
//                if (!$resume) {
//                    $resume = Resume::find()->where(['telegram_chat_id' => $chat_id])->one();
//                }
//                if (!$resume) {
//                    //если резюме тоже нет то просим ввести еще раз
//                    $this->getReq('sendMessage',[
//                        'chat_id'=> $chat_id,
//                        'text'=>"{$name}, мы не нашли ваш ключ! Попробуйте еще раз",
//                        //'parse_mode'=>'HTML',
//                    ]);
//                    return true;
//                } else {
//                    //если нашли резюме проверяем может он уже подключен
//                    if (!$resume->connect_telegram) {
//                        //если он нее подключен но был найден подключаем его
//                        $resume->telegram_chat_id = (string)$chat_id;
//                        $resume->connect_telegram = true;
//                        if (!$resume->save()) {
//                            $a = serialize($resume->getErrors());
//                            $this->sendadmin(['chat_id' => '247187885', 'parse_mode' => 'HTML', 'text' => $a]);
//                        } else {
//                            $this->getReq('sendMessage', [
//                                'chat_id' => $chat_id,
//                                'text' => "Поздравляю! Вы удачно подключили информирование. Ждите новостей!",
//                                //'parse_mode'=>'HTML',
//                            ]);
//                            $a = "Подключен новый пользователь к боту https://demo.teo-job.ru/resume/view?id={$resume->id}";
//                            $this->sendadmin(['chat_id' => '247187885', 'parse_mode' => 'HTML', 'text' => $a]);
//                            return true;
//                        }
//                    } else {
//                        //раз он подключен значит это новое сообщение
//                        $chat = new Chat();
//                        $chat->chat_id = '#resume-' . (string)$resume->id;
//                        $chat->user_id = 1;
//                        $chat->text = $text;
//                        $chat->is_read = 0;
//                        if ($chat->save()) {
//                            $resume->new_sms = 1;
//                            if (!$resume->save()) {
//                                $a = serialize($chat->getErrors());
//                                $this->sendadmin(['chat_id' => $chat_id, 'parse_mode' => 'HTML', 'text' => "{$name}, Что то пошло не так напишите администратору @technology_to_everyone {$a}"]);
//                            } else {
//                                $text = ' Новое сообщение ';
//                                $text .= " https://demo.teo-job.ru/resume/view?id={$resume->id}";
//                                $this->sendadmin(['chat_id' => '247187885', 'parse_mode' => 'HTML', 'text' => $text]);
//                            }
//                            return true;
//                        } else {
//                            $a = serialize($chat->getErrors());
//                            $this->sendadmin(['chat_id' => $chat_id, 'parse_mode' => 'HTML', 'text' => "{$name}, Что то пошло не так напишите администратору @technology_to_everyone {$a}"]);
//                            return true;
//                        }
//                    }
//                }
//            } else {
//                if (!$users->telegram_id) {
//                    $users->telegram_id = (string)$chat_id;
//                    $users->telegram_login = (string)$username;
//                    $users->connect_to_telegram = true;
//                    if ($users->save()) {
//                        $this->getReq('sendMessage',[
//                            'chat_id'=> $chat_id,
//                            'text'=>"Вашу Компанию {$users->fio}. Поздравляю с успешным подключением. Жди новостей",
//                            //'parse_mode'=>'HTML',
//                        ]);
//                        $a = "Подключен новый пользователь к боту https://demo.teo-job.ru/resume/view?id={$resume->id}";
//                        $this->sendadmin(['chat_id' => '247187885', 'parse_mode' => 'HTML', 'text' => $a]);
//                        return true;
//                    }
//                } else {
//                    $chat = new Chat();
//                    $chat->chat_id = '#users-' . (string)$users->id;
//                    $chat->user_id = 1;
//                    $chat->text = $text;
//                    $chat->is_read = 0;
//                    if ($chat->save()) {
//                        $users->new_sms = 1;
//                        if (!$users->save()) {
//                            $a = serialize($chat->getErrors());
//                            $this->sendadmin(['chat_id' => $chat_id, 'parse_mode' => 'HTML', 'text' => "{$name}, Что то пошло не так напишите администратору @technology_to_everyone {$a}"]);
//                        } else {
//                            $text = ' Новое сообщение ';
//                            $text .= " https://demo.teo-job.ru/users/view?id={$users->id}";
//                            $this->sendadmin(['chat_id' => '247187885', 'parse_mode' => 'HTML', 'text' => $text]);
//                        }
//                        return true;
//                    } else {
//                        $a = serialize($chat->getErrors());
//                        $this->sendadmin(['chat_id' => $chat_id, 'parse_mode' => 'HTML', 'text' => "{$name}, Что то пошло не так напишите администратору @technology_to_everyone {$a}"]);
//                        return true;
//                    }
//                }
//
//            }
//
//        }

        $a = serialize($telegtamC->getErrors());

        $this->getReq('sendMessage',[
            'chat_id'=> $chat_id,
            'text'=>"{$name}, Что то пошло не так напишите администратору @technology_to_everyone {$a}",
            'parse_mode'=>'HTML',
        ]);

        return true;
    }

    /**
     * @param $text
     * @return bool|string
     */
    public function Sendadmin($text)
    {


        $token = Settings::find()->where(['key' => 'telegram_bot_for_alert'])->one()->text;
        $proxy = Settings::find()->where(['key' => 'proxy'])->one()->text;

        $url =  "https://api.telegram.org/bot{$token}/sendMessage"; //основная строка и метод
        if(count($text)){
            $url=$url.'?'.http_build_query($text);//к нему мы прибавляем парметры, в виде GET-параметров
        }

        $curl = curl_init($url);    //инициализируем curl по нашему урлу

        curl_setopt($curl, CURLOPT_PROXY, $proxy);

        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);   //здесь мы говорим, чтобы запром вернул нам ответ сервера телеграмма в виде строки, нежели напрямую.
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);   //Не проверяем сертификат сервера телеграмма.
        curl_setopt($ch, CURLOPT_HEADER, 1);
        $result = curl_exec($curl);   // исполняем сессию curl
        curl_close($curl); // завершаем сессию
        if($decoded){
            return json_decode($result);// если установили, значит декодируем полученную строку json формата в объект языка PHP
        }
        return $result; //Или просто возращаем ответ в виде строки
    }


    public function actionNewresume($resume)
    {
        /** @var Resume $resum */
        $resum = Resume::find()->where(['id' => $resume])->one();
        /** @var Questionary $questionary */
        $questionary = Questionary::find()->where(['id' => $resum->questionary_id])->one();
        /** @var Users $user */
        $user = Users::find()->where(['id' => $questionary->user_id])->one();

        $text = " Анкету: {$questionary->name}\n Заполнил: {$resum->fio} \n Посмотреть анкету можно:  https://demo.teo-job.ru/resume/view?id={$resum->id}";
        if (!$user) {
            return 'Пользователь не найден!';
        }
        if (!$user->telegram_id) {
            return 'Пользователь не подключен к чату!';
        }



        return $this->getReq('sendMessage',[
            'chat_id'=> $user->telegram_id,
            'text'=> $text,
            //'parse_mode'=>'HTML',
        ]);;
    }



    public static function getReq($method,$params=[],$decoded=0){ //параметр 1 это метод, 2 - это массив параметров к методу, 3 - декодированный ли будет результат будет или нет.


        $token = Settings::findByKey('telegram_access_token')->value;
        $proxy = '186.65.114.167:9875';

        $url =  "https://api.telegram.org/bot{$token}/{$method}"; //основная строка и метод
        if(count($params)){
            $url=$url.'?'.http_build_query($params);//к нему мы прибавляем парметры, в виде GET-параметров
        }


        $curl = curl_init($url);    //инициализируем curl по нашему урлу

        curl_setopt($curl, CURLOPT_PROXY, $proxy);

        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);   //здесь мы говорим, чтобы запром вернул нам ответ сервера телеграмма в виде строки, нежели напрямую.
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);   //Не проверяем сертификат сервера телеграмма.
        curl_setopt($ch, CURLOPT_HEADER, 1);
        $result = curl_exec($curl);   // исполняем сессию curl
        curl_close($curl); // завершаем сессию
        if($decoded){
            return json_decode($result);// если установили, значит декодируем полученную строку json формата в объект языка PHP
        }
        return $result; //Или просто возращаем ответ в виде строки
    }

    public function actionWebhook()
    {
        $token = Settings::findByKey('telegram_access_token')->value;
        $proxy = '186.65.114.167:9875';

        $url = 'https://api.telegram.org/bot'.$token.'/setWebhook?url=https://like.teo-send.ru/api/botinfo/bot-in';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_PROXY, $proxy);
        //curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxyauth);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        $curl_scraped_page = curl_exec($ch);
        curl_close($ch);

        echo $curl_scraped_page;
    }

    /**
     * @param $text
     * @return bool|string
     */
    public function actionInfoall()
    {
        $allpush = Alert::find()->where(['status' => false])->all();
        /** @var Alert $push */

        foreach ($allpush as $push) {
            $allResume = Resume::find()->where(['connect_telegram' => true,'questionary_id' => $push->questionary_id])->all();
            echo "____{$push->id}";
            $text = "
Новое предложение: {$push->name}

{$push->text}";

            /** @var Resume $resume */
            $i = 0;
            foreach ($allResume as $resume) {
                $i++;
                echo $this->sendadmin(['chat_id' => $resume->telegram_chat_id, 'parse_mode'=>'HTML', 'text' => $text]);
                echo "<br/> $resume->id";
            }
            $a = "По рассылке {$push->name} отправленно {$i} кол-во пользователям";
            $a .= $text;
            $this->sendadmin(['chat_id' => '247187885', 'parse_mode' => 'HTML', 'text' => $a]);
            $push->status = true;
            $push->save();
        }
    }



    /**
     * Принимает информацию о новом реферальном посещении
     * @return array
     */
    public function actionSendRefer()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $headers = Yii::$app->response->headers;
        $headers->add('Access-Control-Allow-Origin', '*');

        $refer = new ReferalRedirects([
            'ip' => $_SERVER['REMOTE_ADDR'],
            'refer_company_id' => $_GET['ref'],
            'user_agent' => $_SERVER['HTTP_USER_AGENT'],
        ]);

        return ['result' => $refer->save()];
    }


    /**
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionGetbotdialog()//проверяем диалоги на не прочитанные сообщения
    {
        /** @var Settings $setingToken */
        /** @var Settings $setingProxy */

        //список всех рассылок с ботом
        $dispatchAll = Dispatch::find()->where(['not', ['botId' => null]])->all();
//        $dispatchAll = Dispatch::find()->where(['id' => 880])->all();

        $i = 0;
        /** @var Dispatch $dispatch */
        foreach ($dispatchAll as $dispatch) {
            $i2 = 0;
            $log[$i]['dispatch'] = $dispatch->id;
            $dispatchStatussAll = DispatchStatus::find()->where(['dispatch_id' => $dispatch->id, 'check_bot' => false])->all();
            /** @var DispatchStatus $dispatchStatus */
            foreach ($dispatchStatussAll as $dispatchStatus) {
                $i2++;
                $log[$i][$i2]['dispatch_id'] = $dispatch->id;
                $myHistory = Message::find()->where([
                    'dispatch_id' => $dispatch->id,
                    'dispatch_status_id' => $dispatchStatus->id,
                ])->orderBy(['id' => SORT_DESC])->one();
                $log[$i][$i2]['mesage'] = $myHistory->id;
                $hisCount = Message::find()->where([
                    'dispatch_id' => $dispatch->id,
                    'dispatch_status_id' => $dispatchStatus->id,
                ])->count();
                $log[$i][$i2]['count'] = $hisCount;

                /** @var BotsDialogs $botDialog */
                $botDialogAll = BotsDialogs::find()
                    ->where('botId = :id and stepNumber = :stepNumber',
                        ['id' => $dispatch->botId, 'stepNumber' => $hisCount])
                    ->all();

                $log[$i][$i2]['sql'] = BotsDialogs::find()
                    ->where('botId = :id and stepNumber = :stepNumber',
                        ['id' => $dispatch->botId, 'stepNumber' => $hisCount])
                    ->createCommand()->rawSql;
                $log[$i][$i2]['sql_count'] = count($botDialogAll);


                $account = DispatchRegist::find()->where(['id' => $dispatchStatus->send_account_id])->one();
                $proxy = Proxy::findOne(['id' => $account->proxy]);
                $send_proxy = "tcp://{$proxy->ip_adress}:{$proxy->port}";
                if ($botDialog) {
                    $dispatchStatus->check_bot = true;
                    $dispatchStatus->save(false);
                    $fullUrl = Url::toRoute(['/dispatch-status/dialog', 'id' => $dispatchStatus->id], true);
                    $text = "Бот  не нашол ответ на сообщение соответствуещего номера 
                              пользователю https://vk.com/id{$dispatchStatus->account_id}  
                              Посмотреть переписку можно тут
                              {$fullUrl}
                              ";
                    $userAll = ArrayHelper::getColumn(DispatchChats::find()->where(['dispatchId' => $dispatchStatus->dispatch_id])->all(), 'chatId');
                    ClientController::sendTelMessage($userAll, $text);
                    continue;
                }


                $bodyText = $myHistory->text;
                $log[$i]['bodyText'] = $bodyText;
                $i3 = 0;
                foreach ($botDialogAll as $botDialog) {
                    $i3++;

                    //стоит ответ на любой вопрос
                    if ($botDialog->anyAnswer) {
                        // $log[$i][$i2][$i3]['$botDialogAvto'] = 'true';

                        $result = ClientController::sendMessage($dispatchStatus->account_id, $botDialog->result, $account, $send_proxy);

                        /** @var Bots $bot */
                        if ($botDialog->status) $dispatchStatus->status = $botDialog->status;
                        $mesHistory = new Message();
                        $mesHistory->date_time = date("Y-m-d H:i:s");
                        $mesHistory->company_id = $dispatch->company_id;
                        $mesHistory->dispatch_id = $dispatch->id;
                        $mesHistory->dispatch_registr_id = $account->id;
                        $mesHistory->dispatch_status_id = $dispatchStatus->id;
                        $mesHistory->text = $botDialog->result;
                        $mesHistory->from = Message::FROM_ACCOUNT;
                        $mesHistory->save();
                        //стоит уведомление
                        if ($botDialog->notify) {
                            $dispatchStatus->check_bot = true;
                            $bot = Bots::find()->where(['id' => $botDialog->botId])->one();
                            $fullUrl = Url::toRoute(['/dispatch-status/dialog', 'id' => $dispatchStatus->id], true);
                            $text = "Бот \"{$bot->name}\" автоматически отправил сообщение \"{$botDialog->result}\" 
                              пользователю https://vk.com/id{$dispatchStatus->account_id}  
                              Посмотреть переписку можно тут
                              {$fullUrl}
                              ";
                            $userAll = ArrayHelper::getColumn(DispatchChats::find()->where(['dispatchId' => $dispatchStatus->dispatch_id])->all(), 'chatId');
                            ClientController::sendTelMessage($userAll, $text);

                        }
                        break;
                    }

                    if ($botDialog->check_search) {
                        $step = BotsAnswers::find()
                            ->where('dialogId = :dialogId and tag LIKE :bodyText',
                                ['dialogId' => $botDialog->id, 'bodyText' => "{$bodyText}"])
                            ->one();
                        $log[$i][$i2][$i3]['sql'] = BotsAnswers::find()
                            ->where('dialogId = :dialogId and tag LIKE :bodyText',
                                ['dialogId' => $botDialog->id, 'bodyText' => "{$bodyText}"])
                            ->createCommand()->rawSql;
                    } else {
                        $step = BotsAnswers::find()
                            ->where('dialogId = :dialogId and tag LIKE :bodyText',
                                ['dialogId' => $botDialog->id, 'bodyText' => "% {$bodyText} %"])
                            ->one();
                        $log[$i][$i2][$i3]['sql'] = BotsAnswers::find()
                            ->where('dialogId = :dialogId and tag LIKE :bodyText',
                                ['dialogId' => $botDialog->id, 'bodyText' => "% {$bodyText} %"])
                            ->createCommand()->rawSql;
                        if (!$step) {
                            $step = BotsAnswers::find()
                                ->where('dialogId = :dialogId and tag LIKE :bodyText',
                                    ['dialogId' => $botDialog->id, 'bodyText' => "%{$bodyText}%"])
                                ->one();
                            $log[$i][$i2][$i3]['sql2'] = BotsAnswers::find()
                                ->where('dialogId = :dialogId and tag LIKE :bodyText',
                                    ['dialogId' => $botDialog->id, 'bodyText' => "%{$bodyText}%"])
                                ->createCommand()->rawSql;
                        }

                    }
                    if ($step) break;
                }

                //нашли совпадение
                if ($step) {
                    $log[$i][$i2][$i3]['$botDialog'] = 'true';
                    //отправляем авто ответ
                    $result = ClientController::sendMessage($dispatchStatus->account_id, $botDialog->result, $account, $send_proxy);

                    if ($botDialog->status) $dispatchStatus->status = $botDialog->status;
                    $dispatchStatus->check_bot = true;


                    $mesHistory = new Message();
                    $mesHistory->date_time = date("Y-m-d H:i:s");
                    $mesHistory->company_id = $dispatch->company_id;
                    $mesHistory->dispatch_id = $dispatch->id;
                    $mesHistory->dispatch_registr_id = $account->id;
                    $mesHistory->dispatch_status_id = $dispatchStatus->id;
                    $mesHistory->text = $botDialog->result;
                    $mesHistory->from = Message::FROM_ACCOUNT;
                    $mesHistory->save();
                    if ($botDialog->notify) {
                        /** @var Bots $bot */
                        $bot = Bots::find()->where(['id' => $botDialog->botId])->one();
                        $fullUrl = Url::toRoute(['/dispatch-status/dialog', 'id' => $dispatchStatus->id], true);
                        $text = "Бот \"{$bot->name}\" автоматически отправил сообщение \"{$botDialog->result}\" 
                              пользователю https://vk.com/id{$dispatchStatus->account_id}  
                              Посмотреть переписку можно тут
                              {$fullUrl}
                              ";
                        $userAll = ArrayHelper::getColumn(DispatchChats::find()->where(['dispatchId' => $dispatchStatus->dispatch_id])->all(), 'chatId');

                        ClientController::sendTelMessage($userAll, $text);

                    }
                    //не нашли нужного ответа
                } else {

                    $dispatchStatus->check_bot = true;
                    $bot = Bots::find()->where(['id' => $botDialog->botId])->one();
                    $fullUrl = Url::toRoute(['/dispatch-status/dialog', 'id' => $dispatchStatus->id], true);
                    $text = "В рассылки \"{$dispatch->name} \" в Боте {$bot->name} нету подходящего ответа
                              Диалог с пользователем https://vk.com/id{$dispatchStatus->account_id}  
                                Посмотреть переписку можно тут  
                              {$fullUrl}
                              ";

                    $userAll = ArrayHelper::getColumn(DispatchChats::find()->where(['dispatchId' => $dispatchStatus->dispatch_id])->all(), 'chatId');
                    ClientController::sendTelMessage($userAll, $text);
                }

                $dispatchStatus->save(false);



            }

            echo "<pre>";
            print_r($log);
            echo "</pre>";
        }
    }


}
