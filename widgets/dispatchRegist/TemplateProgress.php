<?php

namespace app\widgets\dispatchRegist;

use yii\base\Widget;

/**
 * Class TemplateProgress
 * @package app\widgets\dispatchRegist
 */
class TemplateProgress extends Widget
{
    /** @var \app\models\DispatchRegist */
    public $model;

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        $progressClass = 'success';
        $count = $this->model->templateChecker->appliedCount;
        $totalCount = $this->model->templateChecker->autoRegistrsCount;

        $countPercent = round($count/($totalCount/100));

        $hasErrors = $this->model->templateChecker->hasAppliedErrors();

        if($hasErrors){
            $progressClass = 'danger';
        }

        $output = "<p ".($hasErrors ? 'class="text-danger"' : '')." style=\"margin-bottom: 5px; margin-top: 15px;\">Шаблонизация:</p><div class=\"progress progress-sm active\">
                    <div class=\"progress-bar progress-bar-{$progressClass} progress-bar-striped\" role=\"progressbar\" aria-valuenow=\"{$countPercent}\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: {$countPercent}%\">
                    <span class=\"sr-only\">100% Complete</span>
                    </div>
                </div>{$count} из {$totalCount} применено";

        if($hasErrors){
            $output .= "<br><span class=\"text-danger\">{$this->model->templateChecker->getAppliedErrorsCount()} ошибок</span>";
        }

        return $output;
    }
}