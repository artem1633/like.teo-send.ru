<?php

use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use app\models\DailyReport;
use app\models\Statuses;

$totalMessages = array_sum(ArrayHelper::getColumn($dataProvider->models, 'messages'));
$totalInterest = array_sum(ArrayHelper::getColumn($dataProvider->models, 'interest'));
$totalBuy = array_sum(ArrayHelper::getColumn($dataProvider->models, 'buy'));
$totalRefuse = array_sum(ArrayHelper::getColumn($dataProvider->models, 'refuse'));

$totalInterestPer = array_sum(ArrayHelper::getColumn($dataProvider->models, 'interest_per'));
$totalBuyPer = array_sum(ArrayHelper::getColumn($dataProvider->models, 'buy_per'));
$totalRefusePer = array_sum(ArrayHelper::getColumn($dataProvider->models, 'refuse_per'));

$columns = [
    [
        'attribute' => 'date',
        'label' => 'Дата',
        'format' => ['date', 'php:d.m.Y'],
    ],
    [
        'attribute' => 'messages',
        'label' => "Отправлено (Всего: {$totalMessages})",
    ],
    [
        'attribute' => 'interest',
        'label' => "Заинтересовались (Всего: {$totalInterest})",
    ],
    [
        'attribute' => 'interest_per',
        'label' => "Заинтересовались (%) (Всего: {$totalInterestPer}%)",
    ],
    [
        'attribute' => 'buy',
        'label' => "Купило (Всего: {$totalBuy})",
    ],
    [
        'attribute' => 'buy_per',
        'label' => "Купило (%) (Всего: {$totalBuyPer}%)",
    ],
    [
        'attribute' => 'refuse',
        'label' => "Отказалось (Всего: {$totalRefuse})",
    ],
    [
        'attribute' => 'refuse_per',
        'label' => "Отказалось (%) (Всего: {$totalRefusePer}%)",
    ],
];

?>

<?php

$gridView = ArrayHelper::merge([
    'dataProvider' => $dataProvider,
    'columns' => $columns,
], $gridViewOptions);

?>

<?=GridView::widget($gridView)?>