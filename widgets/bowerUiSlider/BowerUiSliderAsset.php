<?php

namespace app\widgets\bowerUiSlider;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 */
class BowerUiSliderAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
    ];

    public $js = [
        'js/plugins/nouislider/nouislider.js'
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
