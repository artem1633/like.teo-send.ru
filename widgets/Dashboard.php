<?php

namespace app\widgets;

use app\models\DailyReport;
use app\models\DispatchStatus;
use yii\base\InvalidConfigException;
use yii\base\Widget;

class Dashboard extends Widget
{
    /**
     * @var int
     */
    public $companyId;

    /**
     * @var int
     */
    public $dispatchId = null;

    private $report;

    private $stts;

    public function init()
    {
        parent::init();

        if($this->companyId == null){
            throw new InvalidConfigException('CompanyId обязательный параметр');
        }

        $this->report = DailyReport::getReportBySendMesasges($this->companyId, $this->dispatchId);
        $this->stts = DispatchStatus::find()->where(['company_id' => $this->companyId])->groupBy(['dispatch_id'])->orderBy('id desc');

        if($this->dispatchId != null){
            $this->stts->andWhere(['dispatch_id' => $this->dispatchId]);
        }

        $this->stts = $this->stts->all();

    }

    public function run()
    {
        return $this->render('dashboard', [
            'report' => $this->report,
            'stts' => $this->stts,
        ]);
    }
}