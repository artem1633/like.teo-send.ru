<?php

namespace app\validators;

use app\models\WordsBlackList;
use yii\validators\Validator;

/**
 * Class DeniedWordsValidator
 * @package app\validators
 */
class DeniedWordsValidator extends Validator
{
    /**
     * {@inheritdoc}
     */
    public function validateAttribute($model, $attribute)
    {
        $deniedWords = WordsBlackList::getWordsAsArray();
        $words = [];
        foreach ($deniedWords as $word)
        {
            $haystack = mb_strtolower($model->$attribute, 'UTF-8');
            $word = mb_strtolower($word, 'UTF-8');
            if(strstr($haystack, $word)){
                $words[] = $word;
            }
        }

        if(count($words) > 0)
        {
            $words = implode(', ', $words);
            $model->addError($attribute, "В строку входят следующие запрещенные слова или словосочетания: {$words}");
        }
    }
}