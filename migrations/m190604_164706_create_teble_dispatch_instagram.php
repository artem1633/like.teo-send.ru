<?php

use yii\db\Migration;

/**
 * Class m190604_164706_create_teble_dispatch_instagram
 */
class m190604_164706_create_teble_dispatch_instagram extends Migration
{
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

        $this->createTable('dispatch_instagram', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->comment('Пользователь'),
            'login' => $this->string()->comment('Логин'),
            'password' => $this->string()->comment('Пароль'),
            'account_url' => $this->string()->comment('Ссылка на аккаунт'),
            'status' => $this->string()->comment('Статус'),
            'coment' => $this->string()->comment('Комментарий'),
            'data' => $this->dateTime()->comment('Время подключение'),
            'last_dispatch_time' => $this->dateTime()->comment('Дата и время последней рассылки'),
            'proxy_id' => $this->integer()->comment('Связь с прокси'),
            'company_id' => $this->integer()->comment('Связь с компанией'),
            'all_sended_message_count' => $this->integer()->comment('Все Кол-во отправвленно '),
            'sended_message_count' => $this->integer()->comment('Кол-во отправвленно '),
            'blocking_date' => $this->dateTime()->comment('Дата блокировки'),
            'proxy_blocked' => $this->string() ->comment('Прокси заблоченый'),
        ]);


        $this->createIndex(
            'idx-dispatch_instagram-proxy_id',
            'dispatch_instagram',
            'proxy_id'
        );

        $this->addForeignKey(
            'fk-dispatch_instagram-proxy_id',
            'dispatch_instagram',
            'proxy_id',
            'proxy',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-dispatch_instagram-company_id',
            'dispatch_instagram',
            'company_id'
        );

        $this->addForeignKey(
            'fk-dispatch_instagram-company_id',
            'dispatch_instagram',
            'company_id',
            'companies',
            'id',
            'SET NULL'
        );
    }
    

    public function down()
    {
        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-dispatch_instagram-proxy_id',
            'dispatch_instagram'
        );

        // drops index for column `author_id`
        $this->dropIndex(
            'idx-dispatch_instagram-proxy_id',
            'dispatch_instagram'
        );


        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-dispatch_instagram-company_id',
            'dispatch_instagram'
        );

        // drops index for column `author_id`
        $this->dropIndex(
            'idx-dispatch_instagram-company_id',
            'dispatch_instagram'
        );
        $this->dropTable('dispatch_instagram');
    }

}
