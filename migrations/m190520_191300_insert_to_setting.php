<?php

use yii\db\Migration;

/**
 * Class m190520_191300_insert_to_setting
 */
class m190520_191300_insert_to_setting extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('settings', [
            'key' => 'group_limit_send',
            'label' => 'Лимит рассылки в группы',
            'type' => 'text',
            'value' => 38
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $setting = \app\models\Settings::findByKey('group_limit_send');
        $setting->delete();
    }
}
