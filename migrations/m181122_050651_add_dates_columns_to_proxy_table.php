<?php

use yii\db\Migration;

/**
 * Handles adding dates to table `proxy`.
 */
class m181122_050651_add_dates_columns_to_proxy_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('proxy', 'purchase_date', $this->dateTime()->comment('Дата покупки'));
        $this->addColumn('proxy', 'expire_date', $this->dateTime()->comment('Дата окончания срока действия прокси'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('proxy', 'purchase_date');
        $this->dropColumn('proxy', 'expire_date');
    }
}
