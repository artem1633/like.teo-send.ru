<?php

use yii\db\Migration;

/**
 * Class m180926_074735_change_foreiginkey_from_dispatch_id_column_in_dispatch_status_table
 */
class m180926_074735_change_foreiginkey_from_dispatch_id_column_in_dispatch_status_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey(
            'fk-dispatch_status-dispatch_id',
            'dispatch_status'
        );
        $this->addForeignKey(
            'fk-dispatch_status-dispatch_id',
            'dispatch_status',
            'dispatch_id',
            'dispatch',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180926_074735_change_foreiginkey_from_dispatch_id_column_in_dispatch_status_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180926_074735_change_foreiginkey_from_dispatch_id_column_in_dispatch_status_table cannot be reverted.\n";

        return false;
    }
    */
}
