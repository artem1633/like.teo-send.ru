<?php

use yii\db\Migration;

/**
 * Class m190531_141310_create_table_avtomation
 */
class m190531_141310_create_table_automation extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('automation', [
            'id' => $this->primaryKey(),
            'status' => $this->string(255),
            'type' => $this->string(255),
            'name' => $this->text(),
            'text' => $this->text(),
            'link' => $this->text(),
            'group_id' => $this->text(),
            'last_post_id' => $this->text(),
            'mission' => $this->integer(),
            'date_time' => $this->datetime(),
            'company_id' => $this->integer(),
        ]);
        $this->createIndex(
            'idx-automation-company_id',
            'automation',
            'company_id'
        );

        $this->addForeignKey(
            'fk-automation-company_id',
            'automation',
            'company_id',
            'companies',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

        $this->dropForeignKey(
            'fk-automation-company_id',
            'automation'
        );

        $this->dropIndex(
            'idx-automation-company_id',
            'automation'
        );

        $this->dropTable('automation');
    }
}
