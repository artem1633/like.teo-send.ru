<?php

use yii\db\Migration;

/**
 * Handles the creation of table `slider`.
 */
class m190210_141220_create_slider_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('slider', [
            'id' => $this->primaryKey(),
            'header' => $this->string()->notNull()->comment('Заголовок'),
            'content' => $this->text()->comment('Текст'),
            'image_path' => $this->string()->comment('Путь до изображения'),
            'image_alt' => $this->string()->comment('Alt атрибут изображения'),
            'sort' => $this->integer()->comment('Порядок показа'),
            'enable' => $this->boolean()->defaultValue(true)->comment('Вкл/Выкл'),
            'created_at' => $this->dateTime(),
        ]);
        $this->addCommentOnTable('slider', 'Содержимое слайдера на странице входа');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('slider');
    }
}
