<?php

use yii\db\Migration;

/**
 * Handles the creation of table `daily_report`.
 */
class m180904_175554_create_daily_report_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        $this->createTable('daily_report', [
            'id' => $this->primaryKey(),
            'date_event' =>  'timestamp DEFAULT NOW() COMMENT "Дата и время события"',
            'company_id' => $this->integer()->notNull()->comment('ID компании'),
            'sended_message_count' => $this->integer()->notNull()->defaultValue(0)->comment('Кол-во отправленных сообщений'),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('daily_report');
    }
}
