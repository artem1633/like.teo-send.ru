<?php

use yii\db\Migration;

/**
 * Handles the creation of table `bots_dialogs`.
 */
class m180828_100431_create_bots_dialogs_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        $this->createTable('bots_dialogs', [
            'id' => $this->primaryKey(),
            'botId' => $this->integer()->notNull()->comment('ID бота'),
            'stepNumber' => $this->integer()->notNull()->comment('Номер шага'),
            'answerSet' => $this->integer()->notNull()->comment('Oтвет пользователя'),
            'result' => $this->string(250)->notNull()->comment('Наш ответ'),
            'notify' => $this->integer()->notNull()->comment('Уведомление'),
        ], $tableOptions);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('bots_dialogs');
    }
}
