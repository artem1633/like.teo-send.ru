<?php

use yii\db\Migration;

/**
 * Handles the creation of table `messages_statistics`.
 */
class m190121_173445_create_messages_statistics_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('messages_statistics', [
            'id' => $this->primaryKey(),
            'content' => $this->text()->comment('Сообщение'),
            'dispatch_id' => $this->integer()->comment('Рассылка'),
            'dispatch_regist_id' => $this->integer()->comment('Аккаунт'),
            'company_id' => $this->integer()->comment('Компания'),
            'created_at' => $this->dateTime(),
        ]);
        $this->addCommentOnTable('messages_statistics','Статистика отправки сообщений');

        $this->createIndex(
            'idx-messages_statistics-dispatch_id',
            'messages_statistics',
            'dispatch_id'
        );

        $this->addForeignKey(
            'fk-messages_statistics-dispatch_id',
            'messages_statistics',
            'dispatch_id',
            'dispatch',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-messages_statistics-dispatch_regist_id',
            'messages_statistics',
            'dispatch_regist_id'
        );

        $this->addForeignKey(
            'fk-messages_statistics-dispatch_regist_id',
            'messages_statistics',
            'dispatch_regist_id',
            'dispatch_regist',
            'id',
            'SET NULL'
        );
        
        $this->createIndex(
            'idx-messages_statistics-company_id',
            'messages_statistics',
            'company_id'
        );

        $this->addForeignKey(
            'fk-messages_statistics-company_id',
            'messages_statistics',
            'company_id',
            'companies',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-messages_statistics-dispatch_id',
            'messages_statistics'
        );

        $this->dropIndex(
            'idx-messages_statistics-dispatch_id',
            'messages_statistics'
        );

        $this->dropForeignKey(
            'fk-messages_statistics-dispatch_regist_id',
            'messages_statistics'
        );

        $this->dropIndex(
            'idx-messages_statistics-dispatch_regist_id',
            'messages_statistics'
        );

        $this->dropForeignKey(
            'fk-messages_statistics-company_id',
            'messages_statistics'
        );

        $this->dropIndex(
            'idx-messages_statistics-company_id',
            'messages_statistics'
        );

        $this->dropTable('messages_statistics');
    }
}
