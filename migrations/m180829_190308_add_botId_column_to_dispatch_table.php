<?php

use yii\db\Migration,
    app\models\Dispatch;

/**
 * Handles adding botId to table `dispatch`.
 */
class m180829_190308_add_botId_column_to_dispatch_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(Dispatch::tableName(), 'autoAnswer', $this->integer()->null()->defaultValue(0)->comment('Авто ответ'));
        $this->addColumn(Dispatch::tableName(), 'botId', $this->integer()->null()->defaultValue(0)->comment('ID Бота'));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
