<?php

use yii\db\Migration;

/**
 * Class m180911_172441_add_column_to_stat_to_dispatch_table
 */
class m180911_172441_add_column_to_stat_to_dispatch_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(\app\models\Dispatch::tableName(), 'to_stat', $this->boolean()->notNull()->defaultValue(0)->comment('Отоброжать в показателях'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180911_172441_add_column_to_stat_to_dispatch_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180911_172441_add_column_to_stat_to_dispatch_table cannot be reverted.\n";

        return false;
    }
    */
}
