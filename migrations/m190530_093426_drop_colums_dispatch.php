<?php

use yii\db\Migration;

/**
 * Class m190530_093426_drop_colums_dispatch
 */
class m190530_093426_drop_colums_dispatch extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {


        $this->dropForeignKey(
            'fk-dispatch-auto_regists_template_id',
            'dispatch'
        );

        $this->dropIndex(
            'idx-dispatch-auto_regists_template_id',
            'dispatch'
        );
        $this->dropColumn('dispatch', 'text1');
        $this->dropColumn('dispatch', 'text2');
        $this->dropColumn('dispatch', 'text3');
        $this->dropColumn('dispatch', 'text4');
        $this->dropColumn('dispatch', 'botId');
        $this->dropColumn('dispatch', 'auto_regists_template_id');
        $this->dropColumn('dispatch', 'use_auto_registr');
        $this->dropColumn('dispatch', 'autoAnswer');
        $this->dropColumn('dispatch', 'type');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('dispatch', 'text1', $this->text());
        $this->addColumn('dispatch', 'text2', $this->text());
        $this->addColumn('dispatch', 'text3', $this->text());
        $this->addColumn('dispatch', 'text4', $this->text());
        $this->addColumn('dispatch', 'use_auto_registr', $this->boolean());
        $this->addColumn('dispatch', 'type', $this->boolean());
        $this->addColumn('dispatch', 'botId', $this->integer());
        $this->addColumn('dispatch', 'autoAnswer', $this->integer());
        $this->addColumn('dispatch', 'auto_regists_template_id', $this->integer());

        $this->createIndex(
            'idx-dispatch-auto_regists_template_id',
            'dispatch',
            'auto_regists_template_id'
        );

        $this->addForeignKey(
            'fk-dispatch-auto_regists_template_id',
            'dispatch',
            'auto_regists_template_id',
            'auto_regists_templates',
            'id',
            'SET NULL'
        );
    }

}
