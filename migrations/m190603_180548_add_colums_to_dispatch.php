<?php

use yii\db\Migration;

/**
 * Class m190603_180548_add_colums_to_dispatch
 */
class m190603_180548_add_colums_to_dispatch extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('dispatch', 'distribute', $this->boolean());
        $this->addColumn('dispatch', 'distribute_count', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('dispatch', 'distribute');
        $this->dropColumn('dispatch', 'distribute_count');
    }
}
