<?php

use yii\db\Migration;

/**
 * Class m180913_184718_add_column_to_message
 */
class m180913_184718_add_column_to_message extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('message', 'text', $this->text()->comment('Текст'));
        $this->execute('ALTER TABLE `message` CHANGE `text` `text` BLOB NULL DEFAULT NULL');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180913_184718_add_column_to_message cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180913_184718_add_column_to_message cannot be reverted.\n";

        return false;
    }
    */
}
