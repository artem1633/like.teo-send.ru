<?php

use yii\db\Migration;

/**
 * Class m181221_060718_add_blocked_proxy_to_dispatch_regists_table
 */
class m181220_180718_add_blocked_proxy_to_dispatch_regists_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('dispatch_regist', 'proxy_blocked', $this->integer()->comment('Прокси при котором был заблокирован аккаунт'));

        $this->createIndex(
            'idx-dispatch_regist-proxy_blocked',
            'dispatch_regist',
            'proxy_blocked'
        );

        $this->addForeignKey(
            'fk-dispatch_regist-proxy_blocked',
            'dispatch_regist',
            'proxy_blocked',
            'proxy',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-dispatch_regist-proxy_blocked',
            'dispatch_regist'
        );

        $this->dropIndex(
            'idx-dispatch_regist-proxy_blocked',
            'dispatch_regist'
        );

        $this->dropColumn('dispatch_regist', 'proxy_blocked');
    }
}
