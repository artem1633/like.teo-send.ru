<?php

use yii\db\Migration;

/**
 * Class m180906_071246_add_column_send_to_dispatch_status_table
 */
class m180906_071246_add_column_send_to_dispatch_status_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(\app\models\DispatchStatus::tableName(), 'send', $this->boolean()->notNull()->defaultValue(0)->comment('Отправлено или нет сообщение'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180906_071246_add_column_send_to_dispatch_status_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180906_071246_add_column_send_to_dispatch_status_table cannot be reverted.\n";

        return false;
    }
    */
}
