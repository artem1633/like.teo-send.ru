<?php

use yii\db\Migration;

/**
 * Handles the creation of table `accounting_report`.
 */
class m180911_080346_create_accounting_report_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        $this->createTable('accounting_report', [
            'id' => $this->primaryKey(),
            'date_report' =>  'timestamp DEFAULT NOW() COMMENT "Дата и время события"',
            'company_id' => $this->integer()->notNull()->comment('ID компании'),
            'operation_type' => $this->integer()->notNull()->comment('Тип операции'),
            'amount' => $this->decimal(10,2)->notNull()->comment('Сумма оперции'),
            'description' => $this->string()->null()->comment('Описание операции'),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('accounting_report');
    }
}
