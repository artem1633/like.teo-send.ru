<?php

use yii\db\Migration;

/**
 * Handles adding auto_applied_count to table `dispatch_regist`.
 */
class m181220_134556_add_auto_applied_count_column_to_dispatch_regist_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('dispatch_regist', 'auto_applied_count', $this->integer()->notNull()->unsigned()->defaultValue(0)->comment('Кол-во примененых записей шаблона'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('dispatch_regist', 'auto_applied_count');
    }
}
