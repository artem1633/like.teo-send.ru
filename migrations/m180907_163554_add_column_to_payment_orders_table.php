<?php

use yii\db\Migration;

/**
 * Class m180907_163554_add_column_to_payment_orders_table
 */
class m180907_163554_add_column_to_payment_orders_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(\app\models\PaymentOrders::tableName(), 'company_id', $this->integer()->notNull()->comment('ID Компании'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180907_163554_add_column_to_payment_orders_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180907_163554_add_column_to_payment_orders_table cannot be reverted.\n";

        return false;
    }
    */
}
