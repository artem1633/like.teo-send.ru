<?php

use yii\db\Migration;

/**
 * Handles the creation of table `company_chat`.
 */
class m190414_205347_create_company_chat_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('company_chat', [
            'id' => $this->primaryKey(),
            'chat_id' => $this->string(255),
            'user_id' => $this->integer(),
            'text' => $this->text(),
            'date_time' => $this->datetime(),
            'is_read' => $this->boolean(),
        ]);

        $this->createIndex('idx-chat-user_id', 'company_chat', 'user_id', false);
        $this->addForeignKey("fk-chat-user_id", "company_chat", "user_id", "users", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-chat-user_id','company_chat');
        $this->dropIndex('idx-chat-user_id','company_chat');

        $this->dropTable('company_chat');
    }
}
