<?php

use yii\db\Migration;

/**
 * Class m190414_233403_add_telegram_id_companies_chat
 */
class m190414_233403_add_telegram_id_companies_chat extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('company_chat', 'telegram_name', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

        $this->dropColumn('company_chat','telegram_name');
    }
}
