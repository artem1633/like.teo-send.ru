<?php

use yii\db\Migration;

/**
 * Class m180902_112237_add_columns_to_dispatch_table
 */
class m180902_112237_add_columns_to_dispatch_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('dispatch', 'days_week',  $this->string(50)->null()->comment('Дни недели'));
        $this->addColumn('dispatch', 'start_time',  $this->time()->null()->comment('Время начала'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180902_112237_add_columns_to_dispatch_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180902_112237_add_columns_to_dispatch_table cannot be reverted.\n";

        return false;
    }
    */
}
