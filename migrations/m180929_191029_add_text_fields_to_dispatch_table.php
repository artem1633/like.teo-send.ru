<?php

use yii\db\Migration;

/**
 * Class m180929_191029_add_text_fields_to_dispatch_table
 */
class m180929_191029_add_text_fields_to_dispatch_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameColumn('dispatch', 'text', 'text1');
        $this->addColumn('dispatch', 'text2', $this->binary()->comment('Текст сообщения'));
        $this->addColumn('dispatch', 'text3', $this->binary()->comment('Текст сообщения'));
        $this->addColumn('dispatch', 'text4', $this->binary()->comment('Текст сообщения'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->renameColumn('dispatch', 'text1', 'text');
        $this->dropColumn('dispatch', 'text2');
        $this->dropColumn('dispatch', 'text3');
        $this->dropColumn('dispatch', 'text4');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180929_191029_add_text_fields_to_dispatch_table cannot be reverted.\n";

        return false;
    }
    */
}
