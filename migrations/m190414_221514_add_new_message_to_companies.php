<?php

use yii\db\Migration;

/**
 * Class m190414_221514_add_new_message_to_companies
 */
class m190414_221514_add_new_message_to_companies extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('companies', 'new_message', $this->boolean()->defaultValue(false));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('companies', 'new_message');
    }
}
