<?php

use yii\db\Migration;

/**
 * Class m180618_102931_add_columns_to_users_table
 */
class m180618_102931_add_columns_to_users_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('users', 'company_id', $this->integer()->comment('Компания, к которой принадлежит запись'));
        $this->addColumn('users', 'last_activity_datetime', $this->dateTime()->comment('Дата и время последней активности'));
        $this->addColumn('users', 'vk_id', $this->string()->comment('Ссылка на страницу ВКонтакте'));
    }

    public function down()
    {        
        $this->dropColumn('users', 'company_id');
        $this->dropColumn('users', 'last_activity_datetime');
        $this->dropColumn('users', 'vk_id');
    }
}
