<?php

use yii\db\Migration;

/**
 * Class m180906_073411_add_column_new_message_to_dispatch_status_table
 */
class m180906_073411_add_column_new_message_to_dispatch_status_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(\app\models\DispatchStatus::tableName(), 'new_message', $this->boolean()->notNull()->defaultValue(0)->comment('Новое сообщение'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180906_073411_add_column_new_message_to_dispatch_status_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180906_073411_add_column_new_message_to_dispatch_status_table cannot be reverted.\n";

        return false;
    }
    */
}
