<?php

use yii\db\Migration;

/**
 * Class m180806_132535_add_new_colums_to_table_rates
 */
class m180806_132535_add_new_colums_to_table_rates extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('rates', 'account_count', $this->integer()->comment('Количество Аккаунтов'));
        $this->addColumn('rates', 'dispatch_count', $this->integer()->comment('Количество Рассылок'));

    }

    public function down()
    {
        $this->dropColumn('rates', 'account_count');
        $this->dropColumn('rates', 'dispatch_count');

    }
}
