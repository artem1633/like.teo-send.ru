<?php

use yii\db\Migration;

/**
 * Class m180805_142822_add_column_table_dispatch_regist
 */
class m180805_142822_add_column_table_dispatch_regist extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('dispatch_regist', 'unread_message', $this->integer()->comment('Кол во непрочитанных сообщении'));

    }

    public function down()
    {
        $this->dropColumn('dispatch_regist', 'unread_message');


    }
}
