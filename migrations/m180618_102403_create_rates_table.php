<?php

use yii\db\Migration;

/**
 * Handles the creation of table `rates`.
 */
class m180618_102403_create_rates_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('rates', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()->comment('Наименование'),
            'price' => $this->float()->comment('Стоимость тарифа'),
            'time' => $this->float()->comment('Период действия лицензии с момента ее получения (в секундах)'),
            'sort' => $this->float()->comment('Сортировка'),
        ]);


        
        $this->addCommentOnTable('rates', 'Тарифы лицензий для компаний');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('rates');
    }
}
