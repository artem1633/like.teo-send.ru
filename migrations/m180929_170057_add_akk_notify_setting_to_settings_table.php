<?php

use yii\db\Migration;

/**
 * Handles adding akk_notify to table `settings`.
 */
class m180929_170057_add_akk_notify_setting_to_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('settings', [
            'key' => 'akk_notify',
            'label' => 'ID аккаунта вконтакте для получения уведомлений',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo 'm180929_170057_add_akk_notify_setting_to_settings_table cannot be reverted.\n';
        return false;
    }
}
