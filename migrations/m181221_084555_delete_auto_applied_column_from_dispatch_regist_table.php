<?php

use yii\db\Migration;

/**
 * Class m181221_084555_delete_auto_applied_column_from_dispatch_regist_table
 */
class m181221_084555_delete_auto_applied_column_from_dispatch_regist_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('dispatch_regist', 'auto_applied_count');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('dispatch_regist', 'auto_applied_count', $this->integer()->notNull()->unsigned()->defaultValue(0)->comment('Кол-во примененых записей шаблона'));
    }
}
