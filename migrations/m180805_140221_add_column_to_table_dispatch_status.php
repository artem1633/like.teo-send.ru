<?php

use yii\db\Migration;

/**
 * Class m180805_140221_add_column_to_table_dispatch_status
 */
class m180805_140221_add_column_to_table_dispatch_status extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('dispatch_status', 'response_id', $this->string()->comment('Ид получан через json'));


    }

    public function down()
    {
        $this->dropColumn('dispatch_status', 'response_id');


    }
}
