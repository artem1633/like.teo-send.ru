<?php

use yii\db\Migration;

/**
 * Handles the creation of table `dispatch_chats`.
 */
class m180902_083122_create_dispatchChats_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        $this->createTable('dispatch_chats', [
            'id' => $this->primaryKey(),
            'dispatchId' => $this->integer()->notNull()->comment('ID рассылки'),
            'chatId' => $this->integer()->notNull()->comment('ID чата'),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('dispatch_chats');
    }
}
