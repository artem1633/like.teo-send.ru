<?php

use yii\db\Migration;

/**
 * Class m190123_112953_update_reload_time
 */
class m190123_112953_update_reload_time extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $companies = \app\models\Companies::find()->all();

        foreach ($companies as $company)
        {
            $rate = \app\models\Rates::findOne($company->rate_id);
            $setting = \app\models\CompanySettings::findOne(['company_id' => $company->id]);

            if($setting == null)
                continue;

            if($setting->reload_time != null)
                continue;

            if($rate != null && $rate->reload_time != null){
                $setting->reload_time = $rate->reload_time;
                $setting->save(false);
            } else {
                $setting->reload_time = 24;
                $setting->save(false);
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
