<?php

use yii\db\Migration;

/**
 * Class m190530_094254_add_colum_dispatch
 */
class m190530_094254_add_colum_dispatch extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('dispatch', 'status_job', $this->string(255));
        $this->addColumn('dispatch', 'type', $this->string(255));
        $this->addColumn('dispatch', 'link', $this->text());
        $this->addColumn('dispatch', 'mission', $this->integer());
        $this->addColumn('dispatch', 'action_mission', $this->integer());
        $this->addColumn('dispatch', 'all_action_mission', $this->integer());
        $this->addColumn('dispatch', 'last_mission_time', $this->dateTime());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('dispatch', 'status_job');
        $this->dropColumn('dispatch', 'type');
        $this->dropColumn('dispatch', 'link');
        $this->dropColumn('dispatch', 'mission');
        $this->dropColumn('dispatch', 'action_mission');
        $this->dropColumn('dispatch', 'all_action_mission');
        $this->dropColumn('dispatch', 'last_mission_time');
    }
}
