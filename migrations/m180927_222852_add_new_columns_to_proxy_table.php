<?php

use yii\db\Migration;

/**
 * Handles adding new to table `proxy`.
 */
class m180927_222852_add_new_columns_to_proxy_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('proxy', 'login', $this->string()->comment('Логин'));
        $this->addColumn('proxy', 'password', $this->string()->comment('Пароль'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180927_222852_add_new_columns_to_proxy_table cannot be reverted.\n";

        return false;
    }
}
