<?php

use yii\db\Migration;

/**
 * Handles adding comment to table `disptch_regist`.
 */
class m180925_185928_add_comment_column_to_disptch_regist_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('dispatch_regist', 'note', $this->text()->comment('Комментарий'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('dispatch_regist', 'note');
    }
}
