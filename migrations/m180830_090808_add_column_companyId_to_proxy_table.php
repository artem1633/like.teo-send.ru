<?php

use yii\db\Migration,
    app\models\Proxy;

/**
 * Class m180830_090808_add_column_companyId_to_proxy_table
 */
class m180830_090808_add_column_companyId_to_proxy_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(Proxy::tableName(), 'companyId', $this->integer()->null()->defaultValue(0)->comment('ID компании'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180830_090808_add_column_companyId_to_proxy_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180830_090808_add_column_companyId_to_proxy_table cannot be reverted.\n";

        return false;
    }
    */
}
