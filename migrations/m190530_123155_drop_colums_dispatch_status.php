<?php

use yii\db\Migration;

/**
 * Class m190530_123155_drop_colums_dispatch_status
 */
class m190530_123155_drop_colums_dispatch_status extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->dropForeignKey(
            'fk-dispatch_status-unique_message_id',
            'dispatch_status'
        );

        $this->dropIndex(
            'idx-dispatch_status-unique_message_id',
            'dispatch_status'
        );

        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-dispatch_status-send_account_id',
            'dispatch_status'
        );

        // drops index for column `author_id`
        $this->dropIndex(
            'idx-dispatch_status-send_account_id',
            'dispatch_status'
        );


        $this->dropColumn('dispatch_status', 'check_bot');
        $this->dropColumn('dispatch_status', 'unique_message_id');
        $this->dropColumn('dispatch_status', 'default_account_id');
        $this->dropColumn('dispatch_status', 'city');
        $this->dropColumn('dispatch_status', 'gender');
        $this->dropColumn('dispatch_status', 'age');
        $this->dropColumn('dispatch_status', 'new_message');
        $this->dropColumn('dispatch_status', 'send');
        $this->dropColumn('dispatch_status', 'status_push_sales');
        $this->dropColumn('dispatch_status', 'read');
        $this->dropColumn('dispatch_status', 'last_message');
        $this->dropColumn('dispatch_status', 'response_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('dispatch_status', 'check_bot', $this->boolean());
        $this->addColumn('dispatch_status', 'new_message', $this->boolean());
        $this->addColumn('dispatch_status', 'send', $this->boolean());
        $this->addColumn('dispatch_status', 'read', $this->boolean());
        $this->addColumn('dispatch_status', 'status_push_sales', $this->boolean());
        $this->addColumn('dispatch_status', 'unique_message_id', $this->integer());
        $this->addColumn('dispatch_status', 'default_account_id', $this->string(255));
        $this->addColumn('dispatch_status', 'city', $this->string(255));
        $this->addColumn('dispatch_status', 'gender', $this->string(255));
        $this->addColumn('dispatch_status', 'age', $this->string(255));
        $this->addColumn('dispatch_status', 'last_message', $this->string(255));
        $this->addColumn('dispatch_status', 'response_id', $this->string(255));

        $this->createIndex(
            'idx-dispatch_status-unique_message_id',
            'dispatch_status',
            'unique_message_id'
        );

        $this->addForeignKey(
            'fk-dispatch_status-unique_message_id',
            'dispatch_status',
            'unique_message_id',
            'unique_message',
            'id',
            'SET NULL'
        );

        // creates index for column `author_id`
        $this->createIndex(
            'idx-dispatch_status-send_account_id',
            'dispatch_status',
            'send_account_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-dispatch_status-send_account_id',
            'dispatch_status',
            'send_account_id',
            'dispatch_regist',
            'id',
            'CASCADE'
        );
    }
}
