<?php

use yii\db\Migration;

/**
 * Handles adding photo to table `dispatch_regist`.
 */
class m180921_112251_add_photo_column_to_dispatch_regist_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('dispatch_regist', 'photo', $this->text()->comment('URL фотографии'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('dispatch_regist','photo');
        return true;
    }
}
