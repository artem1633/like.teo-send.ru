<?php

use yii\db\Migration;

/**
 * Class m190601_191005_addd_colums_dispatch
 */
class m190601_191005_addd_colums_dispatch extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('dispatch', 'platform', $this->string(255));
        $this->addColumn('dispatch', 'login', $this->string(255));
        $this->addColumn('dispatch', 'pass', $this->string(255));
        $this->addColumn('dispatch', 'proxy_id', $this->integer());

        $this->createIndex(
            'idx-dispatch-proxy_id',
            'dispatch',
            'proxy_id'
        );

        $this->addForeignKey(
            'fk-dispatch-proxy_id',
            'dispatch',
            'proxy_id',
            'proxy',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-dispatch-proxy_id',
            'dispatch'
        );

        // drops index for column `author_id`
        $this->dropIndex(
            'idx-dispatch-proxy_id',
            'dispatch'
        );
        $this->dropColumn('dispatch', 'platform');
        $this->dropColumn('dispatch', 'login');
        $this->dropColumn('dispatch', 'pass');
        $this->dropColumn('dispatch', 'proxy_id');
    }


}
