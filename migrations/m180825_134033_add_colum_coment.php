<?php

use yii\db\Migration;

/**
 * Class m180825_134033_add_colum_coment
 */
class m180825_134033_add_colum_coment extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180825_134033_add_colum_coment cannot be reverted.\n";

        return false;
    }

    public function up()
    {

        $this->addColumn('dispatch_regist','coment',  $this->string()->comment('Комментарий ошибки'));

    }

    public function down()
    {
        $this->dropColumn('dispatch_regist','coment');
    }
}
