<?php

use yii\db\Migration;

/**
 * Class m190222_074544_new_colums_regist
 */
class m190222_074544_new_colums_regist extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('dispatch_regist', 'status_read', $this->boolean()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
