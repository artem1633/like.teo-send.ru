<?php

use yii\db\Migration;

/**
 * Class m181211_223939_add_new_admin_setting
 */
class m181211_223939_add_new_admin_setting extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('settings', [
            'key' => 'sleeping_interval',
            'value' => '5',
            'label' => 'Спящий интервал',
            'type' => \app\models\Settings::TYPE_TEXT,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
