<?php

use yii\db\Migration;

/**
 * Class m180806_103205_add_column_dispatch_status
 */
class m180806_103205_add_column_dispatch_status extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('dispatch_status', 'last_message', $this->text()->comment('Последнее сообщение'));

    }

    public function down()
    {
        $this->dropColumn('dispatch_status', 'last_message');

    }
}
