<?php

use yii\db\Migration;

/**
 * Class m180910_102216_add_column_to_rates_table
 */
class m180910_102216_add_column_to_rates_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(\app\models\Rates::tableName(), 'price_message', $this->decimal(10,2)->null()->after('price')->comment('Цена за сообщение'));
        $this->addColumn(\app\models\CompanySettings::tableName(), 'price_message', $this->decimal(10,2)->null()->comment('Цена за сообщение'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180910_102216_add_column_to_rates_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180910_102216_add_column_to_rates_table cannot be reverted.\n";

        return false;
    }
    */
}
