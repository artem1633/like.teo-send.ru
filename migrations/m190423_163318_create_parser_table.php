<?php

use yii\db\Migration;

/**
 * Handles the creation of table `parser`.
 */
class m190423_163318_create_parser_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('parser', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Название'),
            'type' => $this->integer()->comment('Тип'),
            'status' => $this->integer()->comment('Статус'),
            'list' => $this->text()->comment('Список'),
            'link' => $this->string()->comment('Ссылка'),
            'progress' => $this->integer()->comment('Прогрес'),
            'count' => $this->integer()->comment('Кол-во'),
            'sex' => $this->boolean()->comment('Пол'),
            'age_start' => $this->integer()->unsigned()->comment('Возраст (Начальный)'),
            'age_end' => $this->integer()->unsigned()->comment('Возраст (Конечный)'),
            'location' => $this->string()->comment('Месторасположение'),
            'online_period_start' => $this->dateTime()->comment('Пользователь в сети (Старт)'),
            'online_period_end' => $this->dateTime()->comment('Пользователь в сети (Конец)'),
            'company_id' => $this->integer()->comment('Компания'),
            'created_at' => $this->dateTime(),
        ]);

        $this->createIndex(
            'idx-parser-company_id',
            'parser',
            'company_id'
        );

        $this->addForeignKey(
            'fk-parser-company_id',
            'parser',
            'company_id',
            'companies',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-parser-company_id',
            'parser'
        );

        $this->dropIndex(
            'idx-parser-company_id',
            'parser'
        );

        $this->dropTable('parser');
    }
}
