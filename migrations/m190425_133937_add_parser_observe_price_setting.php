<?php

use yii\db\Migration;

/**
 * Class m190425_133937_add_parser_observe_price_setting
 */
class m190425_133937_add_parser_observe_price_setting extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('settings', [
            'key' => 'parser_observe_price',
            'label' => 'Стоимость парсера за отслеживание',
            'value' => 0
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $setting = \app\models\Settings::findByKey('parser_observe_price');
        $setting->delete();
    }
}
