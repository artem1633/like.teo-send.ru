<?php

use yii\db\Migration;

/**
 * Handles adding messages_read_status to table `users`.
 */
class m190416_193149_add_messages_read_status_column_to_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('users', 'messages_read_status', $this->boolean()->defaultValue(false)->comment('Прочитал ли пользователь последнюю новость (Запись из таблицы "global_messages")'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('users', 'messages_read_status');
    }
}
