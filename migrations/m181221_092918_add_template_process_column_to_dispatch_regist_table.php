<?php

use yii\db\Migration;

/**
 * Handles adding template_process to table `dispatch_regist`.
 */
class m181221_092918_add_template_process_column_to_dispatch_regist_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('dispatch_regist', 'template_process', $this->boolean()->defaultValue(0)->comment('Идет ли процесс шаблонизации'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('dispatch_regist', 'template_process');
    }
}
