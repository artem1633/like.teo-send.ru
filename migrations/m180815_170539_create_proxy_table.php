<?php

use yii\db\Migration;

/**
 * Handles the creation of table `proxy`.
 */
class m180815_170539_create_proxy_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function Up()
    {
        $this->addColumn('company_settings', 'proxy_id', $this->integer(11));
        $this->addCommentOnColumn('company_settings', 'proxy_id', 'Код настроек прокси');

        $this->createTable('proxy', [
            'id' => $this->primaryKey(),
            'name' => $this->string('255')->comment('Наименование'),
            'ip_adress' => $this->string('15')->comment('ip адрес'),
            'port' => $this->integer('5')->comment('порт'),
        ]);

        $this->addForeignKey(
            'fk_company-settings_proxy',
            'company_settings',
            'proxy_id',
            'proxy',
            'id',
            'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function Down()
    {
        $this->dropForeignKey('fk_company-settings_proxy', 'company_settings');
        $this->dropTable('proxy');
        $this->dropColumn('company_settings', 'proxy_id');
    }
}
