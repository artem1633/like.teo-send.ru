<?php

use yii\db\Migration;

/**
 * Handles adding client_id to table `automation`.
 */
class m190705_162725_add_client_id_column_to_automation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('automation', 'client_id', $this->integer()->after('id')->comment('Клиент'));

        $this->createIndex(
            'idx-automation-client_id',
            'automation',
            'client_id'
        );

        $this->addForeignKey(
            'fk-automation-client_id',
            'automation',
            'client_id',
            'clients',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-automation-client_id',
            'automation'
        );

        $this->dropIndex(
            'idx-automation-client_id',
            'automation'
        );

        $this->dropColumn('automation', 'client_id');
    }
}
