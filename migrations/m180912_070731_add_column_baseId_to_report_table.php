<?php

use yii\db\Migration;

/**
 * Class m180912_070731_add_column_baseId_to_report_table
 */
class m180912_070731_add_column_baseId_to_report_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(\app\models\DailyReport::tableName(), 'base_id', $this->integer()->null()->comment('Раccылка была по базе'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180912_070731_add_column_baseId_to_report_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180912_070731_add_column_baseId_to_report_table cannot be reverted.\n";

        return false;
    }
    */
}
