<?php

use yii\db\Migration;

/**
 * Handles dropping description from table `template_messages`.
 */
class m190121_163421_drop_description_column_from_template_messages_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('template_messages', 'description');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('template_messages', 'description', $this->text()->comment('Описание'));
    }
}
