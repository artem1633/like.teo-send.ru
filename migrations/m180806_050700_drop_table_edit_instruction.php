<?php

use yii\db\Migration;

/**
 * Class m180806_050700_drop_table_edit_instruction
 */
class m180806_050700_drop_table_edit_instruction extends Migration
{



    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->dropTable('edit_instruction');
    }

    public function down()
    {
        echo "m180806_050700_drop_table_edit_instruction cannot be reverted.\n";

        return false;
    }

}
