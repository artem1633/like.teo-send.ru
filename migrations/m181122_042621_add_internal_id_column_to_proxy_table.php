<?php

use yii\db\Migration;

/**
 * Handles adding internal_id to table `proxy`.
 */
class m181122_042621_add_internal_id_column_to_proxy_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('proxy', 'internal_id', $this->string()->after('id')->comment('Внутренний id в системе API proxy6.net'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('proxy', 'internal_id');
    }
}
