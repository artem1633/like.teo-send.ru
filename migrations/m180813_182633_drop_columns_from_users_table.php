<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `columns_from_users`.
 */
class m180813_182633_drop_columns_from_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function Up()
    {
        $this->dropColumn('users', 'access_for_moving');
        $this->dropForeignKey('fk-users-atelier_id', 'users');
        $this->dropColumn('users', 'atelier_id');
        $this->dropColumn('users', 'sales_percent');
        $this->dropColumn('users', 'cleaner_percent');
        $this->dropColumn('users', 'sewing_percent');
        $this->dropColumn('users', 'repairs_percent');
    }

    /**
     * {@inheritdoc}
     */
    public function Down()
    {
        echo "m180813_182633_drop_columns_from_users_table cannot be reverted.\n";

        return false;

    }
}
