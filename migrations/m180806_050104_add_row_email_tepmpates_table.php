<?php

use yii\db\Migration;

/**
 * Class m180806_050104_add_row_email_tepmpates_table
 */
class m180806_050104_add_row_email_tepmpates_table extends Migration
{



    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->insert(
            'email_templates',
            [
                'key' => 'instruction',
                'key_ru' => 'Инструкция',
                'body' => 'Настривайте инструкция сдесь',
                'deletable' => false,
            ]
        );
    }

    public function down()
    {
        echo "m180806_050104_add_row_email_tepmpates_table cannot be reverted.\n";

        return false;
    }
}
