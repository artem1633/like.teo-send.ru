<?php

use yii\db\Migration;

/**
 * Class m180804_070020_add_rows_to_table_settings
 */
class m180804_070020_add_rows_to_table_settings extends Migration
{
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->insert('settings',array(
            'key' => 'app_id',
            'value' => '10',
            'label' =>'Поля id приложения',

        ));

        $this->insert('settings',array(
            'key' => 'scope',
            'value' => '10',
            'label' =>'список параметров',

        ));
    }

    public function down()
    {
        echo "m180802_183130_add_rows_to_settings cannot be reverted.\n";

        return false;
    }
}
