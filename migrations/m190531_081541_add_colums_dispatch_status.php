<?php

use yii\db\Migration;

/**
 * Class m190531_081541_add_colums_dispatch_status
 */
class m190531_081541_add_colums_dispatch_status extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('dispatch_status', 'link', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('dispatch_status', 'link');
    }
}
