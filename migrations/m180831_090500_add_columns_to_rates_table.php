<?php

use yii\db\Migration;

/**
 * Class m180831_090500_add_columns_to_rates_table
 */
class m180831_090500_add_columns_to_rates_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('rates', 'proxy_count',  $this->integer()->null()->comment('Кол-во прокси'));
        $this->addColumn('rates', 'bots_count',  $this->integer()->null()->comment('Кол-во ботов'));
        $this->addColumn('rates', 'messages_in_transaction',  $this->integer()->null()->comment('Кол-во сообщений за транзакцию'));
        $this->addColumn('rates', 'messages_daily_limit',  $this->integer()->null()->comment('Лимит сообщений в сутки'));
        $this->addColumn('rates', 'send_interval',  $this->integer()->null()->comment('Интервал отправки (мин.)'));
        $this->addColumn('rates', 'add_accounts',  $this->boolean()->defaultValue(0)->comment('Возможность добавлять аккаунты'));
        $this->addColumn('rates', 'add_bots',  $this->boolean()->defaultValue(0)->comment('Возможность добавлять боты'));
        $this->addColumn('rates', 'add_proxy',  $this->boolean()->defaultValue(0)->comment('Возможность добавлять прокси'));
        $this->addColumn('rates', 'dispatch_rules',  $this->boolean()->defaultValue(0)->comment('Возможность управлять рассылкой'));

        $this->addColumn('company_settings', 'dispatch_count',  $this->integer()->null()->comment('Кол-во рассылок'));
        $this->addColumn('company_settings', 'account_count',  $this->integer()->null()->comment('Кол-во аккаунтов'));
        $this->addColumn('company_settings', 'proxy_count',  $this->integer()->null()->comment('Кол-во прокси'));
        $this->addColumn('company_settings', 'bots_count',  $this->integer()->null()->comment('Кол-во ботов'));
        $this->addColumn('company_settings', 'add_accounts',  $this->boolean()->defaultValue(0)->comment('Возможность добавлять аккаунты'));
        $this->addColumn('company_settings', 'add_bots',  $this->boolean()->defaultValue(0)->comment('Возможность добавлять боты'));
        $this->addColumn('company_settings', 'add_proxy',  $this->boolean()->defaultValue(0)->comment('Возможность добавлять прокси'));
        $this->addColumn('company_settings', 'dispatch_rules',  $this->boolean()->defaultValue(0)->comment('Возможность управлять рассылкой'));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180831_090500_add_columns_to_rates_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180831_090500_add_columns_to_rates_table cannot be reverted.\n";

        return false;
    }
    */
}
