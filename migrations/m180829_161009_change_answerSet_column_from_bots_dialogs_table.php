<?php

use yii\db\Migration,
    app\models\BotsDialogs;

/**
 * Class m180829_161009_change_answerSet_column_from_bots_dialogs_table
 */
class m180829_161009_change_answerSet_column_from_bots_dialogs_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn(BotsDialogs::tableName(), 'answerSet', $this->string()->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180829_161009_change_answerSet_column_from_bots_dialogs_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180829_161009_change_answerSet_column_from_bots_dialogs_table cannot be reverted.\n";

        return false;
    }
    */
}
