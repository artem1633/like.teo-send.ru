<?php

use yii\db\Migration;

/**
 * Class m180913_173000_change_column_in_auto_regist_table
 */
class m180913_173000_change_column_in_auto_regist_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('auto_registr', 'action', $this->string()->null()->comment('Действие'));
        $this->alterColumn('auto_registr', 'status', $this->boolean()->notNull()->defaultValue(0)->comment('Статус'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180913_173000_change_column_in_auto_regist_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180913_173000_change_column_in_auto_regist_table cannot be reverted.\n";

        return false;
    }
    */
}
