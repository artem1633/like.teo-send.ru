<?php

use yii\db\Migration;

/**
 * Class m180802_161641_add_dispatch_id_column_to_dispatch_status
 */
class m180802_161641_add_dispatch_id_column_to_dispatch_status extends Migration
{


    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->addColumn('dispatch_status', 'dispatch_id', $this->integer()->comment('Ид отправленного сообщении'));

        // creates index for column `author_id`
        $this->createIndex(
            'idx-dispatch_status-dispatch_id',
            'dispatch_status',
            'dispatch_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-dispatch_status-dispatch_id',
            'dispatch_status',
            'dispatch_id',
            'dispatch',
            'id',
            'CASCADE'
        );
    }

    public function down()
    {
        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-post-author_id',
            'post'
        );

        // drops index for column `author_id`
        $this->dropIndex(
            'idx-post-author_id',
            'post'
        );

        $this->dropColumn('dispatch_status', 'dispatch_id');
    }

}
