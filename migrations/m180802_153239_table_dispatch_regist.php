<?php

use yii\db\Migration;

/**
 * Class m180802_153239_table_dispatch_regist
 */
class m180802_153239_table_dispatch_regist extends Migration
{
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

        $this->createTable('dispatch_regist', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->comment('Пользователь'),
            'account_url' => $this->string()->comment('Ссылка на аккаунт'),
            'data' => $this->dateTime()->comment('Время подключение'),
            'token' => $this->string()->comment('токен'),
            'last_dispatch_time' => $this->dateTime()->comment('Дата и время последней рассылки'),
        ]);
    }

    public function down()
    {
        $this->dropTable('dispatch_regist');
    }

}
