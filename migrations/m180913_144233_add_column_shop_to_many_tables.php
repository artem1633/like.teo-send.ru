<?php

use yii\db\Migration;

/**
 * Class m180913_144233_add_column_shop_to_many_tables
 */
class m180913_144233_add_column_shop_to_many_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(\app\models\DispatchRegist::tableName(), 'shop_id', $this->integer()->null()->comment('ID товара'));
        $this->addColumn(\app\models\Proxy::tableName(), 'shop_id', $this->integer()->null()->comment('ID товара'));
        $this->addColumn(\app\models\Bots::tableName(), 'shop_id', $this->integer()->null()->comment('ID товара'));
        $this->addColumn(\app\models\DataRecipient::tableName(), 'shop_id', $this->integer()->null()->comment('ID товара'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180913_144233_add_column_shop_to_many_tables cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180913_144233_add_column_shop_to_many_tables cannot be reverted.\n";

        return false;
    }
    */
}
