<?php

use yii\db\Migration;

/**
 * Class m190222_040230_new_colums_regist
 */
class m190222_040230_new_colums_regist extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('dispatch_regist', 'status_read', $this->integer()->comment('Поиск не прочитаных'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('dispatch_regist', 'status_read');
    }
}
