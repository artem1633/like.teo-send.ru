<?php

use yii\db\Migration;

/**
 * Class m190610_080931_add_colums_automation
 */
class m190610_080931_add_colums_automation extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('automation', 'platform', $this->string(255));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('automation', 'platform');
    }
}
