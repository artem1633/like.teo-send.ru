<?php

use yii\db\Migration;

/**
 * Class m190705_200142_add_company_id_to_clients_table
 */
class m190705_200142_add_company_id_to_clients_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('clients', 'company_id', $this->integer()->comment('Компания'));

        $this->createIndex(
            'idx-clients-company_id',
            'clients',
            'company_id'
        );

        $this->addForeignKey(
            'fk-clients-company_id',
            'clients',
            'company_id',
            'companies',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-clients-company_id',
            'clients'
        );

        $this->dropIndex(
            'idx-clients-company_id',
            'clients'
        );

        $this->dropColumn('clients', 'company_id');
    }
}
