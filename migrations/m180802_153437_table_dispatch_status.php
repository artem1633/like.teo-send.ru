<?php

use yii\db\Migration;

/**
 * Class m180802_153437_table_dispatch_status
 */
class m180802_153437_table_dispatch_status extends Migration
{
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

        $this->createTable('dispatch_status', [
            'id' => $this->primaryKey(),
            'account_id' => $this->string(),
            'status' => $this->string(),
            'data' => $this->dateTime(),
            'send_account_id' => $this->integer(),

        ]);
    }

    public function down()
    {
        $this->dropTable('dispatch_status');
    }
}
