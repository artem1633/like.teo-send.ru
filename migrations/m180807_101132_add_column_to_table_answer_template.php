<?php

use yii\db\Migration;

/**
 * Class m180807_101132_add_column_to_table_answer_template
 */
class m180807_101132_add_column_to_table_answer_template extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('answer_template', 'company_id', $this->integer()->comment('Ид компании'));


    }

    public function down()
    {
        $this->dropColumn('answer_template', 'company_id');


    }
}
