<?php

use yii\db\Migration;

/**
 * Class m181203_090228_add_new_setting_row_to_settings_table
 */
class m181203_090228_add_new_setting_row_to_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('settings', [
            'key' => 'proxy6_api_key',
            'value' => null,
            'label' => 'API-Ключ для proxy6.net',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return false;
    }
}
