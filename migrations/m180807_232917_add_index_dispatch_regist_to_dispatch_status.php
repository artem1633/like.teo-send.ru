<?php

use yii\db\Migration;

/**
 * Class m180807_232917_add_index_dispatch_regist_to_dispatch_status
 */
class m180807_232917_add_index_dispatch_regist_to_dispatch_status extends Migration
{



    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        // creates index for column `author_id`
        $this->createIndex(
            'idx-dispatch_status-send_account_id',
            'dispatch_status',
            'send_account_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-dispatch_status-send_account_id',
            'dispatch_status',
            'send_account_id',
            'dispatch_regist',
            'id',
            'CASCADE'
        );

    }

    public function down()
    {
        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-dispatch_status-send_account_id',
            'dispatch_status'
        );

        // drops index for column `author_id`
        $this->dropIndex(
            'idx-dispatch_status-send_account_id',
            'dispatch_status'
        );
    }

}
