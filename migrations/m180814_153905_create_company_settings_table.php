<?php

use yii\db\Migration;

/**
 * Handles the creation of table `company_settings`.
 */
class m180814_153905_create_company_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('company_settings', [
            'company_id' => $this->integer(11)->notNull()->unique()->comment('Код компании'),
            'time_diff' => $this->integer(2)->comment('Разница во времени с Мск в часах')->defaultValue(0),
            'notify_bstart' => $this->integer(3)->comment('Уведомлять до начала, мин.')->defaultValue(0),
            'notify_start' => $this->integer(1)->comment('Уведомлять о запуске рассылки после суточного лимита')->defaultValue('0'),
            'notify_new_answer' => $this->integer(1)->comment('Уведомлять о новых ответах')->defaultValue(0),
            'notify_stop' => $this->integer(1)->comment('Уведомлять о прекращении рассылки')->defaultValue(0),
            'notify_block' => $this->integer(1)->comment('Уведомление о блокировки аккаунта')->defaultValue(0),
            'notify_limit' => $this->integer(1)->comment('Уведомления об истечении суточного лимита')->defaultValue(0),
            'notify_promo' => $this->integer(1)->comment('Уведомления об акциях и новостях')->defaultValue(0),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('company_settings');
    }
}
