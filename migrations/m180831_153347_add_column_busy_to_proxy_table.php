<?php

use yii\db\Migration;

/**
 * Class m180831_153347_add_column_busy_to_proxy_table
 */
class m180831_153347_add_column_busy_to_proxy_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('proxy','busy',  $this->boolean()->defaultValue(0)->comment('Занятость'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180831_153347_add_column_busy_to_proxy_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180831_153347_add_column_busy_to_proxy_table cannot be reverted.\n";

        return false;
    }
    */
}
