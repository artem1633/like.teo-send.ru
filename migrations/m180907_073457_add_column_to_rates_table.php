<?php

use yii\db\Migration;

/**
 * Class m180907_073457_add_column_to_rates_table
 */
class m180907_073457_add_column_to_rates_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->addColumn(\app\models\Rates::tableName(), 'affiliate_percent', $this->integer()->notNull()->defaultValue(15)->comment('Партнерский процент'));
        $this->addColumn(\app\models\Rates::tableName(), 'default', $this->boolean()->notNull()->defaultValue(0)->comment('Тариф по умолчанию'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180907_073457_add_column_to_rates_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180907_073457_add_column_to_rates_table cannot be reverted.\n";

        return false;
    }
    */
}
