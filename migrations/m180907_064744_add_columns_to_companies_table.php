<?php

use yii\db\Migration;

/**
 * Class m180907_064744_add_columns_to_companies_table
 */
class m180907_064744_add_columns_to_companies_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(\app\models\CompanySettings::tableName(), 'affiliate_percent', $this->integer()->notNull()->defaultValue(15)->comment('Процент партнерки компаний'));
        $this->addColumn(\app\models\Companies::tableName(), 'referal', $this->integer()->notNull()->defaultValue(1)->comment('Кто пригласил'));
        $this->addColumn(\app\models\Companies::tableName(), 'affiliate_amount', $this->decimal(10,2)->notNull()->defaultValue(0)->comment('Сумма начислений по партнерке'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180907_064744_add_columns_to_companies_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180907_064744_add_columns_to_companies_table cannot be reverted.\n";

        return false;
    }
    */
}
