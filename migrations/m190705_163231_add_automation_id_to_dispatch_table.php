<?php

use yii\db\Migration;

/**
 * Class m190705_163231_add_automation_id_to_dispatch_table
 */
class m190705_163231_add_automation_id_to_dispatch_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('dispatch', 'automation_id', $this->integer()->after('company_id')->comment('Автозадача'));

        $this->createIndex(
            'idx-dispatch-automation_id',
            'dispatch',
            'automation_id'
        );

        $this->addForeignKey(
            'fk-dispatch-automation_id',
            'dispatch',
            'automation_id',
            'automation',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-dispatch-automation_id',
            'dispatch'
        );

        $this->dropIndex(
            'idx-dispatch-automation_id',
            'dispatch'
        );

        $this->dropColumn('dispatch', 'automation_id');
    }
}
