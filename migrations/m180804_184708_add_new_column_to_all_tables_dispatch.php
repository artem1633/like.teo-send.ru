<?php

use yii\db\Migration;

/**
 * Class m180804_184708_add_new_column_to_all_tables_dispatch
 */
class m180804_184708_add_new_column_to_all_tables_dispatch extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('dispatch', 'company_id', $this->integer()->comment('Ид компании'));
        $this->addColumn('dispatch_regist', 'company_id', $this->integer()->comment('Ид компании'));
        $this->addColumn('dispatch_status', 'company_id', $this->integer()->comment('Ид компании'));

    }

    public function down()
    {
        $this->dropColumn('dispatch_status', 'company_id');
        $this->dropColumn('dispatch_regist', 'company_id');
        $this->dropColumn('dispatch', 'company_id');

    }
}
