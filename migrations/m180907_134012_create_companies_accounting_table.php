<?php

use yii\db\Migration;

/**
 * Handles the creation of table `companies_accounting`.
 */
class m180907_134012_create_companies_accounting_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        $this->createTable('companies_accounting', [
            'id' => $this->primaryKey(),
            'date_transaction' =>  'timestamp DEFAULT NOW() COMMENT "Дата и время отчисления"',
            'type' => $this->integer()->notNull()->comment('Тип транзакции'),
            'company_id' => $this->integer()->notNull()->comment('ID компании'),
            'amount' => $this->decimal(10,2)->notNull()->defaultValue(0)->comment('Сумма транзакции'),
            'description' =>$this->string()->null()->comment('Коментарии к транзакции'),

        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('companies_accounting');
    }
}
