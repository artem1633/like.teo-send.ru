<?php

use yii\db\Migration;

/**
 * Class m190526_162451_create_block_ip
 */
class m190526_162451_create_block_ip extends Migration
{
    /**
 * {@inheritdoc}
 */
    public function safeUp()
    {
        $this->createTable('block_ip', [
            'id' => $this->primaryKey(),
            'key' => $this->string(255),
            'message' => $this->text(),
            'date_time' => $this->datetime(),
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('block_ip');
    }

}
