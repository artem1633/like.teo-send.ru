<?php

use yii\db\Migration;

/**
 * Class m180806_125826_add_column_dispatch_status
 */
class m180806_125826_add_column_dispatch_status extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('dispatch_status', 'read', $this->string()->comment('Сообщение прочитан'));

    }

    public function down()
    {
        $this->dropColumn('dispatch_status', 'read');

    }
}
