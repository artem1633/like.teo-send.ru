<?php

use yii\db\Migration;

/**
 * Class m190705_173747_add_new_columns_to_automation
 */
class m190705_173747_add_new_columns_to_automation extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('automation', 'date_start', $this->date()->comment('Дата начала'));
        $this->addColumn('automation', 'date_end', $this->date()->comment('Дата завершения'));
        $this->addColumn('automation', 'sum', $this->integer()->comment('Сумма'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('automation', 'date_start');
        $this->dropColumn('automation', 'date_end');
        $this->dropColumn('automation', 'sum');
    }
}
