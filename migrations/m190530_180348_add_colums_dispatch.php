<?php

use yii\db\Migration;

/**
 * Class m190530_180348_add_colums_dispatch
 */
class m190530_180348_add_colums_dispatch extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('dispatch', 'group_id', $this->string(255));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('dispatch', 'group_id');
    }
}
