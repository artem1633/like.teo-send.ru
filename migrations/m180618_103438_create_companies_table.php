<?php

use yii\db\Migration;
/**
 * Handles the creation of table `companies`.
 */
class m180618_103438_create_companies_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('companies', [
            'id' => $this->primaryKey(),
            'company_name' => $this->string(255)->comment('Название компании'),
            'created_date' => $this->date()->comment('Дата регистрации'),
            'admin_id' => $this->integer()->comment('Id администратора компании'),
            'rate_id' => $this->integer()->comment('Тариф'),
            'last_activity_datetime' => $this->dateTime()->comment('Дата и время последней активности'),
            'access_end_datetime' => $this->datetime()->comment('Дата и время потери доступа к системе'),
            'is_super' => $this->boolean()->defaultValue(0)->comment('Супер компания'),
        ]);

        $this->addCommentOnTable('companies', 'Компании');

        $this->createIndex(
            'idx-companies-admin_id',
            'companies',
            'admin_id'
        );

        $this->addForeignKey(
            'fk-companies-admin_id',
            'companies',
            'admin_id',
            'users',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-companies-rate_id',
            'companies',
            'rate_id'
        );

        $this->addForeignKey(
            'fk-companies-rate_id',
            'companies',
            'rate_id',
            'rates',
            'id',
            'SET NULL'
        );

        $this->insert('companies', [
            'admin_id' => 1,
            'rate_id' => null,
            'is_super' => 1,
        ]);

        $this->update('users', [
            'company_id' => 1,
            'role_id' => 'super_admin',
            ], 
              'id=1'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-companies-admin_id',
            'companies'
        );

        $this->dropIndex(
            'idx-companies-admin_id',
            'companies'
        );

        $this->dropForeignKey(
            'fk-companies-rate_id',
            'companies'
        );

        $this->dropIndex(
            'idx-companies-rate_id',
            'companies'
        );

        $this->dropTable('companies');
    }
}
