<?php

use yii\db\Migration,
    app\models\DailyReport;

/**
 * Class m180905_081432_add_columns_to_daily_reports_table
 */
class m180905_081432_add_columns_to_daily_reports_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(DailyReport::tableName(), 'account_id', $this->integer()->notNull()->defaultValue(0)->comment('ID аккаунта'));
        $this->addColumn(DailyReport::tableName(), 'type', $this->integer()->null()->defaultValue(DailyReport::TYPE_SENT)->comment('Тип события'));
        $this->addColumn(DailyReport::tableName(), 'comments', $this->text()->null()->comment('Комментарии'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180905_081432_add_columns_to_daily_reports_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180905_081432_add_columns_to_daily_reports_table cannot be reverted.\n";

        return false;
    }
    */
}
