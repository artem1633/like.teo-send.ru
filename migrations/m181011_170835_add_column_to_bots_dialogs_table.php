<?php

use yii\db\Migration;

/**
 * Handles adding no_answer to table `bots_dialogs`.
 */
class m181011_170835_add_column_to_bots_dialogs_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('bots_dialogs', 'delayMin', $this->Integer()->comment('Кол-во минут ожидания перед отправкой сообщения (если нет ответа)'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('bots_dialogs', 'delayMin');
    }
}
