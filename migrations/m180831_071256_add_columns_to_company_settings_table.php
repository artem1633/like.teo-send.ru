<?php

use yii\db\Migration;

/**
 * Class m180831_071256_add_columns_to_company_settings_table
 */
class m180831_071256_add_columns_to_company_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('company_settings', 'messages_in_transaction',  $this->integer()->defaultValue(2)->comment('Кол-во сообщений за транзакцию'));
        $this->addColumn('company_settings', 'messages_daily_limit',  $this->integer()->defaultValue(15)->comment('Суточный лимит сообщений'));
        $this->addColumn('company_settings', 'send_interval',  $this->integer()->defaultValue(10)->comment('Интервал отправки (мин.)'));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180831_071256_add_columns_to_company_settings_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180831_071256_add_columns_to_company_settings_table cannot be reverted.\n";

        return false;
    }
    */
}
