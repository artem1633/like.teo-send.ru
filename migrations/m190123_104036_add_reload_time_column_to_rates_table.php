<?php

use yii\db\Migration;

/**
 * Handles adding reload_time to table `rates`.
 */
class m190123_104036_add_reload_time_column_to_rates_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('rates', 'reload_time', $this->integer()->comment('Время перезагрузки'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('rates', 'reload_time');
    }
}
