<?php

use yii\db\Migration;

/**
 * Class m190505_080834_new_colums_type_to_dispatch
 */
class m190505_080834_new_colums_type_to_dispatch extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('dispatch', 'type', $this->integer()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('dispatch', 'type');
    }
}
