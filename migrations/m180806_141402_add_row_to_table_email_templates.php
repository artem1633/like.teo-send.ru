<?php

use yii\db\Migration;

/**
 * Class m180806_141402_add_row_to_table_email_templates
 */
class m180806_141402_add_row_to_table_email_templates extends Migration
{

    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->insert(
            'email_templates',
            [
                'key' => 'account_limit',
                'key_ru' => 'Количество  аккаунты для комапнии',
                'body' => 'Количество  аккаунты для комапнии Super Admin ',
                'deletable' => false,
            ]
        );
    }

    public function down()
    {
        echo "m180806_050104_add_row_email_tepmpates_table cannot be reverted.\n";

        return false;
    }
}
