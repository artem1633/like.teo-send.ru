<?php

use yii\db\Migration;

/**
 * Class m190607_134128_add_colums_dispatch
 */
class m190607_134128_add_colums_dispatch extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('dispatch', 'like_count', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('dispatch', 'like_count');
    }
}
