<?php

use yii\db\Migration;

/**
 * Class m190418_183837_add_check_bot_dispatch_status
 */
class m190418_183837_add_check_bot_dispatch_status extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('dispatch_status', 'check_bot', $this->boolean()->defaultValue(false)->comment('Проверка на ответ ботом'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('dispatch_status', 'check_bot');
    }
}
