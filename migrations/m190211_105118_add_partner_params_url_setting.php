<?php

use yii\db\Migration;

/**
 * Class m190211_105118_add_partner_params_url_setting
 */
class m190211_105118_add_partner_params_url_setting extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('settings', [
            'key' => 'partner_params_url',
            'label' => 'Параметры партерской ссылки',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190211_105118_add_partner_params_url_setting cannot be reverted.\n";

        return false;
    }
}
