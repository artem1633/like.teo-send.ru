<?php

use yii\db\Migration;

/**
 * Handles adding default_account_id to table `dispatch_status`.
 */
class m181121_165939_add_default_account_id_column_to_dispatch_status_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('dispatch_status', 'default_account_id', $this->string()->comment('Необработанная строка с ID'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('dispatch_status', 'default_account_id');
    }
}
