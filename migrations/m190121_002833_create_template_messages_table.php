<?php

use yii\db\Migration;

/**
 * Handles the creation of table `template_messages`.
 */
class m190121_002833_create_template_messages_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('template_messages', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->comment('Наименование'),
            'tag' => $this->string()->comment('Тэг'),
            'description' => $this->text()->comment('Описание'),
            'company_id' => $this->integer()->comment('Компания'),
            'created_at' => $this->dateTime(),
        ]);
        $this->addCommentOnTable('template_messages', 'Шаблоны сообщений');

        $this->createIndex(
            'idx-template_messages-company_id',
            'template_messages',
            'company_id'
        );

        $this->addForeignKey(
            'fk-template_messages-company_id',
            'template_messages',
            'company_id',
            'companies',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-template_messages-company_id',
            'template_messages'
        );

        $this->dropIndex(
            'idx-template_messages-company_id',
            'template_messages'
        );

        $this->dropTable('template_messages');
    }
}
