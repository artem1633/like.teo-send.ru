<?php

use yii\db\Migration;

/**
 * Class m180806_132226_add_new_colums_to_table_companies
 */
class m180806_132226_add_new_colums_to_table_companies extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('companies', 'account_count', $this->integer()->comment('Количество Аккаунтов'));
        $this->addColumn('companies', 'dispatch_count', $this->integer()->comment('Количество Рассылок'));

    }

    public function down()
    {
        $this->dropColumn('companies', 'account_count');
        $this->dropColumn('companies', 'dispatch_count');

    }
}

