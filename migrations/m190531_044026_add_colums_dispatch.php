<?php

use yii\db\Migration;

/**
 * Class m190531_044026_add_colums_dispatch
 */
class m190531_044026_add_colums_dispatch extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('dispatch', 'text', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('dispatch', 'text');
    }
}
