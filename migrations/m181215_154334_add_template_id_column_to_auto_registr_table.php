<?php

use yii\db\Migration;

/**
 * Handles adding template_id to table `auto_registr`.
 */
class m181215_154334_add_template_id_column_to_auto_registr_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('auto_registr', 'template_id', $this->integer()->comment('Шаблон'));

        $this->createIndex(
            'idx-auto_registr-template_id',
            'auto_registr',
            'template_id'
        );

        $this->addForeignKey(
            'fk-auto_registr-template_id',
            'auto_registr',
            'template_id',
            'auto_regists_templates',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-auto_registr-template_id',
            'auto_registr'
        );

        $this->dropIndex(
            'idx-auto_registr-template_id',
            'auto_registr'
        );

        $this->dropColumn('auto_registr', 'template_id');
    }
}
