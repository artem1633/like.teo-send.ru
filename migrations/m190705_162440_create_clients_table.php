<?php

use yii\db\Migration;

/**
 * Handles the creation of table `clients`.
 */
class m190705_162440_create_clients_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('clients', [
            'id' => $this->primaryKey(),
            'fio' => $this->string()->comment('ФИО'),
            'url' => $this->string()->comment('Ссылка на аккаунт'),
            'photo_url' => $this->string()->comment('Ссылка на фото'),
            'status' => $this->string()->comment('Статус'),
            'created_at' => $this->dateTime(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('clients');
    }
}
