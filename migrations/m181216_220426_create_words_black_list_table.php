<?php

use yii\db\Migration;

/**
 * Handles the creation of table `words_black_list`.
 */
class m181216_220426_create_words_black_list_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('words_black_list', [
            'id' => $this->primaryKey(),
            'content' => $this->string()->comment('Содержимое'),
            'created_at' => $this->dateTime(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('words_black_list');
    }
}
