<?php

use yii\db\Migration;

/**
 * Class m180906_142654_add_column_status_to_raport_table
 */
class m180906_142654_add_column_status_to_raport_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(\app\models\DailyReport::tableName(), 'status', $this->integer()->null()->after('type')->comment('Пользовательский статус'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180906_142654_add_column_status_to_raport_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180906_142654_add_column_status_to_raport_table cannot be reverted.\n";

        return false;
    }
    */
}
