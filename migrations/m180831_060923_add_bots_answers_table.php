<?php

use yii\db\Migration,
    app\models\BotsDialogs;

/**
 * Class m180831_060923_add_bots_answers_table
 */
class m180831_060923_add_bots_answers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        $this->createTable('bots_answers', [
            'id' => $this->primaryKey(),
            'dialogId' => $this->integer()->notNull()->comment('ID диалога'),
            'tag' => $this->string(250)->notNull()->comment('Тег (ответ)'),
        ], $tableOptions);
        $this->createIndex('idx-dialogId', 'bots_answers', 'dialogId');
        $this->createIndex('idx-tag', 'bots_answers', 'tag');

        $this->dropColumn(BotsDialogs::tableName(), 'answerSet');
        $this->createIndex('idx-botId', BotsDialogs::tableName(), 'botId');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180831_060923_add_bots_answers_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180831_060923_add_bots_answers_table cannot be reverted.\n";

        return false;
    }
    */
}
