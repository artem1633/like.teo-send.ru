<?php

use yii\db\Migration;

/**
 * Class m190414_230839_add_company_id_company_chat
 */
class m190414_230839_add_company_id_company_chat extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('company_chat', 'company_id', $this->integer());

        $this->createIndex(
            'idx-chat-company_id',
            'company_chat',
            'company_id'
        );

        $this->addForeignKey(
            'fk-chat-company_id',
            'company_chat',
            'company_id',
            'companies',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-chat-company_id',
            'company_chat'
        );

        $this->dropIndex(
            'idx-chat-company_id',
            'company_chat'
        );

        $this->dropColumn('company_chat','company_id');
    }
}
