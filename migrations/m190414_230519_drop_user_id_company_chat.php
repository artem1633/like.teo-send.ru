<?php

use yii\db\Migration;

/**
 * Class m190414_230519_drop_user_id_company_chat
 */
class m190414_230519_drop_user_id_company_chat extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->dropForeignKey('fk-chat-user_id','company_chat');
        $this->dropIndex('idx-chat-user_id','company_chat');

        $this->dropColumn('company_chat', 'chat_id');
        $this->dropColumn('company_chat', 'is_read');
        $this->dropColumn('company_chat', 'user_id');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('company_chat', 'chat_id', $this->string(255));
        $this->addColumn('company_chat', 'is_read', $this->boolean());
        $this->addColumn('company_chat', 'user_id', $this->integer());
        $this->createIndex('idx-chat-user_id', 'company_chat', 'user_id', false);
        $this->addForeignKey("fk-chat-user_id", "company_chat", "user_id", "users", "id");
    }
}
