<?php

use yii\db\Migration;

/**
 * Class m190210_174902_create_form_image_setting
 */
class m190210_174902_create_form_image_setting extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('settings', [
            'key' => 'home_form_image',
            'label' => 'Изображение формы при входе'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190210_174902_create_form_image_setting cannot be reverted.\n";

        return false;
    }
}
