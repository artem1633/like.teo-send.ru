<?php

use yii\db\Migration;

/**
 * Class m181226_101459_add_partner_domain_setting
 */
class m181226_101459_add_partner_domain_setting extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('settings', [
            'key' => 'partner_domain',
            'label' => 'Партнерский домен',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
