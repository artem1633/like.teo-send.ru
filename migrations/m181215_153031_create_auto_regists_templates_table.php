<?php

use yii\db\Migration;

/**
 * Handles the creation of table `auto_regists_templates`.
 */
class m181215_153031_create_auto_regists_templates_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('auto_regists_templates', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование шаблона'),
            'company_id' => $this->integer()->comment('Компания'),
        ]);
        $this->addCommentOnTable('auto_regists_templates', 'Шаблоны автозаполнения аккаунтов');

        $this->createIndex(
            'idx-auto_regists_templates-company_id',
            'auto_regists_templates',
            'company_id'
        );

        $this->addForeignKey(
            'fk-auto_regists_templates-company_id',
            'auto_regists_templates',
            'company_id',
            'companies',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-auto_regists_templates-company_id',
            'auto_regists_templates'
        );

        $this->dropIndex(
            'idx-auto_regists_templates-company_id',
            'auto_regists_templates'
        );

        $this->dropTable('auto_regists_templates');
    }
}
