<?php

use yii\db\Migration;

/**
 * Handles the creation of table `yandex_log`.
 */
class m180910_191922_create_yandex_log_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        $this->createTable('yandex_log', [
            'id' => $this->primaryKey(),
            'secret' =>  $this->string(250)->null()->comment('secret'),
            'notification_type' => $this->string(250)->null()->comment('notification_type'),
            'operation_id' => $this->string(250)->null()->comment('operation_id'),
            'amount' => $this->string(250)->null()->comment('amount'),
            'currency' => $this->string(250)->null()->comment('currency'),
            'datetime' => $this->string(250)->null()->comment('datetime'),
            'sender' => $this->string(250)->null()->comment('sender'),
            'codepro' => $this->string(250)->null()->comment('codepro'),
            'label' => $this->string(250)->null()->comment('label'),
            'sha1_hash' => $this->string(250)->null()->comment('sha1_hash'),
            'sha1_res' => $this->string(250)->null()->comment('sha1_res'),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('yandex_log');
    }
}
