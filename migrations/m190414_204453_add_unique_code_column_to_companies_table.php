<?php

use yii\db\Migration;

/**
 * Handles adding unique_code to table `companies`.
 */
class m190414_204453_add_unique_code_column_to_companies_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('companies', 'unique_code', $this->string(20));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('companies', 'unique_code');
    }
}
