<?php

use yii\db\Migration;

/**
 * Class m180809_062010_alter_table_dispatch
 */
class m180809_062010_alter_table_dispatch extends Migration
{


    // Use up()/down() to run migration code without a transaction.
    public function up(){
        $this->alterColumn('dispatch', 'text', $this->text());
    }

    public function down()
    {
        echo "m180809_062010_alter_table_dispatch cannot be reverted.\n";

        return false;
    }

}
