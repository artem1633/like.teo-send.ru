<?php

use yii\db\Migration;

/**
 * Class m180923_084915_change_age_column_type_in_dispatch_status
 */
class m180923_084915_change_age_column_type_in_dispatch_status extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('dispatch_status', 'age', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180923_084915_change_age_column_type_in_dispatch_status cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180923_084915_change_age_column_type_in_dispatch_status cannot be reverted.\n";

        return false;
    }
    */
}
