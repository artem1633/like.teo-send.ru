<?php

use yii\db\Migration;

/**
 * Handles adding templates to table `dispatch`.
 */
class m181216_105837_add_templates_columns_to_dispatch_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('dispatch', 'use_auto_registr', $this->boolean()->defaultValue(false)->comment('Использовать автозаполнение'));
        $this->addColumn('dispatch', 'auto_regists_template_id', $this->integer()->comment('Шаблон автозаполнения'));

        $this->createIndex(
            'idx-dispatch-auto_regists_template_id',
            'dispatch',
            'auto_regists_template_id'
        );

        $this->addForeignKey(
            'fk-dispatch-auto_regists_template_id',
            'dispatch',
            'auto_regists_template_id',
            'auto_regists_templates',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-dispatch-auto_regists_template_id',
            'dispatch'
        );

        $this->dropIndex(
            'idx-dispatch-auto_regists_template_id',
            'dispatch'
        );

        $this->dropColumn('dispatch', 'use_auto_registr');
        $this->dropColumn('dispatch', 'auto_regists_template_id');
    }
}
