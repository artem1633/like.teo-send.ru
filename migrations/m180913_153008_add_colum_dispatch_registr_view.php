<?php

use yii\db\Migration;

/**
 * Class m180913_153008_add_colum_dispatch_registr_view
 */
class m180913_153008_add_colum_dispatch_registr_view extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(\app\models\DispatchRegist::tableName(), 'auto_view', $this->boolean()->notNull()->defaultValue(0)->comment('Автоматическое оформление'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180911_172441_add_column_to_stat_to_dispatch_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180913_153008_add_colum_dispatch_registr_view cannot be reverted.\n";

        return false;
    }
    */
}
