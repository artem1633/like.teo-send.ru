<?php

use yii\db\Migration;

/**
 * Class m190414_194613_add_unique_message_id_column_dispatch_status_table
 */
class m190414_194613_add_unique_message_id_column_dispatch_status_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('dispatch_status', 'unique_message_id', $this->integer());

        $this->createIndex(
            'idx-dispatch_status-unique_message_id',
            'dispatch_status',
            'unique_message_id'
        );

        $this->addForeignKey(
            'fk-dispatch_status-unique_message_id',
            'dispatch_status',
            'unique_message_id',
            'unique_message',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-dispatch_status-unique_message_id',
            'dispatch_status'
        );

        $this->dropIndex(
            'idx-dispatch_status-unique_message_id',
            'dispatch_status'
        );

        $this->dropColumn('dispatch_status', 'unique_message_id');
    }
}
