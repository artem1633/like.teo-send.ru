<?php

use yii\db\Migration;

/**
 * Handles adding reload_time to table `company_settings`.
 */
class m190123_104228_add_reload_time_column_to_company_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('company_settings', 'reload_time', $this->integer()->comment('Время перезагрузки'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('company_settings', 'reload_time');
    }
}
