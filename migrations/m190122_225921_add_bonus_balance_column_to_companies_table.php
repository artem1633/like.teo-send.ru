<?php

use yii\db\Migration;

/**
 * Handles adding bonus_balance to table `companies`.
 */
class m190122_225921_add_bonus_balance_column_to_companies_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('companies', 'bonus_balance', $this->decimal(10, 2)->comment('Бонусный баланс'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('companies', 'bonus_balance');
    }
}
