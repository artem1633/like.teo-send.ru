<?php

use yii\db\Migration;

/**
 * Handles adding access_token to table `company_settings`.
 */
class m181121_152950_add_access_token_column_to_company_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('company_settings', 'sales_access_token', $this->string()->comment('Sales-Токен'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('company_settings', 'sales_access_token');
    }
}
