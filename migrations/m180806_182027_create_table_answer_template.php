<?php

use yii\db\Migration;

/**
 * Class m180806_182027_create_table_answer_template
 */
class m180806_182027_create_table_answer_template extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('answer_template', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'text' => $this->text(),
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('answer_template');
    }
}
