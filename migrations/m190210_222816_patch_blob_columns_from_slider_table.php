<?php

use yii\db\Migration;

/**
 * Class m190210_222816_patch_blob_columns_from_slider_table
 */
class m190210_222816_patch_blob_columns_from_slider_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('slider', 'content', $this->binary()->comment('Текст'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
