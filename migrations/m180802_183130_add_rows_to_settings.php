<?php

use yii\db\Migration;

/**
 * Class m180802_183130_add_rows_to_settings
 */
class m180802_183130_add_rows_to_settings extends Migration
{



    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->insert('settings',array(
            'key' => 'interval_in_minutes',
            'value' => '10',
            'label' =>'Интервал в минутах',

        ));

        $this->insert('settings',array(
            'key' => 'kol_vo_soobshenii',
            'value' => '10',
            'label' =>'кол-во собщений ',

        ));
    }

    public function down()
    {
        echo "m180802_183130_add_rows_to_settings cannot be reverted.\n";

        return false;
    }

}
