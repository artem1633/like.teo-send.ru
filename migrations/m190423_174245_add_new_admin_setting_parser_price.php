<?php

use yii\db\Migration;

/**
 * Class m190423_174245_add_new_admin_setting_parser_price
 */
class m190423_174245_add_new_admin_setting_parser_price extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('settings', [
            'key' => 'parser_price',
            'label' => 'Стоимость парсера',
            'value' => 0
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $setting = \app\models\Settings::findByKey('parser_price');
        $setting->delete();
    }
}
