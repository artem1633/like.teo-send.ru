<?php

use yii\db\Migration;

/**
 * Handles adding last_observe_pay_datetime to table `parser`.
 */
class m190425_134358_add_last_observe_pay_datetime_column_to_parser_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('parser', 'last_observe_pay_datetime', $this->dateTime()->comment('Дата и время последний оплаты за отслеживание'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('parser', 'last_observe_pay_datetime');
    }
}
