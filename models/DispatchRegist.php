<?php

namespace app\models;

use app\components\helpers\FunctionHelper;
use app\components\TemplateChecker;
use app\modules\api\controllers\ClientController;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\HttpException;

/**
 * This is the model class for table "dispatch_regist".
 *
 * @property int $id
 * @property string $username Пользователь
 * @property string $account_url Ссылка на аккаунт
 * @property string $data Время подключение
 * @property string $token токен
 * @property string $last_dispatch_time Дата и время последней рассылки
 * @property boolean $busy Занятость
 * @property boolean $auto_view Занятость
 * @property string $login Логин
 * @property string $password Пароль
 * @property string $coment Пароль
 * @property string $status Пароль
 * @property string $blocking_date Дата блокировки
 * @property int $all_sended_message_count Общее кол-во отправленных сообщений
 * @property int $company_id ID компании
 * @property int $shop_id ID товара
 * @property string $photo Url фотографии
 * @property string $proxy Прокси
 * @property string $note Комментарий
 * @property integer $template_id Шаблон
 * @property integer $proxy_blocked Прокси при котором был заблокирован аккаунт
 * @property integer $template_process Идет ли процесс шаблонизации
 *
 * @property boolean $templateProcessing Идет ли процесс шаблонизации
 *
 * @property Companies $companyModel
 * @property Proxy $proxyBlocked
 * @property AutoRegistsTemplates $template
 * @property AppliedAutoRegistr[] $appliedAutoRegistrs
 */
class DispatchRegist extends \yii\db\ActiveRecord
{

    /**
     * @const Статус аккаунта "Спящий". Означает что аккаунт не используется
     * уже продолжительное время
     */
    const STATUS_SLEEPING = 'sleeping';

    /**
     * @const Статус аккаунта "Заблокирован". Означает что аккаунт заблокирован
     */
    const STATUS_BLOCKED = 'account_blocked';

    /**
     * @var
     */
    public $token_id;

    /**
     * @var TemplateChecker
     */
    public $templateChecker;

    /**
     * DispatchRegist constructor.
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        $this->templateChecker = new TemplateChecker(['model' => $this]);
        parent::__construct($config);
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dispatch_regist';
    }

    /**
     * {@inheritdoc}
     */
    public function beforeSave($insert)
    {
        if (!$this->isNewRecord && $this->busy) {
            $this->company_id = 0;
        }

//        if($this->template_id != null){
//            $this->auto_view = 0;
//        }

        if($this->isNewRecord)
        {
            $this->last_dispatch_time = date('Y-m-d H:i:s');
        }

        if ($this->token != null) {
            if (stripos($this->token, 'oauth.vk.com')) {
                $this->token = explode('access_token=', $this->token);
                if (isset($this->token[1])) {
                    $this->token = $this->token[1];
                }
            }

            if (stripos($this->token, 'ccess_token=')) {
                $this->token = explode('ccess_token=', $this->token);
                if (isset($this->token[1])) {
                    $this->token = $this->token[1];
                }
            }

            if (stripos($this->token, '&')) {
                $this->token = explode('&', $this->token);
                if (isset($this->token[0])) {
                    $this->token = $this->token[0];
                }
            }
        }

        // Присваиваем прокси
        if ($this->isNewRecord) {
            $proxy = $this->getOneAvailableProxy();

            // Если прокси есть, но он истекает или уже устек, то пытаемся его продлить
            if ($proxy != null && $proxy->haveToProlong) {
                $result = Yii::$app->proxy->prolong($proxy->internal_id);
                if (isset($result->list[0])) {
                    $proxy->expire_date = $result->list[0]->date_end;
                    $proxy->save(false);
                }
            }

            // Если прокси нет, то пытаемся купить прокси
            if ($proxy == null) {
                $proxy = Yii::$app->proxy->buy();
                sleep(1000);
                $list = Proxy::synchronize();
                /** @var \app\models\Proxy $proxy */
                $proxy = $this->getOneAvailableProxy();
            }


            if ($proxy == null) {

                ClientController::sendTelMessage('247187885',  "После покупки прокси все равно пустой");

            }

            if ($proxy != null) {
                $this->proxy = $proxy->id;
                $proxAdd = Proxy::find()->where(['id' => $proxy->id])->one();
                $proxAdd->count_add = $proxAdd->count_add + 1;
                $proxAdd->save();
            }

            if ($this->login != null && $this->password != null && $proxy != null) {
                try {
                    $clientIdSetting = Settings::findByKey('app_id');
                    $scopeSetting = Settings::findByKey('scope');
                    if ($clientIdSetting != null && $scopeSetting != null) {
                        $this->token = get_token($this->login, $this->password, $clientIdSetting->value, $scopeSetting->value, $proxy->fullIp);
                    }
                } catch (\Exception $e) {
                    $this->addError('password', 'Неправильный логин или пароль или аккаунт заблокирован');
                }
            }
        }

        return parent::beforeSave($insert);
    }

    /**
     * {@inheritdoc}
     */
    public function beforeDelete()
    {
        $proxy = Proxy::findOne($this->proxy);

        if ($proxy != null) {
            $proxy->makeAvailable();
        }

        return parent::beforeDelete();
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['data', 'error_notif', 'last_dispatch_time', 'blocking_date', 'photo', 'note', 'status_read'], 'safe'],
            [['company_id', 'shop_id', 'proxy','template_id'], 'integer'],
            [['token'], 'string', 'length' => [85]],
            [['username', 'account_url', 'coment', 'login', 'password'], 'string', 'max' => 255],
            [['busy', 'auto_view'], 'boolean'],
            [['proxy_blocked'], 'exist', 'skipOnError' => true, 'targetClass' => Proxy::className(), 'targetAttribute' => ['proxy_blocked' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ИД',
            'username' => 'ФИО',
            'account_url' => 'Ссылка на аккаунт',
            'data' => 'Дата подключения',
            'token' => 'Токен',
            'last_dispatch_time' => 'Дата и время последней рассылки',
            'unread_message' => 'Сообщения',
            'status' => 'Статус',
            'busy' => 'Занятость',
            'error_notif' => 'Уведомление об ошибки',
            'coment' => 'Коммент',
            'login' => 'Логин',
            'password' => 'Пароль',
            'blocking_date' => 'Дата блокировки',
            'auto_view' => 'Автоматическое оформление',
            'photo' => 'Путь к фотографии',
            'proxy' => 'Прокси',
            'proxy_blocked' => 'Заблокированый прокси',
            'note' => 'Комментарий',
            'template_id' => 'Шаблон',
            'status_read' => 'Поиск не прочитаных',
        ];
    }

    /**
     * @return array
     */
    public function getDispatchList()
    {
        $dispatchs = Companies::find()->where(['is_super' => 0])->all();
        return ArrayHelper::map($dispatchs, 'id', 'company_name');
    }

    /**
     * @return array
     */
    public function DispatchRegistList()
    {
        $dispatch_regist = Companies::find()->all();
        $result = [];

        foreach ($dispatch_regist as $value) {
            $result [] = $value->id;
        }
        return $result;
    }

    /**
     * Меняет статус аккаунта на «Спящий»
     */
    public function makeSleep()
    {
        $this->status = self::STATUS_SLEEPING;
//        if($this->proxy != null)
//        {
//            $proxy = Proxy::findOne($this->proxy);
//            if($proxy != null)
//            {
//                $this->proxy = null;
//                $this->save();
////                $proxy->makeAvailable();
//            }
//        }

        $this->proxy = null;
        $this->save();

        $dispatchConnects = DispatchConnect::find()->where(['dispatch_regist_id' => $this->id])->all();
        foreach ($dispatchConnects as $connect)
        {
            $connect->delete();
        }

        $this->save(false);
    }

    /**
     * Меняет статус аккаунта на «Заблокирован»
     * @param string $comment
     */
    public function makeBlock($comment = null)
    {
        $this->status = self::STATUS_BLOCKED;
        $this->blocking_date = date('Y-m-d'); //Запись даты блокировки аккаунта
        $this->coment = $comment;
//        var_dump($this->proxy);
//        exit;
        $proxy = Proxy::findOne($this->proxy);
        if($proxy != null)
        {
            $this->proxy_blocked = $this->proxy;
            $proxy->count_block = $proxy->count_block + 1;
            $proxy->save();
        }
        $report = new DailyReport([
            'company_id' => $this->company_id,
            'account_id' => $this->id,
            'comments' => 'Блокировка акк',
            'type' => DailyReport::TYPE_BLOCK_DISPATCH_REGIST,
        ]);
        $report->save(false);

        $this->save(false);
    }

    /**
     * @return int
     */
    public function getMaxCountAccounts()
    {
        $company = Companies::findOne(FunctionHelper::getCompanyId());
        $rate = Rates::findOne($company->rate_id);
        return ($settings = FunctionHelper::getCompanySettings()) ? $settings->account_count : $rate->account_count;
    }

    /**
     * @return int|string
     */
    public function getCountAccounts()
    {
        return self::find()->where(['company_id' => FunctionHelper::getCompanyId()])->count();
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getFreeAccounts()
    {
        return self::find()->where(['busy' => true])->all();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyModel()
    {
        return $this->hasOne(Companies::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProxyBlocked()
    {
        return $this->hasOne(Proxy::className(), ['id' => 'proxy_blocked']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDispatchs()
    {
        return $this->hasMany(DispatchConnect::className(), ['dispatch_regist_id' => 'id']);
    }

    /**
     * Возвращает один доступный прокси
     * @return array|null|\yii\db\ActiveRecord|\yii\db\ActiveRecord[]
     */
    public function getOneAvailableProxy()
    {
        $proxy = Proxy::getAvailableProxy();
        if(count($proxy) > 0)
        {
            $proxy = array_shift($proxy);
            return $proxy;
        }

//        var_dump($proxy);
//        exit;

        $proxy = Proxy::getAvailableProxy(true);
        if(count($proxy) > 0)
        {
            $proxy = array_shift($proxy);
            return $proxy;
        }

        return null;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTemplate()
    {
        return $this->hasOne(AutoRegistsTemplates::className(), ['id' => 'template_id']);
    }

    /**
     * @return bool
     */
    public function getTemplateProcessing()
    {
        return $this->template_process == 1 ? true : false;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAppliedAutoRegistrs()
    {
        return $this->hasMany(AppliedAutoRegistr::className(), ['dispatch_regist_id' => 'id']);
    }

    /**
     * Оформаляет страницу исходя из шаблонов
     * @return string
     */
    public function makeAutoPage()
    {
        /** @var \app\models\DispatchRegist $this */
        $model = $this;
        /** @var Proxy $proxy */
        $proxy = Proxy::findOne(['id' => $model->proxy]);
        if($model->template_id)
        {
//            $registAuto = AutoRegistr::find()->where(['company_id' => $model->company_id, 'template_id' => $model->template_id])->all();
            //   $registAuto = $this->templateChecker->getRemainAutoRegistrs(); // Ишем оставшиеся записи шаблона, которые осталось применить
            $registAuto = AutoRegistr::find()->where(['template_id' => $model->template_id])->all();
        } else {
            $registAuto = [];
        }
        $report = '';

        $counter = 1;
        $i = 0;//для того чтобы не было много запросов
        foreach ($registAuto as $reg) {
            $i ++;
            if ($i == 4) {
                $i = 0;
                sleep(3);
            }
            $report .= " {$reg->name} статус = ";
            if ($reg->type == 1 and $reg->status) {
                $url = 'https://api.vk.com/method/groups.join';

                if(stripos($reg->action, 'vk.com/')){
                    $reg->action = explode('vk.com/', $reg->action);
                    if(isset($reg->action[1])){
                        $reg->action = $reg->action[1];
                    } else {
                        $reg->action = null;
                    }
                }

                $resolveScreenName = json_decode(file_get_contents('https://api.vk.com/method/utils.resolveScreenName', false, stream_context_create(array(
                    'http' => array(
                        'proxy' => "tcp://{$proxy->ip_adress}:{$proxy->port}",
                        'method' => 'POST',
                        'header' => 'Content-type: application/x-www-form-urlencoded',
                        'content' => http_build_query(['screen_name' => $reg->action,'access_token' => $model->token, 'v' => '5.80',])
                    )
                ))));

                $report .= " ID группы: {$resolveScreenName->response->object_id}";

                $group = $resolveScreenName->response->object_id;

                $params = array(
                    'group_id' => $group,
                    'access_token' => $model->token, // Токен того кто отправляет
                    'v' => '5.80',
                );

                // В $result вернется id отправленного сообщения
                $result = file_get_contents($url, false, stream_context_create(array(
                    'http' => array(
                        'proxy' => "tcp://{$proxy->ip_adress}:{$proxy->port}",
                        'method' => 'POST',
                        'header' => 'Content-type: application/x-www-form-urlencoded',
                        'content' => http_build_query($params)
                    )
                )));
                $result = json_decode($result, true);
                if (!$result['response']) {
                    $report .= "Не успешно по причине {$result['error']["error_msg"]}";
                } else {
                    $report .= "Успешно";
                }
            }
            // смена статуса
            if ($reg->type == 2 and $reg->status) {
                $url = 'https://api.vk.com/method/status.set';
                $params = array(
                    'text' => $reg->action,
                    'access_token' => $model->token, // Токен того кто отправляет
                    'v' => '5.85',
                );

                // В $result вернется id отправленного сообщения
                $result = file_get_contents($url, false, stream_context_create(array(
                    'http' => array(
                        'proxy' => "tcp://{$proxy->ip_adress}:{$proxy->port}",
                        'method' => 'POST',
                        'header' => 'Content-type: application/x-www-form-urlencoded',
                        'content' => http_build_query($params)
                    )
                )));
                $result = json_decode($result, true);
                if (!$result['response']) {
                    $report .= "Не успешно по причине {$result['error']["error_msg"]}";
                } else {
                    $report .= "Успешно";
                }
            }
            // репост
            if ($reg->type == 3 and $reg->status) {
                $url = 'https://api.vk.com/method/wall.repost';

                if(stripos($reg->action, '?w=')){
                    $reg->action = explode('?w=', $reg->action);
                    if(isset($reg->action[1])){
                        $reg->action = $reg->action[1];
                    } else {
                        $reg->action = null;
                    }
                }

                $report .= " Репост: {$reg->action}";

                $params = array(
                    'object' => $reg->action,
                    'access_token' => $model->token, // Токен того кто отправляет
                    'v' => '5.85',
                );

                // В $result вернется id отправленного сообщения
                $result = file_get_contents($url, false, stream_context_create(array(
                    'http' => array(
                        'proxy' => "tcp://{$proxy->ip_adress}:{$proxy->port}",
                        'method' => 'POST',
                        'header' => 'Content-type: application/x-www-form-urlencoded',
                        'content' => http_build_query($params)
                    )
                )));
                $result = json_decode($result, true);
                if (!$result['response']) {
                    $report .= "Не успешно по причине {$result['error']["error_msg"]}";
                } else {
                    $report .= "Успешно";
                }
            }

//            if($reg->type = AutoRegistr::SET_CITY and $reg->status)
//            {
//                $url = 'https://api.vk.com/method/account.saveProfileInfo';
//                $params = array(
//                    'city_id' => $reg->action,
//                    'access_token' => $model->token, // Токен того кто отправляет
//                    'v' => '5.85',
//                );
//
//                // В $result вернется id отправленного сообщения
//                $result = file_get_contents($url, false, stream_context_create(array(
//                    'http' => array(
//                        'proxy' => "tcp://{$proxy->ip_adress}:{$proxy->port}",
//                        'method' => 'POST',
//                        'header' => 'Content-type: application/x-www-form-urlencoded',
//                        'content' => http_build_query($params)
//                    )
//                )));
//                $result = json_decode($result, true);
//                if (!$result['response']) {
//                    $report .= "Не успешно по причине {$result['error']["error_msg"]}";
//                } else {
//                    $report .= "Успешно";
//                }
//            }
//
//            if($reg->type = AutoRegistr::INTERESTS and $reg->status)
//            {
//                $city = AutoRegistr::find()->where(['company_id' => FunctionHelper::getCompanyId()])->andWhere(['type' => AutoRegistr::SET_CITY])->one();
//                if($city != null)
//                {
//                    $city = $city->action;
//                } else {
//                    $city = null;
//                }
//                $url = 'https://api.vk.com/method/account.saveProfileInfo';
//                $params = array(
//                    'interests' => $reg->action,
//                    'city_id' => $city,
//                    'access_token' => $model->token, // Токен того кто отправляет
//                    'v' => '5.85',
//                );
//
//                // В $result вернется id отправленного сообщения
//                $result = file_get_contents($url, false, stream_context_create(array(
//                    'http' => array(
//                        'proxy' => "tcp://{$proxy->ip_adress}:{$proxy->port}",
//                        'method' => 'POST',
//                        'header' => 'Content-type: application/x-www-form-urlencoded',
//                        'content' => http_build_query($params)
//                    )
//                )));
//                $result = json_decode($result, true);
//                if (!$result['response']) {
//                    $report .= "Не успешно по причине {$result['error']["error_msg"]}";
//                } else {
//                    $report .= "Успешно";
//                }
//            }
//
//            if($reg->type = AutoRegistr::MUSIC and $reg->status)
//            {
//                $url = 'https://api.vk.com/method/account.saveProfileInfo';
//                $params = array(
//                    'music' => $reg->action,
//                    'access_token' => $model->token, // Токен того кто отправляет
//                    'v' => '5.85',
//                );
//
//                // В $result вернется id отправленного сообщения
//                $result = file_get_contents($url, false, stream_context_create(array(
//                    'http' => array(
//                        'proxy' => "tcp://{$proxy->ip_adress}:{$proxy->port}",
//                        'method' => 'POST',
//                        'header' => 'Content-type: application/x-www-form-urlencoded',
//                        'content' => http_build_query($params)
//                    )
//                )));
//                $result = json_decode($result, true);
//                if (!$result['response']) {
//                    $report .= "Не успешно по причине {$result['error']["error_msg"]}";
//                } else {
//                    $report .= "Успешно";
//                }
//            }

            // $report .= " резулт {$result['response']}";
//            if(isset($result))
//            { // Записываем результат применения
//                if (!$result['response']) {
//                    $appliedAutoRegistr = new AppliedAutoRegistr([
//                        'template_id' => $model->template_id,
//                        'dispatch_regist_id' => $model->id,
//                        'auto_registr_id' => $reg->id,
//                        'status' => AppliedAutoRegistr::STATUS_ERROR,
//                        'comment' => $result['error']['error_msg'],
//                    ]);
//                } else {
//                    $appliedAutoRegistr = new AppliedAutoRegistr([
//                        'template_id' => $model->template_id,
//                        'dispatch_regist_id' => $model->id,
//                        'auto_registr_id' => $reg->id,
//                        'status' => AppliedAutoRegistr::STATUS_DONE,
//                    ]);
//                }
//                $appliedAutoRegistr->save(false);
//
//                //if($this->templateChecker->isTemplateProcessingCompleted()){ // Если все записи шаблона применены завершаем шаблонизацию
////                    $applied = $this->getAppliedAutoRegistrs();
////                    foreach ($applied as $row){
////                        $row->delete();
////                    }
////                    $this->template_process = 0;
//                    $this->auto_view = 1;
//                    break;
////                }
//            }
//                  $this->template_process = 0;


            $report .= " <br />";

//            if($counter >= 3){
//                break;
//            }
            $counter++;
        }

        $model->auto_view = true;
        if (!$model->save()){
            echo "<pre>";
            print_r($model);
            echo "</pre>";
        }
        return $report;
    }

    /**
     * Метод получает данные из VK и пишет их в базу
     *
     * @param $model
     */
    public static function setVkInfo($model)
    {

        $acc_id = null;
        $proxy = Proxy::findOne($model->proxy);
        if($proxy == null){
            $proxy = Proxy::find()->one();
        }
        $send_proxy = "tcp://{$proxy->ip_adress}:{$proxy->port}";
        $vkData = ClientController::getUser($acc_id, 'photo_100', $model->token, $send_proxy);
        if(!isset($vkData['response'])){
            throw new HttpException(404, "Аккаунт под логином \"{$model->login}\" и паролем \"{$model->password}\", токен: \"{$model->token}\", прокси: \"$model->proxy\" не был найден. Обратитесь к администратрации");
        }
        $model->account_url = "https://vk.com/id" . $vkData['response'][0]['id'];
        $model->username = $vkData['response'][0]['first_name'] . ' ' . $vkData['response'][0]['last_name'];
        $model->photo = $vkData['response'][0]['photo_100'];
        $model->save();
        Yii::warning($vkData); //Show variable in debug panel

    }
}
