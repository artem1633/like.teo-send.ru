<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "company_chat".
 *
 * @property int $id
 * @property int $company_id
 * @property string $text
 * @property string $telegram_name
 * @property string $date_time
 * @property int $is_read
 *
 * @property Users $user
 */
class CompanyChat extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'company_chat';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id'], 'integer'],
            [['text','telegram_name'], 'string'],
            [['date_time'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Компания',
            'text' => 'Text',
            'telegram_name' => 'Имя',
            'date_time' => 'Date Time',
        ];
    }

}
