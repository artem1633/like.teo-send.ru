<?php

namespace app\models\message;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Message;

/**
 * MessageSearch represents the model behind the search form about `app\models\Message`.
 */
class MessageSearch extends Message
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'dispatch_registr_id', 'dispatch_status_id', 'dispatch_id', 'database_id', 'company_id'], 'integer'],
            [['date_time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Message::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'date_time' => $this->date_time,
            'dispatch_registr_id' => $this->dispatch_registr_id,
            'dispatch_status_id' => $this->dispatch_status_id,
            'dispatch_id' => $this->dispatch_id,
            'database_id' => $this->database_id,
            'company_id' => $this->company_id,
        ]);

        return $dataProvider;
    }
}
