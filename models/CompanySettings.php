<?php

namespace app\models;

/**
 * This is the model class for table "company_settings".
 *
 * @property int $company_id Код компании
 * @property int $time_diff Разница во времени с Мск в часах
 * @property int $notify_bstart Уведомлять до начала, мин.
 * @property int $notify_start Уведомлять о запуске рассылки после суточного лимита
 * @property int $notify_new_answer Уведомлять о новых ответах
 * @property int $notify_stop Уведомлять о прекращении рассылки
 * @property int $notify_block Уведомление о блокировки аккаунта
 * @property int $notify_limit Уведомления об истечении суточного лимита
 * @property int $messages_in_transaction Кол-во сообщений за транзакцию
 * @property int $messages_daily_limit Суточный лимит сообщений
 * @property int $send_interval Интервал отправки (мин.)
 * @property int $account_count Кол-во рассылок
 * @property int $dispatch_count Кол-во аккаунтов
 * @property int $proxy_count Кол-во прокси
 * @property int $bots_count Кол-во ботов
 * @property boolean $add_accounts Возможность добавлять аккаунты
 * @property boolean $add_bots Возможность добавлять боты
 * @property boolean $add_proxy Возможность добавлять прокси
 * @property boolean $dispatch_rules Возможность управлять рассылкой
 * @property int $affiliate_percent Процент по партнерке
 * @property string $price_message Цена за сообщение
 * @property string $sales_access_token Цена за сообщение
 * @property int $reload_time
 *
 * @property int $reloadTime
 *
 * @property Companies $companyModel
 */

class CompanySettings extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'company_settings';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id'], 'required'],
            [['sales_access_token'], 'string', 'max' => 255],
            [['company_id', 'time_diff', 'notify_bstart', 'notify_start', 'notify_new_answer',
                'notify_stop', 'notify_block', 'notify_limit', 'notify_promo', 'messages_in_transaction',
                'messages_daily_limit', 'send_interval', 'dispatch_count', 'account_count', 'proxy_count', 'bots_count',
                'affiliate_percent', 'reload_time'], 'integer'],
            [['company_id'], 'unique'],
            [['add_accounts', 'add_bots', 'add_proxy', 'dispatch_rules'], 'boolean'],
            [['price_message'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'company_id' => 'Код компании',
            'time_diff' => 'Разница во времени с Мск в часах',
            'notify_bstart' => 'Уведомлять до начала, мин.',
            'notify_start' => 'Уведомлять о запуске рассылки после суточного лимита',
            'notify_new_answer' => 'Уведомлять о новых ответах',
            'notify_stop' => 'Уведомлять о прекращении рассылки',
            'notify_block' => 'Уведомление о блокировки аккаунта',
            'notify_limit' => 'Уведомления об истечении суточного лимита',
            'notify_promo' => 'Уведомления об акциях и новостях',
            'messages_in_transaction' => 'Кол-во сообщений за транзакцию',
            'messages_daily_limit' => 'Суточный лимит сообщений',
            'send_interval' => 'Интервал отправки (мин.)',
            'dispatch_count' => 'Кол-во рассылок',
            'account_count' => 'Кол-во аккаунтов',
            'proxy_count' => 'Кол-во прокси',
            'bots_count' => 'Кол-во ботов',
            'add_accounts' => 'Возможность добавлять аккаунты',
            'add_bots' => 'Возможность добавлять боты',
            'add_proxy' => 'Возможность добавлять прокси',
            'dispatch_rules' => 'Возможность управлять рассылкой',
            'affiliate_percent' => 'Партнерский процент',
            'price_message' => 'Цена за сообщение',
            'sales_access_token' => 'Sales-Токен',
            'reload_time' => 'Время перезагрузки (ч.)',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyModel()
    {
        return $this->hasOne(Companies::className(), ['id' => 'company_id']);
    }

    /**
     * Отдает интервал перезагрзки
     * Если интервал перезагрузки нигде не указан то отдаем 24 часа
     * @return int
     */
    public function getReloadTime()
    {
        if($this->reload_time != null)
            return $this->reload_time;

        $company = $this->companyModel;

        $rate = Rates::findOne($company->rate_id);

        if($rate != null && $rate->reload_time != null)
            return $rate->reload_time;

        return 24;
    }

    /**
     * @return float|int|string
     */
    public function getPriceByMessage()
    {
        if ($this->price_message) {
            return $this->price_message;
        } else {
            $company = $this->companyModel;
            if ($company->rate && $company->rate->price_message) {
                return $company->rate->price_message;
            }
            return 1;
        }
        return 1;
    }

    /**
     * @return int|string
     */
    public function getGeneralBalans()
    {
        if ($company = $this->companyModel) {
            return $company->general_balance;
        }
        return 0;
    }

    /**
     * @return bool
     */
    public function payed()
    {
        if ($company = $this->companyModel) {
            $price = $this->getPriceByMessage();
            if($company->bonus_balance > 0){
                $company->bonus_balance -= $price;
            } else {
                $company->general_balance -= $price;
            }
            return $company->save();
        }
    }
}
