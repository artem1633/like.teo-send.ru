<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "template_messages_content".
 *
 * @property int $id
 * @property string $content Содержание
 * @property int $template_message_id Шаблон сообщения
 * @property string $created_at
 *
 * @property TemplateMessages $templateMessage
 */
class TemplateMessagesContent extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'template_messages_content';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => null,
                'value' => date('Y-m-d H:i:s'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['content'], 'required'],
            [['content'], 'string'],
            [['template_message_id'], 'integer'],
            [['created_at'], 'safe'],
            [['template_message_id'], 'exist', 'skipOnError' => true, 'targetClass' => TemplateMessages::class, 'targetAttribute' => ['template_message_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'content' => 'Текст',
            'template_message_id' => 'Шаблон сообщения',
            'created_at' => 'Создан',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTemplateMessage()
    {
        return $this->hasOne(TemplateMessages::class, ['id' => 'template_message_id']);
    }
}
