<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "companies_accounting".
 *
 * @property int $id
 * @property string $date_transaction Дата и время отчисления
 * @property int $type Тип транзакции
 * @property int $company_id ID компании
 * @property string $amount Сумма транзакции
 * @property string $description Коментарии к транзакции
 */
class CompaniesAccounting extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'companies_accounting';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date_transaction'], 'safe'],
            [['type', 'company_id'], 'required'],
            [['type', 'company_id'], 'integer'],
            [['amount'], 'number'],
            [['description'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_transaction' => 'Date Transaction',
            'type' => 'Type',
            'company_id' => 'Company ID',
            'amount' => 'Amount',
            'description' => 'Description',
        ];
    }
}
