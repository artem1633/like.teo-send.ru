<?php

namespace app\models\companies;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Companies;

/**
 * CompaniesSearch represents the model behind the search form about `app\models\Companies`.
 */
class CompaniesSearch extends Companies
{
    public $bossName;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'admin_id', 'rate_id', 'is_super', 'account_count', 'dispatch_count'], 'integer'],
            [['access_end_datetime', 'company_name', 'created_date'], 'safe'],
            [['bossName'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $forInd = null)
    {
        $query = Companies::find()
            ->alias('cm')
            ->joinWith(['admin ad', 'rate rt']);
        $this->load($params);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if (!$forInd) {
            $query->andFilterWhere(['like', 'cm.company_name', $this->company_name]);
            $query->andFilterWhere(['like', "ad.name", $this->bossName]);
            return $dataProvider;
        }
        $query->andFilterWhere([
            'cm.id' => $this->id,
            'cm.admin_id' => $this->admin_id,
            'cm.access_end_datetime' => $this->access_end_datetime,
            'cm.rate_id' => $this->rate_id,
            'cm.company_name' => $this->company_name,
            'cm.is_super' => $this->is_super,
            'cm.account_count' => $this->account_count,
            'cm.dispatch_count' => $this->dispatch_count,
            //'created_date' > $this->created_date,
        ]);

        return $dataProvider;


    }
}
