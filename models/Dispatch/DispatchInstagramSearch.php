<?php

namespace app\models\Dispatch;

use app\models\DispatchInstagram;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\DispatchRegist;

/**
 * DispatchRegistSearch represents the model behind the search form about `app\models\DispatchRegist`.
 */
class DispatchInstagramSearch extends DispatchInstagram
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'sended_message_count', 'all_sended_message_count', 'company_id', 'proxy_id'], 'integer'],
            [['username', 'account_url', 'data','last_dispatch_time', 'status',  'coment',  'login', 'password', 'blocking_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DispatchInstagram::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'data' => $this->data,
            'last_dispatch_time' => $this->last_dispatch_time,
//            'sended_message_count' => $this->sended_message_count,
            'all_sended_message_count' => $this->all_sended_message_count,
            'company_id' => $this->company_id,
//            'unread_message' => $this->unread_message,
            'proxy_id' => $this->proxy_id,
            'blocking_date' => $this->blocking_date,
        ]);

        $query->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'account_url', $this->account_url])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'coment', $this->coment])
//            ->andFilterWhere(['like', 'error_notif', $this->error_notif])
            ->andFilterWhere(['like', 'login', $this->login])
            ->andFilterWhere(['like', 'password', $this->password]);

        return $dataProvider;
    }
}
