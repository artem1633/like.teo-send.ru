<?php

namespace app\models;

use app\components\helpers\FunctionHelper;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Parser;

/**
 * ParserSearch represents the model behind the search form about `app\models\Parser`.
 */
class ParserSearch extends Parser
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'type', 'status', 'company_id', 'progress', 'count', 'age_start', 'age_end'], 'integer'],
            [['name', 'sex', 'location', 'online_period_start', 'online_period_end', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Parser::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'type' => $this->type,
            'status' => $this->status,
            'company_id' => $this->company_id,
            'progress' => $this->progress,
            'count' => $this->count,
            'age_start' => $this->age_start,
            'age_end' => $this->age_end,
            'online_period_start' => $this->online_period_start,
            'online_period_end' => $this->online_period_end,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'sex', $this->sex])
            ->andFilterWhere(['like', 'location', $this->location]);

        if(Yii::$app->user->identity->isSuperAdmin() == false){
            $query->andFilterWhere(['company_id' => FunctionHelper::getCompanyId()]);
        }

        return $dataProvider;
    }
}
