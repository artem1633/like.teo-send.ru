<?php

namespace app\models;

use app\components\helpers\FunctionHelper;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "data_recipient".
 *
 * @property int $id
 * @property string $date_added Дата и время сохранения
 * @property string $name Название базы
 * @property string $text_data Ручной ввод
 * @property string $description Описание базы
 * @property int $count Кол-во адресатов
 * @property int $type Тип базы
 * @property string $file Файл
 * @property string $file_upload Файл
 * @property int $company_id ID  компании
 * @property int $shop_id ID товара
 *
 * @property Companies $companyModel
 */
class DataRecipient extends \yii\db\ActiveRecord
{
    const TYPE_UPLOAD = 1; //Загружена
    const TYPE_PAYD = 2; //Куплена

    public $text_data;
    public $file_upload;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'data_recipient';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date_added', 'file_upload'], 'safe'],
            [['count', 'type', 'file', 'company_id'], 'required'],
            [['count', 'type', 'company_id', 'shop_id'], 'integer'],
            [['file', 'text_data'], 'string'],
            [['name', 'description'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_added' => 'Добавлена',
            'name' => 'Название',
            'description' => 'Описание',
            'count' => 'Содержит строк',
            'type' => 'Тип',
            'file' => 'Файл',
            'company_id' => 'Company ID',
            'text_data' => 'Ручной ввод',
        ];
    }

    /**
     * @return array
     */
    public static function getAllInArray()
    {
        $companies = self::find()
            ->select([
                'id' => "id",
                'name' => "concat(name, ' (', count, ' адресов)')"
            ])
            ->where(['company_id' => FunctionHelper::getCompanyId()])
            ->orderBy(['id' => SORT_DESC]);

        return ArrayHelper::map($companies->all(), 'id', 'name');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyModel()
    {
        return $this->hasOne(Companies::className(), ['id' => 'company_id']);
    }

    /**
     * @return bool
     */
    public function upload()
    {
        if (!file_exists('uploads')) {
            mkdir('uploads', 0777, true);
        }
        $name = time();
        $file = 'uploads/' . $name . '.' . $this->file_upload->extension;
        if ($this->file_upload->saveAs('uploads/' . $name . '.' . $this->file_upload->extension)) {
            $this->file = $file;
            return true;
        }
        return false;
    }
}
