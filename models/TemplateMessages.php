<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use app\components\helpers\FunctionHelper;

/**
 * This is the model class for table "template_messages".
 *
 * @property int $id
 * @property string $title Наименование
 * @property string $tag Тэг
 * @property int $company_id Компания
 * @property string $created_at
 *
 * @property Companies $company
 * @property TemplateMessagesContent[] $templateMessagesContents
 */
class TemplateMessages extends \yii\db\ActiveRecord
{
    /**
     * @var array
     */
    public $variants;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'template_messages';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        $module = Yii::$app->controller->module->id;

        if($module == 'api')
        {
            return [];
        } else {
            return [
                [
                    'class' => TimestampBehavior::class,
                    'createdAtAttribute' => 'created_at',
                    'updatedAtAttribute' => null,
                    'value' => date('Y-m-d H:i:s'),
                ],
                [
                    'class' => BlameableBehavior::class,
                    'createdByAttribute' => 'company_id',
                    'updatedByAttribute' => null,
                    'value' => FunctionHelper::getCompanyId(),
                ],
            ];
        }
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'tag'], 'required'],
            [['company_id'], 'integer'],
            [['title', 'tag'], 'string', 'max' => 255],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Companies::class, 'targetAttribute' => ['company_id' => 'id']],
            [['created_at', 'variants'], 'safe'],

            ['tag', function($attribute, $params, $validator){
                if(Yii::$app->user->identity->isSuperAdmin() == false){

                    $query = self::find()->where(['tag' => $this->$attribute])->andWhere(['company_id' => [FunctionHelper::getCompanyId(), 1]]);

                    if($this->id != null)
                    {
                        $query->andWhere(['!=', 'id', $this->id]);
                    }

                    $result = $query->one();

                    if($result != null){
                        $this->addError($attribute, 'Данный тэг уже используется');
                    }
                } else {

                    $query = self::find()->where(['tag' => $this->$attribute])->andWhere(['company_id' => [FunctionHelper::getCompanyId()]]);

                    if($this->id != null)
                    {
                        $query->andWhere(['!=', 'id', $this->id]);
                    }

                    $result = $query->one();

                    if($result != null){
                        $this->addError($attribute, 'Данный тэг уже используется');
                    }
                }
            }],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Наименование',
            'tag' => 'Тэг',
            'company_id' => 'Компания',
            'created_at' => 'Создания',
            'variants' => 'Варианты',
        ];
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        TemplateMessagesContent::deleteAll(['template_message_id' => $this->id]);
        foreach ($this->variants as $variant)
        {
            $content = new TemplateMessagesContent([
                'template_message_id' => $this->id,
                'content' => $variant,
            ]);
            $content->save(false);
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Companies::class, ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTemplateMessagesContents()
    {
        return $this->hasMany(TemplateMessagesContent::class, ['template_message_id' => 'id']);
    }
}
