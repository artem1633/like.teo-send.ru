<?php

namespace app\models;

use app\components\NeyroBot;
use Yii;

/**
 * This is the model class for table "dispatch_status".
 *
 * @property int $id
 * @property int $account_id
 * @property string $status
 * @property string $data
 * @property string $link
 * @property int $company_id
 * @property int $dispatch_id Ид отправленного сообщении
 * @property string $photo URL фотографии
 * @property string $name Имя пользователя
 *
 * @property Dispatch $dispatch
 * @property Statuses $statusModel
 */
class DispatchStatus extends \yii\db\ActiveRecord
{

    const EVENT_CHANGED_STATUS = 'changed_status';

    /**
     * @const Статус "В обработке"
     */
    const STATUS_HANDLE = 'handle';

    /**
     * @const Статус "В ожидании"
     */
    const STATUS_WAIT = "wait";

    /**
     * @const Статус "В работе"
     */
    const STATUS_JOB = "job";

    /**
     * @const Статус "Завершен"
     */
    const STATUS_FINISH = "finish";

    /**
     * @const Статус "Есть непрочитанное сообщение"
     */
    const STATUS_UNREAD_MESSAGE = "unread_message";

    /**
     * @const Статус "Переписка"
     */
    const STATUS_CONVERSATION = "conversation";

    /**
     * @const Статус "Аккаунт заблокирован"
     */
    const STATUS_ACCOUNT_BLOCKED = "account_blocked";

    /**
     * @const Статус "Отклонен"
     */
    const STATUS_REFUSED = "refused";


    const IS_READ = 'yes';
    public $dialog;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dispatch_status';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dispatch_id'], 'exist', 'skipOnError' => true, 'targetClass' => Dispatch::className(), 'targetAttribute' => ['dispatch_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ИД',
            'account_id' => 'ИД получателя',
            'status' => 'Статус',
            'data' => 'Дата отправки',
            'dispatch_id' => 'Ид сообщении',
            'photo' => 'URL фотографии',
            'name' => 'Имя',
            'link' => 'Ссылка',
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDispatch()
    {
        return $this->hasOne(Dispatch::className(), ['id' => 'dispatch_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatusModel()
    {
        return $this->hasOne(Statuses::className(), ['id' => 'status']);
    }
}
