<?php

namespace app\models\bots;

use app\models\BotsDialogs;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * UsersSearch represents the model behind the search form about `app\models\Users`.
 */
class DialogSearch extends BotsDialogs
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['botId', 'stepNumber', 'notify'], 'integer'],
            [['answerSet'], 'safe'],
            [['result'], 'string', 'max' => 250],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * @param $botId
     * @return ActiveDataProvider
     */
    public function search($botId)
    {
        $query = BotsDialogs::find()->where(['botId' => $botId]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

         return $dataProvider;
    }
}
