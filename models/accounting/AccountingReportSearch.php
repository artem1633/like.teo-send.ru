<?php

namespace app\models\accounting;

use app\components\helpers\FunctionHelper;
use app\models\AccountingReport;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * UsersSearch represents the model behind the search form about `app\models\Users`.
 */
class AccountingReportSearch extends AccountingReport
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_report'], 'safe'],
            [['company_id', 'operation_type', 'amount'], 'required'],
            [['company_id', 'operation_type'], 'integer'],
            [['amount'], 'number'],
            [['description'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        if (Yii::$app->user->identity->isSuperAdmin()) {
            $where = [
                'OR',
                ['operation_type' => AccountingReport::TYPE_INCOME_BALANCE],
                ['operation_type' => AccountingReport::TYPE_DISPATCH_PAYED],
                ['operation_type' => AccountingReport::TYPE_OUTPUT_AFFILIATE],
                ['operation_type' => AccountingReport::TYPE_OUTPUT_SHOP],
                ['operation_type' => AccountingReport::TYPE_SHOP_PAYED],
                ['operation_type' => AccountingReport::TYPE_PARSER],
                ['operation_type' => AccountingReport::TYPE_PARSER_OBSERVE]
            ];
        } else {
            $where = [
                'AND',
                ['company_id' => FunctionHelper::getCompanyId()],
                ['OR',
                    ['operation_type' => AccountingReport::TYPE_INCOME_BALANCE],
                    ['operation_type' => AccountingReport::TYPE_DISPATCH_PAYED],
                    ['operation_type' => AccountingReport::TYPE_OUTPUT_AFFILIATE],
                    ['operation_type' => AccountingReport::TYPE_OUTPUT_SHOP],
                    ['operation_type' => AccountingReport::TYPE_SHOP_PAYED],
                    ['operation_type' => AccountingReport::TYPE_PARSER],
                    ['operation_type' => AccountingReport::TYPE_PARSER_OBSERVE]
                ]
            ];
        }

        $query = self::find()
            ->alias('report')
            ->joinWith(['companyModel company']);
        $query->where($where);

        $this->load($params);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }


        $query->andFilterWhere(['like', 'report.date_report', $this->date_report]);
        $query->andFilterWhere(['like', 'report.operation_type', $this->operation_type]);
        $query->andFilterWhere(['like', 'company.company_name', $this->company_id]);

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchDebit($params)
    {
        if (Yii::$app->user->identity->isSuperAdmin()) {
            $where = [
                'OR',
                ['operation_type' => AccountingReport::TYPE_INCOME_BALANCE],
            ];
        } else {
            $where = [
                'AND',
                ['company_id' => FunctionHelper::getCompanyId()],
                ['OR',
                    ['operation_type' => AccountingReport::TYPE_INCOME_BALANCE],
                ]
            ];
        }

        $query = self::find()
            ->alias('report')
            ->joinWith(['companyModel company']);
        $query->where($where);

        $this->load($params);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }


        $query->andFilterWhere(['like', 'report.date_report', $this->date_report]);
        $query->andFilterWhere(['like', 'report.operation_type', $this->operation_type]);
        $query->andFilterWhere(['like', 'company.company_name', $this->company_id]);

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchCredit($params)
    {
        if (Yii::$app->user->identity->isSuperAdmin()) {
            $where = [
                'OR',
                ['operation_type' => AccountingReport::TYPE_DISPATCH_PAYED],
                ['operation_type' => AccountingReport::TYPE_OUTPUT_AFFILIATE],
                ['operation_type' => AccountingReport::TYPE_OUTPUT_SHOP],
                ['operation_type' => AccountingReport::TYPE_PARSER],
                ['operation_type' => AccountingReport::TYPE_PARSER_OBSERVE],
//                ['operation_type' => AccountingReport::TYPE_SHOP_PAYED]
            ];
        } else {
            $where = [
                'AND',
                ['company_id' => FunctionHelper::getCompanyId()],
                ['OR',
                    ['operation_type' => AccountingReport::TYPE_DISPATCH_PAYED],
                    ['operation_type' => AccountingReport::TYPE_OUTPUT_AFFILIATE],
                    ['operation_type' => AccountingReport::TYPE_OUTPUT_SHOP],
                    ['operation_type' => AccountingReport::TYPE_PARSER],
                    ['operation_type' => AccountingReport::TYPE_PARSER_OBSERVE],
//                    ['operation_type' => AccountingReport::TYPE_SHOP_PAYED]
                ]
            ];
        }

        $query = self::find()
            ->alias('report')
            ->joinWith(['companyModel company']);
        $query->where($where);

        $this->load($params);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }


        $query->andFilterWhere(['like', 'report.date_report', $this->date_report]);
        $query->andFilterWhere(['like', 'report.operation_type', $this->operation_type]);
        $query->andFilterWhere(['like', 'company.company_name', $this->company_id]);

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchShop($params)
    {
        if (Yii::$app->user->identity->isSuperAdmin()) {
            $where = [
                'OR',
                ['operation_type' => AccountingReport::TYPE_SHOP_PAYED]
            ];
        } else {
            $where = [
                'AND',
                ['company_id' => FunctionHelper::getCompanyId()],
                ['OR',
                    ['operation_type' => AccountingReport::TYPE_SHOP_PAYED]
                ]
            ];
        }

        $query = self::find()
            ->alias('report')
            ->joinWith(['companyModel company']);
        $query->where($where);

        $this->load($params);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }


        $query->andFilterWhere(['like', 'report.date_report', $this->date_report]);
        $query->andFilterWhere(['like', 'report.operation_type', $this->operation_type]);
        $query->andFilterWhere(['like', 'company.company_name', $this->company_id]);

        return $dataProvider;
    }
}
