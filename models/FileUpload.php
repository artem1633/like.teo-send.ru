<?php
/**
 * Created by PhpStorm.
 * User: Abbos
 * Date: 8/3/2018
 * Time: 6:53 PM
 */

namespace app\models;


use yii\base\Model;
use yii\web\UploadedFile;

class FileUpload extends Model
{
    public $file;

    public function upload()
    {

        if($this->validate()){
            $path = 'upload/store/' . $this->file->baseName . '.' . $this->file->extension;
            $this->file->saveAs($path);
            $this->attachImage($path,true);
            @unlink($path);
            return true;
        }else{
            return false;
        }
    }

}