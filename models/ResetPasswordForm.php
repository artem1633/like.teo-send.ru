<?php

namespace app\models;

use Yii;
use yii\base\Model;

class ResetPasswordForm extends Model
{
    public $login;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['login'], 'required'],
            [['login'], 'exist', 'targetClass' => '\app\models\User'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'login' => 'Логин',
        ];
    }

    /**
     * @return Users|null
     */
    public function reset()
    {
       if($this->validate())
       {
           /** @var \app\models\Users $user */
           $user = Users::find()->where(['login' => $this->login])->one();
           $newPassword = str_pad(rand(0, 99999), 6, '0', STR_PAD_LEFT);
           $user->password = md5($newPassword);

           try{
               Yii::$app->mailer->compose()
                   ->setFrom('zvonki.crm@mail.ru')
                   ->setTo($user->login)
                   ->setSubject('Изменение пароля')
                   ->setHtmlBody('Временный пароль для авторизации: '.$newPassword)
                   ->send();
           } catch (\Exception $e){

           }
          /* $user->save();
           echo "<pre>";
           print_r($user->errors);
           echo "</pre>";
           die;*/
           return $user->save() ? $user : null;
       }

       return null;
    }
}