<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\web\UploadedFile;

/**
 * This is the model class for table "slider".
 *
 * @property int $id
 * @property string $header Заголовок
 * @property string $content Текст
 * @property string $image_path Путь до изображения
 * @property string $image_alt Alt атрибут изображения
 * @property int $sort Порядок показа
 * @property int $enable Вкл/Выкл
 * @property string $created_at
 */
class Slider extends \yii\db\ActiveRecord
{

    public $file;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'slider';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => null,
                'value' => date('Y-m-d H:i:s'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['header'], 'required'],
            [['content'], 'string'],
            [['sort', 'enable'], 'integer'],
            [['created_at'], 'safe'],
            [['header', 'image_path', 'image_alt'], 'string', 'max' => 255],
            [['file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'header' => 'Заголовок',
            'content' => 'Тест',
            'image_path' => 'Изображение',
            'image_alt' => 'Alt',
            'sort' => 'Сортировка',
            'enable' => 'Включен',
            'created_at' => 'Создан',

            'file' => 'Изображение',
        ];
    }
}
