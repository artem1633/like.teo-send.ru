<?php

namespace app\models;

use app\components\helpers\FunctionHelper;
use app\models\Automation;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * DispatchSearch represents the model behind the search form about `app\models\Dispatch`.
 */
class AutomationSearch extends Automation
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'company_id'], 'integer'],
            [['name', 'text', 'status', 'platform', ], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Automation::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'company_id' => $this->company_id,
            'platform' => $this->platform,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['=', 'platform', $this->platform]);

        if(Yii::$app->user->identity->isSuperAdmin() == false){
            $query->andFilterWhere(['company_id' => FunctionHelper::getCompanyId()]);
        }

        return $dataProvider;
    }
}
