<?php

namespace app\models;

use app\components\helpers\FunctionHelper;
use app\controllers\DispatchController;
use app\modules\api\controllers\ClientController;
use app\modules\api\controllers\VkController;
use app\validators\DeniedWordsValidator;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "dispatch".
 *
 * @property int $id
 * @property string $name
 * @property string $status
 * @property int $company_id
 * @property int $client_id Клиент
 * @property string $link
 * @property string $text
 * @property string $group_id
 * @property string $platform
 * @property int $type
 * @property int $mission Цель
 * @property int $date_time Цель
 * @property int $last_post_id Последний пост
 * @property string date_start
 * @property string date_end
 * @property integer sum
 *
 * @property integer $likesCount
 * @property integer $postsCount
 *
 * @property Clients $client
 * @property Companies $company
 * @property Dispatch[] $dispatches
 */
class Automation extends \yii\db\ActiveRecord
{
    const STATUS_JOB = 'job';
    const STATUS_WAIT = 'wait';

    /**
     * @var string
     */
    public $dates;

    /**
     * @var string
     */
    public $count;

    private $_likesCount;
    private $_postsCount;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'automation';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'date_time',
                'updatedAtAttribute' => null,
                'value' => date('Y-m-d H:i:s'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [

            [['link', 'type', 'mission'], 'required'],
            [['group_id', 'text', 'link', 'platform'], 'string'],
            [['company_id', 'client_id', 'sum'], 'integer'],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Companies::class, 'targetAttribute' => ['company_id' => 'id']],
            [['mission'], 'integer', 'min' => 1, 'max' => 1000, 'message' => 'Пока доступно только'],
            [['name', 'status', 'type'], 'string'],
            [['dates', 'date_start', 'date_end'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ИД',
            'name' => 'Название',
            'status' => 'Статус',
            'client_id' => 'Клиент',
            'date_time' => 'Дата',
            'type' => 'Тип',
            'link' => 'Ссылка на группу',
            'mission' => 'Кол-во',
            'last_post_id' => 'Дата и время последнего действия',
            'group_id' => 'Id  группы',
            'text' => 'Текст',
            'platform' => 'platform',
            'dates' => 'Дата создания и завершения',
            'date_start' => 'Дата начала',
            'date_end' => 'Дата завершения',
            'sum' => 'Сумма',
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function beforeSave($insert)
    {
        if($this->company_id == null){
            $this->company_id = FunctionHelper::getCompanyId();
            $this->status = 'job';
        }

        if($this->dates != null){
            $this->dates = explode(' - ', $this->dates);
            $this->date_start = $this->dates[0];
            $this->date_end = $this->dates[1];
        }

        return parent::beforeSave($insert);
    }

    /**
     * @return array
     */
    public static function getTypesList()
    {
        return [
            'like_new_cont' => 'Лайки новых постов',
            'rep_new_cont' => 'Репосты новых постов',
            'com_new_cont' => 'Комментари новых постов',
        ];
    }

    /**
     * @return array
     */
    public static function getStatusesList()
    {
        return [
            self::STATUS_JOB => 'В работе',
            self::STATUS_WAIT => 'Ожидание',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Clients::class, ['id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Companies::class, ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDispatches()
    {
        return $this->hasMany(Dispatch::class, ['automation_id' => 'id']);
    }

    /**
     * @return integer
     */
    public function getLikesCount()
    {
        if($this->_likesCount == null){
            $this->_likesCount = Dispatch::find()->where(['automation_id' => $this->id])->sum('mission');
        }

        return $this->_likesCount;
    }

    /**
     * @return integer
     */
    public function getPostsCount()
    {
        if($this->_postsCount == null){
            $this->_postsCount = Dispatch::find()->where(['automation_id' => $this->id])->count();
        }

        return $this->_postsCount;
    }
}
