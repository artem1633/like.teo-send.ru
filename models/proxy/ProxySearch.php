<?php

namespace app\models\proxy;

use app\models\Proxy;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * UsersSearch represents the model behind the search form about `app\models\Users`.
 */
class ProxySearch extends Proxy
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['port'], 'integer'],
            [['name', 'status', 'companyId', 'login', 'password'], 'string', 'max' => 255],
            [['ip_adress'], 'string', 'max' => 15],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $company = null)
    {
        $query = self::find()
            ->alias('proxy')
            ->joinWith(['companyModel company']);
        if ($company) {
            $query->where(['companyId' => $company]);
        }
        $this->load($params);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'proxy.name', $this->name]);
        $query->andFilterWhere(['like', 'proxy.ip_adress', $this->ip_adress]);
        $query->andFilterWhere(['like', 'proxy.name', $this->name]);
        $query->andFilterWhere(['like', 'company.company_name', $this->companyId]);
        $query->andFilterWhere(['like', 'proxy.login', $this->login]);
        $query->andFilterWhere(['like', 'proxy.password', $this->password]);

        return $dataProvider;
    }
}
