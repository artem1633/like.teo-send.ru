<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "messages_statistics".
 *
 * @property int $id
 * @property string $content Сообщение
 * @property int $dispatch_id Рассылка
 * @property int $dispatch_regist_id Аккаунт
 * @property int $company_id Компания
 * @property string $created_at
 *
 * @property Companies $company
 * @property Dispatch $dispatch
 * @property DispatchRegist $dispatchRegist
 */
class MessagesStatistics extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'messages_statistics';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => null,
                'value' => date('Y-m-d H:i:s'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['content'], 'string'],
            [['dispatch_id', 'dispatch_regist_id', 'company_id'], 'integer'],
            [['created_at'], 'safe'],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Companies::class, 'targetAttribute' => ['company_id' => 'id']],
            [['dispatch_id'], 'exist', 'skipOnError' => true, 'targetClass' => Dispatch::class, 'targetAttribute' => ['dispatch_id' => 'id']],
            [['dispatch_regist_id'], 'exist', 'skipOnError' => true, 'targetClass' => DispatchRegist::class, 'targetAttribute' => ['dispatch_regist_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'content' => 'Content',
            'dispatch_id' => 'Dispatch ID',
            'dispatch_regist_id' => 'Dispatch Regist ID',
            'company_id' => 'Company ID',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Companies::class, ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDispatch()
    {
        return $this->hasOne(Dispatch::class, ['id' => 'dispatch_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDispatchRegist()
    {
        return $this->hasOne(DispatchRegist::class, ['id' => 'dispatch_regist_id']);
    }
}
