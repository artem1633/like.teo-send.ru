<?php

namespace app\models;

use app\components\helpers\FunctionHelper;
use app\controllers\DispatchController;
use app\modules\api\controllers\ClientController;
use app\modules\api\controllers\LikeController;
use app\modules\api\controllers\VkController;
use app\validators\DeniedWordsValidator;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "dispatch".
 *
 * @property int $id
 * @property string $name
 * @property string $status
 * @property string $status_job
 * @property string $account_id
 * @property string $data
 * @property int $company_id
 * @property int $automation_id Автозадача
 * @property int $chat
 * @property string $days_week
 * @property string $start_time
 * @property string $link
 * @property boolean $to_stat
 * @property string $text_data
 * @property string $text
 * @property string $group_id
 * @property string $platform
 * @property string $login
 * @property string $pass
 * @property int $proxy_id
 * @property int $select_data
 * @property int $type
 * @property int $mission Цель
 * @property int $action_mission Кол-во действий
 * @property int $all_action_mission Всего Кол-во действий
 * @property int $last_mission_time Дата последнего дейстия
 * @property int $distribute
 * @property int $distribute_count
 * @property int $like_count
 *
 * @property AppliedAutoRegistr[] $appliedAutoRegistrs
 * @property Automation $automation
 * @property Proxy $proxy
 * @property DispatchConnect[] $dispatchConnects
 * @property DispatchStatus[] $dispatchStatuses
 * @property MessagesStatistics[] $messagesStatistics
 * @property UniqueMessage[] $uniqueMessages
 */
class Dispatch extends \yii\db\ActiveRecord
{
    const USERS_SESSION_KEY = 'groupUsers';
    //Тэги
    const TAG_DISPATCH_NAME = '{%dispatchName%}'; //Наименование раcсылки
    const TAG_MESSAGE_SENDER = '{%msgSender%}'; //Отправитель сообщения
    const TAG_MESSAGE_RECIPIENT = '{%msgRecipient%}'; //Получатель сообщения
    const TAG_CITY_RECIPIENT = '{%cityRecipient%}'; //Город получателя сообщения
    const TAG_BASE_NAME = '{%baseName%}'; //Наименование базы.

    const PLATFORM_VK = 'vk'; //Наименование базы.
    const PLATFORM_INSTAGRAM = 'instagram'; //Наименование базы.
    const PLATFORM_YOUTUBE = 'youtube'; //Наименование базы.

    public $file;
    public $filename;
    public $accounts;
    public $view;
    public $chat;
    public $text_data;
    public $select_data;
    public $recived_data;
    public $selected_dispatches;
    public $selected_statuses;
    public $base_name;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dispatch';
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if ($this->days_week != null && is_array($this->days_week)) {
            $this->days_week = implode($this->days_week, ',');
        }

        return parent::beforeSave($insert);
    }



    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [

            [['base_name', 'selected_statuses','selected_dispatches', 'data', 'chat', 'days_week', 'recived_data', 'last_mission_time'], 'safe'],
            [['file'], 'file', 'extensions' => 'txt'],
            [['text_data', 'link', 'group_id', 'text', 'platform', 'text', 'login', 'pass'], 'string'],
            [[ 'company_id', 'select_data', 'action_mission', 'all_action_mission', 'proxy_id', 'like_count'], 'integer'],
            [['mission'], 'integer', 'min' => 1, 'max' => 1000, 'message' => 'Пока доступно только'],
            [['distribute_count'], 'integer', 'min' => 1, 'max' => 50, 'message' => 'Пока доступно только'],
            [['name', 'status', 'status_job', 'start_time', 'type'], 'string'],
            [['to_stat','distribute'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ИД',
            'name' => 'Название',
            'status' => 'Статус',
            'status_job' => 'Статус работы',
            'account_id' => 'Ид отправителя',
            'data' => 'Дата',
            'view' => 'просмотр',
            'accounts' => 'Акк для рассылки',
            'file' => 'Файл с базой формате txt',
            'chat' => 'Куда отправлять уведомления',
            'days_week' => 'Дни недели',
            'start_time' => 'Время начала',
            'to_stat' => 'Отображать в показателях',
            'text_data' => 'База получателей',
            'select_data' => 'Сохраненная база',
            'base_name' => 'Сборная база',
            '$selected_statuses' => 'Выбранные статусы',
            '$selected_dispatches' => 'Выбранные расслки',
            'type' => 'Тип',
            'link' => 'Ссылка',
            'mission' => 'Кол-во акк(доступно 1000)',
            'action_mission' => 'Кол-во дейсвий',
            'all_action_mission' => 'Всего Кол-во дейсвий',
            'last_mission_time' => 'Дата и время последнего действия',
            'group_id' => 'Ссылка на страницу автора',
            'text' => 'Текст',
            'platform' => 'Платформа',
            'login' => 'Логин',
            'pass' => 'Пароль',
            'proxy_id' => 'Прокси',
            'distribute' => 'Распределить по последним постам',
            'distribute_count' => 'Кол-во постов (max 50)',
            'like_count' => 'Кол-во лайков',
        ];
    }


    /**
     * @return bool
     */
    public function upload()
    {
        if ($this->validate()) {
            if (!file_exists('uploads')) {
                mkdir('uploads', 0777, true);
            }
            $name = time();
            $this->file->saveAs('uploads/' . $name . '.' . $this->file->extension);
            $this->filename = 'uploads/' . $name . '.' . $this->file->extension;
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return int
     */
    public function getMaxCountDispatch()
    {

        $company = Companies::findOne(FunctionHelper::getCompanyId());
        $rate = Rates::findOne($company->rate_id);
        return ($settings = FunctionHelper::getCompanySettings()) ? $settings->dispatch_count : $rate->dispatch_count;

    }

    /**
     * @return int|string
     */
    public function getCountDispatch()
    {
        return self::find()->where(['company_id' => FunctionHelper::getCompanyId()])->count();
    }

    /**
     * Метод получает список пользователей группы
     *
     * @param string $urlGroup Ссыылка на группу
     * @return array Индексный массив с ID пользователей
     */
    public static function getGroupUsers($urlGroup)
    {
        $groupId = self::getGroupId($urlGroup);
        $proxy_server = Settings::find()->where(['key' => 'proxy_server'])->one()->value;

        FunctionHelper::checkProxy($proxy_server) ? $message = 'Прокси сервер доступен' : $message = 'Прокси сервер недоступен';

        Yii::warning($message, __METHOD__);

        $params['group_id'] = $groupId;

        $users = ClientController::request('groups.getMembers', "tcp://{$proxy_server}", $params);

        return $users['response']['items'];
    }

    /**
     * Метод получает список пользователей группы
     *
     * @param string $urlGroup Ссыылка на группу
     * @return array Индексный массив с ID пользователей
     */
    public static function getGroupUsersCount($urlGroup)
    {
        $groupId = self::getGroupId($urlGroup);
        $proxy_server = Settings::find()->where(['key' => 'proxy_server'])->one()->value;

        FunctionHelper::checkProxy($proxy_server) ? $message = 'Прокси сервер доступен' : $message = 'Прокси сервер недоступен';

        Yii::warning($message, __METHOD__);

        $params['group_id'] = $groupId;

        $users = ClientController::request('groups.getMembers', "tcp://{$proxy_server}", $params);

        return $users['response']['count'];
    }

    /**
     * @param int $id Код группы
     * @param string $fields Запрашиваемые поля. Значения, разделенные запятыми.
     * @return array
     */
    public static function getGroupInfoById($id, $fields)
    {

        $proxy_server = Settings::find()->where(['key' => 'proxy_server'])->one()->value;

        $params['group_id'] = $id;
        $params['fields'] = $fields;

        $groupInfo = ClientController::request('groups.getById', $proxy_server, $params);
        return $groupInfo['request'];


    }

    /**
     * Метод возвращает ифнормацию о группе
     *
     * @param string $urlGroup URL адрес группы
     * @param string $fields Запрашиваемые поля. Значения, разделенные запятыми.
     * @return mixed
     */
    public static function getGroupInfoByUrl($urlGroup, $fields)
    {
        $id = self::getGroupId($urlGroup);

        $proxy_server = Settings::find()->where(['key' => 'proxy_server'])->one()->value;

        $params['group_id'] = $id;
        $params['fields'] = $fields;

        $groupInfo = ClientController::request('groups.getById', $proxy_server, $params);

        return $groupInfo['response'];

    }

    /**
     * Метод парсит URL группы и возвращает id или screen_name группы
     *
     * @param string $urlGroup URL адрес группы
     * @return string
     */
    public static function getGroupId($urlGroup)
    {
        //Парсим имя группы
        if (strpos($urlGroup, 'com/public') > 0) {
            //имя группы = int
            $groupId = mb_substr($urlGroup, mb_strpos($urlGroup, 'com/public') + 10);
        } else {
            //имя группы произвольная строка
            $groupId = mb_substr($urlGroup, mb_strrpos($urlGroup, '/') + 1);
        }
        return $groupId;
    }


    /**
     * @return array
     */
    public static function getTypesList()
    {
        return [
            'like' => 'Лайки + Просмотры',
            'rep' => 'Репосты + Лайк + Просмотры',
//            'view' => 'Просмотры',
            'in' => 'Вступления',
            'com' => 'Комментарии',
            //           'like_new_cont' => 'Лайки новых постов',
            //           'rep_new_cont' => 'Репосты новых постов',
            //           'com_new_cont' => 'Комментари новых постов',
        ];
    }

    /**
     * @return array
     */
    public static function getTypesListInstagram()
    {
        return [
            'ins_view' => 'Просмотр чужих сторис',
            'ins_like' => 'Лайк поста',
            'ins_video' => 'Просмотры видео',
        ];
    }


    /**
     * Метод парсит URL группы и возвращает id или screen_name группы
     *
     * @param Dispatch $model URL адрес группы
     * @return string
     */
    public static function getListNewPost ($model)
    {
        $token = Settings::find()->where(['key' => 'vk_access_token'])->one();
        $token = $token->value;

        $setingProxy = Settings::find()->where(['key' => 'proxy_server'])->one();
        $proxy = $setingProxy->value;
        if (!$model->group_id){
            ClientController::sendTelMessage('247187885','getListNewPost error');
            return false;
        }
        $params = array(
            'owner_id' => '-'.$model->group_id,   // Что отправляем
            'count' => $model->distribute_count,   // Что отправляем
            'access_token' => $token, // Токен того кто отправляет
            'v' => '5.95',
        );

        $result = LikeController::request('wall.get', $proxy, $params);

        for ($i = 0; $i < $model->distribute_count; $i++) {
            $a[] = "{$model->link}?w=wall-{$model->group_id}_".$result['response']['items'][$i]['id'];
        }
        return $a;
    }

    /**
     * Метод парсит URL группы и возвращает id или screen_name группы
     *
     * @param Dispatch $model Model
     * @param array $list URL адрес группы
     * @return string
     */
    public static function getNewMission ($model, $list)
    {
        for ($i = 0; $i < count($list); $i++){
            $dispatch = new Dispatch([
                'name' => $list[$i],
                'link' => $list[$i],
                'status' => $model->status,
                'status_job' => $model->status_job,
                'group_id' => $model->group_id,
                'data' =>  $model->data,
                'type' => $model->type,
                'mission' => $model->mission,
                'text' => $model->text,
                'platform' => $model->platform,
                'company_id' => $model->company_id,
            ]);
            $dispatch->save();
        }

        ClientController::sendTelMessage('247187885', 'Запустили расспределение');

        return true;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAutomation()
    {
        return $this->hasOne(Automation::class, ['id' => 'automation_id']);
    }
}
