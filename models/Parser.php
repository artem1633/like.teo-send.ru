<?php

namespace app\models;

use app\components\helpers\FunctionHelper;
use app\components\NeyroBot;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "parser".
 *
 * @property int $id
 * @property string $name Название
 * @property int $type Тип
 * @property int $status Статус
 * @property int $company_id Компания
 * @property int $progress Прогрес
 * @property string $list Список
 * @property string $link Ссылка
 * @property int $count Кол-во
 * @property int $sex Пол
 * @property int $age_start Возраст (Начальный)
 * @property int $age_end Возраст (Конечный)
 * @property string $location Месторасположение
 * @property string $online_period_start Пользователь в сети (Старт)
 * @property string $online_period_end Пользователь в сети (Конец)
 * @property string $last_observe_pay_datetime Дата и время последний оплаты за отслеживание
 * @property string $created_at
 *
 * @property Companies $company
 */
class Parser extends \yii\db\ActiveRecord
{
    const SEX_FEMALE = 0;
    const SEX_MALE = 1;

    public $ages;
    public $onlinePeriod;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'parser';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => null,
                'value' => date('Y-m-d H:i:s'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'type'], 'required'],
            [['type', 'status', 'company_id', 'progress', 'count', 'sex', 'age_start', 'age_end'], 'integer'],
            [['onlinePeriod', 'online_period_start', 'online_period_end', 'created_at'], 'safe'],
            [['list'], 'string'],
            [['name', 'location', 'link'], 'string', 'max' => 255],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Companies::class, 'targetAttribute' => ['company_id' => 'id']],
            ['status', 'default', 'value' => 0],

            ['link', function($attribute){
                $count = Dispatch::getGroupUsersCount($this->link);

                if($count > 50000){
                    $this->addError($attribute, 'Превышает кол-во подпищиков (Максимальное значение: 50.000)');
                }
            }],

            ['type', function($attribute){
                $company = FunctionHelper::getCompanyModel();
                if($this->$attribute == 0){
                    $price = Settings::findByKey('parser_observe_price')->value;
                    if($company->general_balance < $price){
                        $this->addError($attribute, 'Недостаточно средств');
                    }
                } else {
                    $price = Settings::findByKey('parser_price')->value;
                    if($company->general_balance < $price){
                        $this->addError($attribute, 'Недостаточно средств');
                    }
                }
            }],

            ['list', function($attribute){
                $lines = explode("\n", $this->$attribute);

                $counter = 0;

                foreach ($lines as $line) {
                    if($counter >= 5000){
                        $this->addError($attribute, 'Объем базы не может превышать более 5000 строк');
                        break;
                    }
                    $counter++;
                }

                $this->list = '';
                foreach ($lines as $line) {

                    if(stripos($line, 'vk.com/'))
                    {
                        if(stripos($line, 'vk.com/id'))
                        {
                            $line = explode('vk.com/id', $line)[1];
                        } else {
                            $line = explode('vk.com/', $line)[1];
                        }
                    }

                    $this->list .= "{$line},";
                }
            }],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'type' => 'Тип',
            'status' => 'Статус',
            'company_id' => 'Компания',
            'progress' => 'Прогресс',
            'count' => 'Собранно',
            'sex' => 'Пол',
            'list' => 'Список',
            'link' => 'Ссылка',
            'age_start' => 'Возраст (начало)',
            'age_end' => 'Возраст (Конец)',
            'location' => 'Местоположение',
            'onlinePeriod' => 'Время онлайна',
            'online_period_start' => 'Начало онлайна',
            'online_period_end' => 'Конец оклайна',
            'created_at' => 'Дата и время создания',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function beforeSave($insert)
    {
        if($this->link != null){
            if(stripos($this->link, 'vk.com/'))
            {
                if(stripos($this->link, 'vk.com/id'))
                {
                    $this->link = explode('vk.com/id', $this->link)[1];
                } else {
                    $this->link = explode('vk.com/', $this->link)[1];
                }
            }
        }

        if($this->company_id == null){
            $this->company_id = FunctionHelper::getCompanyId();
        }

        if($this->onlinePeriod != null){
            $this->onlinePeriod = explode(' - ', $this->onlinePeriod);
            $this->online_period_start = $this->onlinePeriod[0];
            $this->online_period_end = $this->onlinePeriod[1];
        }



        if($this->isNewRecord)
        {
            if($this->type == 0)
            {
                $observePrice = Settings::findByKey('parser_observe_price')->value;

                $company = FunctionHelper::getCompanyModel();

                if($company->general_balance >= $observePrice){
                    $company->general_balance -= $observePrice;
                    $company->save(false);

                    $report = new AccountingReport([
                        'operation_type' => AccountingReport::TYPE_PARSER_OBSERVE,
                        'company_id' => $company->id,
                        'amount' => $observePrice,
                        'description' => "Слежение парсера «{$this->name}»"
                    ]);
                    $report->save(false);

                    $this->last_observe_pay_datetime = date('Y-m-d H:i:s');
                } else {

                }
            } else {
                $price = Settings::findByKey('parser_price')->value;

                $company = FunctionHelper::getCompanyModel();

                if($company->general_balance >= $price){
                    $company->general_balance -= $price;
                    $company->save(false);

                    $report = new AccountingReport([
                        'operation_type' => AccountingReport::TYPE_PARSER,
                        'company_id' => $company->id,
                        'amount' => $price,
                        'description' => "Парсер «{$this->name}»"
                    ]);
                    $report->save(false);
                } else {

                }
            }
        }

        return parent::beforeSave($insert);
    }

    /**
     * {@inheritdoc}
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($insert) {
            $output = NeyroBot::sendParser([
                'name' => $this->name,
                'link' => $this->link,
                'list' => $this->list,
                'outer_tracking_group_id' => $this->id,
                'company_id' => $this->company_id,
                'type' => $this->type,
                'gender' => $this->sex,
                'age_min' => $this->age_start,
                'age_max' => $this->age_end,
                'country' => $this->location,
                'city' => $this->location,
                'stay_network_start' => $this->online_period_start,
                'stay_network_end' => $this->online_period_end,
            ]);
        }

    }

    /**
     * {@inheritdoc}
     */
    public function afterDelete()
    {
        parent::afterDelete();

        NeyroBot::deleteParser($this->id);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Companies::class, ['id' => 'company_id']);
    }

    /**
     * @return array
     */
    public static function getTypesList()
    {
        return [
            0 => 'Новые вступившие',
            1 => 'Чистка',
            2 => 'Собрать участников',
        ];
    }

    /**
     * @return array
     */
    public static function getStatusList()
    {
        return [
            0 => 'В работе',
            1 => 'Готово',
        ];
    }

    /**
     * @return array
     */
    public static function getSexList()
    {
        return [
            self::SEX_FEMALE => "Ж",
            self::SEX_MALE => "М",
        ];
    }
}
