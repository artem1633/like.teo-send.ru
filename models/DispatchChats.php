<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dispatch_chats".
 *
 * @property int $id
 * @property int $dispatchId ID рассылки
 * @property int $chatId ID чата
 */
class DispatchChats extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dispatch_chats';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dispatchId', 'chatId'], 'required'],
            [['dispatchId', 'chatId'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'dispatchId' => 'Dispatch ID',
            'chatId' => 'Chat ID',
        ];
    }
}
