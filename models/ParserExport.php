<?php

namespace app\models;

use app\components\NeyroBot;
use yii\base\Model;

/**
 * Class ParserExport
 * @package app\models
 */
class ParserExport extends Model
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $period;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['period', 'id'], 'required'],
            [['period'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'period' => 'Период',
        ];
    }

    /**
     * @return boolean
     */
    public function export()
    {
       if($this->validate()){
           $this->period = explode(' - ', $this->period);
           NeyroBot::exportParser($this->id, ['start' => $this->period[0], 'end' => $this->period[1]]);

           $content = file_get_contents('http://neyro.teo-bot.ru/export.txt');

           $f = fopen('export2.txt', 'w');

           fwrite($f, $content);

           fclose($f);

           return true;
       }

       return false;
    }

}