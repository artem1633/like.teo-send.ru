<?php

namespace app\models\uploads;

use yii\base\Model;
use yii\helpers\FileHelper;
use app\models\Shop;

class UploadItem extends Model
{

    public $fileItem;

    public function rules()
    {
        return [
            [['fileItem'], 'file', 'extensions' => 'png, jpg', 'maxFiles' => 1],
        ];
    }

    public function upload($id)
    {
        if ($this->validate()) {
            $pathFile = 'shop/items/';

            if (FileHelper::createDirectory($pathFile, 0775)) {
                if ($this->fileItem->saveAs($pathFile . $this->fileItem->baseName . '.' . $this->fileItem->extension)) {
                    $modelItem = Shop::findOne($id);
                    $modelItem->image = 'shop/items/'. $this->fileItem->baseName . '.' . $this->fileItem->extension;
                    $modelItem->save();
                }
            }
            return true;
        } else {
            return false;
        }
    }

}