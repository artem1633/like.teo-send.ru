<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Application;

/**
 * ApplicationSearch represents the model behind the search form about `app\models\Application`.
 */
class ApplicationSearch extends Application
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'atelier_id', 'creator', 'product_id', 'count', 'status_id'], 'integer'],
            [['date_cr'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $post)
    {
        $query = Application::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'date_cr' => $this->date_cr,
            //'atelier_id' => $this->atelier_id,
            'creator' => $this->creator,
            //'product_id' => $this->product_id,
            'count' => $this->count,
            'status_id' => $this->status_id,
        ]);
        $query->andFilterWhere(['like', 'product_id', $post['Application']['search_product'] ]);
        $query->andFilterWhere(['like', 'atelier_id', $post['Application']['search_atelier'] ]);

        return $dataProvider;
    }
}
