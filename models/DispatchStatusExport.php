<?php

namespace app\models;

use yii\base\Model;
use yii\helpers\ArrayHelper;

/**
 * Class DispatchStatusExport
 * @package app\models
 */
class DispatchStatusExport extends Model
{
    /**
     * @var $dispatchId
     */
    public $dispatchId;

    /**
     * @var array
     */
    public $statuses;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dispatchId', 'statuses'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'dispatchId' => 'Рассылка',
            'statuses' => 'Статусы',
        ];
    }

    /**
     * @return string
     */
    public function export()
    {
        if($this->validate())
        {
            $dispatchStatus = DispatchStatus::find()->where(['dispatch_id' => $this->dispatchId]);
            $orConditions = [];
            foreach ($this->statuses as $status)
            {
                if(stristr($status, 's')){
                    $status = substr($status, 1);
                    $orConditions[] = ['send' => $status];
                } else {
                    $orConditions[] = ['status' => $status];
                }
            }
            $dispatchStatus->andFilterWhere(ArrayHelper::merge(['or'], $orConditions));

            $models = $dispatchStatus->all();

            $filePath = 'export.txt';

            $file = fopen('export.txt', 'w');

            foreach ($models as $model)
            {
                fwrite($file,$model->default_account_id."\n");
            }

            fclose($file);

            return $filePath;
        }
    }

}