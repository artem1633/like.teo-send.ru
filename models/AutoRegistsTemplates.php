<?php

namespace app\models;

use app\components\helpers\FunctionHelper;
use Yii;

/**
 * This is the model class for table "auto_regists_templates".
 *
 * @property int $id
 * @property string $name Наименование шаблона
 * @property int $company_id Компания
 *
 * @property AutoRegistr[] $autoRegistrs
 * @property Companies $company
 */
class AutoRegistsTemplates extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'auto_regists_templates';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Companies::className(), 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function beforeSave($insert)
    {
        if($this->isNewRecord)
        {
            $this->company_id = FunctionHelper::getCompanyId();
        }

        return parent::beforeSave($insert);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'company_id' => 'Company ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAutoRegistrs()
    {
        return $this->hasMany(AutoRegistr::className(), ['template_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Companies::className(), ['id' => 'company_id']);
    }
}
