<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "rates".
 *
 *
 * @property int $id
 * @property string $name Наименование
 * @property double $price Стоимость тарифа
 * @property double $price_message Стоимость сообщения
 * @property double $time Период действия лицензии с момента ее получения (в секундах)
 * @property double $sort Сортировка
 * @property string $affiliate_percent Партнерский процент
 * @property int $messages_in_transaction Кол-во сообщений за транзакцию
 * @property int $messages_daily_limit Суточный лимит сообщений
 * @property int $send_interval Интервал отправки (мин.)
 * @property int $account_count Кол-во аккаунтов
 * @property int $dispatch_count Кол-во рассылок
 * @property int $proxy_count Кол-во прокси
 * @property int $bots_count Кол-во ботов
 * @property boolean $add_accounts Возможность добавлять аккаунты
 * @property boolean $add_bots Возможность добавлять боты
 * @property boolean $add_proxy Возможность добавлять прокси
 * @property boolean $dispatch_rules Возможность управлять рассылкой
 * @property string $default Тариф по умолчанию
 * @property string $bonus Бонус при регистрации
 * @property int $reload_time
 *
 * @property Companies[] $companies
 */
class Rates extends \yii\db\ActiveRecord
{
    const IS_DEFAULT = 1;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rates';
    }

    public function beforeSave($insert)
    {
        if ($this->default)
        {
            $this->clearDefault();
            $this->default = self::IS_DEFAULT;
        }

        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'price', 'time'], 'required'],
            [['price', 'price_message', 'time', 'sort', 'bonus'], 'number'],
            [['name'], 'string', 'max' => 255],
            [['messages_in_transaction', 'messages_daily_limit', 'send_interval', 'dispatch_count', 'account_count', 'proxy_count', 'bots_count', 'affiliate_percent', 'default', 'reload_time'], 'integer'],
            [['add_accounts', 'add_bots', 'add_proxy', 'dispatch_rules'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'price' => 'Стоимость тарифа',
            'price_message' => 'Стоимость сообщения',
            'time' => 'Время',
            'sort' => 'Сортировка',
            'messages_in_transaction' => 'Кол-во сообщений за транзакцию',
            'messages_daily_limit' => 'Суточный лимит сообщений',
            'send_interval' => 'Интервал отправки (мин.)',
            'account_count' => 'Кол-во аккаунтов',
            'dispatch_count' => 'Кол-во рассылок',
            'proxy_count' => 'Кол-во прокси',
            'bots_count' => 'Кол-во ботов',
            'add_accounts' => 'Возможность добавлять аккаунты',
            'add_bots' => 'Возможность добавлять боты',
            'add_proxy' => 'Возможность добавлять прокси',
            'dispatch_rules' => 'Возможность управлять рассылкой',
            'affiliate_percent' => 'Процент по партнерке',
            'bonus' => 'Бонус при регистрации',
            'default' => 'Тариф по умолчанию',
            'reload_time' => 'Время перезагрузки (ч.)',
        ];
    }

    /**
     * @return array
     */
    public static function getTimeList()
    {
        return [
            '604800' => 'Неделя',
            '1209600' => '2 недели',
            '1814400' => '3 недели',
            '2419200' => '1 месяц',
            '4838400' => '2 месяца',
            '7257600' => '3 месяца',
            '14515200' => '6 месяцов',
            '29030400' => '1 год',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanies()
    {
        return $this->hasMany(Companies::className(), ['rate_id' => 'id']);
    }

    /**
     * @return bool
     */
    public function clearDefault() {
        $defaults = self::find()->where(['!=', 'default', 0])->all();
        if ($defaults) {
            foreach ($defaults as $default) {
                $default->default = 0;
                $default->save();
            }
        }

        return null;
    }

    /**
     * @return int
     */
    public static function getDefaultRate()
    {
        if ($rate = self::findOne(['default' => self::IS_DEFAULT])) {
            return $rate->id;
        } else {
            return 1;
        }
    }
}
