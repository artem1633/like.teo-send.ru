<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * Class RegisterForm
 * @package app\models
 *
 * @property float $company_id
 * @property float $amount
 */
class HandBalance extends Model
{
    public $company_id;
    public $amount;

    /**
     * @param $attribute
     * валидатор суммы
     */
    public function notNull ( $attribute ) {
        if($this->amount <= 0){
            $this->addError ( $attribute, "Неверная сумма" );
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'amount'], 'required'],
            [['amount'], 'notNull'],
            [['company_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'company_id' => 'Компания',
            'amount' => 'Сумма зачисления',
        ];
    }

    /**
     * @return bool|null
     */
    public function save()
    {
        if ($this->validate() === false) {
            return null;
        }
        $company = Companies::findOne($this->company_id);
        $company->general_balance += floatval($this->amount);
        if ($company->save()) {
            $report = new AccountingReport([
                'company_id' => $this->company_id,
                'operation_type' => AccountingReport::TYPE_INCOME_BALANCE,
                'amount' => floatval($this->amount),
                'description' => 'Поплнение основного счета.'
            ]);
            $report->save();
        } else {
            return false;
        }

        $referal = Companies::findOne($company->referal);
        $settings = CompanySettings::findOne(['company_id' => $company->referal]);
        if ($referal && $settings) {
            $persent = ($this->amount / 100 * floatval($settings->affiliate_percent));
            $referal->affiliate_amount += $persent;
            if ($referal->save()) {
                $reportRef = new AccountingReport([
                    'company_id' => $referal->id,
                    'operation_type' => AccountingReport::TYPE_INCOME_AFFILIATE,
                    'amount' => $persent,
                    'description' => 'Партнерские отчисления от компании ' . $company->company_name,
                ]);
                $reportRef->save();
                $reportAff = new AffiliateAccounting([
                    'company_id' => $referal->id,
                    'referal_id' => $company->id,
                    'amount' => $persent,
                ]);
                $reportAff->save();
            }
            else {
                return false;
            }
        }
        return true;
    }

}