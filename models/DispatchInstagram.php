<?php

namespace app\models;

use app\components\helpers\FunctionHelper;
use app\components\TemplateChecker;
use app\modules\api\controllers\ClientController;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\HttpException;

/**
 * This is the model class for table "dispatch_regist".
 *
 * @property int $id
 * @property string $username Пользователь
 * @property string $account_url Ссылка на аккаунт
 * @property string $data Время подключение
 * @property string $token токен
 * @property string $last_dispatch_time Дата и время последней рассылки
 * @property string $login Логин
 * @property string $password Пароль
 * @property string $coment Пароль
 * @property string $status Пароль
 * @property string $blocking_date Дата блокировки
 * @property int $all_sended_message_count Общее кол-во отправленных сообщений
 * @property int $sended_message_count Общее кол-во отправленных сообщений
 * @property int $company_id ID компании
 * @property string $proxy_id Прокси
 * @property integer $proxy_blocked Прокси при котором был заблокирован аккаунт
 *
 * @property Companies $companyModel
 * @property Proxy $proxyBlocked
 * @property AutoRegistsTemplates $template
 * @property AppliedAutoRegistr[] $appliedAutoRegistrs
 */
class DispatchInstagram extends \yii\db\ActiveRecord
{

    /**
     * @const Статус аккаунта "Спящий". Означает что аккаунт не используется
     * уже продолжительное время
     */
    const STATUS_SLEEPING = 'sleeping';

    /**
     * @const Статус аккаунта "Заблокирован". Означает что аккаунт заблокирован
     */
    const STATUS_BLOCKED = 'account_blocked';

    /**
     * @var
     */
    public $token_id;

    /**
     * @var TemplateChecker
     */
    public $templateChecker;

    /**
     * DispatchRegist constructor.
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        $this->templateChecker = new TemplateChecker(['model' => $this]);
        parent::__construct($config);
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dispatch_instagram';
    }

    /**
     * {@inheritdoc}
     */
    public function beforeSave($insert)
    {
        if (!$this->isNewRecord) {
            $this->company_id = 1;
        }

//        if($this->template_id != null){
//            $this->auto_view = 0;
//        }

        if($this->isNewRecord)
        {
            $this->last_dispatch_time = date('Y-m-d H:i:s');
        }

        return parent::beforeSave($insert);
    }


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['data', 'last_dispatch_time', 'blocking_date'], 'safe'],
            [['company_id',  'proxy_id'], 'integer'],
            [['username', 'account_url', 'coment', 'login', 'password'], 'string', 'max' => 255],
            [['proxy_blocked'], 'exist', 'skipOnError' => true, 'targetClass' => Proxy::className(), 'targetAttribute' => ['proxy_blocked' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ИД',
            'username' => 'ФИО',
            'account_url' => 'Ссылка на аккаунт',
            'data' => 'Дата подключения',
            'last_dispatch_time' => 'Дата и время последней рассылки',
            'status' => 'Статус',
            'busy' => 'Занятость',
            'coment' => 'Коммент',
            'login' => 'Логин',
            'password' => 'Пароль',
            'blocking_date' => 'Дата блокировки',
            'proxy_id' => 'Прокси',
            'proxy_blocked' => 'Заблокированый прокси',
            'company_id' => 'Компания',
        ];
    }

    /**
     * @return array
     */
    public function getDispatchList()
    {
        $dispatchs = Companies::find()->where(['is_super' => 0])->all();
        return ArrayHelper::map($dispatchs, 'id', 'company_name');
    }

    /**
     * @return array
     */
    public function DispatchRegistList()
    {
        $dispatch_regist = Companies::find()->all();
        $result = [];

        foreach ($dispatch_regist as $value) {
            $result [] = $value->id;
        }
        return $result;
    }

    /**
     * Меняет статус аккаунта на «Спящий»
     */
    public function makeSleep()
    {
        $this->status = self::STATUS_SLEEPING;

        $this->save();
    }

    /**
     * Меняет статус аккаунта на «Заблокирован»
     * @param string $comment
     */
    public function makeBlock($comment = null)
    {
        $this->status = self::STATUS_BLOCKED;
        $this->blocking_date = date('Y-m-d'); //Запись даты блокировки аккаунта
        $this->coment = $comment;
//        var_dump($this->proxy);
//        exit;

        $report = new DailyReport([
            'company_id' => $this->company_id,
            'account_id' => $this->id,
            'comments' => 'Блокировка акк',
            'type' => DailyReport::TYPE_BLOCK_DISPATCH_REGIST,
        ]);
        $report->save(false);

        $this->save(false);
    }

    /**
     * @return int
     */
    public function getMaxCountAccounts()
    {
        $company = Companies::findOne(FunctionHelper::getCompanyId());
        $rate = Rates::findOne($company->rate_id);
        return ($settings = FunctionHelper::getCompanySettings()) ? $settings->account_count : $rate->account_count;
    }

    /**
     * @return int|string
     */
    public function getCountAccounts()
    {
        return self::find()->where(['company_id' => FunctionHelper::getCompanyId()])->count();
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getFreeAccounts()
    {
        return self::find()->where(['busy' => true])->all();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyModel()
    {
        return $this->hasOne(Companies::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProxyBlocked()
    {
        return $this->hasOne(Proxy::className(), ['id' => 'proxy_blocked']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDispatchs()
    {
        return $this->hasMany(DispatchConnect::className(), ['dispatch_regist_id' => 'id']);
    }

    /**
     * Возвращает один доступный прокси
     * @return array|null|\yii\db\ActiveRecord|\yii\db\ActiveRecord[]
     */
    public function getOneAvailableProxy()
    {
        $proxy = Proxy::getAvailableProxy();
        if(count($proxy) > 0)
        {
            $proxy = array_shift($proxy);
            return $proxy;
        }

//        var_dump($proxy);
//        exit;

        $proxy = Proxy::getAvailableProxy(true);
        if(count($proxy) > 0)
        {
            $proxy = array_shift($proxy);
            return $proxy;
        }

        return null;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTemplate()
    {
        return $this->hasOne(AutoRegistsTemplates::className(), ['id' => 'template_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDispatchStatus()
    {
        return $this->hasOne(DispatchStatus::className(), ['send_account_id' => 'id']);
    }

    /**
     * @return bool
     */
    public function getTemplateProcessing()
    {
        return $this->template_process == 1 ? true : false;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAppliedAutoRegistrs()
    {
        return $this->hasMany(AppliedAutoRegistr::className(), ['dispatch_regist_id' => 'id']);
    }

}
