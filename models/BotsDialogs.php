<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "bots_dialogs".
 *
 * @property int $id
 * @property int $botId
 * @property int $stepNumber Номер ответа
 * @property string $result Наш ответ
 * @property string $answerSet Наш ответ
 * @property int $notify Уведомление
 * @property bool $anyAnswer Любой ответ
 * @property bool $check_search Точный поиск
 * @property int $delayMin Количество минут до отправки сообщения
 * @property int $message_sent Дата и время отправки последнего сообщения
 *
 * @property BotsAnswers $answersModel Модель возможных вариантов
 */
class BotsDialogs extends \yii\db\ActiveRecord
{

    public $answerSet;
    const START_STEP_NUMBER = 1;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bots_dialogs';
    }

    public function evenNumber($attribute)
    {
        if (($this->stepNumber % 2) == 1) {
            $this->addError($attribute, "Только четное число");
        }
    }


    /**
     * @param bool $insert
     * @param array $changedAttributes
     * @return bool
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);


        if ($this->answerSet != null) {
            foreach ($this->answerSet as $item) {
                $modelAnswer = new BotsAnswers([
                    'dialogId' => $this->id,
                    'tag' => $item,
                ]);
                $modelAnswer->save();
            }
        }
        if (!$this->isNewRecord && !$this->answerSet && $this->answersModel) {
            foreach ($this->answersModel as $item) {
                $item->delete();
            }
        }


        return parent::beforeSave($insert);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['botId', 'stepNumber', 'result', 'notify'], 'required'],
            [['delayMin', 'botId', 'notify', 'status'], 'integer'],
            [['stepNumber'], 'evenNumber'],
            [['message_sent', 'answerSet'], 'safe'],
            [['anyAnswer','check_search'], 'boolean'],
            [['result'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'botId' => 'ID Бота',
            'stepNumber' => 'Номер сообщения',
            'answerSet' => 'Возможные ответы пользователя',
            'result' => 'Наш ответ',
            'notify' => 'Уведомлять об ответах',
            'anyAnswer' => 'Любой ответ пользователя',
            'status' => 'Статус',
            'delayMin' => 'Время ожидания перед отправкой сообщения',
            'message_sent' => 'Дата и время отправки последнего сообщения',
            'check_search' => 'Точный поиск',
        ];
    }

    /**
     * @return mixed
     */
    public function getNextStepNumber()
    {
        if (($model = self::find()->where(['botId' => $this->botId])->orderBy(['stepNumber' => SORT_DESC])->one()) !== null) {
            return $model->stepNumber + 1;
        } else {
            return self::START_STEP_NUMBER;
        }
    }

    /**
     * @param null $str
     * @return array|string
     */
    public function getAnswers($str = null)
    {
        $answers = BotsAnswers::find()->where(['dialogId' => $this->id])->asArray()->all();
        $answers = ArrayHelper::getColumn($answers, 'tag');
        return $str ? implode($answers, '; ') : $answers;
    }

    /**
     *
     */
    public function saveStep()
    {
        $transaction = Yii::$app->db->beginTransaction();

        if ($this->save()) {
            $transaction->commit();
        } else {
            $transaction->rollBack();
        }
    }

    /**
     * @param $id
     * @return array|bool|\yii\db\ActiveRecord[]
     */
    public static function getStepsByBot($id)
    {
        if ($model = self::find()->where(['botId' => $id])->all()) {
            return $model;
        }
        return false;
    }

    /**
     * @param $id
     * @return bool|mixed
     */
    public static function getGreatestStep($id)
    {
        if ($model = self::find()->where(['botId' => $id])->orderBy(['stepNumber' => SORT_DESC])->one()) {
            return $model->stepNumber;
        }
        return false;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBotModel()
    {
        return $this->hasOne(Bots::className(), ['id' => 'botId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnswersModel()
    {
        return $this->hasMany(BotsAnswers::className(), ['dialogId' => 'id']);
    }
}
