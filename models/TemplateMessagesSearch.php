<?php

namespace app\models;

use app\components\helpers\FunctionHelper;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * TemplateMessagesSearch represents the model behind the search form about `app\models\TemplateMessages`.
 */
class TemplateMessagesSearch extends TemplateMessages
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'company_id'], 'integer'],
            [['title', 'tag', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TemplateMessages::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'tag', $this->tag]);

        if(Yii::$app->user->identity->isSuperAdmin() == false)
        {
            $query->andWhere(['company_id' => FunctionHelper::getCompanyId()]);
        }

        return $dataProvider;
    }
}
