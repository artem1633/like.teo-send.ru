<?php

namespace app\bootstrap;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;

class AppBootstrap implements \yii\base\BootstrapInterface
{
    public function bootstrap($app)
    {
        $actionColumn = [
            'contentOptions' => ['style' => 'white-space: nowrap;'],
            'buttons' => [
                'view' => function ($url, $model) {
                    $url = Url::to([$url, 'id' => $model->id]);
                    return Html::a('<span class="fa fa-eye"></span>', $url, [
                        'class' => 'btn btn-primary btn-xs',
                        'role' => 'modal-remote',
                        'title' => 'Просмотр',
                        'data-toggle' => 'tooltip'
                    ]);
                },
                'update' => function ($url, $model) {
                    $url = Url::to([$url, 'id' => $model->id]);
                    return Html::a('<span class="fa fa-pencil-square-o"></span>', $url, [
                        'class' => 'btn btn-success btn-xs',
                        'role' => 'modal-remote',
                        'title' => 'Изменить',
                        'data-toggle' => 'tooltip'
                    ]);
                },
                'delete' => function ($url, $model) {
                    $url = Url::to([$url, 'id' => $model->id]);
                    return Html::a('<span class="fa fa-trash-o"></span>', $url, [
                        'class' => 'btn btn-danger btn-xs',
                        'role' => 'modal-remote', 'title' => 'Удалить',
                        'data-confirm' => false, 'data-method' => false,// for overide yii data api
                        'data-request-method' => 'post',
                        'data-toggle' => 'tooltip',
                        'data-confirm-title' => 'Подтвердите действие',
                        'data-confirm-message' => 'Вы уверены что хотите удалить этого элемента?',
                    ]);
                },
            ],
        ];

        Yii::$container->set('kartik\grid\ActionColumn', $actionColumn);

        Yii::$container->set('kartik\grid\GridView', ['responsiveWrap' => false]);
    }
}