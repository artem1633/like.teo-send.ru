<?php

namespace app\assets\pages;

use yii\web\AssetBundle;

/**
 * Class ChatPageAsset
 * @package app\assets\pages
 */
class ChatPageAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'css/chat.css'
    ];

    public $depends = [
        'app\assets\AppAsset',
    ];
}
