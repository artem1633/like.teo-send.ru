<?php

namespace app\assets\plugins;

use yii\web\AssetBundle;

/**
 * Class MagnificPopupAsset
 * @package app\assets\plugins
 */
class MagnificPopupAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'js/plugins/magnificPopup/magnific-popup.css'
    ];

    public $js = [
        'js/plugins/magnificPopup/jquery.magnific-popup.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'app\assets\AppAsset',
    ];
}