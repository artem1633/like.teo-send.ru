<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Class AppAsset
 * @package app\assets
 */
class PopperAsset extends AssetBundle
{
    public $sourcePath = '@bower/popper.js';
//    public $basePath = '@webroot';
//    public $baseUrl = '@web';

    public $css = [
        //'css/adminlte.css',
    ];

    public $js = [
        'dist/popper.js',
        'dist/popper-utils.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
