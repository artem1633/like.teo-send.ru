<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TemplateMessagesContent */

?>
<div class="template-messages-content-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
