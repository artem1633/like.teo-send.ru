<?php
use Yii,
    yii\helpers\Html,
    yii\helpers\Url,
    yii\bootstrap\Tabs;


$this->title = $model->name;
$remains = $model->getRemains();

$this->params['breadcrumbs'][] = ['label' => $this->title];
?>


<div class="panel">

    <div class="panel-body">
        <form id="form-<?= $model->id ?>">
            <input type="hidden" name="item" value="<?= $model->id ?>">
            <div class="row"  style="padding-left: 20px">
                <div class="col-xs-12">
                    <h2><?= $model->name ?></h2>
                    <h3 class="text-success js-price-<?= $model->id ?>">
                        <?= number_format($model->price, 2, ',', ' ') ?>
                        <i class="fa fa-rub"></i>
                        <br/>
                        <span style="font-size: 12px" class="js-alert-<?= $model->id ?>"></span>
                    </h3>
                    <div>
                        В наличии: <span id="countItem"><?= $remains ?></span>
                    </div>
                    <h4 style="padding: 30px 0px"> <?= $model->description_short ?></h4>
                    <div class="row">
                        <div class="col-md-2">
                            <?= Html::input('hidden', 'count', 1, ['class' => 'js-count form-control', 'min' => 1, 'max' => 1, 'step' => 1]); ?>
                        </div>
                        <div class="col-md-2 item" data-key="<?= $model->id ?>" data-name="<?=$model->name?>" data-price="<?=$model->price?>">
                            <?= Html::button('Купить сейчас', ['class' => 'btn btn-success js-buy']); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="padding: 30px 20px; margin-top: 50px;">
                <?php
                echo \yii\bootstrap\Tabs::widget([
                    'items' => [
                        [
                            'label' => 'Описание',
                            'content' => '<div style="padding: 20px 10px">' . $model->description_long . '</div>',
                            'active' => true,
                        ],
                        [
                            'label' => 'Дополнительная информация',
                            'content' => '<div style="padding: 20px 10px">' . $model->additional_info . '</div>',
                        ],
                    ]
                ]); ?>
            </div>
        </form>
    </div>
</div>

<?php

$buyUrl = Url::to(['/trade/buy']);

if($model->type != \app\models\Shop::TYPE_ACCOUNT_DATA){
    $script = <<< JS
    $(function () {
        $('.item').on('click', '.js-buy', function () {
            var id = $(this).closest('div').data('key');
            var name = $(this).closest('div').data('name');
            var price = $(this).closest('div').data('price');
            var count = $('.js-count').val();
            price = price*count;
            var data = $('#form-' + id).serialize();

            if (confirm("Купить " + count + " " + name + " за " + price + " рублей?")) {
                $.ajax({
                    url: '{$buyUrl}',
                    type: 'POST',
                    data: data,
                    dataType: 'json',
                    beforeSend: function () {
                        $('#background-wait').show();
                    },
                    success: function (responseData) {
                       $('#background-wait').hide();
                       if (json.buy) {
                           location.reload()
                       } else {
                           $('.js-alert-' + id).html(json.reason);
                           $('.js-alert-' + id).removeClass('text-success').addClass('text-danger');
                           $('.js-price-' + id).removeClass('text-success').addClass('text-danger');
                       }
                    }
                });
            }
        });
    });
JS;
} else {
    $script = "
        $(function () {
            $('.item').on('click', '.js-buy', function () {
                var id = $(this).closest('div').data('key');
                var name = $(this).closest('div').data('name');
                var price = $(this).closest('div').data('price');
                var count = $('.js-count').val();
                price = price*count;
                var data = $('#form-' + id).serialize();
    
                if (confirm('Купить ' + count + ' ' + name + ' за ' + price + ' рублей?')) {
                    $.ajax({
                        url: '{$buyUrl}',
                        type: 'POST',
                        data: data,
                        xhrFields: {
                            responseType: 'blob'
                        },
                        beforeSend: function () {
                            $('#background-wait').show();
                        },
                        success: function (responseData, textStatus, xhr) {
                            var a = document.createElement('a');
                            var url = window.URL.createObjectURL(responseData);
                            a.href = url;
                            a.download = 'Аккаунты.txt';
                            a.click();
                            window.URL.revokeObjectURL(url);   
                        },
                        error: function(xhr, textStatus, errorThrown){
                            if(xhr.status == 402)
                            {
                                alert('Недостаточно средств');
                            }
                            if(xhr.status == 400)
                            {
                                alert('Товара нет в наличии');
                            }
                        },
                    });
                }
            });
        });
    ";
}

$this->registerJs($script, \yii\web\View::POS_READY);

?>