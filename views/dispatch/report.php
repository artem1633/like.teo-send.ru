<?php
//$this->title = "Отчет";
use \app\models\Dispatch;
use \app\models\DispatchStatus;
use \app\models\DispatchRegist;

//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default" style="margin-top: 20px;">
            <div class="panel-heading">
                <div class="panel-title">Отчет</div>
            </div>
            <div class="panel-body">
                <table class="table">
                    <thead class="thead-dark">
                    <tr>
                        <th>Название Рассылок</th>
                        <th>Всего людей</th>
                        <th>Отправленно</th>
                        <th>Заинтерисован</th>
                        <th>отказался</th>
                    </tr>
                    </thead>
                    <?php if(isset($stts) && $stts != null):?>
                        <tbody>
                        <?php foreach ($stts as $status):?>
                            <tr class="success">
                                <td><?php $a = Dispatch::findOne(['id'=> $status->dispatch_id]); echo $a->name;?></td>
                                <td><?= DispatchStatus::find()->where(['dispatch_id' => $status->dispatch_id])->count()?></td>
                                <td><?= DispatchStatus::find()->where(['send' => true,'dispatch_id' => $status->dispatch_id])->count() ?>
                                <td><?php echo DispatchStatus::find()
                                        ->where('status != :status and status IS NOT NULL and dispatch_id = :dispatch_id',
                                            ['status' => 4,'dispatch_id' => $status->dispatch_id])->count() ?></td>
                                <td><?php echo DispatchStatus::find()->where(['status' => 4,'dispatch_id' => $status->dispatch_id])->count()?> </td>
                            </tr>
                        <?php endforeach;?>
                        </tbody>

                    <?php else:?>

                        <div class="alert alert-info alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <p>Пусто....</p>
                        </div>

                    <?php endif;?>
                </table>

            </div>
        </div>
    </div>
</div>

