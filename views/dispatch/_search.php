<?php

use Yii;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\Dispatch;
use app\models\DispatchStatus;
use app\models\Companies;
use kartik\select2\Select2;

?>

<?php $form = ActiveForm::begin(['id' => 'search-form', 'method' => 'GET']) ?>
<?php if(Yii::$app->user->identity->isSuperAdmin()): ?>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($searchModel, 'name')->textInput() ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($searchModel, 'status')->dropDownList([
                DispatchStatus::STATUS_HANDLE => 'В обработке',
                'wait' => 'Готов к запуску',
                'job' => 'В работе',
                'finish' => 'Завершено',
            ], ['prompt' => 'Выберите статус']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($searchModel, 'company_id')->widget(Select2::class, [
                'data' => ArrayHelper::map(Companies::find()->all(), 'id', 'company_name'),
                'options' => [
                    'placeholder' => 'Поиск',
                    'allowClear' => true,
                ],
            ]) ?>
        </div>
    </div>
<?php else: ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($searchModel, 'name')->textInput() ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($searchModel, 'status')->dropDownList([
                DispatchStatus::STATUS_HANDLE => 'В обработке',
                'wait' => 'Готов к запуску',
                'job' => 'В работе',
                'finish' => 'Завершено',
            ], ['prompt' => 'Выберите статус']) ?>
        </div>
    </div>
<?php endif; ?>
<?php ActiveForm::end() ?>