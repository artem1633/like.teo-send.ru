<?php
use app\models\Users;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $platform string */
/* @var $dataProvider yii\data\ActiveDataProvider */

CrudAsset::register($this);

$this->title = 'Накрутки';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="dispatch-index">

    <?php

    $content = '';

    $content .=
        Html::a('Добавить накрутку <i class="glyphicon glyphicon-plus"></i>', ['create', 'platform' => $platform],
            ['role' => 'modal-remote', 'title' => 'Создать новую накрутку', 'class' => 'btn btn-info', 'style' => 'margin-left: 50px;']);


    if(Yii::$app->user->identity->isSuperAdmin()){
        $content .= Html::a('Калькулятор', ['calculation'],
                ['data-pjax' => '0', 'class' => 'btn btn-warning']) . $content;
    }

    ?>


    <div class="panel panel-success <?= isset($_GET['DispatchSearch']) ? '' : 'panel-toggled' ?>" style="margin-bottom: 10px;">
        <div class="panel-heading">
            <div class="panel-title">Фильтры</div>
            <ul class="panel-controls">
                <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
            </ul>
        </div>
        <div class="panel-body">
            <?= $this->render('_search', ['searchModel' => $searchModel]) ?>
        </div>
        <div class="panel-footer">
            <?= Html::a('<i class="fa fa-search"></i>Поиск', ['#'], [
                'class' => 'btn btn-warning pull-right',
                'onclick' => 'event.preventDefault(); $("#search-form").submit();',
            ]) ?>
            <?= Html::a('Сброс', ['index?DispatchSearch%5Bname%5D='], [
                'class' => 'btn btn-primary pull-right',
                'style' => 'margin-right: 7px;',
            ]) ?>
        </div>
    </div>

    <div id="ajaxCrudDatatable">
        <?= GridView::widget([
            'id' => 'crud-datatable',
            'dataProvider' => $dataProvider,
            //'filterModel' => $searchModel,
            'pjax' => true,
            'responsiveWrap' => false,
            'columns' => require(__DIR__ . '/_columns.php'),
            'toolbar' => [
                ['content' =>$content],
            ],
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'toolbarContainerOptions' => ['style' => 'margin-top: 15px;'],
            'panel' => [
                'type' => 'default',
                'heading' => Yii::$app->session->hasFlash('danger') || Yii::$app->session->hasFlash('warning')
                    ? '<i class="glyphicon glyphicon-list"></i> '
                    . ' Внимание! ' . Yii::$app->session->getFlash('danger')
                    : '<i class="glyphicon glyphicon-list"></i> ' . $this->title,
                'after' => BulkButtonWidget::widget([
                        'buttons' => Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; Delete All',
                            ["bulk-delete"],
                            [
                                "class" => "btn btn-danger btn-xs",
                                'role' => 'modal-remote-bulk',
                                'data-confirm' => false, 'data-method' => false,// for overide yii data api
                                'data-request-method' => 'post',
                                'data-confirm-title' => 'Ты уверен?',
                                'data-confirm-message' => 'Вы действительно хотите удалить этот элемент'
                            ]),
                    ]) .
                    '<div class="clearfix"></div>',
            ]
        ]) ?>
    </div>
</div>
<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "",// always need it for jquery plugin
]) ?>


<?php Modal::end(); ?>
