<?php

use app\models\Dispatch;
use yii\helpers\ArrayHelper;
use yii\helpers\Html,
    app\components\helpers\FunctionHelper,
    yii\widgets\ActiveForm,
    kartik\select2\Select2,
    kartik\datecontrol\DateControl;

/* @var $this yii\web\View */
/* @var $model app\models\Dispatch */
/* @var $form yii\widgets\ActiveForm */

$help_text = Dispatch::TAG_DISPATCH_NAME . " - Наименование раcсылки <br>" .
                Dispatch::TAG_MESSAGE_SENDER . " - Отправитель сообщения <br>".
                Dispatch::TAG_MESSAGE_RECIPIENT . " - Получатель сообщения <br>".
                Dispatch::TAG_CITY_RECIPIENT . " - Город получателя сообщения <br>".
                Dispatch::TAG_BASE_NAME . " - Наименование базы";

?>

<div class="dispatch-form row">
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

    <div class="first-step">
        <!--        Наименование-->
        <div class="col-md-12">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>

        <!--        Текст-->

        <div class="col-md-12">

            <div class="panel panel-default tabs">

                <ul class="nav nav-tabs" role="tablist">
                    <li class="active"><a href="#txt-tab1" data-toggle="tab"><i class="fa fa-file-text-o"
                                                                                aria-hidden="true"></i></a></li>
                    <li><a href="#txt-tab2" data-toggle="tab"><i class="fa fa-file-text-o" aria-hidden="true"></i></a></li>
                    <li><a href="#txt-tab3" data-toggle="tab"><i class="fa fa-file-text-o" aria-hidden="true"></i></a></li>
                    <li><a href="#txt-tab4" data-toggle="tab"><i class="fa fa-file-text-o" aria-hidden="true"></i></a></li>
                </ul>
                <div class="panel-body tab-content">
                    <div class="tab-pane active" id="txt-tab1">
                        <?= $form->field($model, 'text1')->textarea(['rows' => 10, 'cols' => 10]) ?>
                    </div>
                    <div class="tab-pane" id="txt-tab2">
                        <?= $form->field($model, 'text2')->textarea(['rows' => 10, 'cols' => 10]) ?>
                    </div>
                    <div class="tab-pane" id="txt-tab3">
                        <?= $form->field($model, 'text3')->textarea(['rows' => 10, 'cols' => 10]) ?>
                    </div>
                    <div class="tab-pane" id="txt-tab4">
                        <?= $form->field($model, 'text4')->textarea(['rows' => 10, 'cols' => 10]) ?>
                    </div>
                </div>

            </div>
            <p>Пример использования тегов: <i>«Привет {dispatchStatus.name}, меня зовут {dispatchRegist.username}, приглашаю вас на распродажу  городе {dispatchStatus.city}!»</i></p>
            <p>Шаг 1. Пишем тексты <?= Html::a('https://youtu.be/ktgeGvT5JBM ', 'https://youtu.be/ktgeGvT5JBM ', ['target' => '_blank']) ?></p>
            <p>
                Тэги:
                <?php foreach ($tags as $tag => $title): ?>
                        <?= $tag.' - '.$title.', ' ?>
                <?php endforeach; ?>
            </p>
        </div>

    </div>

    <div class="second-step hidden">
        <!--Аккаунт-->
        <div class="col-md-12">
            <?php if (!isset($_GET)): ?>
                <?= $form->field($model, 'accounts')->label()->widget(Select2::class, [
                    'data' => $model->getDispatchList(),
                    'options' => [
                        'placeholder' => 'Выберите',
                        'multiple' => true,
                        'value' => $model->isNewRecord ? null : $model->DispatchRegistList(),
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]); ?>
            <?php else: ?>
                <?= $form->field($model, 'accounts')->label()->widget(\kartik\select2\Select2::class, [
                    'data' => $model->getDispatchList(),
                    'options' => [
                        'placeholder' => 'Выберите',
                        'multiple' => true,
                        'value' => $model->isNewRecord ? null : $model->DispatchRegistList($model->id),
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]); ?>

            <?php endif; ?>

        </div>


        <!-- Табы-->

        <div class="col-md-12">
            <div class="panel panel-default tabs">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab1" data-toggle="tab"><i class="fa fa-pencil-square-o"
                                                                            aria-hidden="true"></i></a></li>
                    <li><a href="#tab2" data-toggle="tab"><i class="fa fa-files-o" aria-hidden="true"></i></a></li>
<!--                    <li><a href="#tab3" data-toggle="tab"><i class="fa fa-cloud-upload" aria-hidden="true"></i></a>-->
<!--                    </li>-->
<!--                    <li><a href="#tab4" data-toggle="tab"><i class="fa fa-book" aria-hidden="true"></i></a></li>-->
                </ul>
                <div class="panel-body tab-content">
                    <div class="tab-pane active" id="tab1">
                        <?= $form->field($model, 'text_data')->textarea(['rows' => 10, 'placeholder' => 'Введите аккаунты получателей по одному на строку'])->label(''); ?>
                    </div>
                    <div class="tab-pane" id="tab2">
                        <?= $form->field($model, 'file')->fileInput(); ?>
                    </div>
                    <div class="tab-pane" id="tab3">

                        <div class="row">
                            <div class="col-md-12">
                                <div id="get-users" class="hidden alert"><strong id="get-users-group-info"></strong>
                                </div>

                                <div id="get-users-group" style="padding-bottom: 15px">
                                    <div class="input-group">
                                        <?= $form->field($model, 'recived_data')->textInput([
                                            'placeholder' => 'Например: https://vk.com/teo_finance'
                                        ])->label('URL группы ВКонтакте') ?>
                                        <span class="input-group-btn">
                            <button id="getUsersBtn" class="btn btn-primary" type="button"
                                    onclick="getGroupUsers(this);">Получить список участников группы</button>
                                </span>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="tab-pane" id="tab4">
                        <?= $form->field($model, 'select_data')->widget(Select2::className(), [
                            'data' => \app\models\DataRecipient::getAllInArray(),
                            'options' => [
                                'placeholder' => 'Выберите базу',
                            ],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]); ?>
                    </div>
                </div>
            </div>
            <p style="margin-top: 10px;">2 способа добавления аккаунтов <?= Html::a('https://youtu.be/I4uw04ytck0', 'https://youtu.be/I4uw04ytck0', ['target' => '_blank']) ?>. Шаг 2. Выбираем аккаунты от которых будем рассылать и вставляем базу получателей <?= Html::a('https://youtu.be/Qofe0k0EMwo', 'https://youtu.be/Qofe0k0EMwo', ['target' => '_blank']) ?></p>
            <i style="display: inline-block; margin-top: 5px;">Для сбора базы перейдите по ссылке <a
                        href="https://vk.targethunter.ru/" target="_blank">https://vk.targethunter.ru/</a>  и введите промокод teo_send_ru (вы получите 2 бесплатных дня)</i>

        </div>

    </div>

    <div class="third-step hidden">
        <div class="col-md-12">
            <?= $form->field($model, 'chat')->label()->widget(Select2::class, [
                'data' => ArrayHelper::map(\app\models\Telegram::find()->where(['company_id' => FunctionHelper::getCompanyId()])->all(), 'telegram_id', 'name'),
                'options' => [
                    'placeholder' => 'Введите id ...',
//                    'value' => $model->isNewRecord ? Dispatch::getLastChatsId() : $model->getChatsId(),
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                    'tags' => true,
                    'multiple' => true,
                ],
            ]); ?>
        </div>

        <div class="hidden">
            <div class="col-md-12">
                <?= $form->field($model, 'to_stat')->checkbox() ?>
            </div>
            <div class="col-md-12">
                <?= $form->field($model, 'autoAnswer')->checkbox() ?>
            </div>
        </div>

        <div class="col-md-12">
            <?= $form->field($model, 'botId', ['options' => []])->widget(Select2::className(), [
                'data' => \yii\helpers\ArrayHelper::map($bots, 'id', 'name'),
                'options' => [
                    'placeholder' => 'Выбор автоответы',
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]) ?>
        </div>
        <div class="col-md-12">
            <?= $form->field($model, 'use_auto_registr')->checkbox(['id' => 'template-visible-checkbox']) ?>
        </div>
        <div id="templates-wrapper" <?=$model->use_auto_registr == 1 ? '' : 'style="display: none;"'?>>
            <div class="col-md-12">
                <?= $form->field($model, 'auto_regists_template_id')->widget(Select2::class, [
                    'options' => ['placeholder' => 'Выберите шаблон'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                    'data' => ArrayHelper::map(\app\models\AutoRegistsTemplates::find()->where(['company_id' => FunctionHelper::getCompanyId()])->all(), 'id', 'name'),
                ]) ?>
            </div>
        </div>
        <div class="hidden">
            <div class="col-md-6">
                <?= $form->field($model, 'days_week')->label()->widget(Select2::class, [
                    'data' => FunctionHelper::getDaysWeekList(),
                    'options' => [
                        'placeholder' => 'Выберите',
                        'multiple' => true,
                        'value' => $model->isNewRecord ? null : explode(',', $model->days_week),
                        'class' => 'hidden'
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]); ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'start_time')->widget(DateControl::class, [
                    'type' => DateControl::FORMAT_TIME,
                    'displayFormat' => 'php:H:i',
                    'options' => ['class' => 'hidden'],
                ]); ?>
            </div>
        </div>
        <div class="col-md-12">
            <p style="margin-top: 10px;">Шаг 3. Настройка уведомлений о новых ответах  <?= Html::a('https://youtu.be/llvVV1m3PTY', 'https://youtu.be/llvVV1m3PTY', ['target' => '_blank']) ?> Показатели рассылки <?= Html::a('https://youtu.be/xik0YVj4lc8', 'https://youtu.be/xik0YVj4lc8', ['target' => '_blank']) ?></p>
        </div>
    </div>

    <?php if (!Yii::$app->request->isAjax) { ?>
        <div class="form-group">
            <?= Html::Button('Далее', [
                'type' => 'button',
                'class' => 'btn btn-primary',
                'id' => 'next',
                'onclick' => 'nextStep()']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>
</div>

<script>
    $(function () {
        $('.dispatch-form').on('change', '#dispatch-autoanswer', function () {
            if ($(this).is(":checked")) {
                $('.field-dispatch-botid').removeClass('hidden');
            } else {
                $('.field-dispatch-botid').addClass('hidden');
                $('#dispatch-botid').select2('val', 0);

            }
        });

    });

    function handMode() {
        $('.ja-btns').removeClass('btn-warning');
        $('.js-hand').addClass('btn-warning');
        emptyMode();
        $('.js-mode').hide();
        $('.js-input-hand').show();
    }

    function uploadMode() {
        $('.ja-btns').removeClass('btn-warning');
        $('.js-upload').addClass('btn-warning');
        emptyMode();
        $('.js-mode').hide();
        $('.js-file').show();
    }

    function selectMode() {
        $('.ja-btns').removeClass('btn-warning');
        $('.js-select').addClass('btn-warning');
        emptyMode();
        $('.js-mode').hide();
        $('.js-select-base').show();
    }

    function emptyMode() {
        $('#dispatch-text_data').val('');
        $('#dispatch-file').val(null);
        $('#dispatch-select_data').select2('val', 0);
    }

    function step() {

        var btnId = $('.nextBtn').attr('id');

        console.log(btnId);
        if (btnId == 1) {
            $('.first-step').addClass('hidden'); //Скрываем первый блок
            $('.second-step').removeClass('hidden'); //Показываем второй блок
            $('.nextBtn').attr('id', 2);
            $('.modal-title').text('Создать новую рассылку. Шаг 2 из 3')
        } else if (btnId == 2) {
            $('.second-step').addClass('hidden'); //Скрываем второй блок
            $('.third-step').removeClass('hidden');//Показываем третий блок
            $('.modal-title').text('Создать новую рассылку. Шаг 3 из 3');

            $('.saveBtn').removeClass('hidden');
            $('.nextBtn').addClass('hidden');
        }
    }

    function getGroupUsers() {
        $('#getUsersBtn').addClass('disabled');
        $('#get-users').addClass('hidden');
        var urlGroup = $('#dispatch-recived_data').val();
        console.log(urlGroup);
        $.ajax({
            url: 'dispatch/parsegroup',
            type: 'POST',
            data: 'url=' + urlGroup,
            success: function (data) {
                var recivedData = jQuery.parseJSON(data);
                $('#get-users-group-info').text(recivedData[1]);
                if (recivedData[0]) {
                    $('#get-users').addClass('alert-success');
                } else {
                    $('#get-users').addClass('alert-danger');
                }
                $('#get-users').removeClass('hidden');
                $('#getUsersBtn').removeClass('disabled');
            },
            error: function (jqXHR, exception) {
                var msg = '';
                if (jqXHR.status === 0) {
                    msg = 'Not connect.\n Verify Network.';
                } else if (jqXHR.status == 404) {
                    msg = 'Requested page not found. [404]';
                } else if (jqXHR.status == 500) {
                    msg = 'Internal Server Error [500].';
                } else if (exception === 'parsererror') {
                    msg = 'Requested JSON parse failed.';
                } else if (exception === 'timeout') {
                    msg = 'Time out error.';
                } else if (exception === 'abort') {
                    msg = 'Ajax request aborted.';
                } else {
                    msg = 'Uncaught Error.\n' + jqXHR.responseText;
                }
                $('#get-users').addClass('alert-danger');
                $('#get-users-group-info').text(msg);
                $('#getUsersBtn').removeClass('disabled');
            }
        })
    }


</script>

<?php
$script = <<< JS
    $('#template-visible-checkbox').change(function(){
        if(this.checked){
            $('#templates-wrapper').slideDown();
        } else {
            $('#templates-wrapper').slideUp();
        }
    });
JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>


