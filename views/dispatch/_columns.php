<?php

use app\models\Companies;
use app\models\CompanySettings;
use app\models\DispatchConnect;
use app\models\DispatchRegist;
use app\models\DispatchStatus;
use app\models\Users;
use yii\bootstrap\Html;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'template' => '{delete}',
        'urlCreator' => function($action, $model, $key, $index) {
            return Url::to([$action,'id'=>$key]);
        },
        'buttons'=>[
            'export' => function ($url, $model, $key) {
                return Html::a('<span class="glyphicon glyphicon-share"></span>', ['status-export', 'id' => $model->id], [
                    'class' => 'btn btn-success btn-xs',
                    'role' => 'modal-remote',
                ]);
            },
            'change_status' => function ($url, $model, $key) {
                if (Yii::$app->user->identity->isSuperAdmin() && $model->status != 'finish'){
                    return Html::a('<i class="fa fa-exchange" aria-hidden="true"></i>', ['change-status', 'id'=>$model->id],['class' => 'btn btn-warning btn-xs', 'title'=>'Сменить статус на "Отправлено"']);
                }
            },
            'kill' => function ($url, $model, $key) {
                if (Yii::$app->user->identity->isSuperAdmin() && $model->status != 'finish'){
                    return Html::a('kill', ['kill', 'id'=>$model->id],['class' => 'btn btn-warning btn-xs', 'title'=>'Удалить не обработанные']);
                }
            },
            'back-to-handle' => function ($url, $model, $key) {
                if (Yii::$app->user->identity->isSuperAdmin() && $model->status != \app\models\DispatchStatus::STATUS_HANDLE){
                    return Html::a('О', ['back-to-handle', 'id'=>$model->id],['class' => 'btn btn-success btn-xs', 'title'=>'Сменить статус на "В обработке"']);
                }
            },
        ],
        'viewOptions'=>['class' => 'btn btn-primary btn-xs', 'data-pjax' => '0','title'=>'View','data-toggle'=>'tooltip'],
        'updateOptions'=>['class' => 'btn btn-success btn-xs', 'role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['class' => 'btn btn-danger btn-xs', 'role'=>'modal-remote','title'=>'Delete',
            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
            'data-request-method'=>'post',
            'data-toggle'=>'tooltip',
            'data-confirm-title'=>'Are you sure?',
            'data-confirm-message'=>'Are you sure want to delete this item'],
    ],


//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'company_id',
//        'content' => function($model){
//            $d = \app\models\User::findIdentity($model->company_id);
//            return $d->name;
//        }
//    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
        'content' => function($data) {
            $c = '';
            if ($data->like_count > 1) {
                $c = $data->like_count;
            }
            $content = $data->name.' - '.$c.'<br>';

            /** @var CompanySettings $settings */
            $settings = CompanySettings::findOne($data->company_id);


            if($data->status == DispatchStatus::STATUS_HANDLE){
                $content .=  '<span class=\'text-warning\'><b>В обработке</b> </span><i class="glyphicon glyphicon-check"></i>';
            }
            if($data->status == "wait"){
                $content .=  \yii\helpers\Html::a('<span class=\'text-danger\'><b>Запустить?</b> </span><i class="glyphicon glyphicon-check"></i>', ['faster','id' => $data->id, 'status' =>'job'], ['data-pjax' => 1]);
            }
            if($data->status == "job"){
                $col = $data->mission - $data->all_action_mission;
                $d = 0;
                $for = $col / $settings->messages_in_transaction;
                $min = $for * $settings->send_interval;
                $h = intval($min / 60);
                $m = $min % 60;
                $d = intval($col / $settings->messages_daily_limit);
                $time = "Осталось:  {$d}д {$h}:{$m}";
                $content .= Html::a("<span class='text-info'><b>Остановить? {$time}</b> </span><i class='glyphicon glyphicon-check'></i>", ['faster','id' => $data->id,'status' =>'wait'], ['data-pjax' => 1]);
            }
            if($data->status == "finish"){
                $content .= "<span class='text-success'><b>Выполнено</b></span>";
            }





            return $content;
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'mission',
        'content' => function($data) {
            $content = Html::a($data->link ? $data->link : "{$data->link}",
                    "{$data->link}", ['target' => '_blank', 'data' => ['pjax' => 0]]).'<br>';
            if ($data->type == 'like') {
                $content .= $data->mission.'<span class="glyphicon glyphicon-heart"></span>';
            }
            if ($data->type == 'rep') {
                $content .= $data->mission.' <span class="glyphicon glyphicon-share-alt"></span>';
            }
            if ($data->type == 'com') {
                $content .= $data->mission.' <span class="glyphicon glyphicon-send"></span>';
            }
            if ($data->type == 'in') {
                $content .= $data->mission.' <span class="glyphicon glyphicon-log-in"></span>';
            }
            return $content;
        }

    ],
    [
        'attribute'=>'process',
        'label'=>'Процесс',
        'content' =>  function($data){
            $all = $data->mission;
            $send = $data->all_action_mission;
            $a = 0;
            $color = 'active';
            if ($data->status == "finish") {
                $a = 100;
                $color = 'success';
            } elseif ($send == 0) {
                $send = 0;
                $a = 0;
            } elseif ($all != $send) {
                $a = round((($send/$all) * 100),0);
            }
            $clickAll = "{$all}";
            $clickSend = Html::a("{$send}", ['show','send' => $data->id]);

            return
                "<div class=\"progress progress-sm active\">
                <div class=\"progress-bar progress-bar-{$color} progress-bar-striped\" role=\"progressbar\" aria-valuenow=\"{$a}\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: {$a}%\">
                  <span class=\"sr-only\">{$a}% Complete</span>
                </div>
              </div>{$a}% / {$clickAll} / {$clickSend}"
                ;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'status_job',
        'visible' => Yii::$app->user->identity->isSuperAdmin(),
        'content' => function ($data){


            if ($data->status == 'finish') {
                return "<span class='text-success'>Законченна</span>";
            }

            if($data->status_job == "ok"){
                return "<span class='text-success'>Доступен</span>";
            }

//            $settings = S;

            if($data->status_job == "limit_exceeded" ) {
                $d = date("Y-m-d H:i:s", strtotime($data->last_mission_time . " + 24 hours"));
                $now = date("Y-m-d H:i:s");
                $hour = round((strtotime($d) - strtotime($now)) / (60 * 60));

                return "<span class='text-black'><b>Суточный лимит<br>Повтор через {$hour} часов<b></span>";
            }

            if($data->status_job == "interval_not_end"){

                // Проверяем еще не прошел интервал (статус становиться interval_not_end при отправке)
                $date_2 = date("Y-m-d H:i:s");

                $companyS = CompanySettings::find()->where(['company_id' => $data->company_id])->one();
                $interval_min = $companyS->send_interval;
                $diff = strtotime($date_2) - strtotime($data->last_mission_time) ;
                $diff = round($interval_min - $diff /60);

                return "<span class='text-yellow'><b> Интервал<br>Повтор через {$diff} минут</b></span>";

            }

            return $data->status_job;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'company_id',
        'visible' => Yii::$app->user->identity->isSuperAdmin(),
        'content' => function ($data){
            $comp = Companies::findOne(['id' => $data->company_id]);
            $info = Html::a($comp->company_name, ['/companies/view', 'id' => $data->company_id], ['target' => '_blank']);
            $info .= '<br/>'.$comp->general_balance.'  <i class="fa fa-rub">    </i>    ';
            $info .= $comp->bonus_balance.'  <i class="fa fa-gift text-info"></i>';
            return $info;
        }
    ],

];   