<?php

use app\components\helpers\FunctionHelper;
use app\models\Dispatch;
use app\models\DispatchStatus;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

CrudAsset::register($this);

$this->title = 'Рассылки';
$this->params['breadcrumbs'][] = $this->title;

$company_id = FunctionHelper::getCompanyId();
$items = ArrayHelper::map(Dispatch::find()->where(['company_id' => $company_id])->all(), 'id', 'name');

?>
<div class="dispatch-index">
    <?php $form = ActiveForm::begin() ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'selected_dispatches')->widget(Select2::className(), [
                'data' => $items,
                'options' => [
                    'placeholder' => 'Выберите рассылки ...',
                    'multiple' => true
                ]])->label('Рассылки') ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'selected_statuses')->widget(Select2::className(), [
                'data' => DispatchStatus::getDispatchStatuses(),
                'options' => [
                    'placeholder' => 'Выберите статусы ...',
                    'multiple' => true
                ],
            ])->label('Статусы') ?>

        </div>
        <div class="col-md-12">
            <div class="form-group">
                <?= $form->field($model, 'base_name')->textInput(['placeholder' => 'Введите название...'])->label('Наименование базы')?>

            </div>
        </div>
        <div class="col-md-12">
            <?php if (!Yii::$app->request->isAjax) { ?>
                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
            <?php } ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>

