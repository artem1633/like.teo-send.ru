<?php

use app\models\Dispatch;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Dispatch */
/* @var $form yii\widgets\ActiveForm */
/* @var $platform string */
    $style_vk = 'style="display:none;"';
    $style_instagram = 'style="display:none;"';
    if ($platform == 'vk' or $platform == null){
        $type = Dispatch::getTypesList();
        $style_instagram = 'style="display:none;"';
    } elseif ($platform == 'instagram') {
        $style_vk = 'style="display:none;"';
        $type = Dispatch::getTypesListInstagram();
    }

?>

<div class="parser-form">

    <?php $form = ActiveForm::begin(); ?>


        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'type')->dropDownList($type, ['prompt' => 'Выберите тип']) ?>
            </div>
        </div>
        <div class="vk-wapper" <?=$style_vk?>>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'link')->textInput() ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'group_id')->textInput() ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'distribute')->checkbox() ?>
                </div>
            </div>

            <div class="distribute-wrapper" style="display: none;">
                <div class="row">
                    <div class="col-md-12">
                        <?= $form->field($model, 'distribute_count')->textInput(['type' => 'number', 'max' => 49]) ?>
                    </div>
                </div>
            </div>
            <div class="link-wrapper" style="display:none;">
                <div class="row">
                    <div class="col-md-12">
                        <?= $form->field($model, 'text')->textarea() ?>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'mission')->textInput(['type' => 'number', 'max' => 49]) ?>
                </div>
            </div>
        </div>


    <div class="inst_wrapper" <?=$style_instagram?>>
        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'login')->textInput() ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'pass')->textInput() ?>
            </div>
        </div>
    </div>


    <?= $form->field($model, 'platform')->textInput(['value' => $platform, 'style' => 'display:none;'])->label(false) ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
<?php
if ($platform == 'instagram') {

    $script = <<< JS
    
     $('#dispatch-type').change(function(){
         var value = $(this).val();

         if (value == 'ins_like' || value == 'ins_video') {
            $('.vk-wapper').show();
            $('.inst_wrapper').hide();
         } else {
            $('.inst_wrapper').show();
            $('.vk-wapper').hide();
         }
     });
    
     $('#dispatch-distribute').change(function(){
         var chbox = document.getElementById('dispatch-distribute');;
        if (chbox.checked) {
            $('.distribute-wrapper').show();
         } else {
            $('.distribute-wrapper').hide();
         }
     });

JS;

    $this->registerJs($script, \yii\web\View::POS_READY);

} else {

    $script = <<< JS
    
     $('#dispatch-type').change(function(){
         var value = $(this).val();
         
         if(value == 'com'){
            $('.link-wrapper').show();
         } else {
            $('.link-wrapper').hide();
            $('.vk-wapper').show();
         }
     });
    
     $('#dispatch-distribute').change(function(){
         var chbox = document.getElementById('dispatch-distribute');;
        if (chbox.checked) {
            $('.distribute-wrapper').show();
         } else {
            $('.distribute-wrapper').hide();
         }
     });

JS;

    $this->registerJs($script, \yii\web\View::POS_READY);

}
?>
