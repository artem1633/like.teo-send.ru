<?php

use yii\helpers\Html,
    app\models\Companies,
    app\models\Bots;


/* @var $this yii\web\View */
/* @var $platform string*/
/* @var $model app\models\Dispatch */




?>
<div class="dispatch-create">
    <?= $this->render('_form', [
        'model' => $model,
        'platform' => $platform,
    ]) ?>
</div>
