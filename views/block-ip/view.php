<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\AnswerTemplate */
?>
<div class="answer-template-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'key',
            'message:ntext',
        ],
    ]) ?>

</div>
