<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AnswerTemplate */
?>
<div class="answer-template-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
