<?php

use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'Добро пожаловать';


?>
<div class="site-index">

    <div class="body-content">
     <div class="box box-default">
        <div class="box-body">
        <div class="row">

            <div class="col-lg-11">
               <?php
               $instruction = \app\models\EmailTemplates::find()->where(['key' => 'instruction'])->one();

               echo $instruction->body;


               ?>
            </div>

            
        </div>
    </div>
</div>

    </div>
</div>
