<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/**
 * @var $model \app\models\RegisterForm
 */

$this->title = 'Востановить пароль';

$fieldOptions1 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-user form-control-feedback'></span>"
];

?>

<div class="register-box">
    <div class="register-logo">
        <a href="#"><b>TEO</b>-SEND</a>
    </div>

    <div class="register-box-body">
        <p class="login-box-msg">Введите данные для востановления пароля</p>   
        <div class="login-content">
            <?php $form = ActiveForm::begin(['id' => 'login-form', 'enableClientValidation' => false], $options = ['class' => 'margin-bottom-0']); ?>
            <div class="form-group has-feedback">
                <?= $form
                  ->field($model, 'login', $fieldOptions1)
                  ->label(false)
                  ->textInput(['placeholder' => $model->getAttributeLabel('login'), 'class' => 'form-control']) ?>
            </div>
            <div class="row">
                <div class="col-xs-12 ">
                    <?= Html::submitButton('Востановить', ['class' => 'btn btn-primary', 'style' => 'width:100%', 'name' => 'login-button']) ?>
                </div>
            </div>
            <br>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>