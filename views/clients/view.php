<?php

use johnitvn\ajaxcrud\BulkButtonWidget;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Clients */
/* @var integer $likesAmount */
/* @var integer $postsAmount */
/* @var integer $automationsCount */

\app\assets\plugins\MagnificPopupAsset::register($this);
\johnitvn\ajaxcrud\CrudAsset::register($this);

$this->title = "Профиль «{$model->fio}»";


$content = '';

$content .=
    Html::a('Добавить задачу <i class="glyphicon glyphicon-plus"></i>', ['automation/create', 'platform' => 'instagram', 'client_id' => $model->id],
        ['role' => 'modal-remote', 'title' => 'Создать новую задачу', 'class' => 'btn btn-info', 'style' => 'margin-left: 50px;']);


?>
<div class="clients-view">

    <div class="row">
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading"></div>
                <div class="panel-body">
                    <div class="text-center">
                        <a class="image-popup-vertical-fit" href="<?= $model->photo_url ?>">
                            <img class="profile-img" src="<?= $model->photo_url ?>" alt="">
                        </a>
                        <h3 class="profile-title"><?= $model->fio ?></h3>
                        <h4 class="profile-subtitle"><?= Html::a($model->url, (new \app\values\InstagramProfileUrl(['input' => $model->url]))->getUrl(), ['target' => '_blank']); ?></h4>
                        <ul class="profile-info">
                            <li><b>Статус:</b> <span class="pull-right"><?= $model->status ?></span></li>
                            <li><b>Дата создания:</b> <span class="pull-right"><?= Yii::$app->formatter->asDatetime($model->created_at, 'php:d.m.Y H:i:s') ?></span></li>
                            <li><b>Всего лайков:</b> <span class="pull-right"><?= intval($likesAmount) ?></span></li>
                            <li><b>Всего постов:</b> <span class="pull-right"><?= intval($postsAmount) ?></span></li>
                            <li><b>Всего автозадач:</b> <span class="pull-right"><?= intval($automationsCount) ?></span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <?= GridView::widget([
                'id' => 'crud-datatable',
                'dataProvider' => $dataProvider,
                //'filterModel' => $searchModel,
                'pjax' => true,
                'responsiveWrap' => false,
                'columns' => require(__DIR__ . '/../automation/_columns.php'),
                'toolbar' => [
                    ['content' => $content],
                ],
                'striped' => true,
                'condensed' => true,
                'responsive' => true,
                'toolbarContainerOptions' => ['style' => 'margin-top: 15px;'],
                'panel' => [
                    'type' => 'default',
                    'heading' => Yii::$app->session->hasFlash('danger') || Yii::$app->session->hasFlash('warning')
                        ? '<i class="glyphicon glyphicon-list"></i> '
                        . ' Внимание! ' . Yii::$app->session->getFlash('danger')
                        : '<i class="glyphicon glyphicon-list"></i> ' . $this->title,
                    'after' => BulkButtonWidget::widget([
                            'buttons' => Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; Delete All',
                                ["bulk-delete"],
                                [
                                    "class" => "btn btn-danger btn-xs",
                                    'role' => 'modal-remote-bulk',
                                    'data-confirm' => false, 'data-method' => false,// for overide yii data api
                                    'data-request-method' => 'post',
                                    'data-confirm-title' => 'Ты уверен?',
                                    'data-confirm-message' => 'Вы действительно хотите удалить этот элемент'
                                ]),
                        ]) .
                        '<div class="clearfix"></div>',
                ]
            ]) ?>
        </div>
    </div>
</div>

<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "",// always need it for jquery plugin
]) ?>


<?php Modal::end(); ?>


<?php

$script = <<< JS
	$('.image-popup-vertical-fit').magnificPopup({
		type: 'image',
		closeOnContentClick: true,
		mainClass: 'mfp-img-mobile',
		image: {
			verticalFit: true
		}
		
	});
JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>