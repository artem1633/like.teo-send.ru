<?php

use yii\helpers\Html;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'photo_url',
        'content' => function($model){
            return Html::img(
                $model->photo_url ? $model->photo_url :  "@web/images/user.png" ,
                ['alt' => 'message user image', 'class' => 'direct-chat-img']);
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'fio',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'url',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'status',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'created_at',
        'format' => ['date', 'php:d.m.Y H:i:s'],
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'buttons' => [
            'view' => function ($url, $model) {
                $url = Url::to([$url]);
                return Html::a('<span class="fa fa-eye"></span>', $url, [
                    'class' => 'btn btn-primary btn-xs',
                    'title' => 'Просмотр',
                    'data-toggle' => 'tooltip',
                    'target' => '_blank',
                    'data-pjax' => 0,
                ]);
            },
            'update' => function ($url, $model) {
                $url = Url::to([$url]);
                return Html::a('<span class="fa fa-pencil-square-o"></span>', $url, [
                    'class' => 'btn btn-success btn-xs',
                    'role' => 'modal-remote',
                    'title' => 'Изменить',
                    'data-toggle' => 'tooltip'
                ]);
            },
            'delete' => function ($url, $model) {
                $url = Url::to([$url]);
                return Html::a('<span class="fa fa-trash-o"></span>', $url, [
                    'class' => 'btn btn-danger btn-xs',
                    'role' => 'modal-remote', 'title' => 'Удалить',
                    'data-confirm' => false, 'data-method' => false,// for overide yii data api
                    'data-request-method' => 'post',
                    'data-toggle' => 'tooltip',
                    'data-confirm-title' => 'Подтвердите действие',
                    'data-confirm-message' => 'Вы уверены что хотите удалить этого элемента?',
                ]);
            },
        ],
    ],

];   