<?php

use app\models\Proxy;
use yii\helpers\Html,
    yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
     [
     'class'=>'\kartik\grid\DataColumn',
     'attribute'=>'internal_id',
     ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'name',
        'content' => function($data){
            $text = "{$data->ip_adress}:{$data->port}";
            $text .= "<br/><span class='text-danger'>{$data->count_block}</span> / <span class='text-success'>{$data->count_add}</span>";
            return $text;
        },
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'ip_adress',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'port',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'status',
        'content' => function ($data) {
            if ($data->status == "no") {
                return "<span class='text-danger'><b>Не работает</b></span>";
            }
            if ($data->status == "yes") {
                return "<span class='text-success'><b>Работает</b></span>";
            }

        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'expireDays',
        'label' => 'Осталось',
        'vAlign' => \kartik\grid\GridView::ALIGN_CENTER,
        'hAlign' => \kartik\grid\GridView::ALIGN_CENTER,
        'content' => function($data) {
            if($data->expireDays != null)
            {
                return "{$data->expireDays} дней";
            }
            return null;
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'accounts',
        'content' => function ($data) {
               return Proxy::getAccountsUsesProxy($data->id);
            }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'filter' => true,
        'attribute' => 'companyId',
        'content' => function ($data) {
            return $data->companyModel ? $data->companyModel->company_name : null;
        },
        'visible' => Yii::$app->user->identity->isSuperAdmin(),
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'headerOptions' => ['width' => '140'],
        'urlCreator' => function ($action, $model, $key, $index) {
            return Url::to([$action, 'id' => $key]);
        },


        'template' => '{view}  {leadUpdate}  {leadDelete}',
        'contentOptions' => ['style' => 'white-space: nowrap;'],
        'buttons' => [
            'view' => function ($url, $model) {
                $url = Url::to(['/proxy/view', 'id' => $model->id]);
                return Html::a('<span class="fa fa-eye"></span>', $url, [
                    'class' => 'btn btn-primary btn-xs',
                    'role' => 'modal-remote',
                    'title' => 'Просмотр',
                    'data-toggle' => 'tooltip'
                ]);
            },
            'leadUpdate' => function ($url, $model) {
                $url = Url::to(['/proxy/update', 'id' => $model->id]);
                return Html::a('<span class="fa fa-pencil-square-o"></span>', $url, [
                    'class' => 'btn btn-success btn-xs',
                    'role' => 'modal-remote',
                    'title' => 'Изменить',
                    'data-toggle' => 'tooltip'
                ]);
            },
            'leadDelete' => function ($url, $model) {
                $url = Url::to(['/proxy/delete', 'id' => $model->id]);
                return Html::a('<span class="fa fa-trash-o"></span>', $url, [
                    'class' => 'btn btn-danger btn-xs',
                    'role' => 'modal-remote', 'title' => 'Удалить',
                    'data-confirm' => false, 'data-method' => false,// for overide yii data api
                    'data-request-method' => 'post',
                    'data-toggle' => 'tooltip',
                    'data-confirm-title' => 'Подтвердите действие',
                    'data-confirm-message' => 'Вы уверены что хотите удалить этого элемента?',
                ]);
            },
        ]
    ],

];   