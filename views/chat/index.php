<?php
use yii\helpers\Html,
    yii\helpers\Url,
    yii\widgets\Breadcrumbs,
    app\components\helpers\FunctionHelper,
    app\models\DispatchRegist,
    skeeks\widget\highcharts\Highcharts;

$this->title = 'Чат';
Yii::$app->user->identity->isSuperAdmin() ? $this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['/chat']] : null;
$this->params['breadcrumbs'][] = $company->company_name;
?>
<!-- исходник https://bootsnipp.com/snippets/gNPj5 -->
<div class="dispatch-index">

<div class="wrap">
    <section class="left">
        <div class="profile">
            <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/1089577/user.jpg">
            <div class="icons">
                <i class="fa fa-commenting fa-lg i2" aria-hidden="true"></i>
                <i class="fa fa-bars fa-lg i2" aria-hidden="true"></i>
            </div>
        </div>
        <div class="wrap-search">
            <div class="search">
                <i class="fa fa-search fa i2" aria-hidden="true"></i>
                <input type="text" class="input-search" placeholder="Suchen oder neuen Chat beginnen">
            </div>
        </div>
        <div class="contact-list"></div>
    </section>

    <section class="right">
        <div class="chat-head">
            <img alt="profilepicture">
            <div class="chat-name">
                <h1 class="font-name" style="margin-bottom: 0px;padding-bottom:0px;margin-top:13px;"></h1>
                <p class="font-online"></p>
            </div>
            <i class="fa fa-search fa-lg i2" aria-hidden="true"></i>
            <i class="fa fa-paperclip fa-lg i2" aria-hidden="true"></i>
            <i class="fa fa-bars fa-lg i2" aria-hidden="true" id="show-contact-information"></i>
            <i class="fa fa-times fa-lg i2" aria-hidden="true" id="close-contact-information"></i>
        </div>
        <div class="wrap-chat">
            <div class="chat"></div>
            <div class="information"></div>
        </div>
        <div class="wrap-message">
            <i class="fa fa-smile-o fa-lg i2" aria-hidden="true"></i>
            <div class="message">
                <input type="text" class="input-message" placeholder="Schreibe eine Nachricht">
            </div>
            <i class="fa fa-microphone fa-lg i2" aria-hidden="true"></i>
        </div>
    </section>
</div>
</div>
<script>
    "use strict";"object"!=typeof window.CP&&(window.CP={}),
        window.CP.PenTimer={
            programNoLongerBeingMonitored:!1,timeOfFirstCallToShouldStopLoop:0,_loopExits:{},_loopTimers:{},
            START_MONITORING_AFTER:2e3,STOP_ALL_MONITORING_TIMEOUT:5e3,MAX_TIME_IN_LOOP_WO_EXIT:2200,exitedLoop:function(o)
            {
                this._loopExits[o]=!0
            },
            shouldStopLoop:function(o){
                if(this.programKilledSoStopMonitoring)return!0;
                if(this.programNoLongerBeingMonitored)return!1;
                if(this._loopExits[o])return!1;var t=this._getTime();
                if(0===this.timeOfFirstCallToShouldStopLoop)return this.timeOfFirstCallToShouldStopLoop=t,!1;
                var i=t-this.timeOfFirstCallToShouldStopLoop;
                if(i<this.START_MONITORING_AFTER)return!1;
                if(i>this.STOP_ALL_MONITORING_TIMEOUT)return this.programNoLongerBeingMonitored=!0,!1;
                try{this._checkOnInfiniteLoop(o,t)}catch(o){return this._sendErrorMessageToEditor(),
                this.programKilledSoStopMonitoring=!0,!0}return!1
            },
            _sendErrorMessageToEditor:function(){
                try{
                    if(this._shouldPostMessage()){
                        var o={
                            action:"infinite-loop",line:this._findAroundLineNumber()
                        };
                        parent.postMessage(JSON.stringify(o),"*")
                    }else this._throwAnErrorToStopPen()
                }catch(o){
                    this._throwAnErrorToStopPen()
                }
            },
            _shouldPostMessage:function(){
                return document.location.href.match(/boomerang/)
            },
            _throwAnErrorToStopPen:function(){
                throw"We found an infinite loop in your Pen. We've stopped the Pen from running. Please correct it or contact support@codepen.io."
            },
            _findAroundLineNumber:function(){
                var o=new Error,t=0;if(o.stack){
                var i=o.stack.match(/boomerang\S+:(\d+):\d+/);i&&(t=i[1])
            }return t
            },
            _checkOnInfiniteLoop:function(o,t){
                if(!this._loopTimers[o])return
                this._loopTimers[o]=t,!1;var i=t-this._loopTimers[o];
                if(i>this.MAX_TIME_IN_LOOP_WO_EXIT)throw"Infinite Loop found on loop: "+o},
                _getTime:function(){return+new Date}},
                window.CP.shouldStopExecution=function(o){
                    var t=window.CP.PenTimer.shouldStopLoop(o);
                    return t===!0&&console.warn("[CodePen]: An infinite loop (or a loop taking too long) was detected, so we stopped its execution. Sorry!"),t
                },
            window.CP.exitedLoop=function(o){window.CP.PenTimer.exitedLoop(o)};

</script>
<script >//contacts
    var WhatsApp = (function (app) {
        function Contact (name,img,online) {
            this.id = contactList.length;
            this.name = name;
            this.img = img;
            this.online = online;
            this.messages = new Array();
            this.newmsg = 0;
            this.groups = new Array();

            contactList.push(this);
        }

        Contact.prototype.addMessage = function (msg) {
            this.messages.push(msg);
        }

        Contact.prototype.addGroup = function (group) {
            this.groups.push(group);
        }

        appContacts =  Contact;
        return appContacts;
    })(WhatsApp || {});

    //groups
    var WhatsApp = (function (app) {
        function Group (name,img) {
            this.id = contactList.length;
            this.name = name;
            this.img = img;
            this.members = new Array();
            this.messages = new Array();
            this.newmsg = 0;

            contactList.push(this);
        }

        Group.prototype.addMember = function (contact) {
            this.members.push(contact);
        }

        Group.prototype.addMessage = function (msg) {
            this.messages.push(msg);
        }

        appGroups = Group;
        return appGroups;

    })(WhatsApp || {});

    //messages
    var WhatsApp = (function (app) {
        function Message (text,name,time,type,group) {
            this.text = text;
            this.name = name,
                this.time = time;
            this.type = type;
            this.group = group;
        }

        appMessages =  Message;
        return appMessages;
    })(WhatsApp || {});

    //subject
    /**
     * Created by Tanja on 02.01.2017.
     */
    var WhatsApp = (function(app) {

        function Subject() {
            this.observers = [];
        };

        Subject.prototype.subscribe = function(item) {
            this.observers.push(item);
        };

        Subject.prototype.unsubscribeAll = function() {
            this.observers.length = 0;
        };

        Subject.prototype.notifyObservers = function() {
            this.observers.forEach(elem => {elem.notify();})
        };

        app.Subject = Subject;
        return app;

    })(WhatsApp || {});

    //model
    /**
     * Created by Tanja on 02.01.2017.
     */
    var currentChat;
    var contactList = new Array();

    var WhatsApp = (function ToDoModel (app) {
        var subject = new app.Subject();

        var Model = {
            start : function() {
                $.getJSON("https://s3-us-west-2.amazonaws.com/s.cdpn.io/1089577/contacts2.json", function (data) {
                    for (var i=0; i<data.elements.length; i++) {if (window.CP.shouldStopExecution(4)){break;}
                        var e = data.elements[i];

                        if (e.online == undefined) {
                            var group = new appGroups(e.name,e.img);
                            for (var j = 0; j < e.members.length; j++) {if (window.CP.shouldStopExecution(1)){break;}
                                group.addMember(contactList[e.members[j].contact]);
                                contactList[e.members[j].contact].addGroup(group);
                            }
                            window.CP.exitedLoop(1);

                            for (var j = 0; j < e.messages.length; j++) {if (window.CP.shouldStopExecution(2)){break;}
                                var m = e.messages[j];
                                var message = new appMessages(m.text, m.name, m.time, m.type, true);
                                group.addMessage(message);
                            }
                            window.CP.exitedLoop(2);

                        }
                        else {
                            var contact = new appContacts(e.name, e.img, e.online);
                            for (var j = 0; j < e.messages.length; j++) {if (window.CP.shouldStopExecution(3)){break;}
                                var m = e.messages[j];
                                var message = new appMessages(m.text, m.name, m.time, m.type, false);
                                contact.addMessage(message);
                            }
                            window.CP.exitedLoop(3);

                        }
                    }
                    window.CP.exitedLoop(4);

                    subject.notifyObservers();
                });
            },
            writeMessage : function() {
                var msg = new appMessages($(".input-message").val(),"",new Date().getHours() + ":" + new Date().getMinutes(),true);
                WhatsApp.View.printMessage(msg);
                currentChat.addMessage(msg);
                $(".input-message").val("");
                $("#" + currentChat.id).addClass("active-contact");
                subject.notifyObservers();
            },
            getMessage : function (text,id,name) {
                if (name == undefined) {
                    var msg = new appMessages(text, contactList[id].name, new Date().getHours() + ":" + new Date().getMinutes(), false, false);
                }
                else {
                    var msg = new appMessages(text, name, new Date().getHours() + ":" + new Date().getMinutes(), false, true);
                }
                contactList[id].addMessage(msg);
                contactList[id].online = new Date().getHours() + ":" + new Date().getMinutes();

                if(contactList[id] == currentChat) {
                    WhatsApp.View.printMessage(msg);
                    WhatsApp.View.printContact(contactList[id]);
                }
                else {
                    contactList[id].newmsg ++;
                    WhatsApp.View.printContact(contactList[id]);
                }
            },
            register : function(...args) {
                subject.unsubscribeAll();
                args.forEach(elem => {
                    subject.subscribe(elem);
                })
            }
        };
        app.Model = Model;
        return app;

    })(WhatsApp || {});

    //view
    /**
     * Created by Tanja on 02.01.2017.
     */
    var first = true;

    var WhatsApp = (function ToDoView(app) {
        var view = {
            printContact : function (c) {
                $("#" + c.id).remove();
                var lastmsg = c.messages[c.messages.length - 1];

                if (c.newmsg == 0) {
                    var html = $("<div class='contact' id='" + c.id + "'><img src='" + c.img + "' alt='profilpicture'><div class='contact-preview'><div class='contact-text'><h1 class='font-name'>" + c.name + "</h1><p class='font-preview'>" + lastmsg.text + "</p></div></div><div class='contact-time'><p>" + lastmsg.time + "</p></div></div>");
                }
                else {
                    var html = $("<div class='contact new-message-contact' id='" + c.id + "'><img src='" + c.img + "' alt='profilpicture'><div class='contact-preview'><div class='contact-text'><h1 class='font-name'>" + c.name + "</h1><p class='font-preview'>" + lastmsg.text + "</p></div></div><div class='contact-time'><p>" + lastmsg.time + "</p><div class='new-message' id='nm" + c.id + "'><p>" + c.newmsg + "</p></div></div></div>");
                }

                var that = c;
                $(".contact-list").prepend(html);
                WhatsApp.Ctrl.addClick(html, that);
            } ,
            printChat : function (cg) {
                WhatsApp.View.closeContactInformation();
                $(".chat-head img").attr("src",cg.img);
                $(".chat-name h1").text(cg.name);
                if(cg.members == undefined) {
                    $(".chat-name p").text("zuletzt online um " + cg.online);
                    // Nachrichten konfigurieren
                    $(".chat-bubble").remove();
                    for (var i=0; i<cg.messages.length; i++) {if (window.CP.shouldStopExecution(5)){break;}
                        WhatsApp.View.printMessage(cg.messages[i]);
                    }
                    window.CP.exitedLoop(5);

                    currentChat = cg;
                }
                else {
                    var listMembers = "";
                    for (var i=0; i<cg.members.length; i++) {if (window.CP.shouldStopExecution(6)){break;}
                        listMembers += cg.members[i].name;
                        if (i < cg.members.length - 1) {
                            listMembers  += ", ";
                        }
                    }
                    window.CP.exitedLoop(6);

                    $(".chat-name p").text(listMembers);
                    // Nachrichten konfigurieren
                    $(".chat-bubble").remove();
                    for (var i=0; i<cg.messages.length; i++) {if (window.CP.shouldStopExecution(7)){break;}
                        WhatsApp.View.printMessage(cg.messages[i]);
                    }
                    window.CP.exitedLoop(7);

                    currentChat = cg;
                }
            },
            printMessage : function (gc) {
                if (gc.group) {
                    if (gc.type) {
                        $(".chat").append("<div class='chat-bubble me'><div class='my-mouth'></div><div class='content' style='min-height:0px;'>" + gc.text + "</div><div class='time'>" + gc.time + "</div></div>");
                    }
                    else {
                        $(".chat").append("<div class='chat-bubble you'><div class='your-mouth'></div><h4>" + gc.name + "</h4><div class='content' style='min-height:0px;'>" + gc.text + "</div><div class='time'>" + gc.time + "</div></div>");
                    }
                }
                else {
                    if (gc.type) {
                        $(".chat").append("<div class='chat-bubble me'><div class='my-mouth'></div><div class='content' style='min-height:0px;'>" + gc.text + "</div><div class='time'>" + gc.time + "</div></div>");
                    }
                    else {
                        $(".chat").append("<div class='chat-bubble you'><div class='your-mouth'></div><div class='content' style='min-height:0px;'>" + gc.text + "</div><div class='time'>" + gc.time + "</div></div>");
                    }
                }
            },
            showContactInformation : function () {
                $(".chat-head i").hide();
                $(".information").css("display", "flex");
                $("#close-contact-information").show();
                if (currentChat.members == undefined) {
                    $(".information").append("<img src='" + currentChat.img + "'><div><h1>Name:</h1><p>" + currentChat.name + "</p></div><div id='listGroups'><h1>Gemeinsame Gruppen:</h1></div>");
                    for (var i = 0; i < currentChat.groups.length; i++) {if (window.CP.shouldStopExecution(9)){break;}
                        html = $("<div class='listGroups'><img src='" + currentChat.groups[i].img + "'><p>" + currentChat.groups[i].name + "</p></div>");
                        $("#listGroups").append(html);
                        $(html).click(function (e) {
                            for (var i = 0; i < contactList.length; i++) {if (window.CP.shouldStopExecution(8)){break;}
                                if (($(currentChat).find("p").text()) == contactList[i].name) {
                                    $(".active-contact").removeClass("active-contact");
                                    $("#" + contactList[i].id).addClass("active-contact");
                                    WhatsApp.Groups.printChat(contactList[i]);
                                }
                            }
                            window.CP.exitedLoop(8);

                        });
                    }
                    window.CP.exitedLoop(9);

                }
                else {
                    $(".information").append("<img src='" + currentChat.img + "'><div><h1>Name:</h1><p>" + currentChat.name + "</p></div><div id='listGroups'><h1>Mitglieder:</h1></div>");
                    for (var i = 0; i < currentChat.members.length; i++) {if (window.CP.shouldStopExecution(11)){break;}
                        html = $("<div class='listGroups'><img src='" + currentChat.members[i].img + "'><p>" + currentChat.members[i].name + "</p></div>");
                        $("#listGroups").append(html);
                        $(html).click(function (e) {
                            for (var i = 0; i < contactList.length; i++) {if (window.CP.shouldStopExecution(10)){break;}
                                if (($(currentChat).find("p").text()) == contactList[i].name) {
                                    $(".active-contact").removeClass("active-contact");
                                    $("#" + contactList[i].id).addClass("active-contact");
                                    WhatsApp.Contacts.printChat(contactList[i]);
                                }
                            }
                            window.CP.exitedLoop(10);

                        });
                    }
                    window.CP.exitedLoop(11);

                }
            },
            closeContactInformation : function () {
                $(".chat-head i").show();
                $("#close-contact-information").hide();
                $(".information >").remove();
                $(".information").hide();
            },

            //Observer-Methode
            notify: function () {
                if (first) {
                    first = false;
                    for (var i = 0; i < contactList.length; i++) {if (window.CP.shouldStopExecution(12)){break;}
                        WhatsApp.View.printContact(contactList[i]);
                        currentChat = contactList[i];
                    }
                    window.CP.exitedLoop(12);

                    first = false;
                }
                else {
                    WhatsApp.View.printContact(currentChat);
                }
            }
        }

        app.View = view;
        return app;

    })(WhatsApp);

    //controller
    /**
     * Created by Tanja on 02.01.2017.
     */
    var start = true;

    var WhatsApp = (function ToDoCtrl(app) {

        $(document).ready(function () {
            app.Model.start();
        });

        var Ctrl = {
            addClick : function (html, that) {
                $(html).click(function(e) {
                    $(".active-contact").removeClass("active-contact");
                    $(this).addClass("active-contact");
                    $(this).removeClass("new-message-contact");
                    $("#nm" + that.id).remove();
                    that.newmsg = 0;
                    WhatsApp.View.printChat(that);
                });
            },

            //Observer-Methode
            notify : function() {
                if (start) {
                    $(".input-message").keyup(function(ev) {
                        if(ev.which == 13 || ev.keyCode == 13) {
                            app.Model.writeMessage();
                        }
                    });

                    $("#show-contact-information").on("click",function(){
                        WhatsApp.View.showContactInformation();
                    });

                    $("#close-contact-information").on("click",function(){
                        WhatsApp.View.closeContactInformation();
                    });
                    start = false;
                }
            }
        };
        app.Ctrl = Ctrl;
        return app;

    })(WhatsApp);

    WhatsApp.Model.register(WhatsApp.View, WhatsApp.Ctrl);

</script>