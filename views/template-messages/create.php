<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TemplateMessages */

?>
<div class="template-messages-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
