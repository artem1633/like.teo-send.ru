<?php

use yii\helpers\Url,
    yii\helpers\Html,
    yii\bootstrap\Modal,
    kartik\grid\GridView,
    johnitvn\ajaxcrud\CrudAsset,
    johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AtelierSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Боты';

$this->params['breadcrumbs'][] = ['label' => $this->title];

CrudAsset::register($this);

$panel_heading =
    '<i class="glyphicon glyphicon-list"></i> Боты';

?>
    <div class="atelier-index">
        <div id="ajaxCrudDatatable">
            <?= GridView::widget([
                'id' => 'crud-datatable',
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'pjax' => ['enablePushState' => false],
                'responsiveWrap' => false,
                'columns' => [
                    [
                        'class' => 'kartik\grid\SerialColumn',
                        'width' => '30px',
                    ],
                    [
                        'class' => '\kartik\grid\DataColumn',

                        'attribute' => 'name',
                        'content' => function ($model) {
                            return $model->name;
                        }
                    ],
                    [
                        'class' => '\kartik\grid\DataColumn',
                        'filter' => true,
                        'attribute' => 'companyId',
                        'content' => function ($model) {
                            return $model->companyModel ? $model->companyModel->company_name : null;
                        },
                        'visible' => Yii::$app->user->identity->isSuperAdmin(),
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'headerOptions' => ['width' => '160'],
                        'contentOptions' => ['style' => 'white-space: nowrap;'],
                        'template' => '{constructor}  {clone}  {leadUpdate}  {leadDelete}',
                        'buttons' => [
                            'constructor' => function ($url, $model) {
                                $url = Url::to(['/bots/constructor/', 'id' => $model->id]);
                                return Html::a('<span class="fa fa-comment "></span>', $url, [
                                    'class' => 'btn btn-info btn-xs',
                                    'data-pjax' => 0,
                                    'title' => 'Конструктор'
                                ]);
                            },
                            'clone' => function ($url, $model) {
                                $url = Url::to(['', 'clone' => $model->id]);
                                return Html::a('<span class="fa fa-copy"></span>', $url, [
                                    'class' => 'btn btn-warning btn-xs',
                                    'title' => 'Копировать',

                                ]);
                            },
                            'leadUpdate' => function ($url, $model) {
                                $url = Url::to(['/bots/update', 'id' => $model->id]);
                                return Html::a('<span class="fa fa-pencil-square-o"></span>', $url, [
                                    'class' => 'btn btn-success btn-xs',
                                    'role' => 'modal-remote',
                                    'title' => 'Изменить',
                                    'data-toggle' => 'tooltip'
                                ]);
                            },
                            'leadDelete' => function ($url, $model) {
                                $url = Url::to(['/bots/delete', 'id' => $model->id]);
                                return Html::a('<span class="fa fa-trash-o"></span>', $url, [
                                    'class' => 'btn btn-danger btn-xs',
                                    'role' => 'modal-remote', 'title' => 'Удалить',
                                    'data-confirm' => false, 'data-method' => false,// for overide yii data api
                                    'data-request-method' => 'post',
                                    'data-toggle' => 'tooltip',
                                    'data-confirm-title' => 'Подтвердите действие',
                                    'data-confirm-message' => 'Вы уверены что хотите удалить этого элемента?',
                                ]);
                            },
                        ]
                    ]
                ],
                'toolbar' => [

                    ['content' =>
                        Html::a('Создать', ['create'], ['role' => 'modal-remote', 'title' => 'Создать', 'class' => 'btn btn-info']) .
                        Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''], ['data-pjax' => 1, 'class' => 'btn btn-default', 'title' => 'Обновить']) .
                        '{toggleData}'
                    ],
                ],
                'striped' => true,
                'condensed' => true,
                'responsive' => true,
                'toolbarContainerOptions' => ['style' => 'margin-top: 15px;'],
                'panel' => [
                    'type' => 'default',
                    'heading' => $panel_heading, //'<i class="glyphicon glyphicon-list"></i> Боты',
                    'before' => '',
                    'after' => false,
                ]
            ]) ?>
        </div>
    </div>
<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "",// always need it for jquery plugin
]) ?>
<?php Modal::end(); ?>

<?php
$this->registerJsFile('/js/find_bot.js')
?>