<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Bots */

$this->title = 'Create Bots';
$this->params['breadcrumbs'][] = ['label' => 'Bots', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bots-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
