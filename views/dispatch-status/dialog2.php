<?php

use app\models\DispatchRegist;
use yii\helpers\Html,
    kartik\select2\Select2,
    yii\helpers\ArrayHelper,
    app\models\Statuses;
$this->title = "Переписка";
/** @var \app\models\Message  $history*/
/** @var \app\models\Dispatch  $dispatch*/
/** @var \app\models\DispatchStatus  $clients*/

?>


<div style="margin-bottom: 30px;">
    <div class="row">
        <div class="col-md-12">
            <?php if( Yii::$app->session->hasFlash('status_info') ): ?>
                <div class="alert alert-info alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <?php echo Yii::$app->session->getFlash('status_info'); ?>
                </div>
            <?php endif;?>
            <?php if( Yii::$app->session->hasFlash('status_info_success') ): ?>
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <?php echo Yii::$app->session->getFlash('status_info_success'); ?>
                </div>
            <?php endif;?>
            <?php if($account->status == DispatchRegist::STATUS_BLOCKED || $account->status == DispatchRegist::STATUS_SLEEPING ): ?>
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    Аккаунт заблокирован или не используется
                </div>
            <?php endif;?>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <p>Шаблоны ответов <?=Html::a('https://youtu.be/U115xMKcI6E', 'https://youtu.be/U115xMKcI6E', ['target' => '_blank'])?>. Cтатус переписки и добавление в CRM <?=Html::a('https://youtu.be/8pRBFXfQFco', 'https://youtu.be/8pRBFXfQFco', ['target' => '_blank'])?></p>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="dispatch-regist-index">

            <!-- DIRECT CHAT PRIMARY -->
            <div class="panel panel-default" style="margin-bottom: 35px;">
                <div class="panel-heading with-border" style="padding: 0px">

                    <div class="col-md-4">

                        <h3 class="panel-title"><?php if ($dispatch) echo $dispatch->name ?></h3>
                    </div>
                    <div class="col-md-4 text-right">
                        <?php if (!$clients->status_push_sales && \app\components\helpers\FunctionHelper::getCompanySettings()->sales_access_token != null) {
                            echo Html::a("SALES",['pushsls','id' => $clients->id], ['class' =>'btn btn-success', 'data' => ['pjax' => 0]]);}
                        ?>
                    </div>
                    <div class="col-md-4">
                        <?= Select2::widget([
                            'name' => 'status',
                            'data' => ArrayHelper::map(Statuses::find()->where(['type' => Statuses::TYPE_CLIENT])->all(), 'id', 'name'),
                            'value' => $clients->status,
                            'options' => [
                                'placeholder' => Yii::t('app', 'Выберите статус...'),
                                'class' => 'form-control'
                            ],
                            'pluginOptions' => [
                                'allowClear' => true,
                            ],
                            'pluginEvents' => [
                                'change' => "function() {
                            statuses.changeStatus($(this).val());                               
                        }"
                            ]
                        ]) ?>
                    </div>
                </div>






                <!-- /.panel-heading -->
                <div class="panel-body" style="max-height: 70vh; overflow-y: scroll;">
                    <!-- Conversations are loaded here -->
                    <div class="direct-chat-messages" style="height: 500px;">

                        <?= $this->render('@app/views/dispatch-status/dialogs/_historic', compact('results','dispatch','myHistory','clients','from_id','answers','token','id','app_id', 'account')) ?>



                    </div><!--/.direct-chat-messages-->

                </div><!-- /.panel-body -->
                <!-- /.panel-body -->
                <div class="panel-footer">

                    <?=$form = Html::beginForm('send-to-client','post'); ?>
                    <div class="input-group">
                        <input type="hidden" name="<?=Yii::$app->request->csrfParam?>" value="<?=Yii::$app->request->csrfToken?>">
                        <input type="text" name="from_id" hidden value="<?=$from_id?>">
                        <input type="text" name="token" hidden value="<?=$token?>">
                        <input type="text" name="id" hidden value="<?=$id?>">
                        <input type="text" name="app_id" hidden value="<?=$app_id?>">
                        <textarea id="shablon-message" required type="text" name="message"  class="form-control">

                </textarea>
                        <span class="input-group-btn">
                            <?php if($account->status != DispatchRegist::STATUS_BLOCKED && $account->status != DispatchRegist::STATUS_SLEEPING): ?>
                                <?= Html::submitButton('Отправить', ['class' =>'btn btn-primary']) ?>
                            <?php endif; ?>
                    </span>
                    </div>
                    <?=Html::endForm() ?>
                </div>
            </div><!--/.direct-chat -->




        </div>
    </div>




    <div class="col-md-6">
        <div class="panel panel-warning direct-chat direct-chat-warning1" style="margin-bottom: 35px;">
            <div class="panel-heading with-border">
                <h3 class="panel-title">Шаблоные ответы</h3>
                <div class="box-tools pull-right">

                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>

                </div>
            </div>
            <div class="panel-body" style="max-height: 75vh; overflow-y: scroll;">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Название</th>
                        <th scope="col">Текст</th>
                        <th scope="col">Действие</th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php if($account->status != DispatchRegist::STATUS_BLOCKED && $account->status != DispatchRegist::STATUS_SLEEPING): ?>
                        <?php $count = 1;?>
                        <?php foreach ($answers as $answer):?>
                            <tr>
                                <th scope="row"><?=$count?></th>
                                <td><?=$answer->name?></td>
                                <td><?=$answer->text?></td>
                                <td><button  type="button" value="<?=$answer->text?>"class="btn btn-primary message-template-add">Выбрать</button></td>
                            </tr>
                            <?php $count++; ?>
                        <?php endforeach;?>
                    <?php endif; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="dispatch-regist-index">

</div>

<script>
    var statuses = {
        changeStatus: function (id) {
            $.ajax({
                url: "?changeStatus=1&id=<?= $clients->id;?>",
                type: 'POST',
                data: {
                    update: <?=$clients->id?>,
                    status: id,
                },
                dataType: 'json',
                success: function (json) {
                    $('#filterForm').submit();
                }
            });
        },
    };
</script>



<?php

$script = <<< JS

$('.message-template-add').click(function(){
    var value = $(this).attr('value');
    $('#shablon-message').val(value);
});

JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>
