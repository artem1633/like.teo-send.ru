<?php

/** @var \app\models\Message[] $myHistory */

use app\models\Message;
use yii\helpers\Html;

$a = \app\models\DispatchRegist::findOne(['id' => $app_id]);
//echo "<pre>";
//print_r($myHistory);
//echo "</pre>";
//
//echo $clients->id;
//echo $a->id;


?>

<div class="messages messages-img">
    <?php foreach ($myHistory as $msg):?>
        <!-- Message. Default to the left -->
        <?php if($msg->from == Message::FROM_CLIENT):?>
            <div class="item in item-visible">
                <div class="image">
                    <?= Html::img($clients->photo ? $clients->photo :  "@web/images/user.png", ['alt' => 'message user image', 'class' => 'direct-chat-img'])?>
                </div>
                <div class="text">
                    <div class="heading">
                        <a href="#"><?=Html::a($clients->name ? $clients->name : $clients->default_account_id, $clients->default_account_id, [ 'target' => '_blank',  'data' => ['pjax' => 0]]);?></a>
                    </div>
                    <?= $msg->text ?>
                </div>
            </div>

        <?php else:?>
            <div class="item item-visible">
                <div class="image">
                    <?= Html::img($a->photo ? $a->photo :  "@web/images/user.png", ['alt' => 'message user image', 'class' => 'direct-chat-img'])?>
                </div>
                <div class="text">
                    <div class="heading">
                        <a href="#"><?php echo $a->username;?></a>
                        <span class="date"><?= $date = $msg->date_time;?> </span>
                    </div>
                    <?= $msg->text?>
                </div>
            </div>
        <?php endif;?>

    <?php endforeach;?>

</div><!--/.direct-chat-messages-->