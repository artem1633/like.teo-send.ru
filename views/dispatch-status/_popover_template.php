<a href="/answer-template/create" role="modal-remote" class="btn btn-sm btn-success btn-block" style="margin-bottom: 10px;">Добавить</a>
<div style="max-height: 300px; overflow-y: scroll;">
    <?php foreach($answers as $answer): ?>
        <h4 style="margin-top: 10px;"><?=$answer->name?></h4><p style="cursor: pointer;" data-text="<?=$answer->text?>" onclick="$('#input-chat').val($(this).data('text'));"><?=iconv_substr($answer->text, 0 , 30 , "UTF-8" )?></p>
    <?php endforeach; ?>
</div>