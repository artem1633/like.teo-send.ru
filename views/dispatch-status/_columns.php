<?php

use app\models\Dispatch;
use app\models\DispatchRegist;
use yii\helpers\Url,
    yii\helpers\Html;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'id',
    // ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => "Фото",
        'content' => function ($data) {

            return Html::img(
                $data->photo ? $data->photo :  "@web/images/user.png" ,
                ['alt' => 'message user image', 'class' => 'direct-chat-img']);


        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'name',
        'content' => function ($data) {
            Yii::warning($data, __METHOD__);
            if (!$data->name) {
                $content = Html::a($data->name . $data->name, 'https://www.instagram.com/'.$data->name,
                    [ 'target' => '_blank',  'data' => ['pjax' => 0]]);
            } else {
                $content = Html::a($data->name ? $data->name : "{$data->account_id}",
                    "{$data->account_id}", ['target' => '_blank', 'data' => ['pjax' => 0]]);
            }
            return $content;

        },
    ],


    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign' => 'middle',
        'template' => '{delete}',
        'urlCreator' => function ($action, $model, $key, $index) {
            return Url::to(['/dispatch-status/delete', 'id' => $key]);
        },
        'viewOptions' => ['role' => 'modal-remote', 'title' => 'View', 'data-toggle' => 'tooltip'],
        'updateOptions' => ['role' => 'modal-remote', 'title' => 'Update', 'data-toggle' => 'tooltip'],
        'deleteOptions' => ['role' => 'modal-remote', 'title' => 'Delete',
            'data-confirm' => false, 'data-method' => false,// for overide yii data api
            'data-request-method' => 'post',
            'data-toggle' => 'tooltip',
            'data-confirm-title' => 'Are you sure?',
            'data-confirm-message' => 'Are you sure want to delete this item'],
    ],

];   