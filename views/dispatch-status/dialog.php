<?php

use app\models\DispatchRegist;
use app\models\DispatchStatus;
use app\models\Message;
use yii\bootstrap\Modal;
use yii\helpers\Html,
    kartik\select2\Select2,
    yii\helpers\ArrayHelper,
    app\models\Statuses;
$this->title = "Переписка";
/** @var \app\models\Message         $history*/
/** @var \app\models\Dispatch        $dispatch*/
/** @var \app\models\DispatchStatus  $clients*/
/** @var \app\models\DispatchStatus  $clientList*/
/** @var \app\models\Message         $msg*/
/** @var \app\models\DispatchRegist  $account*/

\app\assets\pages\ChatPageAsset::register($this);
\johnitvn\ajaxcrud\CrudAsset::register($this);

$clientNames = ArrayHelper::map($clientList, 'id', 'name');
$block ='';
$inf = '';
if ($account->status == DispatchRegist::STATUS_BLOCKED or $account->status == DispatchRegist::STATUS_SLEEPING) {
    $block = "style='display:none;'";
    $inf = "<p style='color: red'>  -  Акк отправителя Заблокирован или спит</p>";
}
?>

<style>
    .popover {
        max-width: 100%;
    }
</style>


<div style="margin-bottom: 0px;">
    <div class="row" style="margin-left: -15px; margin-right: -15px;">
        <div class="col-md-12">

            <!--            <script src='//production-assets.codepen.io/assets/editor/live/console_runner-079c09a0e3b9ff743e39ee2d5637b9216b3545af0de366d4b9aad9dc87e26bfd.js'></script><script src='//production-assets.codepen.io/assets/editor/live/events_runner-73716630c22bbc8cff4bd0f07b135f00a0bdc5d14629260c3ec49e5606f98fdd.js'></script><script src='//production-assets.codepen.io/assets/editor/live/css_live_reload_init-2c0dc5167d60a5af3ee189d570b1835129687ea2a61bee3513dee3a50c115a77.js'></script><meta charset='UTF-8'><meta name="robots" content="noindex"><link rel="shortcut icon" type="image/x-icon" href="//production-assets.codepen.io/assets/favicon/favicon-8ea04875e70c4b0bb41da869e81236e54394d63638a1ef12fa558a4a835f1164.ico" /><link rel="mask-icon" type="" href="//production-assets.codepen.io/assets/favicon/logo-pin-f2d2b6d2c61838f7e76325261b7195c27224080bc099486ddd6dccb469b8e8e6.svg" color="#111" /><link rel="canonical" href="https://codepen.io/emilcarlsson/pen/ZOQZaV?limit=all&page=74&q=contact+" />-->
            <!--            <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,700,300' rel='stylesheet' type='text/css'>-->
            <!---->
            <!--            <script src="https://use.typekit.net/hoy3lrg.js"></script>-->
            <!--            <script>try{Typekit.load({ async: true });}catch(e){}</script>-->
            <!--            <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css'><link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.2/css/font-awesome.min.css'>-->
            <!--

            A concept for a chat interface.

            Try writing a new message! :)


            Follow me here:
            Twitter: https://twitter.com/thatguyemil
            Codepen: https://codepen.io/emilcarlsson/
            Website: http://emilcarlsson.se/

            -->

            <?php \yii\widgets\Pjax::begin(['id' => 'chat-pjax-container']) ?>

            <?php

            \app\assets\PopperAsset::register($this);

            ?>

            <div id="frame" class="col-md-12">
                <div id="sidepanel" style="width: 23%;">
                    <div id="profile">
                        <div class="wrap">
                            <div class="meta">
                                <p class="name"  style="text-overflow: ellipsis;overflow: hidden;width: 80%;"><?=$dispatch->name?></p>
                                <p class="preview"><?= count($clientList)?> <?= \php_rutils\RUtils::numeral()->choosePlural(count($clientList), ['человек', 'человека', 'человек']) ?></p>
                            </div>

                        </div>
                    </div>
                    <div id="search">
                        <label for=""><i class="fa fa-search" aria-hidden="true"></i></label>
                        <input type="text" placeholder="Поиск ..." />
                    </div>
                    <div id="contacts">
                        <ul>
                            <?php
                            //                                    echo "<pre>";
                            //                                    print_r($clientList);
                            //                                    echo "</pre>";
                            /** @var DispatchStatus $item */
                            foreach ($clientList as $item){
                                $st = '';
                                $new = '';
                                if ($item->id == $clients->id) {
                                    $st = 'active';
                                }
                                if ($item->new_message) {
                                    $new = 'online';
                                }

                                $curUrl = \yii\helpers\Url::current();
                                $get = Yii::$app->request->get();
                                $route = [];
                                foreach ($get as $key => $value){

                                    $route = ['dialog'];
                                    $route[$key] = $value;
                                    $route['id'] = $item->id;
                                    break;
                                }
                                $url = \yii\helpers\Url::toRoute($route);

                                $info = [];

                                if($item->city != null){
                                    $info[] = $item->city;
                                }

                                if($item->age != null){
                                    $info[] = $item->age.' '.\php_rutils\RUtils::numeral()->choosePlural($item->age, ['год', 'года', 'лет']);
                                }

                                $info = implode(', ', $info);




                                echo '
                            <li data-client-id="'.$item->id.'" class="contact '.$st.'">
                                <a href="'.$url.'">
                                                                <div class="wrap">
                                    <span class="contact-status '.$new.'"></span>
                                    <img src="'.$item->photo.'" alt="" />
                                    <div class="meta">
                                        <p class="name">'.$item->name.'
                                        <p class="preview">'.$info.'</p>
                                    </div>
                                </div>
                                </a>
                            </li>
                                ';
                            }

                            ?>

                        </ul>
                    </div>
                </div>
                <div class="content">
                    <div class="contact-profile">
                        <img src="<?=$clients->photo ?>" alt="" />
                        <p style="font-size: 14px;"> <?=Html::a($clients->name ? $clients->name : $clients->account_id, "https://vk.com/id{$clients->account_id}", [ 'target' => '_blank', 'data' => ['pjax' => 0]])?>
                        <?=$inf?></p>
                        <div class="social-media">
                            <div class="col-md-4 text-right" style="margin-top: -8px;">
                                <?php if (!$clients->status_push_sales && \app\components\helpers\FunctionHelper::getCompanySettings()->sales_access_token != null) {
                                    echo Html::a("SALES",['pushsls','id' => $clients->id], ['class' =>'btn btn-success', 'data' => ['pjax' => 0]]);}
                                ?>

                            </div>
                            <div class="col-md-8" >
                                <?= Html::dropDownList( 'status',$clients->status,ArrayHelper::map(Statuses::find()->where(['type' => Statuses::TYPE_CLIENT])->all(), 'id', 'name'),
                                    [ 'value' => $clients->status,
                                        'prompt' => 'Выберите статус...',
                                        'class' => 'dropdown-wrapper form-control',
                                        'onchange' => "statuses.changeStatus($(this).val());"
                                    ]
                                ); ?>

                            </div>
                        </div>
                    </div>
                    <div class="messages">
                        <ul>
                            <?php
                            //                            echo "<pre>";
                            //                            print_r($myHistory);
                            //                            echo "</pre>";
                            foreach ($myHistory as $msg){
                                $who = 'sent';
                                $photo = $clients->photo ? $clients->photo :  "@web/images/user.png";
                                $name = Html::a($clients->name ? $clients->name : $clients->account_id, "https://vk.com/id{$clients->account_id}", [ 'target' => '_blank', 'data' => ['pjax' => 0]]);
                                if($msg->from != Message::FROM_CLIENT){
                                    $who = 'replies';
                                    $photo = $account->photo ? $account->photo :  "@web/images/user.png";
                                    $name = Html::a($account->username ? $account->username : $account->account_url, "{$account->account_url}", [ 'target' => '_blank', 'data' => ['pjax' => 0]]);

                                }
                                echo '
                                
                                <li class="'.$who.'">
                                    
                                    <img src="'.$photo.'" alt="" />
                                    <p>
                                        <span style="padding: 0px 0px; font-size: 14px;">'.$name.' </span>
                                        <span style="padding: 0px 0px; font-size: 10px; float: right;">'.$msg->date_time.'</span>
                                        <br/><br/>
                                        '.$msg->text.'
                                    </p>
                                </li>
                                ';
                            }
                            ?>
                        </ul>
                    </div>
                    <div class="message-input">
                        <div class="wrap">
                            <input id="input-chat" type="text" placeholder="Напишите текст сообщения..." <?=$block?>/>




                            <button class="submit" <?=$block?>><i class="fa fa-paper-plane" aria-hidden="true" ></i></button>

                            <button id="template-btn" <?=$block?> >Шаблоны</button>

                        </div>
                    </div>
                </div>
            </div>

            <script src='//production-assets.codepen.io/assets/common/stopExecutionOnTimeout-b2a7b3fe212eaa732349046d8416e00a9dec26eb7fd347590fbced3ab38af52e.js'></script><script src='https://code.jquery.com/jquery-2.2.4.min.js'></script>
            <script >

                var clientNames = JSON.parse('<?=json_encode($clientNames)?>');

                $(".messages").animate({ scrollTop: $(document).height() }, "fast");

                $("#profile-img").click(function() {
                    $("#status-options").toggleClass("active");
                });

                $(".expand-button").click(function() {
                    $("#profile").toggleClass("expanded");
                    $("#contacts").toggleClass("expanded");
                });

                $("#status-options ul li").click(function() {
                    $("#profile-img").removeClass();
                    $("#status-online").removeClass("active");
                    $("#status-away").removeClass("active");
                    $("#status-busy").removeClass("active");
                    $("#status-offline").removeClass("active");
                    $(this).addClass("active");

                    if($("#status-online").hasClass("active")) {
                        $("#profile-img").addClass("online");
                    } else if ($("#status-away").hasClass("active")) {
                        $("#profile-img").addClass("away");
                    } else if ($("#status-busy").hasClass("active")) {
                        $("#profile-img").addClass("busy");
                    } else if ($("#status-offline").hasClass("active")) {
                        $("#profile-img").addClass("offline");
                    } else {
                        $("#profile-img").removeClass();
                    };

                    $("#status-options").removeClass("active");
                });

                $('#search input').keyup(function(){
                    var search = $(this).val();

                    $.each(clientNames, function(index, key){
                        if(key.indexOf(search) < 0){
                            $('li[data-client-id="'+index+'"]').hide();
                        } else {
                            $('li[data-client-id="'+index+'"]').show();
                        }
                    });
                });

                function newMessage() {
                    message = $(".message-input input").val();
                    $.ajax({
                        url: "send-to-client",
                        type: 'POST',
                        data: {
                            'from_id': <?=$clients->account_id?>,
                            'token': null,
                            'message': message,
                            'id': <?=$clients->id?>,
                            'app_id': <?=$account->id?>,
                        },
                        dataType: 'json',
                        success: function (json) {
                            console.log(json);
                        }
                    });
                    if($.trim(message) == '') {
                        return false;
                    }

                    $('<li class="replies"><img src="<?= $account->photo ? $account->photo :  "@web/images/user.png"?>" alt="" /><p>' + message + '</p></li>').appendTo($('.messages ul'));
                    $('.message-input input').val(null);
                    $('.contact.active .preview').html('<span>You: </span>' + message);
                    $(".messages").animate({ scrollTop: $(document).height() }, "fast");
                };

                $('.submit').click(function() {
                    newMessage();
                });

                $(window).on('keydown', function(e) {
                    if (e.which == 13) {
                        newMessage();
                        return false;
                    }
                });
                //# sourceURL=pen.js
            </script>
            <script>
                var statuses = {
                    changeStatus: function (id) {
                        $.ajax({
                            url: "?changeStatus=1&id=<?= $clients->id;?>",
                            type: 'POST',
                            data: {
                                update: <?=$clients->id?>,
                                status: id,
                            },
                            dataType: 'json',
                            success: function (json) {
                                $('#filterForm').submit();
                            }
                        });
                    },
                };
            </script>


            <?php

            $script = <<< JS
            
                $('#chat-pjax-container').on('pjax:start', function(){
                    $('.popover').hide();
                });
            
                $('#template-btn').popover({
                    content: function(){
                        return $('#popover-content-wrapper').html();
                    },
                    title: 'Шаблоны',
                    placement: 'top',
                    html: true,
                    container: 'body',
                });
JS;


            $this->registerJs($script, \yii\web\View::POS_READY);

            ?>


            <?php \yii\widgets\Pjax::end() ?>

        </div>
    </div>
</div>


<div id="popover-content-wrapper" style="display: none;">
    <?= $this->render('_popover_template', ['answers' => $answers]) ?>
</div>


<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
    'options' => [
        'tabindex' => false,
    ],
])?>
<?php Modal::end(); ?>
