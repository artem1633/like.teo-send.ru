<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Rates */
?>
<div class="rates-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'messages_in_transaction',
            'messages_daily_limit',
            'send_interval',
            'dispatch_count',
            'account_count',
            'proxy_count',
            'bots_count',
            [
                'attribute' => 'add_accounts',
                'value' => function ($model) {
                    return $model->add_accounts ? 'ДА' : 'НЕТ';
                }
            ],
            [
                'attribute' => 'add_bots',
                'value' => function ($model) {
                    return $model->add_bots ? 'ДА' : 'НЕТ';
                }
            ],
            [
                'attribute' => 'add_proxy',
                'value' => function ($model) {
                    return $model->add_proxy ? 'ДА' : 'НЕТ';
                }
            ],
            [
                'attribute' => 'dispatch_rules',
                'value' => function ($model) {
                    return $model->dispatch_rules ? 'ДА' : 'НЕТ';
                }
            ],
        ],
    ]) ?>

</div>
