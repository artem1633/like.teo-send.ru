<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Application */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="application-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'atelier_id')->dropDownList($model->getAtelierList(), [ 'prompt' => 'Выберите']) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'product_id')->dropDownList($model->getProductList(), [ 'prompt' => 'Выберите']) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'status_id')->dropDownList($model->getStatusList(), [ 'prompt' => 'Выберите']) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'count')->textInput(['type' => 'number']) ?>
        </div>
    </div>

    <div style="display: none;">
        <?= $form->field($model, 'date_cr')->textInput() ?>
        <?= $form->field($model, 'creator')->textInput() ?>       
    </div>
  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
