<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Application */
?>
<div class="application-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'date_cr',
            'atelier_id',
            'creator',
            'product_id',
            'count',
            'status_id',
        ],
    ]) ?>

</div>
