<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\GlobalMessages */
?>
<div class="global-messages-view">


    <?=$model->text?>
    <div class="panel-body">
        <table id="w0" class="table table-striped table-bordered detail-view">
            <tbody>
            <tr><th>Наименование</th><td><?=$model->name?></td></tr>
            <tr><th>Просмотры</th><td><?=$model->views_count?></td></tr>
            <tr><th>Дата и время</th><td><?=$model->created_at?></td></tr>
            </tbody>
        </table>
    </div>

</div>
