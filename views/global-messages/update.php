<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\GlobalMessages */
?>
<div class="global-messages-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
