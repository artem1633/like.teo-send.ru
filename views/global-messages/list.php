<?php

use johnitvn\ajaxcrud\CrudAsset;
use Yii,
    yii\helpers\Html,
    yii\helpers\Url,
    app\models\Shop;
use yii\bootstrap\Modal;


$this->title = 'Новости';

$this->params['breadcrumbs'][] = ['label' => $this->title];

$this->registerJsFile("js/plugins/equalheights/equalheights.min.js", [
    'depends' => [\app\assets\AppAsset::className()],
], 'js-equals-heights');
$this->registerCssFile("css/pages/trade.css", [
    'depends' => [\app\assets\AppAsset::className()],
], 'css-page');

CrudAsset::register($this);
?>

<div class="panel shop-panel">
    <div class="panel-body">
        <div class="row" style="padding: 20px; 5px;">
            <?php
            if ($items) {
                $col = 0;
                foreach ($items as $item) {
                    $remains = 0;
                    $col++;
                    if ($col == 1) {
                        ?>
                        <div class="row">
                        <?php
                    }
                    ?>
                    <div class="col-md-3" style="padding-bottom: 30px;">
                        <form id="form-<?= $item->id ?>">
                            <input type="hidden" name="item" value="<?= $item->id ?>">
                            <div style="padding: 10px; border-radius: 10px; border: 1px solid #c2bec4; box-shadow: 0 5px 15px rgba(0,0,0,0.3);">
                                <div class="prod-info">
                                    <h4>
                                        <?= Html::a($item->name, Url::to(['global-messages/view', 'id' => $item->id]), [ 'role' => "modal-remote", 'data-toggle' => "tooltip", 'data-original-title' =>"Подробнее"]) ?>
                                    </h4>
                                    <h5 class="shop-card-description">
                                        <?= Html::a($item->created_at, Url::to(['global-messages/view', 'id' => $item->id]), [ 'role' => "modal-remote", 'data-toggle' => "tooltip", 'data-original-title' =>"Подробнее"]) ?>

                                    </h5>

                                </div>
                                <div class="shop-card-full-description">
                                    <?php
                                        $text = substr($item->text,0,600);
                                        $text = str_replace(" height=\"300\" ", " height=\"150\" ",  $text);
                                        $text = str_replace("width=\"550\"", "width=\"237\"", $text);
                                        echo $text;
                                    ?>
                                </div>
                                <div class="row shop-card-floor">
                                    <div class="col-xs-6" style="padding-left: 0;padding-top: 2px;">
                                        <h6 class="text-success-<?= $item->id ?>" style="margin-top: 6px;">
                                            <?=$item->views_count ?>
                                            <i class="fa fa-eye"></i>
                                            <br/>
                                            <span style="font-size: 12px" class="js-alert-<?= $item->id ?>"></span>
                                        </h6>
                                    </div>
                                        <div class="col-xs-2 text-right item-accounts">
                                            <a class="btn btn-success js-buy" href="/global-messages/view?id=<?=$item->id?>" title="" role="modal-remote" data-toggle="tooltip" data-original-title="Подробнее">Подробнее</a>
                                        </div>
                                </div>
                            </div>
                        </form>
                    </div>

                    <?php
                    if ($col == 4) {
                        $col = 0;
                        ?>
                        </div>
                        <?php
                    }
                }
            }
            ?>
        </div>
    </div>
</div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
