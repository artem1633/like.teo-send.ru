<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Parser */
?>
<div class="parser-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
