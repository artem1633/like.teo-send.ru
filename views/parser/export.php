<?php

/**
 * @var $model \app\models\ParserExport
 */

use yii\widgets\ActiveForm;

?>

<?php $form = ActiveForm::begin() ?>

<div class="row">
    <div class="col-md-12">
        <?= $form->field($model ,'period')->widget(\kartik\daterange\DateRangePicker::class, [
            'options' => [
                'class' => 'form-control',
                'autocomplete' => 'off',
            ],
        ]) ?>
    </div>
</div>

<?php ActiveForm::end() ?>
