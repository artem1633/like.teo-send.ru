<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Parser */
?>
<div class="parser-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'type',
            'status',
            'company_id',
            'progress',
            'count',
            'sex',
            'age_start',
            'age_end',
            'location',
            'online_period_start',
            'online_period_end',
            'created_at',
        ],
    ]) ?>

</div>
