<?php

use app\models\AccountingReport;
use app\models\Companies;
use app\models\DailyReport;
use johnitvn\ajaxcrud\CrudAsset;
use skeeks\widget\highcharts\Highcharts;
use yii\bootstrap\Html;
use yii\bootstrap\Modal;
use yii\widgets\ActiveForm;
use dosamigos\chartjs\ChartJs;

CrudAsset::register($this);

/**
 * @var $report array
 * @var $model \app\models\RegistrationReportFilter
 */
$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, 'http://neyro.teo-bot.ru/api/vk/report');
curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, ['id' => $model->id]);
$output = curl_exec($ch);
curl_close($ch);

$report = json_decode($output, true);

?>


<div class="panel panel-success">
    <div class="panel-heading">
        <div class="panel-title">Отчет о вступлениях</div>

    </div>
    <div class="panel-body">

        <?= ChartJs::widget([
            'type' => 'line',
            'options' => [
                'class' => 'chartjs-render-monitor',
                'height' => 80,
                'width' => 300
            ],
            'data' => [
                'labels' => $report[0],
                'datasets' => [
                    [
                        'label' => "Вступивших",
                        'backgroundColor' => "rgba(179,181,198,0.2)",
                        'borderColor' => "rgba(179,181,198,1)",
                        'pointBackgroundColor' => "rgba(179,181,198,1)",
                        'pointBorderColor' => "#fff",
                        'pointHoverBackgroundColor' => "#fff",
                        'pointHoverBorderColor' => "rgba(179,181,198,1)",
                        'data' => $report[1]
                    ],
                    /*[
                        'label' => "My Second dataset",
                        'backgroundColor' => "rgba(255,99,132,0.2)",
                        'borderColor' => "rgba(255,99,132,1)",
                        'pointBackgroundColor' => "rgba(255,99,132,1)",
                        'pointBorderColor' => "#fff",
                        'pointHoverBackgroundColor' => "#fff",
                        'pointHoverBorderColor' => "rgba(255,99,132,1)",
                        'data' => [28, 48, 40, 19, 96, 27, 100]
                    ]*/
                ]
            ]
        ]);
        ?>
    </div>
    <div class="col-md-12">
        <div class="panel panel-default" style="margin-top: 20px;">
            <div class="panel-body">
                <table class="table table-bordered" style="padding:0px;">
                    <thead>
                    </thead>
                    <tbody>
                    <tr style="background: darkorange;">
                        <td><?='Итого'?></td>
                        <td><?="Вступлений: ".array_sum($report[1])?></td>
                        <td>
                            <?= "Всего в группе: ".array_sum($report[2])?>
                        </td>
                    </tr>


                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>


<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "",// always need it for jquery plugin
]) ?>


<?php Modal::end(); ?>
