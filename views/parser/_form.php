<?php
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Parser */
/* @var $form yii\widgets\ActiveForm */

\app\widgets\bowerUiSlider\BowerUiSliderAsset::register($this);

$linkWrapperStyle = '';
$listWrapperStyle = '';
$formWrapperStyle = '';

if($model->type === null){
    $linkWrapperStyle = 'style="display:none;"';
    $listWrapperStyle = '';
    $formWrapperStyle = '';
} else if($model->type === 1){
    $listWrapperStyle = '';
    $linkWrapperStyle = 'style="display:none;"';
    $formWrapperStyle = '';
} else if($model->type === 2){
    $linkWrapperStyle = '';
    $listWrapperStyle = 'style="display:none;"';
    $formWrapperStyle = '';
} else if($model->type === 0){
    $linkWrapperStyle = '';
    $listWrapperStyle = 'style="display:none;"';
    $formWrapperStyle = 'style="display:none;"';
}


$formatJs = <<< JS
var formatRepo = function (repo) {
    if(repo.loading){
        return 'Поиск';
    }
    
    if(repo.region != null && repo.area != null)
    {   
        return '<span style="font-size: 15px;">'+repo.title+'</span>'+'<br>'+repo.region+' | '+repo.area;
    }
    
    if(repo.region == null && repo.area == null)
    {
        return '<div style="display: block; padding-bottom: 5px;"><span style="font-size: 17px;">'+repo.title+'</span></div>';
    }
    
    if(repo.region != null && repo.area == null)
    {
        return '<span style="font-size: 15px;">'+repo.title+'</span>'+'<br>'+repo.region;
    }
};
var formatRepoSelection = function (repo) {
    return repo.title || repo.text;
}
JS;

// Register the formatting script
$this->registerJs($formatJs, View::POS_HEAD);

// script to parse the results into the format expected by Select2
$resultsJs = <<< JS
function (data, params) {
    params.page = params.page || 1;
    return {
        results: data.results,
        pagination: {
            more: (params.page * 30) < data.total_count
        }
    };
}
JS;

?>

<div class="parser-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'type')->dropDownList(\app\models\Parser::getTypesList(), ['prompt' => 'Выберите тип']) ?>
        </div>
    </div>

    <div class="link-wrapper" <?=$linkWrapperStyle?>>
        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'link')->textInput() ?>
            </div>
        </div>
    </div>

    <div class="list-wrapper" <?=$listWrapperStyle?>>
        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'list')->textarea() ?>
            </div>
        </div>
    </div>

    <div class="form-wrapper" <?=$formWrapperStyle?>>
        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'sex')->dropDownList(\app\models\Parser::getSexList(), ['prompt' => 'Все']) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'age_start')->input('number') ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'age_end')->input('number') ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="slider"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <?php
                    echo $form->field($model, 'location')->widget(Select2::class, [
                    'initValueText' => 'Город', // set the initial display text
                    'options' => ['placeholder' => 'Поиск города'],
                    'pluginOptions' => [
                        'allowClear' => false,
                        'minimumInputLength' => 3,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'Поиск...'; }"),
                        ],
                        'ajax' => [
                            'url' => \yii\helpers\Url::toRoute(['dispatch-regist/vk-cities']),
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term, page: params.page}; }'),
                            'processResults' => new JsExpression($resultsJs),
                            'cache' => true
                        ],
                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                        'templateResult' => new JsExpression('formatRepo'),
                        'templateSelection' => new JsExpression('formatRepoSelection'),
                    ],
                ]);?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'onlinePeriod')->widget(\kartik\daterange\DateRangePicker::class, [
                    'convertFormat'=>true,
                    'pluginOptions' => [
                        'timePicker' => true,
                        'timePickerIncrement' => 15,
                        'timePicker24Hour' => true,
                        'locale'=>['format' => 'Y-m-d H:i:s'],
                    ],
                ]) ?>
            </div>
        </div>
    </div>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>


<?php

$script = <<< JS

 $('#parser-type').change(function(){
     var value = $(this).val();
     if(value == 1){
        $('.list-wrapper').show();
        $('.link-wrapper').hide();
        $('.form-wrapper').show();
     } else if(value == 2){
        $('.link-wrapper').show();
        $('.list-wrapper').hide();
        $('.form-wrapper').show();
     } else if(value == 0){
         $('.link-wrapper').show();
         $('.form-wrapper').hide();
         $('.list-wrapper').hide();
     }
 });

JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>