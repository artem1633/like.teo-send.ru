<?php
use app\models\Companies;
use app\models\Parser;
use yii\helpers\Html;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
        'content' => function($model){
            $output = $model->name.'<br>';
            if ($model->type == 0) {
                $output .= Html::a('<span class="glyphicon glyphicon-equalizer"></span>', ['report', 'id' => $model->id], ['class' => 'btn btn-success btn-xs']);
            }
            $output .= ' '.Html::a('<span class="glyphicon glyphicon-share"></span>', ['export', 'id' => $model->id], ['class' => 'btn btn-success btn-xs', 'role' => 'modal-remote']);
            $output .= ' '.Html::a('<i class="fa fa-files-o"></i>', ['create', 'copy_id' => $model->id], ['class' => 'btn btn-info btn-xs', 'role' => 'modal-remote']);
            $output .= ' '.Html::a('<span class="fa fa-eye"></span>', ['view', 'id' => $model->id], [
                'class' => 'btn btn-primary btn-xs',
                'role' => 'modal-remote',
                'title' => 'Просмотр',
                'data-toggle' => 'tooltip'
            ]);
            $output .= ' '.Html::a('<span class="fa fa-trash-o"></span>', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger btn-xs',
                'role' => 'modal-remote', 'title' => 'Удалить',
                'data-confirm' => false, 'data-method' => false,// for overide yii data api
                'data-request-method' => 'post',
                'data-toggle' => 'tooltip',
                'data-confirm-title' => 'Подтвердите действие',
                'data-confirm-message' => 'Вы уверены что хотите удалить этого элемента?',
            ]);

            return $output;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'type',
        'content' => function($model){
            if($model->type !== null)
            {
                $parser = Parser::getTypesList()[$model->type];
                if ($model->type == 0 or $model->type == 2){
                    $parser .= "<br/> {$model->link}";
                }
                return $parser;
            }

            return null;
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'status',
        'content' => function($model){

            $style = 'active';
            $status = Parser::getStatusList()[$model->status];
            if ($model->count == null and $model->type == 0) {
                $model->progress  = 0;
                $model->count  = 0;
                $status = 'Сбор текущих пользователей';
                $style = 'danger';
            } elseif ($model->count != null and $model->type == 0) {
                $model->progress  = 100;
                $style = 'danger';
            } elseif ($status == 0) {
                $style = 'info';
            } elseif ($status == 1) {
                $style = 'active';
            }

            $text = "<div class=\"progress progress-sm {$style}\">
                <div class=\"progress-bar progress-bar-{$style} progress-bar-striped\" role=\"progressbar\" aria-valuenow=\"{$model->progress}\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width: {$model->progress}%\">
                  <span class=\"sr-only\">{$model->progress}% Complete</span>
                </div>
              </div>";
            if($model->status !== null)
            {
                $text .= '  '. $status;
                return $text;
            }

            return $text;
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'count',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'company_id',
        'visible' => Yii::$app->user->identity->isSuperAdmin(),
        'content' => function ($data){
            $comp = Companies::findOne(['id' => $data->company_id]);
            $info = Html::a($comp->company_name, ['/companies/view', 'id' => $data->company_id], ['target' => '_blank']);
            $info .= '<br/>'.$comp->general_balance.'  <i class="fa fa-rub">    </i>    ';
            $info .= $comp->bonus_balance.'  <i class="fa fa-gift text-info"></i>';
            return $info;
        }
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'sex',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'age_start',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'age_end',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'location',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'online_period_start',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'online_period_end',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'created_at',
    // ],
//    [
//        'class' => 'kartik\grid\ActionColumn',
//        'dropdown' => false,
//        'vAlign'=>'middle',
//        'template' => '{view} {delete}',
//        'urlCreator' => function($action, $model, $key, $index) {
//            return Url::to([$action,'id'=>$key]);
//        },
//        'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
//        'updateOptions'=>['role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'],
//        'deleteOptions'=>['role'=>'modal-remote','title'=>'Delete',
//            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
//            'data-request-method'=>'post',
//            'data-toggle'=>'tooltip',
//            'data-confirm-title'=>'Are you sure?',
//            'data-confirm-message'=>'Are you sure want to delete this item'],
//    ],

];   