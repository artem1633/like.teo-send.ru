<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\autoregist\AutoRegistrSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

CrudAsset::register($this);

?>
<div class="auto-registr-index">
    <div id="ajaxCrudDatatable">
        <?=GridView::widget([
            'id'=>'crud-auto-registr-datatable',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'pjax'=>true,
            'columns' => [
                [
                    'class' => 'kartik\grid\CheckboxColumn',
                    'width' => '20px',
                ],
                [
                    'class' => 'kartik\grid\SerialColumn',
                    'width' => '30px',
                ],
                // [
                // 'class'=>'\kartik\grid\DataColumn',
                // 'attribute'=>'id',
                // ],
                [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'name',
                ],
                [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'type',
                    'content' => function($model) {
                        if($model->type != null)
                        {
                            return \app\models\AutoRegistr::getTypesList()[$model->type];
                        }
                        return null;
                    },
                ],
                [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'status',
                    'content' => function($model) {
                        if($model->status != null)
                        {
                            return \app\models\AutoRegistr::getStatusList()[$model->status];
                        }
                        return null;
                    },
                ],
                [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'action',
                ],
                [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute' => 'template.name',
                    'label' => 'Шаблон'
                ],
                [
                    'class' => 'kartik\grid\ActionColumn',
                    'dropdown' => false,
                    'vAlign'=>'middle',
                    'urlCreator' => function($action, $model, $key, $index) {
                        return Url::to(['auto-registr/'.$action,'id'=>$key]);
                    },
                    'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
                    'updateOptions'=>['role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'],
                    'deleteOptions'=>['role'=>'modal-remote','title'=>'Delete',
                        'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                        'data-request-method'=>'post',
                        'data-toggle'=>'tooltip',
                        'data-confirm-title'=>'Вы уверены?',
                        'data-confirm-message'=>'Вы уверены, что хотите удалить эту записись?'],
                ],

            ],
            'toolbar'=> [
                ['content'=>
                    Html::a('<i class="glyphicon glyphicon-plus"></i>', ['auto-registr/create'],
                        ['role'=>'modal-remote','title'=> 'Создать','class'=>'btn btn-info']).
                    Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                        ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Reset Grid']).
                    '{toggleData}'.
                    '{export}'
                ],
            ],
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'toolbarContainerOptions' => ['style' => 'margin-top: 15px;'],
            'panel' => [
                'type' => 'default',
                'heading' => '<i class="glyphicon glyphicon-list"></i> Автоматическое заполнение аккаунта',
                'after'=>BulkButtonWidget::widget([
                        'buttons'=>Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; Удалить',
                            ["auto-registr/bulk-delete"] ,
                            [
                                "class"=>"btn btn-danger btn-xs",
                                'role'=>'modal-remote-bulk',
                                'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                                'data-request-method'=>'post',
                                'data-confirm-title'=>'Вы уверены?',
                                'data-confirm-message'=>'Вы уверены, что хотите удалить эти записи?'
                            ]),
                    ]).
                    '<div class="clearfix"></div>',
            ]
        ])?>
    </div>
</div>
