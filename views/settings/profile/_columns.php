<?php
use yii\helpers\Url;
use yii\helpers\Html;
use app\models\Users;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'login',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'vk_id',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'filter' => Users::getRoleList(),
        'attribute'=>'role_id',
        'content' => function($data){
            return $data->getRoleDescription();
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'telephone',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'data_cr',
        'format' => 'date',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'filter' => Users::getStatusList(),
        'attribute'=>'status',
        'content' => function($data){
            return $data->getStatusDescription();
        }
    ],
    [
        'class'    => 'kartik\grid\ActionColumn',
        'template' => '{leadView} {leadUpdate} {leadDelete}',
        'contentOptions' => ['style' => 'white-space: nowrap;'],
        'buttons'  => [ 
            'leadView' => function ($url, $model) 
            {
                $url = Url::to(['/users/view', 'id' => $model->id]);
                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, ['class' => 'btn btn-primary btn-xs', 'role'=>'modal-remote','title'=>'', 'data-toggle'=>'tooltip']);
            },           
            'leadUpdate' => function ($url, $model) 
            {
                $url = Url::to(['/users/update', 'id' => $model->id]);
                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['class' => 'btn btn-success btn-xs', 'role'=>'modal-remote','title'=>'', 'data-toggle'=>'tooltip']);
            },
            'leadDelete' => function ($url, $model) 
            {
                if($model->id != 1 && \Yii::$app->user->identity->isSuperAdmin())
                {
                    $url = Url::to(['/users/delete', 'id' => $model->id]);
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                        'role'=>'modal-remote','title'=>'',
                        'class' => 'btn btn-danger btn-xs',
                              'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                              'data-request-method'=>'post',
                              'data-toggle'=>'tooltip',
                              'data-confirm-title'=>'Подтвердите действие',
                              'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?',
                    ]);
                }
            },
        ]
    ]

];   