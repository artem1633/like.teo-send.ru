<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\models\Shop;
use kartik\file\FileInput;
use app\models\uploads\UploadItem;

/* @var $this yii\web\View */
/* @var $model app\models\Shop */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="shop-form">

    <div class="row">
        <div class="col-md-12">
            <?= FileInput::widget([
                'model' => new UploadItem,
                'attribute' => 'fileItem',
                'options' => ['multiple' => false],
                'pluginOptions' => [
                    'showUpload' => true,
                    'uploadUrl' => Url::to(['/store/upload']),
                    'uploadExtraData' => [
                        'id' => $model->id,
                        Yii::$app->getRequest()->csrfParam => Yii::$app->getRequest()->getCsrfToken(),
                    ],
                    'overwriteInitial' => false,
                    'fileActionSettings' => [
                        'showHandle' => false,
                    ],
                    'initialPreview' => $model->image ? [Url::to([$model->image])] : false,
                    'initialPreviewAsData' => $model->image ? true : null,
                ]
            ]); ?>
        </div>
    </div>
</div>
