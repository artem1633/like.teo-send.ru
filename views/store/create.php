<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Shop */

?>
<div class="shop-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
