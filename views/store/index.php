<?php

use yii\helpers\Html,
    yii\helpers\Url,
    yii\bootstrap\Modal,
    kartik\grid\GridView,
    johnitvn\ajaxcrud\CrudAsset,
    app\components\helpers\FunctionHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\shop\ShopSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Shops';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);
?>
    <div class="shop-index">
        <div class="atelier-index">
            <div id="ajaxCrudDatatable">
                <?= GridView::widget([
                    'id' => 'crud-datatable',
                    'dataProvider' => $dataProvider,
//                    'filterModel' => $searchModel,
                    'pjax' => ['enablePushState' => false],
                    'responsiveWrap' => false,
                    'rowOptions' => function ($model) {
                        return ['style' => 'background-color: ' . ($model->access ? '' : '#ffe6b3')];
                    },
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        'name',
                        [
                            'class' => '\kartik\grid\DataColumn',
                            'filter' => false,
                            'attribute' => 'description_short',
                        ],
                        [
                            'class' => '\kartik\grid\DataColumn',
                            'filter' => false,
                            'attribute' => 'price',
                            'hAlign' => GridView::ALIGN_CENTER,
                            'content' => function($model){
                                if($model->price != null){
                                    return "{$model->price} <i class='fa fa-ruble'></i>";
                                }

                                return null;
                            },
                        ],
                        [
                            'class' => '\kartik\grid\DataColumn',
                            'filter' => false,
                            'attribute' => 'remains',
                            'hAlign' => GridView::ALIGN_CENTER,
                            'content' => function ($model) {
                                return $model->getRemains();
                            },

                        ],
                        [
                            'class' => '\kartik\grid\DataColumn',
                            'filter' => false,
                            'attribute' => 'date_added',
                            'content' => function ($model) {
                                return FunctionHelper::dateForForm($model->date_added, 1);
                            },

                        ],
                        [
                            'class' => '\kartik\grid\DataColumn',
                            'filter' => false,
                            'attribute' => 'date_updated',
                            'content' => function ($model) {
                                return $model->date_updated ? FunctionHelper::dateForForm($model->date_updated, 1) : 'не обновлялся';
                            },

                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'headerOptions' => ['width' => '160'],
                            'template' => '{copy} {view} {image} {update} {delete}',
                            'buttons' => [
                                'copy' => function ($url, $model) {
                                    $url = Url::to(['copy', 'id' => $model->id]);
                                    return Html::a('<span class="fa fa-files-o" style="font-size: 12px;"></span>', $url, [
                                        'class' => 'btn btn-info btn-xs',
                                        'title' => 'Скопировать',
                                        'data-toggle' => 'tooltip'
                                    ]);
                                },
                                'view' => function ($url, $model) {
                                    $url = Url::to(['view', 'id' => $model->id]);
                                    return Html::a('<span class="fa fa-eye" style="font-size: 12px;"></span>', $url, [
                                        'class' => 'btn btn-primary btn-xs',
                                        'role' => 'modal-remote',
                                        'title' => 'Просмотр',
                                        'data-toggle' => 'tooltip'
                                    ]);
                                },
                                'image' => function ($url, $model) {
                                    $url = Url::to(['image', 'id' => $model->id]);
                                    return Html::a('<span class="fa fa-image" style="font-size: 12px;"></span>', $url, [
                                        'class' => 'btn btn-warning btn-xs',
                                        'role' => 'modal-remote',
                                        'title' => 'Изображение',
                                        'data-toggle' => 'tooltip'
                                    ]);
                                },
                                'update' => function ($url, $model) {
                                    $url = Url::to(['update', 'id' => $model->id]);
                                    return Html::a('<span class="fa fa-pencil-square-o" style="font-size: 12px;"></span>', $url, [
                                        'class' => 'btn btn-success btn-xs',
                                        'role' => 'modal-remote',
                                        'title' => 'Изменить',
                                        'data-toggle' => 'tooltip'
                                    ]);
                                },
                                'delete' => function ($url, $model) {
                                    $url = Url::to(['delete', 'id' => $model->id]);
                                    return Html::a('<span class="fa fa-trash-o" style="font-size: 12px;"></span>', $url, [
                                        'class' => 'btn btn-danger btn-xs',
                                        'role' => 'modal-remote',
                                        'data-confirm' => true,
                                        'title' => 'Подтвердите Удаление',
                                        'data-toggle' => 'tooltip',
                                        'data-method' => 'post',
                                        'data-confirm-title' => 'Подтвердите действие',
                                        'data-confirm-message' => 'Вы уверены что хотите удалить этого элемента?',
                                    ]);
                                },
                            ],
                        ]

                    ],

                    'toolbar' => [
                        ['content' =>
                            Html::a('Создать', ['create'], ['role' => 'modal-remote', 'title' => 'Создать', 'class' => 'btn btn-info']) .
                            Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''], ['data-pjax' => 1, 'class' => 'btn btn-default', 'title' => 'Обновить']) .
                            '{toggleData}'
                        ],
                    ],
                    'striped' => true,
                    'condensed' => true,
                    'responsive' => true,
                    'toolbarContainerOptions' => ['style' => 'margin-top: 15px;'],
                    'panel' => [
                        'type' => 'default',
                        'heading' => '<i class="glyphicon glyphicon-list"></i> Товары',
                        'before' => '',
                        'after' => false,
                    ]
                ]); ?>
            </div>
        </div>
    </div>
<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "",// always need it for jquery plugin
]) ?>
<?php Modal::end(); ?>