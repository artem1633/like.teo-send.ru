<?php
use app\models\Users;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

CrudAsset::register($this);

$this->title = 'Авто задачи';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="dispatch-index">

    <?php

    $content = '';

    $content .=
        Html::a('Добавить задачу <i class="glyphicon glyphicon-plus"></i>', ['create', 'platform' => $platform],
            ['role' => 'modal-remote', 'title' => 'Создать новую задачу', 'class' => 'btn btn-info', 'style' => 'margin-left: 50px;']);


    ?>



    <div id="ajaxCrudDatatable">
        <?= GridView::widget([
            'id' => 'crud-datatable',
            'dataProvider' => $dataProvider,
            //'filterModel' => $searchModel,
            'pjax' => true,
            'responsiveWrap' => false,
            'columns' => require(__DIR__ . '/_columns.php'),
            'toolbar' => [
                ['content' =>$content],
            ],
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'toolbarContainerOptions' => ['style' => 'margin-top: 15px;'],
            'panel' => [
                'type' => 'default',
                'heading' => Yii::$app->session->hasFlash('danger') || Yii::$app->session->hasFlash('warning')
                    ? '<i class="glyphicon glyphicon-list"></i> '
                    . ' Внимание! ' . Yii::$app->session->getFlash('danger')
                    : '<i class="glyphicon glyphicon-list"></i> ' . $this->title,
                'after' => BulkButtonWidget::widget([
                        'buttons' => Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; Delete All',
                            ["bulk-delete"],
                            [
                                "class" => "btn btn-danger btn-xs",
                                'role' => 'modal-remote-bulk',
                                'data-confirm' => false, 'data-method' => false,// for overide yii data api
                                'data-request-method' => 'post',
                                'data-confirm-title' => 'Ты уверен?',
                                'data-confirm-message' => 'Вы действительно хотите удалить этот элемент'
                            ]),
                    ]) .
                    '<div class="clearfix"></div>',
            ]
        ]) ?>
    </div>
</div>
<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "",// always need it for jquery plugin
]) ?>


<?php Modal::end(); ?>
