<?php

use yii\helpers\Html,
    app\models\Companies,
    app\models\Bots;


/* @var $this yii\web\View */
/* @var $model app\models\Dispatch */




?>
<div class="automation-create">
    <?= $this->render('_form', [
        'model' => $model,
        'platform' => $platform,
        'client_id' => $client_id,
    ]) ?>
</div>
