<?php

use yii\helpers\Html,
    app\models\Companies,
    app\models\Bots;

/* @var $this yii\web\View */
/* @var $model app\models\Dispatch */
?>
<div class="dispatch-update">

    <?= $this->render('_form', [
        'model' => $model,
        'client_id' => $client_id,
    ]) ?>

</div>
