<?php

use app\models\Companies;
use app\models\CompanySettings;
use app\models\DispatchConnect;
use app\models\DispatchRegist;
use app\models\DispatchStatus;
use app\models\Users;
use yii\bootstrap\Html;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'template' => '{view} {update} {delete}',
        'urlCreator' => function($action, $model, $key, $index) {
            return Url::to(['automation/'.$action,'id'=>$key]);
        },
        'buttons'=>[
            'export' => function ($url, $model, $key) {
                return Html::a('<span class="glyphicon glyphicon-share"></span>', ['status-export', 'id' => $model->id], [
                    'class' => 'btn btn-success btn-xs',
                    'role' => 'modal-remote',
                ]);
            },
            'view' => function ($url, $model, $key) {
                return Html::a('<span class="fa fa-eye"></span>', ['dispatch/index', 'platform' => 'instagram', 'automation_id' => $model->id], [
                    'class' => 'btn btn-info btn-xs',
                    'data-pjax' => 0,
                ]);
            },
//            'copy' => function ($url, $model, $key) {
//                    return Html::a('<i class="fa fa-files-o"></i>', ['create', 'copy_id' => $model->id], ['class' => 'btn btn-info btn-xs', 'role' => 'modal-remote']);
//            },
        ],
        'viewOptions'=>['class' => 'btn btn-primary btn-xs', 'data-pjax' => '0','title'=>'View','data-toggle'=>'tooltip'],
        'updateOptions'=>['class' => 'btn btn-success btn-xs', 'role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['class' => 'btn btn-danger btn-xs', 'role'=>'modal-remote','title'=>'Delete',
            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
            'data-request-method'=>'post',
            'data-toggle'=>'tooltip',
            'data-confirm-title'=>'Are you sure?',
            'data-confirm-message'=>'Are you sure want to delete this item'],
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'company_id',
//        'content' => function($model){
//            $d = \app\models\User::findIdentity($model->company_id);
//            return $d->name;
//        }
//    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'mission',
        'content' => function($data) {
            $content = Html::a($data->link ? $data->link : "{$data->link}",
                    "{$data->link}", ['target' => '_blank', 'data' => ['pjax' => 0]]).'<br>';
            if ($data->type == 'like_new_cont') {
                $content .= $data->mission.'<span class="glyphicon glyphicon-heart"></span>';
            }
            if ($data->type == 'rep_new_cont') {
                $content .= $data->mission.' <span class="glyphicon glyphicon-share-alt"></span>';
            }
            if ($data->type == 'com_new_cont') {
                $content .= $data->mission.' <span class="glyphicon glyphicon-send"></span>';
            }

//            if(in_array($data->status, \app\models\Automation::getStatusesList()))
//            {
                $content .= '—'.\app\models\Automation::getStatusesList()[$data->status].' (Сумма: '.$data->sum.')';
//            }

            return $content;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'count',
        'label' => 'Кол-во',
        'content' => function($model){
            $output = '';

            $output .= 'Кол-во лайков: '.$model->likesCount.'<br>';
            $output .= 'Кол-во постов: '.$model->postsCount;

            return $output;
        },
    ],
    [
        'class' => 'kartik\grid\DataColumn',
        'attribute' => 'dates',
        'label' => 'Дата',
        'content' => function($model){
            return implode(' <br> ', [$model->date_start, $model->date_end]);
        },
        'width' => '100px;',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'company_id',
        'visible' => Yii::$app->user->identity->isSuperAdmin(),
        'content' => function ($data){
            $comp = Companies::findOne(['id' => $data->company_id]);
            $info = Html::a($comp->company_name, ['/companies/view', 'id' => $data->company_id], ['target' => '_blank']);
            $info .= '<br/>'.$comp->general_balance.'  <i class="fa fa-rub">    </i>    ';
            $info .= $comp->bonus_balance.'  <i class="fa fa-gift text-info"></i>';
            return $info;
        }
    ],

];   