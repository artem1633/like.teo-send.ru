<?php
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Automation */
/* @var $platform string */
/* @var $form yii\widgets\ActiveForm */
$style = '';
if ($model->type != 'com_new_cont') {
    $style = 'style="display:none;"';
}

if($model->isNewRecord){
    $model->client_id = $client_id;
} else if($model->date_start != null && $model->date_end != null) {
    $model->dates = implode(' - ', [$model->date_start, $model->date_end]);
}
?>

<div class="automation-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'client_id')->widget(Select2::class, [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\Clients::find()->all(), 'id' ,'fio'),
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'type')->dropDownList(\app\models\Automation::getTypesList(), ['prompt' => 'Выберите тип']) ?>
        </div>
    </div>

    <div class="row">
        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'link')->textInput() ?>
            </div>
        </div>
    </div>

    <div class="link-wrapper" <?=$style?>>
        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'text')->textarea() ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'mission')->textInput(['type' => 'number']) ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'dates')->widget(\kartik\daterange\DateRangePicker::class, [

                ]) ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'status')->dropDownList(\app\models\Automation::getStatusesList()) ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'sum')->textInput(['type' => 'number']) ?>
            </div>
        </div>
    </div>

    <?= $form->field($model, 'platform')->textInput(['value' => $platform, 'style' => 'display:none;'])->label(false) ?>



    <?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
<?php

$script = <<< JS

 $('#automation-type').change(function(){
     var value = $(this).val();
     if(value == 'com_new_cont'){
        $('.link-wrapper').show();
     } else {
        $('.link-wrapper').hide();
     }
 });

JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>
