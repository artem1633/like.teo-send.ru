<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Statuses */

?>
<div class="statuses-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>