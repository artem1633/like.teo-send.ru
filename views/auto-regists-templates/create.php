<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\AutoRegistsTemplates */

?>
<div class="auto-regists-templates-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
