<?php

use yii\widgets\ActiveForm;

/**
 * @var $model \app\models\Dispatch\DispatchRegistSearch
 */

?>

<?php $form = ActiveForm::begin(['id' => 'search-form', 'method' => 'get', 'options' => ['style' => 'width: 200px; float: left;']]) ?>

    <?= $form->field($model, 'company_id')->widget(\kartik\select2\Select2::class, [
            'data' => \yii\helpers\ArrayHelper::map(\app\models\Companies::find()->all(), 'id', 'company_name'),
            'size' => 'sm',
            'options' => [
                'placeholder' => 'Выберите компанию',
                'allowClear' => true,
                'style' => "float: left;",
                'onchange' => '$("#search-form").submit();'
            ],
        ])->label(false) ?>

<?php ActiveForm::end() ?>
