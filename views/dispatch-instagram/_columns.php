<?php

use app\models\Companies;
use app\models\CompanySettings;
use app\models\DispatchInstagram;
use app\models\DispatchRegist;
use app\models\DispatchStatus;
use app\models\Proxy;
use app\models\Settings;
use yii\bootstrap\Html;
use yii\helpers\Url;

/** @var DispatchRegist $data */

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'username',
        'content' => function($data){

            $find_copy = DispatchInstagram::find()->where(['login' => $data->login])->count();
            if ($find_copy > 1) {
                $text = "<span class='text-danger'> - {$find_copy}</span>";
            }
            $content = Html::a($data->login . $text, 'https://www.instagram.com/'.$data->login,
                [ 'target' => '_blank',  'data' => ['pjax' => 0]]);
            $url = Url::to(['/dispatch-regist/view', 'id' => $data->id]);
            $view = Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url,
                ['class' => 'btn btn-primary btn-xs', 'role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip']);

            $url = Url::to(['/dispatch-regist/delete', 'id' => $data->id]);
            $delete = Html::a('<button class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span></button>', $url,
                [
                    'role'=>'modal-remote','title'=>'Удалить',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                    'data-toggle'=>'tooltip',
                    'data-confirm-title'=>'Подтвердите действие',
                    'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?'
                ]);


            $url = Url::to(['/dispatch-regist/update', 'id' => $data->id]);
            $update = Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url,
                ['class' => 'btn btn-success btn-xs', 'role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip']);




            $url = Url::to(['show-status', 'id' => $data->id]);
            $s = Html::a('S', $url, [
                'class' => 'btn btn-info btn-xs',
                'data-pjax' => 0,
                "onclick" => '
                        event.preventDefault();
                        $.get("'.$url.'").done(function(){
                            $.pjax.reload("#crud-datatable-pjax");
                        });',
            ]);

//            if($data->templateProcessing){
//                $content .= \app\widgets\dispatchRegist\TemplateProgress::widget([
//                    'model' => $data
//                ]);
//            }

            return $content ."<br/> {$auto} {$s} {$view} {$update} {$delete}";
        }
    ],
    [
        'attribute'=>'process',
        'label'=>'Процесс',
        'content' =>  function($data){
            $a = 0;
            $all = Settings::findByKey('allowed_message_per_account')->value;;
            $send = $data->sended_message_count;
            $color = 'active';
            if ($send >= $all) {
                $send = $all;
                $a = 100;
                $color = 'success';
            } elseif ($send == 0) {
                $send = 0;
                $a = 0;
            } elseif ($all != $send) {
                $a = round((($send/$all) * 100),0);
            }
            return
                "<div class='progress progress-sm active'>
                    <div class='progress-bar progress-bar-{$color} progress-bar-striped' role='progressbar' 
                    aria-valuenow='{$a}' aria-valuemin='0' aria-valuemax='100' style='width: {$a}%'>
                    <span class='sr-only'>{$a}% Complete</span>
                    </div>
                </div>{$a}% / {$send} из {$all} / "
                ;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'status',
        'content' => function ($model){

            if($model->status == "ok"){
                return "<span class='text-success'>Доступен</span>";
            }

//            $settings = S;

            if($model->status == "limit_exceeded" ) {
                $d = date("Y-m-d H:i:s", strtotime($model->last_dispatch_time . " + 24 hours"));
                $now = date("Y-m-d H:i:s");
                $hour = round((strtotime($d) - strtotime($now)) / (60 * 60));

                return "<span class='text-black'><b>Суточный лимит<br>Повтор через {$hour} часов<b></span>";
            }

            if($model->status == "interval_not_end"){
                $date_2 = date("Y-m-d H:i:s");

                $interval_min = \app\models\Settings::find()->where(['key' => 'interval_in_minutes'])->one();
                $diff = strtotime($date_2) - strtotime($model->last_dispatch_time) ;
                $diff = round($interval_min->value - $diff /60);

                return "<span class='text-yellow'><b> Интервал<br>Повтор через {$diff} минут</b></span>";

            }
            if($model->status == DispatchRegist::STATUS_BLOCKED){
                if($model->proxy_id == null) {
                    return "<span class='text-danger'><b>Акаунт не доступен<br>Заморожен</b></span>";
                } else {
                    return "<span class='text-danger'><b>Акаунт не доступен<br>{$model->coment}</b></span>";
                }
            }

            if($model->status == DispatchRegist::STATUS_SLEEPING){
                return "<span class='text-gray'>Спящий</span>";
            }

        }

    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label'=>'Без блока',
        'content' => function ($data){
            $blockingDate = date('Y-m-d');
//            $blockingDate = !$data->blocking_date ? date('Y-m-d') : $data->blocking_date;
            $createDate = $data->data;
            return round((strtotime($blockingDate)-strtotime($data->data))/(60*60*24)). ' дня(ей)<br>Создан: ' . $createDate;
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'company_id',
        'visible' => Yii::$app->user->identity->isSuperAdmin(),
        'content' => function ($data){
            $text = '';

            $company = Companies::find()->where(['id' => $data->company_id])->one();

            if($company != null)
            {
                $text .= "{$company->company_name}<br>";
            }

            if($data->proxy_id != null)
            {
                $proxy = Proxy::findOne(['id' => $data->proxy_id]);
                if($proxy != null)
                {
                    $text .= "{$proxy->ip_adress}:{$proxy->port}";
                }
            } else {
                $proxy = Proxy::findOne(['id' => $data->proxy_blocked]);
                if($proxy != null)
                {
                    $text .= "<span class='text-danger'>{$proxy->ip_adress}:{$proxy->port}</span>";
                }
            }

            return $text;
        }
    ],
//    [
//        'class' => 'kartik\grid\ActionColumn',
//        'dropdown' => false,
//        'contentOptions' => ['style' => 'white-space: nowrap;'],
//        'template' => '{leadAuto} {S} {view} {update} {delete}',
//        'buttons' => [
//            'leadAuto' => function ($url, $model) {
//                if (!$model->auto_view) {
//                    $url = Url::to(['auto', 'id' => $model->id]);
//                    return Html::a('<span class="fa fa-cloud"></span>', $url, [
//                        'class' => 'btn btn-danger btn-xs',
//                        //'role' => 'modal-remote', 'title' => 'Оформить',
//                        //'data-confirm' => false, 'data-method' => false,// for overide yii data api
//                        'data-request-method' => 'post',
//                        'data-toggle' => 'tooltip',
//                        'data-confirm-title' => 'Подтвердите действие',
//                        'data-confirm-message' => 'Вы уверены что хотите автоматически оформить акк?',
//                    ]);
//                } else {
//                    $url = Url::to(['auto', 'id' => $model->id]);
//                    return Html::a('<span class="fa fa-cloud"></span>', $url, [
//                        'class' => 'btn btn-success btn-xs',
//                        //'role' => 'modal-remote', 'title' => 'Оформить',
//                        //'data-confirm' => false, 'data-method' => false,// for overide yii data api
//                        'data-request-method' => 'post',
//                        'data-toggle' => 'tooltip',
//                        'data-confirm-title' => 'Подтвердите действие',
//                        'data-confirm-message' => 'Вы уверены что хотите автоматически оформить акк?',
//                    ]);
//                }
//            },
//            'S' => function ($url, $model) {
//                $url = Url::to(['show-status', 'id' => $model->id]);
//                return Html::a('S', $url, [
//                    'class' => 'btn btn-info btn-xs',
//                    'data-pjax' => 0,
//                    "onclick" => '
//                        event.preventDefault();
//                        $.get("'.$url.'").done(function(){
//                            $.pjax.reload("#crud-datatable-pjax");
//                        });',
//                ]);
//            },
//        ],
//        'vAlign'=>'middle',
//        'urlCreator' => function($action, $model, $key, $index) {
//                return Url::to([$action,'id'=>$key]);
//        },
//        'viewOptions'=>['class' => 'btn btn-primary btn-xs', 'role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
//        'updateOptions'=>['class' => 'btn btn-success btn-xs', 'role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'],
//        'deleteOptions'=>['class' => 'btn btn-danger btn-xs', 'role'=>'modal-remote','title'=>'Delete',
//                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
//                          'data-request-method'=>'post',
//                          'data-toggle'=>'tooltip',
//                          'data-confirm-title'=>'Ты уверен?',
//                          'data-confirm-message'=>'Вы действительно хотите удалить этот элемент'],
//    ],

];   