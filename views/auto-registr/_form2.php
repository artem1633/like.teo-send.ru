<?php

use kartik\select2\Select2;
use yii\web\View;
use yii\bootstrap\ActiveForm;
use yii\web\JsExpression;
use app\models\AutoRegistr;

/* @var $this yii\web\View */
/* @var $model app\models\AutoRegistr */
/* @var $form yii\widgets\ActiveForm */

$formatJs = <<< JS
var formatRepo = function (repo) {
    if(repo.loading){
        return 'Поиск';
    }
    
    if(repo.region != null && repo.area != null)
    {
        return '<span style="font-size: 15px;">'+repo.title+'</span>'+'<br>'+repo.region+' | '+repo.area;
    }
    
    if(repo.region == null && repo.area == null)
    {
        return '<div style="display: block; padding-bottom: 5px;"><span style="font-size: 17px;">'+repo.title+'</span></div>';
    }
    
    if(repo.region != null && repo.area == null)
    {
        return '<span style="font-size: 15px;">'+repo.title+'</span>'+'<br>'+repo.region;
    }
};
var formatRepoSelection = function (repo) {
    return repo.title || repo.text;
}
JS;

// Register the formatting script
$this->registerJs($formatJs, View::POS_HEAD);

// script to parse the results into the format expected by Select2
$resultsJs = <<< JS
function (data, params) {
    params.page = params.page || 1;
    return {
        results: data.results,
        pagination: {
            more: (params.page * 30) < data.total_count
        }
    };
}
JS;

?>

<div class="auto-registr-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-12">

            <?php if($model->type == AutoRegistr::PHOTO): ?>
                <?= $form->field($model, 'action')->fileInput() ?>
            <?php endif; ?>

            <?php if($model->type == AutoRegistr::SET_CITY): ?>

                <?=$form->field($model, 'action')->widget(Select2::classname(), [
                    'initValueText' => 'Город', // set the initial display text
                    'options' => ['placeholder' => 'Поиск города'],
                    'pluginOptions' => [
                        'allowClear' => false,
                        'minimumInputLength' => 3,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'Поиск...'; }"),
                        ],
                        'ajax' => [
                            'url' => \yii\helpers\Url::toRoute(['dispatch-regist/vk-cities']),
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term, page: params.page}; }'),
                            'processResults' => new JsExpression($resultsJs),
                            'cache' => true
                        ],
                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                        'templateResult' => new JsExpression('formatRepo'),
                        'templateSelection' => new JsExpression('formatRepoSelection'),
                    ],
                ]);?>

            <?php endif; ?>

            <?php if(in_array($model->type, [AutoRegistr::ADDED_TO_GROUP, AutoRegistr::STATUS, AutoRegistr::REPOST, AutoRegistr::INTERESTS, AutoRegistr::MUSIC])): ?>
                <?= $form->field($model, 'action')->textInput(['max' => 255]) ?>
            <?php endif; ?>
        </div>
    </div>

    <div class="hidden">
        <?= $form->field($model, 'name')->hiddenInput() ?>

        <?= $form->field($model, 'type')->hiddenInput() ?>

        <?= $form->field($model, 'status')->hiddenInput() ?>

        <?= $form->field($model, 'scenario')->hiddenInput() ?>

        <?= $form->field($model, 'template_id')->hiddenInput() ?>
    </div>


    <?php ActiveForm::end(); ?>

</div>
