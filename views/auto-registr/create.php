<?php

use app\models\AutoRegistr;

/* @var $this yii\web\View */
/* @var $model app\models\AutoRegistr */

?>
<div class="auto-registr-create">
    <?php if($model->scenario == AutoRegistr::SCENARIO_DEFAULT) { ?>
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    <?php } elseif($model->scenario == AutoRegistr::SCENARIO_ACTION) { ?>
        <?= $this->render('_form2', [
            'model' => $model,
        ]) ?>
    <?php } ?>
</div>
