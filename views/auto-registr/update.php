<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AutoRegistr */
?>
<div class="auto-registr-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
