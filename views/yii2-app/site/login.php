<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */
/* @var $slider app\models\Slider[] */

$this->title = 'Войти';

$fieldOptions1 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-envelope form-control-feedback'></span>"
];

$fieldOptions2 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-lock form-control-feedback'></span>"
];

if(count($slider) > 0 && Yii::$app->mobileDetect->isMobile() == false)
{
    \app\assets\pages\LoginPageAsset::register($this);
    \app\widgets\owlCarousel\OwlCarouselAsset::register($this);
}

$image = \app\models\Settings::findByKey('home_form_image')->value;

if($image == null)
{
    $image = '';
}

?>

    <div class="login-box" style="background: url('<?=$image?>')">
        <div class="login-logo">
            <a href="#"><b>TEO-SEND</b></a>
        </div>
        <?php if(Yii::$app->session->hasFlash('register_success')): ?>
            <p>
            <div class="alert alert-success show m-b-0">
                <span class="close" data-dismiss="alert">×</span>
                <strong>Успех!</strong>
                <?=Yii::$app->session->getFlash('register_success')?>
            </div>
            </p>
        <?php endif; ?>
        <div class="login-box-body">
            <p class="login-box-msg">Введите данные авторизации</p>

            <?php $form = ActiveForm::begin(['id' => 'login-form', 'enableClientValidation' => false]); ?>

            <?= $form
                ->field($model, 'username', $fieldOptions1)
                ->label(false)
                ->textInput(['placeholder' => $model->getAttributeLabel('username')]) ?>

            <?= $form
                ->field($model, 'password', $fieldOptions2)
                ->label(false)
                ->passwordInput(['placeholder' => $model->getAttributeLabel('password')]) ?>

            <div class="row">
                <div class="col-xs-9">
                    <?= $form->field($model, 'rememberMe')->checkbox()->label('Запомни меня') ?>
                </div>
                <div class="col-xs-12">
                    <?= Html::submitButton('Войти по логину и паролю', ['class' => 'btn btn-primary btn-block btn-flat',  'style' => 'width:100%', 'name' => 'login-button']) ?>
                </div>
            </div>
            <div class="row">
                <br>
                <div class="col-xs-6">
                    <p>
                        <?= Html::a('Востановить пароль', $url = '/site/reset', ['option' => 'value']); ?>
                    </p>
                </div>
                <div class="col-xs-6">
                    <p class="pull-right">
                        <?= Html::a('Зарегистрироваться', $url = '/site/register', ['option' => 'value']); ?>
                    </p>
                </div>
            </div>


            <?php ActiveForm::end(); ?>

            <!-- <div class="social-auth-links text-center">
                <p>- OR -</p>
                <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in
                    using Facebook</a>
                <a href="#" class="btn btn-block btn-social btn-google-plus btn-flat"><i class="fa fa-google-plus"></i> Sign
                    in using Google+</a>
            </div> -->
            <!-- /.social-auth-links -->

            <!-- <a href="#">Забыл пароль</a><br> -->
            <!-- <a href="register.html" class="text-center">Register a new membership</a> -->

        </div>
        <!-- /.login-box-body -->
    </div><!-- /.login-box -->

<?php if(count($slider) > 0 && Yii::$app->mobileDetect->isMobile() == false): ?>

    <div class="slider">
        <div class="login-carousel">
            <?php foreach ($slider as $item): ?>
                <div class="item">
                    <div class="item-body">
                        <img class="item-image" src="/<?=$item->image_path?>" alt="<?=$item->image_alt?>">
                        <div class="overlay"></div>
                        <div class="item-text-content">
                            <h2><?=$item->header?></h2>
                            <p><?=$item->content?></p>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>

<?php endif; ?>

<?php

$delayTime = \app\models\Settings::findByKey('slider_delay')->value;

if($delayTime == null){
    $delayTime = 10000;
}

$script = <<< JS
$('.login-carousel').owlCarousel({
    loop:true,
    nav:true,
    items: 1,
    dots: false,
    nav: false,
    autoplay:true,
    autoplayTimeout:{$delayTime},
    autoplayHoverPause:true
});
JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>