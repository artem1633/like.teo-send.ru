<?php
use yii\helpers\Html;
//use app\models\Soglosovaniya;
use yii\bootstrap\Modal;
//use johnitvn\ajaxcrud\CrudAsset;
//use yii\helpers\Url;

\Yii::$app->db->createCommand()->update('users', ['last_activity_datetime' => date('Y-m-d H:i:s') ], [ 'id' => \Yii::$app->user->identity->id ])->execute(); 
\Yii::$app->db->createCommand()->update('companies', ['last_activity_datetime' => date('Y-m-d H:i:s') ], [ 'id' => \Yii::$app->user->identity->company_id ])->execute();

?>
<!--    <a href="">-->
<!--    <div class="wrapper1">-->
<!--        <div class="line"></div>-->
<!--        <div class="circle-2">-->
<!--        </div>-->
<!--        <div class="circle">-->
<!--            Акция-->
<!--        </div>-->
<!--    </div>-->
<!--    </a>-->


    <!-- UI # -->
    <div class="ui-92">

        <!-- Status slide box starts -->
        <div class="status-slide hidden-xs">
            <!-- Slide button -->
            <div class="status-button">
                <!-- Button text with icon -->
                <span class="status-button-arrow bg-red"><i class="fa fa-chevron-left"></i></span>
                <!-- Blue circle -->
<!--                <span class="status-circle"><span class="status-circle-blue bg-lblue">20 <i class="fa fa-angle-double-up"></i></span></span>-->
            </div>
            <div class="clearfix"></div>
            <!-- Content -->
            <div class="status-content">
                <!-- Status head section with graph -->
                <div class="status-head">
                    <div class="status-head-top">
                        Данные на сегодня <span class="label label-lblue pull-right">Отправлено: 58 @</span>
                    </div>
                    <div class="status-head-content bg-lblue">
                        <!-- Graph -->
                        <div class="status-head-left">
                            <div id="status-chart"></div>
                        </div>
                        <div class="status-head-right">
                            <table class="table">
                                <tr>
                                    <td>Акаунтов</td>
                                    <td>20</td>
                                </tr>
                                <tr>
                                    <td>Скорость</td>
                                    <td>500</td>
                                </tr>
                                <tr>
                                    <td>Рассылки</td>
                                    <td>5/2/3</td>
                                </tr>
                                <tr>
                                    <td>Партнерка</td>
                                    <td>5000Р</td>
                                </tr>
                            </table>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <!-- Status body -->
                <div class="status-body text-center">
                    <!-- Three columns -->
                    <div class="status-body-left">
                        <!-- Label -->
                        <div class="label label-green"><i class="fa fa-angle-double-up"></i> 50</div><br />
                        <!-- Name -->
                        Согласны
                    </div>
                    <div class="status-body-center">
                        <div class="label label-red"><i class="fa fa-angle-double-up"></i> 30</div><br />
                        Отказ
                    </div>
                    <div class="status-body-right">
                        <div class="label label-orange"><i class="fa fa-angle-double-down"></i> 20</div><br />
                        Новых
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>

        </div>
        <!-- Status slide box ends -->

    </div>


    <script>


        <!-- Sparkline -->
        $("#status-chart").sparkline([20,23,25,19,20,19,18,17,16,20,18,17,19,23,25,19,22,25], {
            type: 'bar',
            height: '70',
            barColor: 'rgba(255,255,255,1)'});

        /* Status slide */

        $('.status-button').click(function() {
            var $slidebtn=$(this);
            var $slidebox=$(this).parent();
            if($slidebox.css('right')=="-282px"){
                $slidebox.animate({
                    right:0
                },500);
                $slidebtn.find(".status-circle").hide();
                $slidebtn.width(41);
                $slidebtn.animate({
                    left:-42
                },100);
                $slidebtn.children().children("i").removeClass().addClass("fa fa-chevron-right");
            }
            else{
                $slidebox.animate({
                    right:-282
                },100);
                $slidebtn.animate({
                    left:-35
                },500);
                $slidebtn.width(35);
                $slidebtn.find(".status-circle").show(700);
                $slidebtn.children().children("i").removeClass().addClass("fa fa-chevron-left");
            }
        });

    </script>






<header class="main-header">

    <?= Html::a('<span class="logo-mini">SEND</span><span class="logo-lg">' . 'TEO-SEND' . '</span>' , Yii::$app->homeUrl, ['class' => 'logo']) ?>



    <nav class="navbar navbar-static-top" role="navigation">

        <a href="#" onclick="$.post('/site/menu-position');" class="sidebar-toggle" data-toggle="push-menu" role="button"><span class="sr-only">Toggle navigation</span> </a>

        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">
                <?php if(Yii::$app->user->identity->isSuperAdmin()): ?>
                    <li>
                        <?=Html::a('proxy: '.Yii::$app->proxy->balance.' '.Yii::$app->proxy->currency, ['#'])?>
                    </li>
                <?php endif; ?>
                <li>
                    <?php if(Yii::$app->user->isGuest === false): ?>
                        <?=Html::a(
                                Html::img('/images/icon/yandex.svg', ['style' => 'margin-top:-5px'])
                                . ' '
                                . number_format(Yii::$app->user->identity->getCompanyInstance()->general_balance, 2, ',', ' ')
                                . ' <i class="fa fa-rub"></i>',
                                ['/payment'])?>
                    <?php endif; ?>
                </li>
                <?php if(Yii::$app->user->identity->isSuperAdmin()): ?>
                <li>
                    <?= Html::a(
                        '<i class="fa fa-cog fa-lg"></i>',
                        ['/settings'],
                        ['title' => 'Настройки']
                    ) ?>
                 </li>
                <?php endif; ?>

                <!-- <li class="dropdown notifications-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-bell-o"></i>
                        <span class="label label-warning"><?=0?></span>
                    </a>
                    <ul class="dropdown-menu" id="ajaxCrudDatatable" >
                        <li class="header">У вас есть <?=0?> уведомление</li> 
                    </ul>
                </li> -->

                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="hidden-xs"><?=Yii::$app->user->identity->name ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="user-header">
                            <img src="<?= 'http://' . $_SERVER['SERVER_NAME'] ?>/images/nouser.png" class="img-circle" alt="User Image"/>
                            <p> <?=Yii::$app->user->identity->name ?> </p>
                        </li>
                        <li class="user-footer">
                            <div class="pull-left">
                                <?= Html::a('Изменить пароль', ['users/change', 'id' => Yii::$app->user->identity->id],
                                ['role'=>'modal-remote','title'=> 'Изменить пароль','class'=>'btn btn-default btn-flat']); ?>
                            </div>
                            <div class="pull-right">
                                <?= Html::a(
                                    'Выход',
                                    ['/site/logout'],
                                    ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                ) ?>
                            </div>
                        </li>
                    </ul>
                </li>

                
            </ul>
        </div>
    </nav>
</header>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "size" => "modal-lg",
    "options" => [
        "tabindex" => -1,
    ],
    "footer"=>"",// always need it for jquery plugin
])?>




<?php Modal::end(); ?>