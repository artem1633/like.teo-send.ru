<?php

use app\components\helpers\FunctionHelper;
use app\models\DailyReport;
use app\models\GlobalMessages;
use yii\helpers\Html;
use yii\helpers\Url;

if (class_exists('backend\assets\AppAsset')) {
        backend\assets\AppAsset::register($this);
    } else {
        app\assets\AppAsset::register($this);
    }
\Yii::$app->view->registerLinkTag(['rel' => 'icon', 'type' => 'image/png', 'href' => Url::to(['/favicon.png'])]);
if (Yii::$app->controller->action->id === 'login' || Yii::$app->controller->action->id === 'register' || Yii::$app->controller->id === 'default' || Yii::$app->controller->id === 'setting') { 
    echo $this->render( 'main-login', ['content' => $content] );
} else {
    ?>
    <?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <?php 
        $session = Yii::$app->session;
        if($session['body'] == null | $session['body'] == 'small') $body="page-navigation-toggled page-container-wide";
        else $body="";
    ?>
    <body class="">
    <?php $this->beginBody() ?>
        <div class="page-container ">
            <?= $this->render( 'left.php', [] ) ?>
            <!-- PAGE CONTENT -->
            <div class="page-content">
                <?= Yii::$app->controller->id == 'default' ? $this->render( 'header-menu.php', [] ) : $this->render( 'header.php', [] ) ?>
                <?= $this->render( 'content.php', ['content' => $content, ] ) ?>
             </div>                       
            <!-- END PAGE CONTENT -->
        </div>

    <script type="text/javascript" src="https://vk.com/js/api/openapi.js?160"></script>

    <!-- VK Widget -->
    <div id="vk_community_messages"></div>
    <script type="text/javascript">
        VK.Widgets.CommunityMessages("vk_community_messages", 166230711, {widgetPosition: "left",disableButtonTooltip: "1"});
    </script>

    <?php
        /** @var \app\models\Users $user */
        $user = Yii::$app->user->identity;

        //проверяем первый раз сегодня зашол пользователь или нет
        $findRep = DailyReport::find()
            ->andfilterWhere(['=','type', 5])
            ->andfilterWhere(['=','company_id', $user->company_id])
            ->andfilterWhere(['like','date_event', date('Y-m-d')])->one();
       // var_dump($findRep);
        if (!$findRep) {
            $report = new DailyReport([
                'company_id' => $user->company_id,
                'account_id' => $user->company_id,
                'comments' => "{$_SERVER['REMOTE_ADDR']}",
                'type' => DailyReport::TYPE_VISIT,
            ]);
            $report->save(false);
        }

    ?>
    <?php
    $block = \app\models\BlockIp::find()->where(['key' => $_SERVER['REMOTE_ADDR']])->one();
    ?>
    <?php if($block): ?>
        <?php \yii\bootstrap\Modal::begin([
            'header' => "Вы нарушии правила сервиса!!",
            'id' => 'notifications-modal',
            'footer' => '',
        ]) ?>
        <?=$block->message?>
        <?php \yii\bootstrap\Modal::end() ?>
    <?php endif; ?>

    <?php if($user->messages_read_status == 0): ?>

        <?php
            /** @var GlobalMessages $message */
            $message = GlobalMessages::getLastMessage();
            $user->messages_read_status = 1;
            $user->save(false);
            $message->views_count++;
            $message->save(false);
        ?>

        <?php if($message != null): ?>
            <?php \yii\bootstrap\Modal::begin([
                'header' => $message->name,
                'id' => 'notifications-modal',
                'footer' => '',
            ]) ?>
            <?=$message->text?>
            <?php \yii\bootstrap\Modal::end() ?>
        <?php endif; ?>

    <?php endif; ?>


    <?php $this->endBody() ?>
    </body>
    </html>
    <?php $this->endPage() ?>
<?php } ?>
