<aside class="main-sidebar">

    <section class="sidebar">
        <!-- <img src="http://www.all-sbor.net/forum/images/smilies/smile227.gif" alt="Пример" width="20" height="20"> -->
        <?php
        if (isset(Yii::$app->user->identity->id)) {
            $role = Yii::$app->user->identity->role_id;
            echo dmstr\widgets\Menu::widget(
                [
                    'options' => ['class' => 'sidebar-menu tree', 'data-widget' => 'tree'],
                    'items' => [
                        [
                            'label' => 'Показатели',
                            'icon' => 'line-chart',
                            'url' => ['/indicators'],
                        ],
                        [
                            'label' => 'Рассылки',
                            'icon' => 'clone',
                            'url' => ['/dispatch'],
                        ],
                        [
                            'label' => 'Аккаунты',
                            'icon' => 'users',
                            'url' => ['/dispatch-regist'],
                        ],
                        [
                            'label' => 'Чаты',
                            'icon' => 'fa fa-chat',
                            'url' => ['/chat'],
                            'visible' => false
                        ],
                        [
                            'label' => 'Авто ответы',
                            'icon' => 'cubes',
                            'url' => ['/bots'],
                        ],
                        [
                            'label' => 'Администратор',
                            'icon' => 'book',
                            'url' => '#',
                            'visible' => \Yii::$app->user->identity->isSuperAdmin(),
                            'items' => [
                                ['label' => 'Компании', 'icon' => 'list-alt', 'url' => ['/companies'],],
                                ['label' => 'Логи', 'icon' => 'list-alt', 'url' => ['/logs'],],
                                ['label' => 'Тарифы', 'icon' => 'list-alt', 'url' => ['/rates'],],
                                ['label' => 'Прокси', 'icon' => 'list-alt', 'url' => ['/proxy'],],
                                ['label' => 'Шаблоны сообщений', 'icon' => 'list-alt', 'url' => ['/email-templates'],],
                                ['label' => 'Статусы', 'icon' => 'check-circle-o', 'url' => ['/statuses'],],
                                ['label' => 'Статус Рассылок', 'icon' => 'check', 'url' => ['/dispatch-status'],],
                                ['label' => 'Яндекс Деньги', 'icon' => 'credit-card-alt', 'url' => ['/yandex-money'],],
                                ['label' => 'Движение средств', 'icon' => 'credit-card', 'url' => ['/accounting'],],

                            ],
                        ],
                        [
                            'label' => 'Пользователи',
                            'icon' => 'users',
                            'visible' => \Yii::$app->user->identity->isSuperAdmin(),
                            'url' => ['/users'],
                        ],
                        [
                            'label' => 'Отчет',
                            'icon' => 'line-chart',
                            'url' => ['/dispatch/report'],
                        ],
                        [
                            'label' => 'Партнерская программа',
                            'icon' => 'handshake-o',
                            'url' => ['/affiliate-program'],
                        ],
                        [
                            'label' => 'Магазин',
                            'icon' => 'cart-arrow-down',
                            'url' => Yii::$app->user->identity->isSuperAdmin() ? ['/store'] : ['/trade'],

                        ],
                        [
                            'label' => 'Финансы',
                            'icon' => 'credit-card',
                            'url' => ['/payment'],
                        ],
                        [
                            'label' => 'Настройки',
                            'icon' => 'cog',
                            'url' => ['/settings'],
                        ],
                    ],
                ]
            );
        } else {
            echo dmstr\widgets\Menu::widget(
                [
                    'options' => ['class' => 'sidebar-menu tree', 'data-widget' => 'tree'],
                    'items' => [
                        ['label' => 'Menu', 'options' => ['class' => 'header']],
                        ['label' => 'Signup', 'icon' => 'sign-in', 'url' => ['/signup']],
                    ],
                ]
            );
        }
        ?>

    </section>

</aside>
