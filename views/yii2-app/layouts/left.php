<?php
use yii\helpers\Html;
use yii\bootstrap\Modal;
use johnitvn\ajaxcrud\CrudAsset; 
use yii\helpers\Url;
use app\models\Users;
use yii\widgets\Pjax;


?>
<?php 
//        $session = Yii::$app->session;
//        if($session['left'] == null | $session['left'] == 'small') $left="x-navigation-minimized";
//        else $left="x-navigation-custom";
    ?>
<!-- START PAGE SIDEBAR -->
            <div class="page-sidebar page-sidebar-fixed scroll">
                <!-- START X-NAVIGATION -->
                <ul class="x-navigation x-navigation-customd">
                    <li class="xn-logo">
                        <a href="<?=Url::toRoute([Yii::$app->homeUrl])?>"><?=Yii::$app->name?></a>
                        <a href="#" class="x-navigation-control"></a>
                    </li>

                    <li class="xn-title">Меню</li>


                    <li class="xn-openable">
<!--                        <a href="#"><span class="fa fa-vk "></span> <span class="xn-text">ВКонтакте</span></a>-->
<!--                        <ul>-->
<!--                            <li>-->
<!--                                <a href="--><?php //// echo Url::toRoute(['/dispatch?platform=vk'])?><!--"><span class="fa fa-envelope"></span> <span class="xn-text">Накрутки</span></a>-->
<!--                            </li>-->
<!--                        </ul>-->
<!--                        <ul>-->
<!--                            <li>-->
<!--                                <a href="--><?php //// echo Url::toRoute(['/automation?platform=vk'])?><!--"><span class="fa fa-clock-o"></span> <span class="xn-text">Авто задачи</span></a>-->
<!--                            </li>-->
<!--                        </ul>-->
<!--                    </li>-->

                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-instagram "></span> <span class="xn-text">Instagram</span></a>
                        <ul>
                            <li>
                                <a href="<?=Url::toRoute(['/clients'])?>"><span class="fa fa-users"></span> <span class="xn-text">Клиенты</span></a>
                            </li>
                        </ul>
                        <ul>
                            <li>
                                <a href="<?=Url::toRoute(['/dispatch?platform=instagram'])?>"><span class="fa fa-envelope"></span> <span class="xn-text">Накрутки</span></a>
                            </li>
                        </ul>
                        <ul>
                            <li>
                                <a href="<?=Url::toRoute(['/automation?platform=instagram'])?>"><span class="fa fa-clock-o"></span> <span class="xn-text">Авто задачи</span></a>
                            </li>
                        </ul>
                    </li>

                    <li>
                        <a href="<?=Url::toRoute(['/settings'])?>"><span class="fa fa-cog"></span> <span class="xn-text">Настройки</span></a>
                    </li>
                    <li>
                        <a href="<?=Url::toRoute(['/affiliate-program'])?>"><span class="fa fa-sitemap"></span> <span class="xn-text">Партнерская программа</span></a>
                    </li>
                    <li>
                        <a href="<?=Url::toRoute(['/payment'])?>"><span class="fa fa-credit-card"></span> <span class="xn-text">Финансы</span></a>
                    </li>
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-life-ring "></span> <span class="xn-text">Помощь</span></a>
                        <ul>
                            <li><a href="<?=Url::toRoute(['/global-messages'])?>"><span class="fa fa-graduation-cap"></span> <span>Новости</span></a></li>
                        </ul><ul>
                            <li>
                                <?= Html::a('<span class="fa fa-envelope"></span> <span>Написать нам</span></a>', 'https://vk.com/im?sel=-166230711', ['title'=> 'Профиль','class'=>'profile-control-right']); ?>
                            </li>
                        </ul>
                    </li>
                    <?php if(Yii::$app->user->identity->isSuperAdmin()): ?>
                        <li class="xn-openable">
                            <a href="#"><span class="fa fa-star"></span> <span class="xn-text">Администратор</span></a>
                            <ul>
                                <li><a href="<?=Url::toRoute(['/companies'])?>"><span class="fa fa-star-o"></span> <span>Компании</span></a></li>
                                <li>
                                    <a href="<?=Url::toRoute(['/dispatch-regist'])?>"><span class="fa fa-users"></span> <span class="xn-text">Аккаунты</span></a>
                                </li>
                                <li>
                                    <a href="<?=Url::toRoute(['/dispatch-instagram'])?>"><span class="fa fa-users"></span> <span class="xn-text">Аккаунты Instagram</span></a>
                                </li>
                                <li>
                                    <a href="<?=Url::toRoute(Yii::$app->user->identity->isSuperAdmin() ? ['/store'] : ['/trade'])?>"><span class="fa fa-shopping-cart"></span> <span class="xn-text">Магазин</span></a>
                                </li>
                                <li><a href="<?=Url::toRoute(['/block-ip'])?>"><span class="fa fa-star-o"></span> <span>Блок лист</span></a></li>
                                <li><a href="<?=Url::toRoute(['/rates'])?>"><span class="fa fa-star-o"></span> <span>Тарифы</span></a></li>
                                <li><a href="<?=Url::toRoute(['/proxy'])?>"><span class="fa fa-star-o"></span> <span>Прокси</span></a></li>
                                <li><a href="<?=Url::toRoute(['/email-templates'])?>"><span class="fa fa-star-o"></span> <span>Шаблоны сообщений</span></a></li>
                                <li><a href="<?=Url::toRoute(['/statuses'])?>"><span class="fa fa-star-o"></span> <span>Статусы</span></a></li>
                                <li><a href="<?=Url::toRoute(['/yandex-money'])?>"><span class="fa fa-star-o"></span> <span>Яндекс Деньги</span></a></li>
                                <li><a href="<?=Url::toRoute(['/global-messages'])?>"><span class="fa fa-star-o"></span> <span>Рассылка</span></a></li>
                                <li><a href="<?=Url::toRoute(['/logs'])?>"><span class="fa fa-star-o"></span> <span>Логи</span></a></li>
                            </ul>
                        </li>
                        <li class="xn-openable">
                            <a href="#"><span class="fa fa-star"></span> <span class="xn-text">Отчеты</span></a>
                            <ul>
                                <li><a href="<?=Url::toRoute(['/report/registration'])?>"><span class="fa fa-star-o"></span> <span>Регистрация</span></a></li>
                                <li><a href="<?=Url::toRoute(['/report/visit'])?>"><span class="fa fa-star-o"></span> <span>Посетители</span></a></li>
                            </ul>
                        </li>
                    <?php endif; ?>

                </ul>
                <!-- END X-NAVIGATION -->
            </div>
            <!-- END PAGE SIDEBAR -->