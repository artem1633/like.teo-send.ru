<?php
use yii\helpers\Html,
    yii\widgets\ActiveForm,
    app\models\CompanySettings;

/* @var $this yii\web\View */
/* @var $model app\models\Companies */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="companies-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'admin_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\Users::find()->all(), 'id', 'name')) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'access_end_datetime')->widget(\kartik\date\DatePicker::class, [
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd'
                ]
            ]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'rate_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\Rates::find()->all(), 'id', 'name')) ?>
        </div>
    </div>
    <?php
    if (Yii::$app->user->identity->isSuperAdmin() && $settingsModel) {
        ?>
        <div class="row">
            <div class="col-md-4">
                <?= $form->field($settingsModel, 'messages_in_transaction')->textInput(['type' => 'number']) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($settingsModel, 'messages_daily_limit')->textInput(['type' => 'number']) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($settingsModel, 'send_interval')->textInput(['type' => 'number']) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <?= $form->field($settingsModel, 'proxy_count')->textInput(['type' => 'number']) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($settingsModel, 'bots_count')->textInput(['type' => 'number']) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($settingsModel, 'price_message')->textInput(['type' => 'number', 'step' => '0.01']) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <?= $form->field($settingsModel, 'dispatch_count')->textInput(['type' => 'number']) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($settingsModel, 'reload_time')->textInput(['type' => 'number', 'value' => $settingsModel->reloadTime]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($settingsModel, 'account_count')->textInput(['type' => 'number']) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <?= $form->field($model, 'general_balance')->textInput(['type' => 'number']) ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'bonus_balance')->textInput(['type' => 'number']) ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'affiliate_amount')->textInput(['type' => 'number']) ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($settingsModel, 'affiliate_percent')->textInput(['type' => 'number']) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($settingsModel, 'add_accounts')->checkbox() ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($settingsModel, 'add_bots')->checkbox() ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($settingsModel, 'add_proxy')->checkbox() ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($settingsModel, 'dispatch_rules')->checkbox() ?>
            </div>
        </div>
        <?php
    }
    ?>

    <?php if (!Yii::$app->request->isAjax) { ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>
