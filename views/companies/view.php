
<?php

use app\components\helpers\FunctionHelper;
use app\models\AccountingReport;
use app\models\Companies;
use kartik\grid\GridView;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\DetailView;
use skeeks\widget\highcharts\Highcharts;
use dosamigos\chartjs\ChartJs;

\johnitvn\ajaxcrud\CrudAsset::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\Companies */
?>
    <div class="companies-view">

        <div class="row">
            <div class="col-md-4">
                <div class="panel panel-success panel-toggled" style="margin-top: 15px;">
                    <div class="panel-heading ui-draggable-handle">
                        <h1 class="panel-title"> <b data-introindex="5-3">История посещений</b>

                        </h1>

                        <ul class="panel-controls">
                            <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                        </ul>
                    </div>
                    <div class="panel-body">
                        <?= GridView::widget([
                            'id' => 'crud-datatable-2',
                            'dataProvider' => $visit,
                            //'filterModel' => $searchModel,
                            'pjax' => true,
                            'responsiveWrap' => false,
                            'columns' => [
                                'date_event',
                            ],

                            'striped' => true,
                            'condensed' => true,
                            'responsive' => true,
                            'toolbarContainerOptions' => ['style' => 'margin-top: 15px;'],
                            'panel' => null,
                        ]); ?>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-success panel-toggled" style="margin-top: 15px;">
                    <div class="panel-heading ui-draggable-handle">
                        <h1 class="panel-title"> <b data-introindex="5-3">Рефералы</b>

                        </h1>

                        <ul class="panel-controls">
                            <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                        </ul>
                    </div>
                    <div class="panel-body">
                        <?= GridView::widget([
                            'id' => 'crud-datatable-2',
                            'dataProvider' => $refList,
                            //'filterModel' => $searchModel,
                            'pjax' => true,
                            'responsiveWrap' => false,
                            'columns' => [
                                    'id',
                                'admin.name',
                                'general_balance',
                                'bonus_balance',
                                'affiliate_amount',
                                'referal',
                                [
                                    'class'=>'\kartik\grid\DataColumn',
                                    'attribute'=>'referal',
                                    'label' => 'Рефералов',
                                    'content' => function($data){

                                        $ref = Companies::find()->where(['referal' => $data->id])->count();


                                        return $ref;
                                    }
                                ],
                                'admin.data_cr',

                            ],

                            'striped' => true,
                            'condensed' => true,
                            'responsive' => true,
                            'toolbarContainerOptions' => ['style' => 'margin-top: 15px;'],
                            'panel' => null,
                        ]); ?>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="panel panel-success panel-toggled" style="margin-top: 15px;">
                    <div class="panel-heading ui-draggable-handle">
                        <h1 class="panel-title"> <b data-introindex="5-3">Пополнения</b>

                        </h1>

                        <ul class="panel-controls">
                            <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                        </ul>
                    </div>
                    <div class="panel-body">
                        <?= GridView::widget([
                            'id' => 'crud-datatable-1',
                            'dataProvider' => $dataProviderDebit,
                            //'filterModel' => $searchModel,
                            'pjax' => true,
                            'responsiveWrap' => false,
                            'columns' => [
                                [
                                    'class' => '\kartik\grid\DataColumn',
                                    'attribute' => 'amount',
                                    'content' => function ($model) {
                                        return number_format($model->amount, 2, ',', ' ') . ' <i class="fa fa-rub"></i>';
                                    }
                                ],
                                [
                                    'class' => '\kartik\grid\DataColumn',
                                    'attribute' => 'date_report',
                                    'content' => function ($model) {
                                        return FunctionHelper::dateForForm($model->date_report, 1);
                                    }
                                ],
                            ],

                            'striped' => true,
                            'condensed' => true,
                            'responsive' => true,
                            'toolbarContainerOptions' => ['style' => 'margin-top: 15px;'],
                            'panel' => null,
                        ]); ?>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-success panel-toggled" style="margin-top: 15px;">
                    <div class="panel-heading ui-draggable-handle">
                        <h1 class="panel-title"> <b data-introindex="5-3">Списания</b>

                        </h1>

                        <ul class="panel-controls">
                            <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                        </ul>
                    </div>
                    <div class="panel-body">
                        <?= GridView::widget([
                            'id' => 'crud-datatable-1',
                            'dataProvider' => $dataProviderShop,
                            //'filterModel' => $searchModel,
                            'pjax' => true,
                            'responsiveWrap' => false,
                            'columns' => [
                                [
                                    'class' => '\kartik\grid\DataColumn',
                                    'attribute' => 'amount',
                                    'content' => function ($model) {
                                        return number_format($model->amount, 2, ',', ' ') . ' <i class="fa fa-rub"></i>';
                                    }
                                ],
                                [
                                    'class' => '\kartik\grid\DataColumn',
                                    'attribute' => 'date_report',
                                    'content' => function ($model) {
                                        return FunctionHelper::dateForForm($model->date_report, 1);
                                    }
                                ],
                            ],

                            'striped' => true,
                            'condensed' => true,
                            'responsive' => true,
                            'toolbarContainerOptions' => ['style' => 'margin-top: 15px;'],
                            'panel' => null,
                        ]); ?>
                    </div>
                </div>
            </div>
            <div class="col-md-4" style="padding-top: 20px;">
                <div class="panel panel-success panel-toggled">
                    <div class="panel-heading ui-draggable-handle">
                        <h1 class="panel-title"> <b data-introindex="5-3">Информация о компании</b>

                        </h1>

                        <ul class="panel-controls">
                            <li>
                                <?= Html::a('<i class="fa fa-pencil"></i>', ['update', 'id' => $model->id], [
                                    'title' => 'Редактировать',
                                    'role' => 'modal-remote'
                                ]); ?>
                            </li>
                            <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                        </ul>
                    </div>
                    <div class="panel-body">
                        <?php \yii\widgets\Pjax::begin(['id' => 'company-info-pjax', 'enablePushState' => false]) ?>
                        <?= DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                'id',
                                'admin.name',
                                'admin.login',
                                'admin.telephone',
                                'admin.data_cr',
                                'admin.last_activity_datetime',
                                'general_balance',
                                'bonus_balance',
                                'affiliate_amount',
                                'company_name',
                                'created_date',
                                'is_super',
                                'account_count',
                                'dispatch_count',
                                'referal',
                                [
                                    'class'=>'\kartik\grid\DataColumn',
                                    'attribute'=>'referal',
                                    'label' => 'Рефералов',
                                    'content' => function($data){

                                        $ref = Companies::find()->where(['referal' => $data->id])->count();


                                        return $ref;
                                    }
                                ],
                            ],
                        ]) ?>
                        <?php \yii\widgets\Pjax::end() ?>
                    </div>
                </div>

                <div class="col-md-12" style="padding-top: 30px;">

                    <div class="panel panel-warning panel-toggled">
                        <div class="panel-heading ui-draggable-handle">
                            <h3 class="panel-title"><b>Чат</b></h3>
                            <ul class="panel-controls">
                                <li><a href="#" class="panel-collapse"><span class="fa fa-angle-up"></span></a></li>
                                <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                            </ul>
                        </div>
                        <div class="panel-body">
                            <div class="content-frame-body content-frame-body-left" style="max-height: 400px; overflow: auto; " >
                                <div class="messages messages-img">
                                    <?php
                                    /** @var \app\models\CompanyChat $value */
                                    foreach ($chatText as $value) {
                                        if($value->telegram_name != 1) {
                                            $in = '';
                                            $name_chat = $value->telegram_name;
                                            } else {
                                            $name_chat = 'Супер компания';
                                            $in = 'in';
                                        }
                                            $path = '/favicon.png';
                                        ?>
                                        <div class="item <?=$in?> item-visible">
                                            <div class="image">
                                                <img src="<?=$path?>" alt="<?=$name_chat?>">
                                            </div>
                                            <div class="text">
                                                <div class="heading">
                                                    <a href="#"><?=$name_chat?></a>
                                                    <span class="date"><?= date( 'H:i:s d.m.Y', strtotime($value->date_time) ) ?></span>
                                                </div>
                                                <?=$value->text?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="panel panel-default push-up-10">
                                <div class="panel-body panel-body-search">
                                    <?php $form = ActiveForm::begin(); ?>
                                    <div class="input-group">
                                        <div class="input-group-btn">
                                            <button class="btn btn-warning"><span class="fa fa-camera"></span></button>
                                            <button class="btn btn-danger"><span class="fa fa-chain"></span></button>
                                        </div>
                                        <input type="text" name="text" class="form-control" placeholder="Написать сообщение...">
                                        <div class="input-group-btn">
                                            <button class="btn btn-info">Отправить</button>
                                        </div>
                                    </div>
                                    <?php ActiveForm::end(); ?>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>




        </div>

        <div class="row" style="margin-top: 20px;">
            <div class="col-md-12">
        <div class="panel panel-success">
            <div class="panel-heading">
                <div class="panel-title">Отчет о регистрации</div>
            </div>
            <div class="panel-body">

                <?php $form = ActiveForm::begin(['method' => 'get']) ?>

                <div class="row">
                    <div class="col-md-12">
                        <?= $form->field($modelRep, 'dates')->widget(\kartik\daterange\DateRangePicker::class, [
                            'options' => [
                                'autocomplete' => 'off',
                                'class' => 'form-control'
                            ],
                        ]) ?>
                    </div>
                    <div class="col-md-2">
                        <?= \yii\helpers\Html::submitButton('Поиск', ['class' => 'btn btn-block btn-success', 'style' => 'margin-top: 21px;']) ?>
                    </div>
                </div>

                <?php ActiveForm::end() ?>

                <div class="row">
                    <div class="col-md-12">

            <div class="panel-body introduction-first" data-introindex="17" >

                <div class="col-md-12">

                    <?= Highcharts::widget([
                        'options' => [
                            'chart' => [
                                'height' => 225,
                                'type' => 'line'

                            ],
                            'title' => false,//['text' => 'Всего отправлено сообщений'],
                            //'subtitle' => ['text' => 'Notice the difference between a 0 value and a null point'],
                            'plotOptions' => [
                                'column' => ['depth' => 25],
                                'line' => [
                                    'dataLabels' => [
                                        'enabled' => true
                                    ]
                                ],
                                'enableMouseTracking' => false,
                            ],
                            'xAxis' => [
                                'categories' => $report[0],
                                'labels' => [
                                    'skew3d' => true,
                                    'style' => ['fontSize' => '10px']
                                ]
                            ],
                            'yAxis' => [
                                'title' => ['text' => null]
                            ],
                            'series' => [
                                ['name' => 'Отправленно сообщений', 'data' => $report[7]],
                                ['name' => 'Сумма оплат', 'data' => $report[3]],
                            ],
                            'credits' => ['enabled' => false],
                            'legend' => ['enabled' => true],
                        ]
                    ]);
                    ?>
                </div>
            </div>

            <div class="panel-body introduction-first" data-introindex="17" >

                <?= Highcharts::widget([
                    'options' => [
                        'chart' => [
                            'height' => 225,
                            'type' => 'line'

                        ],
                        'title' => false,//['text' => 'Всего отправлено сообщений'],
                        //'subtitle' => ['text' => 'Notice the difference between a 0 value and a null point'],
                        'plotOptions' => [
                            'column' => ['depth' => 25],
                            'line' => [
                                'dataLabels' => [
                                    'enabled' => true
                                ]
                            ],
                            'enableMouseTracking' => false,
                        ],
                        'xAxis' => [
                            'categories' => $report[0],
                            'labels' => [
                                'skew3d' => true,
                                'style' => ['fontSize' => '10px']
                            ]
                        ],
                        'yAxis' => [
                            'title' => ['text' => null]
                        ],
                        'series' => [
                            ['name' => 'Блок акк', 'data' => $report[4]],
                            ['name' => 'Купили акк', 'data' => $report[6]],
                        ],
                        'credits' => ['enabled' => false],
                        'legend' => ['enabled' => true],
                    ]
                ]);
                ?>
            </div>

        </div>
        </div>

        </div>
        </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default" style="margin-top: 20px;">
                            <div class="panel-heading">
                                <div class="panel-title">Статистика по финансам</div>
                            </div>
                            <div class="panel-body">
                                <table class="table table-bordered" style="padding:0px;">
                                    <thead>
                                    <tr>
                                        <th>№</th>
                                        <th>Дата</th>
                                        <th>Сумма пополнений</th>
                                        <th>Купили ботов</th>
                                        <th>Списали за отправку</th>
                                        <th>Сумма итого</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $counter = 1;
                                    ?>
                                    <tr style="background: darkorange;">
                                        <td><?='Итого'?></td>
                                        <td></td>
                                        <td><?=array_sum($report[3])."<br/> (кол-во ".array_sum($report[2]).")"?></td>
                                        <td>
                                            <?php
                                            $sumG1 = array_sum($report[6])*40;
                                            $sumCH1 = array_sum($report[6]) *7;
                                            echo array_sum($report[6])."<br/> Сумма {$sumG1} / Чистыми {$sumCH1}";
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            $sumG2 = array_sum($report[7]) * 1;
                                            $sumCH2 = array_sum($report[7]) * 0.5;
                                            echo array_sum($report[7])."<br/> Сумма {$sumG2} / Чистыми {$sumCH2}";
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            $finishG = $sumG1 + $sumG2;
                                            $finishC = $sumCH1 + $sumCH2;
                                            echo " Сумма {$finishG} / Чистыми {$finishC}";
                                            ?>
                                        </td>
                                    </tr>
                                    <?php for ($i=0; $i < count($report[0]); $i++): ?>

                                        <tr>
                                            <td><?=$counter?></td>
                                            <td><?=$report[0][$i]?></td>
                                            <td><?=$report[3][$i]."<br/> (кол-во {$report[2][$i]})"?></td>
                                            <td>
                                                <?php
                                                $sumG1 = $report[6][$i]*40;
                                                $sumCH1 = $report[6][$i]*7;
                                                echo $report[6][$i]."<br/> Сумма {$sumG1} / Чистыми {$sumCH1}";
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                $sumG2 = $report[7][$i]*1;
                                                $sumCH2 = $report[7][$i]*0.5;
                                                echo $report[7][$i]."<br/> Сумма {$sumG2} / Чистыми {$sumCH2}";
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                $finishG = $sumG1 + $sumG2;
                                                $finishC = $sumCH1 + $sumCH2;
                                                echo "<br/> Сумма {$finishG} / Чистыми {$finishC}";
                                                ?>
                                            </td>
                                        </tr>
                                        <?php $counter++; ?>
                                    <?php endfor; ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
    </div>

<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "",// always need it for jquery plugin
]) ?>


<?php Modal::end(); ?>