<?php

use app\models\Companies;
use yii\helpers\Url;
use yii\helpers\Html;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'bossName',
        'label' => 'ФИО',
        'content' => function($data){
            $info = Html::a($data->admin->name, ['/companies/view', 'id' => $data->id], ['target' => '_blank']);
            if ($data->isConnectedTelegram) {
                $info .= "  <span style='color:green'>  <i class='fa fa-envelope-o'></i></span>";
            }
            $info .= '<br/>'.$data->general_balance.'  <i class="fa fa-rub">    </i>    ';
            $info .= $data->bonus_balance.'  <i class="fa fa-gift text-info"></i>';
            $ref = Companies::find()->where(['referal' => $data->id])->count();
            $info .= 'Рефералов '.$ref.'  <i class="fa fa-gift text-info"></i>';

            return "{$info}";
        }
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'access_end_datetime',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'rate.name',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'general_balance',
        'vAlign' => \kartik\grid\GridView::ALIGN_CENTER,
        'hAlign' => \kartik\grid\GridView::ALIGN_CENTER,
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'bonus_balance',
        'vAlign' => \kartik\grid\GridView::ALIGN_CENTER,
        'hAlign' => \kartik\grid\GridView::ALIGN_CENTER,
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'last_activity_datetime',
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'template' => '{update} {delete}',
        'contentOptions' => ['style' => 'white-space: nowrap;'],
        'buttons' => [
            'delete' => function ($url, $model) {
                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                    'class' => 'btn btn-danger btn-xs',
                    'role'=>'modal-remote', 'title'=>'Удалить',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                    'data-confirm-title'=>'Вы уверены?',
                    'data-confirm-message'=>'Вы действительно хотите удалить данную запись?'
                ]);
            },
            'update' => function ($url, $model) {
                return Html::a('<i class="fa fa-pencil" style="font-size: 16px;"></i>', $url, [
                    'class' => 'btn btn-success btn-xs',
                    'role'=>'modal-remote', 'title'=>'Изменить',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                ])."&nbsp;";
            }
        ],
    ],

];   