<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\WordsBlackList */

?>
<div class="words-black-list-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
