<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\DispatchRegist */
?>
<div class="dispatch-regist-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'username',
            'account_url:url',
            'data',
            'token',
            'proxy_blocked',
            'last_dispatch_time',
        ],
    ]) ?>

</div>
