<?php

use app\models\Companies;
use app\models\Proxy;
use app\models\Settings;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DispatchRegist */
/* @var $form yii\widgets\ActiveForm */

$proxy = new Proxy();
$setting = Settings::findByKey('max_proxy');
$maxProxyCount = $setting != null ? $setting->value : 0;

?>

<?php if (\Yii::$app->user->identity->isSuperAdmin()): ?>
    <div class="dispatch-regist-form">

        <?php $form = ActiveForm::begin(); ?>


        <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'account_url')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'login')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'password')->textInput(['maxlength' => true]) ?>

        <div class="input-group">
            <?= $form->field($model, 'token')->textInput(['maxlength' => true]); ?>
            <span class="input-group-btn">
                    <?= Html::a('Получить Токен', ['get-token'], ['target' => '_blank', 'class' => 'btn btn-success', 'data' => ['pjax' => 0]]); ?>
                </span>
        </div>

        <?= $form->field($model, 'proxy')->dropDownList($proxy->getListProxy($maxProxyCount)) ?>

        <?= $form->field($model, 'template_id')->dropDownList(\yii\helpers\ArrayHelper::map((\app\models\AutoRegistsTemplates::find()->all()), 'id', 'name'), ['prompt' => 'Выберите шаблон']) ?>

          <?= $form->field($model, 'note')->textarea() ?>

        <?php
        echo $form->field($model, 'company_id')->dropDownList(Companies::getAllInArray(), [
            'prompt' => 'Выберите компанию'
        ])->label('Компания');
        ?>

        <?php
        echo $form->field($model, 'busy')->dropDownList(['0' => 'Свободен', '1' => 'Занят'], [
            'prompt' => 'Выберите статус'
        ])->label('Статус аккаунта');
        ?>

        <?php if (!Yii::$app->request->isAjax) { ?>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        <?php } ?>

        <?php ActiveForm::end(); ?>

    </div>

<?php else: ?>

    <div class="dispatch-regist-form">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'login')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'password')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'template_id')->dropDownList(\yii\helpers\ArrayHelper::map((\app\models\AutoRegistsTemplates::find()->where(['company_id' => \app\components\helpers\FunctionHelper::getCompanyId()]))->all(), 'id', 'name'), ['prompt' => 'Выберите шаблон']) ?>

        <?= $form->field($model, 'note')->textarea() ?>

        <?php if (!Yii::$app->request->isAjax) { ?>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        <?php } ?>

        <?php ActiveForm::end(); ?>

    </div>

<?php endif; ?>
