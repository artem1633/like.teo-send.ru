<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\DispatchRegist */

?>
<div class="dispatch-regist-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
