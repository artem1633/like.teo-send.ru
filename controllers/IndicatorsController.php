<?php

namespace app\controllers;

use app\components\helpers\FunctionHelper;
use app\models\bots\BotSearch;
use app\models\bots\DialogSearch;
use app\models\BotsDialogs;
use app\models\Companies;
use app\models\companies\CompaniesSearch;
use app\models\DailyReport;
use app\models\Dispatch;
use app\models\DispatchRegist;
use app\models\DispatchStatus;
use app\models\Rates;
use app\models\Statuses;
use Yii;
use app\models\Bots;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * BotsController implements the CRUD actions for Bots model.
 */
class IndicatorsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function actionIndex()
    {
        $request = Yii::$app->request;

        if ($request->isAjax) { //Если пользователь нажал закрытие круговой диаграммы

            $dispatch_id = $request->get('id');

            $dispatch_model = Dispatch::findOne($dispatch_id);
            $dispatch_model->to_stat = 0;
            if ($dispatch_model->update() !== false) {
                return 'ok';
            } else {
                return null;
            }

        }

        if (Yii::$app->user->identity->isSuperAdmin() && !Yii::$app->request->get('view-company')) {

            $stts = DispatchStatus::find()->groupBy(['dispatch_id'])->orderBy('id desc')->all();
            $searchModel = new CompaniesSearch;
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams, 1);

            $maxTermWithoutBlocking = 0;

            $limit = 0;
            if ($accounts = DispatchRegist::find()->all()) {
                $dates = [];
                foreach ($accounts as $account) {
                    $blockingDate = !$account->blocking_date ? date('Y-m-d') : $account->blocking_date;
                    $days = (strtotime($blockingDate) - strtotime($account->data)) / (60 * 60 * 24);
                    $dates[] = $days;
                    if (!$account->blocking_date) {
                        $limit += $account->companyModel && $account->companyModel->rate ? $account->companyModel->rate->messages_daily_limit : 0;
                    }

                }
                $maxTermWithoutBlocking = round(max($dates), 0);
            }

            return $this->render('companies', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'countAccounts' => DispatchRegist::find()->count(),
                'countSendedMessages' => DispatchRegist::find()->sum('all_sended_message_count'),
                'maxTermWithoutBlocking' => $maxTermWithoutBlocking,
                'speedDispatch' => $limit,
                'report' => DailyReport::getReportBySendMesasges(),
                'stts' => $stts,
            ]);
        }


        $companyId = Yii::$app->user->identity->isSuperAdmin()
            ? Yii::$app->request->get('view-company')
            : FunctionHelper::getCompanyId();

        $dispatchForStat = Dispatch::find()->where(['to_stat' => 1])->all();
        $charts = [];
        foreach ($dispatchForStat as $dispatch) {
            if ($report = DailyReport::find()->where(['dispatch_id' => $dispatch])->one()) {
                $charts[] = [
                    'dispatch' => $dispatch->id,
                    'name' => $dispatch->name,
                    'date' => $report->date_event,
                    'show' => $dispatch->to_stat,
                    'charts' => [
                        'send' => floatval(DailyReport::find()->where(['dispatch_id' => $dispatch])->sum('sended_message_count')),
                        'read' => floatval(DailyReport::find()->where(['AND', ['type' => DailyReport::TYPE_READ], ['dispatch_id' => $dispatch]])->sum('sended_message_count')),
                        'answer' => floatval(DailyReport::find()->where(['AND', ['type' => DailyReport::TYPE_USER_STATUS], ['status' => Statuses::STATUS_CLIENT_CONVERSATION], ['dispatch_id' => $dispatch]])->sum('sended_message_count')),
                        'interest' => floatval(DailyReport::find()->where(['AND', ['type' => DailyReport::TYPE_USER_STATUS], ['status' => Statuses::STATUS_CLIENT_INTERESTED], ['dispatch_id' => $dispatch]])->sum('sended_message_count')),
                        'buy' => floatval(DailyReport::find()->where(['AND', ['type' => DailyReport::TYPE_USER_STATUS], ['status' => Statuses::STATUS_CLIENT_BUY], ['dispatch_id' => $dispatch]])->sum('sended_message_count')),
                        'refuse' => floatval(DailyReport::find()->where(['AND', ['type' => DailyReport::TYPE_USER_STATUS], ['status' => Statuses::STATUS_CLIENT_REFUSED], ['dispatch_id' => $dispatch]])->sum('sended_message_count')),
                    ],
                ];
            }
        }

        $stts = DispatchStatus::find()->where(['company_id' => FunctionHelper::getCompanyId()])->groupBy(['dispatch_id'])->orderBy('id desc')->all();

        return $this->render('index', [
            'company' => Companies::findOne($companyId),
            'report' => DailyReport::getReportBySendMesasges($companyId),
            'charts' => $charts,
            'stts' => $stts,
            'generalReport' => DailyReport::getGeneralReport(),
        ]);

    }

}
