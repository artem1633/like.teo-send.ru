<?php

namespace app\controllers;

use app\models\Rates;
use app\models\ReferalRedirects;
use app\models\Slider;
use app\models\Users;
use app\models\Settings;
use app\modules\api\controllers\ClientController;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\IdentityInterface;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

use app\models\Logs;
use app\models\RegisterForm;
use app\models\ResetPasswordForm;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->render('index');
        } else {
            return $this->redirect(['site/login']);
        }
    }

    /**
     * Login action.
     *
     * @return Response|string
     */

    public function actionAvtorizatsiya()
    {
        if (isset(Yii::$app->user->identity->id)) {
            return $this->render('error');
        } else {
            Yii::$app->user->logout();
            $this->redirect(['login']);
        }
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        if (Yii::$app->request->get('ref')) {
            Yii::$app->response->cookies->add(new \yii\web\Cookie(['name' => 'referal', 'value' => Yii::$app->request->get('ref')]));
        }

        $slider = Slider::find()->where(['enable' => 1])->orderBy('sort asc')->all();

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            $log = new Logs([
                'user_id' => Yii::$app->user->id,
                'event_datetime' => date('Y-m-d H:i:s'),
                'event' => Logs::EVENT_USER_AUTHORIZED,
            ]);
            $log->description = $log->generateEventDescription();
            $log->save();
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
            'slider' => $slider,
        ]);
    }

    public function actionVk()
    {

        if (isset($_GET['code'])) {
            $result = false;
            $client_secret = 'wsQ8HFU3yGx9wvAXSAua';
            $client_id = '6651556';
            $redirect_uri = 'https://demo.teo-send.ru/site/vk';
            $params = array(
                'client_id' => $client_id,
                'client_secret' => $client_secret,
                'code' => $_GET['code'],
                'redirect_uri' => $redirect_uri
            );
            $token_url =  'https://api.vk.com/oauth/access_token?client_id='.$client_id.'&client_secret='.$client_secret.'&code='.$_GET['code'].'&redirect_uri='.$redirect_uri;
            $params = json_decode(@file_get_contents($token_url));
            if (!$params) {
                $this->redirect(['login']);
            }
            $user_id = $params->user_id;
            $request_params = array(
                'uid' => $user_id,
                'fields' => 'photo,nickname',
                'v' => '5.52',
                'access_token' => $params->access_token,
                'sig' => md5('/method/getProfiles?uid='.$user_id.'&fields=photo,nickname&v=5.52&access_token='.$params->access_token.$params->secret)
            );
            $get_params = http_build_query($request_params);
            $result = json_decode(file_get_contents('https://api.vk.com/method/getProfiles?'. $get_params));
            echo "<pre>";
            print_r($result);
            echo "</pre>";

            $user = Users::find()->where([ 'vk_id' => $result->response[0]->id])->one();

            if($user)
            {
                $login = new LoginForm();
                $login->username = $user->login;
                $login->password = $user->auth_key;
                $login->login();
                return $this->goBack();
            } else {
                $model = new RegisterForm();
                $model->rate_id = Rates::getDefaultRate();
                $model->fio = $result->response[0]->last_name.' '. $result->response[0]->first_name;
                $model->login = $result->response[0]->id.'@mail.ru';
                // $model->telephone = $result->response[0]->id;
                $model->password = md5($result->response[0]->id);
                $model->telephone = ''.$result->response[0]->id;
                $model->vk_id = ''.$result->response[0]->id;
                $model->data_processing = 1;
                $model->register();
                $login = new LoginForm();
                $login->username = $result->response[0]->id;
                $login->password = md5($result->response[0]->id);
                if ($login->login()){
                    return $this->goBack();
                }
            }

        }

        $this->redirect(['login']);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        $this->redirect(['login']);
    }

    public function actionMenuPosition()
    {
        $session = Yii::$app->session;
        if ($session['menu'] == null | $session['menu'] == 'large') $session['menu'] = 'small';
        else $session['menu'] = 'large';

        return $session['menu'];
    }

    public function actionRegister()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        if (Yii::$app->request->get('ref')) {
            Yii::$app->response->cookies->add(new \yii\web\Cookie(['name' => 'referal', 'value' => Yii::$app->request->get('ref')]));
        }

        $log = new Logs([
            'user_id' => null,
            'event_datetime' => date('Y-m-d H:i:s'),
            'event' => Logs::EVENT_USER_OPEN_REGISTRATION_PAGE,
        ]);
        $log->description = $log->generateEventDescription();
        $log->save();

        $this->layout = 'main-login';

        $model = new RegisterForm();
        $model->rate_id = Rates::getDefaultRate();

        if ($model->load(Yii::$app->request->post()) && $model->register()) {

            $request1 = file_get_contents("http://api.sypexgeo.net/json/".$_SERVER['REMOTE_ADDR']);
            $ipCity = json_decode($request1);

            ClientController::sendTelMessage('247187885',  "Зарегистрирован новый пользователь:
                    ФИО: {$model->fio}
                    Логин: {$model->login}
                    Телефон: {$model->telephone}
                    Город: {$ipCity->city->name_ru}
                    ip: {$_SERVER['REMOTE_ADDR']}");

            $login = new LoginForm([
                'username' => $model->login,
                'password' => Yii::$app->request->post('RegisterForm')['password'],
            ]);
            $log = new Logs([
                'user_id' => Yii::$app->user->id,
                'event_datetime' => date('Y-m-d H:i:s'),
                'event' => Logs::EVENT_USER_AUTHORIZED,
            ]);
            $log->description = $log->generateEventDescription();
            $log->save();
            if ($login->login()){
                return $this->goHome();
            }

        } else {
            return $this->render('register', [
                'model' => $model,
            ]);
        }
    }

    public function actionReset()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $this->layout = 'main-login';

        $model = new ResetPasswordForm();

        if ($model->load(Yii::$app->request->post()) && $model->reset()) {
            Yii::$app->session->setFlash('register_success', 'На вашу почту был выслан временный пароль. Воспользуйтесь им для авторизации');
            return $this->redirect(['login']);
        } else {
            return $this->render('reset', [
                'model' => $model,
            ]);
        }
    }
}
