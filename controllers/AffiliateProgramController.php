<?php

namespace app\controllers;

use app\models\AccountingReport;
use app\models\AffiliateAccounting;
use app\models\accounting\AffiliateAccountingSearch;
use app\models\Companies;
use app\models\companies\CompaniesSearch;
use app\models\CompanySettings;
use app\models\EmailTemplates;
use app\models\PaymentOrders;
use app\models\PaymentOrdersSearch;
use Yii,
    yii\helpers\Html,
    app\components\helpers\FunctionHelper,
    yii\web\Controller,
    yii\filters\VerbFilter,
    yii\web\Response;
use yii\data\ActiveDataProvider;

/**
 * BotsController implements the CRUD actions for Bots model.
 */
class AffiliateProgramController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function actionIndex()
    {
        $companyId = FunctionHelper::getCompanyId();
        $settings = CompanySettings::findOne(['company_id' => $companyId]);
        $company = Companies::findOne($companyId);
        $description = EmailTemplates::findOne(['key' => 'affiliate_program']);



        $referalsProvider = new ActiveDataProvider([
            'query' => !Yii::$app->user->identity->isSuperAdmin()
                ? Companies::find()->where(['referal' => $companyId])
                : Companies::find()->where(['!=', 'referal', 1])->orderBy(['referal' => SORT_DESC]),
        ]);
        $referalsProvider->sort->defaultOrder = ['id' => SORT_DESC];
        $referalsSearchModel = new CompaniesSearch;


        $retentionProvider = new ActiveDataProvider([
            'query' => !Yii::$app->user->identity->isSuperAdmin()
                ? AffiliateAccounting::find()->where(['company_id' => $companyId])->orderBy(['date_transaction' => SORT_DESC])
                : AffiliateAccounting::find()->orderBy(['date_transaction' => SORT_DESC]),
        ]);
        $retentionProvider->sort->defaultOrder = ['id' => SORT_DESC];
        $retentionSearchModel = new AffiliateAccountingSearch;

        $refPaymentsProvider = new ActiveDataProvider([
            'query' => !Yii::$app->user->identity->isSuperAdmin()
                ? PaymentOrders::find()->where(['company_id' => $companyId])->orderBy(['date_order' => SORT_DESC])
                : PaymentOrders::find()->orderBy(['date_order' => SORT_DESC]),
        ]);
        $refPaymentsProvider->sort->defaultOrder = ['id' => SORT_DESC];
        $refPaymentsSearchModel = new PaymentOrdersSearch;

        return $this->render('index', [
            'description' => $description->body,
            'company' => $company,
            'settings' => $settings,
            'referalsProvider' => $referalsProvider,
            'retentionProvider' => $retentionProvider,
            'refPaymentsProvider' => $refPaymentsProvider,
            'referalsSearchModel' => $referalsSearchModel,
            'retentionSearchModel' => $retentionSearchModel,
            'refPaymentsSearchModel' => $refPaymentsSearchModel,

        ]);
    }

    /**
     * @return array|string|Response
     */
    public function actionAddOrder()
    {

        $model = new PaymentOrders(['scenario' => PaymentOrders::SCENARIO_SEND_ORDER]);

        $request = Yii::$app->request;
        if (!Yii::$app->user->identity->isSuperAdmin()) {
            $model->company_id = FunctionHelper::getCompanyId();
        }


        if ($request->isPost) {
//            Yii::$app->response->format = Response::FORMAT_JSON;

            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect('index');
                return [
                    'forceReload' => '#crud-datatable-pjax',
                    'title' => "Новая заявка",
                    'size' => 'normal',
                    'content' => '<span class="text-success">Успешно выполнено</span>',
                    'footer' => Html::button('Ок', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
                ];
            }
            return $this->render('accounting/_add-order', ['model' => $model]);
            return [
                'title' => "Новая заявка",
                'size' => 'large',
                'content' => $this->renderAjax('accounting/_add-order', [
                    'model' => $model,
                ]),
                'footer' => Html::button('Отмена', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
            ];
        } else {
            return $this->render('accounting/_add-order', ['model' => $model]);
        }
    }

    public function actionDeleteOrder($id)
    {
        $request = Yii::$app->request;

        $order = $this->findOrder($id);
        if ($order->status != PaymentOrders::STATUS_SUCCESS) {
            $order->delete();
        }


        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    public function actionExchange()
    {
        $request = Yii::$app->request;

//        Yii::warning($request->isAjax, __METHOD__);

//        if ($request->isPost) {

            Yii::$app->response->format = Response::FORMAT_JSON;

            $order = $this->findOrder($request->get('id'));
            $company = Companies::findOne($order->company_id);


            if ($company->affiliate_amount >= $order->amount) {

                Yii::warning($company->affiliate_amount);

                $company->affiliate_amount -= $order->amount;

                $order->status = PaymentOrders::STATUS_SUCCESS;
                $order->date_payment = date('Y-m-d H:i:s');
                $order->save();

                if ($order->type == PaymentOrders::TYPE_BALLANCE) {
                    $company->general_balance += $order->amount;
                    $report = new AccountingReport([
                        'company_id' => $order->company_id,
                        'operation_type' => AccountingReport::TYPE_OUTPUT_AFFILIATE,
                        'amount' => $order->amount,
                        'description' => 'Перевод партнерских отчислений на основной счет',
                    ]);
                    $report->save();
                }
                $company->save();
                $res = ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
            }
//            $res = ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
            return  $this->redirect('index');
//        }

    }

    protected function findOrder($id)
    {
        if (($model = PaymentOrders::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
