<?php

namespace app\controllers;

use app\models\AccountingReport;
use app\models\Companies;
use app\models\CompanySettings;
//use Faker\Provider\Company;
use app\models\Statuses;
use app\models\statuses\StatusesSearch;
use app\models\YandexMoney;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use app\models\Settings;
use yii\web\NotFoundHttpException;

//use yii\web\ForbiddenHttpException;

/**
 * SettingsController implements the CRUD actions for Users model.
 */
class AccountingController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Отображет главную страницу настроек
     * @return mixed
     */
    public function actionIndex()
    {
        if (!Yii::$app->user->identity->isSuperAdmin()) {
            $this->goHome();
        }

        $dataProvider = new ActiveDataProvider([
            'query' => AccountingReport::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

}

