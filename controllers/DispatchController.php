<?php

namespace app\controllers;

use app\components\helpers\FunctionHelper;
use app\models\Bots;
use app\models\calculation\Calculation;
use app\models\Companies;
use app\models\CompanySettings;
use app\models\DataRecipient;
use app\models\Dispatch\DispatchSearch;
use app\models\DispatchConnect;
use app\models\DispatchRegist;
use app\models\DispatchStatus;
use app\models\DispatchStatusExport;
use app\models\Settings;
use app\models\TemplateMessages;
use app\models\Users;
use app\modules\api\controllers\ClientController;
use app\modules\api\controllers\VkController;
use Yii;
use app\models\Dispatch;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\data\Sort;
use yii\db\Exception;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use yii\web\UploadedFile;

/**
 * DispatchController implements the CRUD actions for Dispatch model.
 */
class DispatchController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Dispatch models.
     * @return mixed
     */
    public function actionIndex($platform = null, $automation_id = null)
    {
        $searchModel = new DispatchSearch();

        if (Yii::$app->user->identity->isSuperAdmin()) {
            $dataProvider = new ActiveDataProvider([
                'query' => Dispatch::find(),
            ]);
            $dataProvider->sort->defaultOrder = ['id' => SORT_DESC];
            $dataProvider->pagination = ['pageSize' => 40];
            $stts = DispatchStatus::find()->groupBy(['dispatch_id'])->all();

            $searchModel->load(Yii::$app->request->get());

            $dataProvider->query->andFilterWhere(['like', 'name', $searchModel->name]);
            $dataProvider->query->andFilterWhere(['status' => $searchModel->status]);
            $dataProvider->query->andFilterWhere(['company_id' => $searchModel->company_id]);
            $dataProvider->query->andFilterWhere(['platform' => $platform]);
            $dataProvider->query->andFilterWhere(['automation_id' => $automation_id]);

            return $this->render('index', [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
                'platform' => $platform,
                'stts' => $stts
            ]);
        } else {
            $dataProvider = new ActiveDataProvider([
//                'query' => Dispatch::find()->where(['company_id' => Yii::$app->user->getId()]),
                'query' => Dispatch::find()->where(['company_id' => FunctionHelper::getCompanyId()]),
            ]);
            $dataProvider->sort->defaultOrder = ['id' => SORT_DESC];
            $dataProvider->pagination = ['pageSize' => 40];
            $stts = DispatchStatus::find()->where(['company_id' => FunctionHelper::getCompanyId()])->groupBy(['dispatch_id'])->all();

            $searchModel->load(Yii::$app->request->get());

            $dataProvider->query->andFilterWhere(['like', 'name', $searchModel->name]);
            $dataProvider->query->andFilterWhere(['status' => $searchModel->status]);
            $dataProvider->query->andFilterWhere(['platform' => $platform]);
            $dataProvider->query->andFilterWhere(['automation_id' => $automation_id]);

            return $this->render('index', [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
                'platform' => $platform,
                'stts' => $stts
            ]);
        }
    }

    public function actionStatusExport($id)
    {
        $request = Yii::$app->request;
        $dispatch = $this->findModel($id);

        $model = new DispatchStatusExport();
        $model->dispatchId = $dispatch->id;

        Yii::$app->response->format = Response::FORMAT_JSON;
        if($model->load($request->post())){
            $path = $model->export();


            return [
                'title' => 'Экспорт',
                'content' => '<span class="text-success">Файл сформирован</span>',
                'footer' => Html::button('Скачать',['class'=>'btn btn-success btn-block','onclick'=>"
                  var element = document.createElement('a');
                  element.setAttribute('href', 'export.txt');
                  element.setAttribute('download', 'Экспорт.txt');
                
                  element.style.display = 'none';
                  document.body.appendChild(element);
                
                  element.click();
                
                  document.body.removeChild(element);
                "]),
            ];

        } else {
            return [
                'title' => 'Экспорт',
                'content' => $this->renderAjax('status-export', [
                    'model' => $model,
                ]),
                'footer' => Html::button('Экспорт',['class'=>'btn btn-primary btn-block','type'=>"submit"]),
            ];

        }
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function actionBackToHandle($id)
    {
        $model = $this->findModel($id);
        $model->status = DispatchStatus::STATUS_HANDLE;
        $model->save(false);

        return $this->redirect(['index']);
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function actionKill($id)
    {
        DispatchStatus::deleteAll('`account_id` IS NULL AND `dispatch_id` = :id',[':id' => $id]);

        return $this->redirect(['index']);
    }

    /**
     * Displays a single Dispatch model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "Dispatch #" . $id,
                'content' => $this->renderAjax('view', [
                    'model' => $this->findModel($id),
                ]),
                'footer' => Html::button('Close', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::a('Edit', ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
            ];
        } else {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    public function actionCalculation()
    {
        $request = Yii::$app->request;
        $model = new Calculation();

        if($model->load($request->post()) && $model->calculate())
        {
            $dataProvider = new ArrayDataProvider([
                'allModels' => $model->calculatedData,
//                'sort' => $sort,
                'pagination' => false,
            ]);
            return $this->render('calculation', [
                'model' => $model,
                'dataProvider' => $dataProvider,
            ]);
        } else {
            return $this->render('calculation', [
                'model' => $model,
                'dataProvider' => null,
            ]);
        }
    }

    /**
     * Creates a new Dispatch model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($platform = null)
    {

        $request = Yii::$app->request;
        $model = new Dispatch;
        $maxDispatch = $model->getMaxCountDispatch();
        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;

            if ($model->getCountDispatch() >= $maxDispatch && !Yii::$app->user->identity->isSuperAdmin()) {
                return [
                    'title' => "Вы создали максимальное разрешенное количество рассылок!",
                    'content' => 'Ваш тарифный план предусматривает создание ' . $maxDispatch . ' рассылок',
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])

                ];
            }

            if ($request->isGet) {
                return [
                    'title'=> "Создать новую накрутку",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                        'platform' => $platform,
                    ]),
                    'footer'=> Html::button('Отменить',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            } else if ($model->load($request->post()) && $model->save()) {

                $model->company_id = FunctionHelper::getCompanyId();


                $model->data = date("Y-m-d H:i:s");
                /*
                 * status can be
                 * wait - в очереди
                 * job - В работе
                 * finish - Отправленно
                 */
                $model->status = DispatchStatus::STATUS_JOB;
                $model->status_job = 'ok';
                if ($model->type == 'in' or $model->distribute) {
                    $results = $this->getGroup($model->link);
                    $model->group_id = $results['response'][0]['id'];
                }


                $model->save(false);


                $company = CompanySettings::findOne(['company_id' => $model->company_id]);
                if($company == null){
                    $company = new CompanySettings([
                        'company_id' => $model->company_id
                    ]);
                    $company->save(false);
                }
                $priceMessage = $company->getPriceByMessage();
                $generalBalance = $company->companyModel->bonus_balance > 0 ? $company->companyModel->bonus_balance : $company->companyModel->general_balance;
                $countMessages = $model->mission;
                $dispatch_ammount = floatval($countMessages * $priceMessage);
                //$company->companyModel->general_balance >= $dispatch_ammount
                if (($generalBalance >= $priceMessage)
                    || ($generalBalance >= $dispatch_ammount)
                ) {

                    $danger = null;

                    if (($generalBalance >= $priceMessage)
                        && ($generalBalance < $dispatch_ammount)
                    ) {
                        $danger = '<span class="text-warning">Ваша рассылка будет обработана частично!</span><br/>';
                    }
                }
                if ($generalBalance <= 0 || $generalBalance < $priceMessage) {
                    $danger = '<span class="text-danger">Для выполнения выполнения рассылки пополните Ваш баланс<br/></span>';
                }


                if ($model->distribute){
                    $list = $model->getListNewPost($model);
                    if ($list) {
                       $st = $model->getNewMission($model, $list);
                       $model->delete();
                    }
                }

                return [
                    'forceReload' => '#crud-datatable-pjax',
                    'title' => "Создать новый",
                    'content' => $a,
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::a('Создать еще', ['create', 'platform' => $platform], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])

                ];
            } else {

                $tags = ArrayHelper::map(TemplateMessages::find()->where(['company_id' => FunctionHelper::getCompanyId()])->all(), 'tag', 'title');

                return [
                    'title' => "Создать новый",
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                        'platform' => $platform,
                        'tags' => $tags,
                    ]),
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {

                return $this->render('create', [
                    'model' => $model,
                    'platform' => $platform,
                ]);
            }
        }

    }

    /**
     * @return string
     */
    public function actionShow()
    {
        if (isset($_GET['send'])) {
            $id = $_GET['send'];
            $dataProvider = new ActiveDataProvider([
                'query' => DispatchStatus::find()->where(['dispatch_id' => $id]),
            ]);
            $dataProvider->pagination = ['pageSize' => 40];
            return $this->render('/dispatch-status/index', [
                'dataProvider' => $dataProvider,
            ]);
        }


    }

    /**
     * @return string
     */
    public function actionReport()
    {
        if (Yii::$app->user->identity->isSuperAdmin()) {
            $stts = DispatchStatus::find()->groupBy(['dispatch_id'])->all();

            return $this->render('report', compact('stts'));
        } else {
            $stts = DispatchStatus::find()->where(['company_id' => FunctionHelper::getCompanyId()])->groupBy(['dispatch_id'])->all();

            return $this->render('report', compact('stts'));
        }


    }

    /**
     * @return mixed
     */
    public function actionFaster()
    {
        $stts = Dispatch::find()->where(['id' => $_GET['id']])->one();

        $countMessages = DispatchStatus::find()->where(['dispatch_id' => $_GET['id']])->count();
        $company = CompanySettings::findOne(['company_id' => $stts->company_id]);
        $priceMessage = $company->getPriceByMessage();
        $generalBalance = $company->companyModel->bonus_balance > 0 ? $company->companyModel->bonus_balance : $company->companyModel->general_balance;
        $dispatch_ammount = floatval($countMessages * $priceMessage);
        //$company->companyModel->general_balance >= $dispatch_ammount
        if (($generalBalance >= $priceMessage)
            || ($generalBalance >= $dispatch_ammount)
            && $stts != null
            && ($_GET['status'] == 'job' || $_GET['status'] == 'wait')
        ) {
            if (($generalBalance >= $priceMessage)
                && ($generalBalance < $dispatch_ammount)
            ) {
                Yii::$app->session->setFlash('danger', 'Ваша рассылка будет обработана частично!');
            }
            $stts->status = $_GET['status'];
            if ($stts->update()) {
                return $this->actionIndex();
            }

        }

        if ($generalBalance <= 0 || $generalBalance < $priceMessage) {
            Yii::$app->session->setFlash('danger', 'Для выполнения этого действия пополните Ваш баланс');
        }

        return $this->actionIndex();


    }

    /**
     * Updates an existing Dispatch model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                $tags = ArrayHelper::map(TemplateMessages::find()->where(['or', ['company_id' => FunctionHelper::getCompanyId()], ['company_id' => 1]])->all(), 'tag', 'title');
                return [
                    'title' => "Изменить  #" . $id,
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                        'tags' => $tags,
                    ]),
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', [
                            'class' => 'btn btn-primary saveBtn hidden',
                            'type' => "submit",
                        ]) . Html::button('Далее', [
                            'id' => '1',
                            'class' => 'btn btn-primary nextBtn',
                            'onclick' => 'step();',
                        ])
                ];
            } else if ($model->load($request->post()) && $model->save()) {

                $conns = DispatchConnect::findAll(['dispatch_id' => $model->id]);
                $arrdb = array();
                $arrpost = array();
                foreach ($conns as $con) {
                    array_push($arrdb, $con->dispatch_regist_id);
                }
                foreach ($request->post()['Dispatch']['accounts'] as $value) {
                    array_push($arrpost, $value);
                }
                $delete = array_diff($arrdb, $arrpost);
                $update = array_diff($arrpost, $arrdb);
                $new = array_intersect($arrdb, $arrpost);


                if ($model->text_data) {

                    $lines = explode("\n", $model->text_data);

                    $counter = 0;
                    foreach ($lines as $line) {
                        if ($counter >= 5000)
                            break;

                        if ($line != null) {
                            FunctionHelper::addRecipient($line, $model);
                        }
                        $counter++;
                    }

                    FunctionHelper::saveDataRecipient($model, $model->text_data);
                }

                if ($model->recived_data) {
                    $recived_data = Yii::$app->session->get(Dispatch::USERS_SESSION_KEY);
                    $lines = explode(PHP_EOL, $recived_data);
                    Yii::$app->session->remove(Dispatch::USERS_SESSION_KEY);

                    $counter = 0;

                    foreach ($lines as $line) {
                        if($counter >= 5000)
                            break;

                        if ($line != null) {
                            FunctionHelper::addRecipient($line, $model);
                        }
                        $counter++;
                    }

                    FunctionHelper::saveDataRecipient($model, $recived_data);
                }

                if ($model->select_data) {
                    $file_data = DataRecipient::findOne($model->select_data);
                    if ($file = fopen($file_data->file, "r")) {
                        $counter = 0;
                        while (!feof($file) && $counter < 5000) {
                            $line = fgets($file);
                            if ($line != null) {
                                FunctionHelper::addRecipient($line, $model);
                            }
                            $counter++;
                        }
                        fclose($file);
                    }
                }

                foreach ($delete as $r) {
                    $d = DispatchConnect::findOne(['dispatch_id' => $model->id, 'dispatch_regist_id' => $r]);
                    $d->delete();
                }

                foreach ($update as $u) {
                    $projectsUser = new DispatchConnect();
                    $projectsUser->dispatch_id = $model->id;
                    $projectsUser->dispatch_regist_id = $u;
                    $projectsUser->company_id = Users::getCompanyId($model->id);
                    $projectsUser->save();
                }


                $model->file = UploadedFile::getInstance($model, 'file');

                if ($model->file) {


                    if ($model->upload()) {

                        if ($file = fopen($model->filename, "r")) {
                            $counter = 0;
                            while (!feof($file) && $counter < 5000) {
                                $line = fgets($file);
                                if ($line != null) {
                                    FunctionHelper::addRecipient($line, $model);
                                }
                                $counter++;
                            }
                            FunctionHelper::saveDataRecipient($model, $file);
                            fclose($file);
                            unlink($file);
                        }

                        //                        if ($file = fopen("uploads/{$model->filename}", "r")) {
//                            while (!feof($file)) {
//                                $line = fgets($file);
//                                if ($line != null) {
//                                    FunctionHelper::addRecipient($line, $model);
//                                    $id = explode('/id', $line);
//                                    if ((int)$id[1] != 0) {
//                                        $status = new DispatchStatus();
//                                        $status->account_id = $id[1];
//                                        $status->company_id = Yii::$app->user->getId();
//                                        //$status->status = "wait";
//                                        $status->dispatch_id = $model->id;
//                                        $status->save();
//                                    }
//
//                                }
//
//                            }
//                            FunctionHelper::saveDataRecipient($model, $file);
//                            fclose($file);
//                            unlink($file);
//                        }

                    }


                }

                return [
                    'forceReload' => '#crud-datatable-pjax',
                    'title' => "Изменить  #" . $id,
                    'content' => $this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::a('Редактировать', ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
                ];
            } else {
                return [
                    'title' => "Изменить #" . $id,
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing Dispatch model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    /**
     * Delete multiple existing Dispatch model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post('pks')); // Array or selected records primary keys
        foreach ($pks as $pk) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    /**
     * @param string $urlGroup Ссылка на группу
     * @return array|null|string
     */
    public function actionParsegroup()
    {

        $postUrl = Yii::$app->request->post('url');
        if ($postUrl) {
            $users = Dispatch::getGroupUsers($postUrl);
            $data = '';
            foreach ($users as $user) {
                $data = $data . 'http://vk.com/id' . $user . PHP_EOL;
            }

            Yii::$app->session->set(Dispatch::USERS_SESSION_KEY, $data);

            $response = [];

            if (count($users) > 0) {
                $response[0] = true;
                $response[1] = 'Получены данные на ' . count($users) . ' пользователей';
            } else {
                $response[0] = false;
                $response[1] = 'Ошибка получения данных. Проверьте прокси и URL адрес группы.';
            }

            return Json::encode($response);
        }
    }

    /**
     * @param $value
     * @param $filename
     */
    public static function object2file($value, $filename)
    {
        $f = fopen($filename, 'w');
        fwrite($f, $value);
        fclose($f);
    }

    /**
     * Finds the Dispatch model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Dispatch the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Dispatch::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionResult()
    {
        $request = Yii::$app->request;
        $model = new Dispatch;

        if ($request->isAjax) {

            Yii::$app->response->format = Response::FORMAT_JSON;

            if ($request->isGet) {

                Yii::warning($request->isGet, __METHOD__);

                return [
                    'title' => "Создать новую базу",
                    'content' => $this->renderAjax('result', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сгенерировать', [
                            'class' => 'btn btn-primary',
                            'type' => "submit",
                        ])

                ];
            } else if ($model->load($request->post()) && $model->save()) {
                $model->company_id = FunctionHelper::getCompanyId();
                $resultBase = '';

                Yii::warning($model, __METHOD__);

                //Перебираем все рассылки
                foreach ($model->selected_dispatches as $dispatch) {

                    Yii::warning($dispatch, __METHOD__);

                    //Перебираем статусы
                    foreach ($model->selected_statuses as $status) {
                        Yii::warning($status, __METHOD__);
                        $tempBase = DispatchStatus::find()
                            ->where(['dispatch_id' => $dispatch])
                            ->andWhere(['status' => $status])->asArray()->all();

                        Yii::warning($tempBase, __METHOD__);

                        if (count($tempBase) > 0) {
                            //Переводим в строку
                            $resultBase .= implode(',', $tempBase);
                        }

                    }
                }

                if ($resultBase) {
//                   $resultBase = str_replace('Array', '', $resultBase);
                    $ids = explode(',', $resultBase);
                }

                Yii::warning($ids, __METHOD__);


                if ($ids) {
                    FunctionHelper::addRecipientFromArray($ids, $model);
                    $data = '';
                    foreach ($ids as $id) {
                        if ($id) {
                            $data = $data . 'https://vk.com/id' . $id . PHP_EOL;
                        }
                    }

                    FunctionHelper::saveDataRecipient($model, $data);

                    Yii::warning($data, __METHOD__);

                    $countUsers = DispatchStatus::find()->where(['dispatch_id' => $model->id])->count();
                    return[
                        'title' => "Создание новой базы",
                        'content' => 'База создана успешно. Добавлено ' . $countUsers . ' записей' ,
                        'forceReload' => '#crud-datatable-database-pjax',
                        'footer' => Html::button('Закрыть', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::a('Создать еще', ['result'], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
                    ];
                } else {
                    return [
                        'title' => "Создание новой базы",
                        'content' => $this->renderAjax('result', [
                            'model' => $model,
                        ]),
                        'footer' => Html::tag('div', 'Не найдено ни одного совпадения удовлетворяющего запросу', ['class' => 'alert alert-warning text-center']) .
                            Html::button('Закрыть', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('Сгенерировать', [
                                'class' => 'btn btn-primary',
                                'type' => "submit",
                            ])
                    ];
                }


            } else {

                Yii::warning('Не запись', __METHOD__);

                return [
                    'title' => "Создать новый",
                    'content' => $this->renderAjax('result', [
                        'model' => $model,
                    ]),
                ];
            }
        } else {

            Yii::warning('Не аякс', __METHOD__);

            return $this->redirect('index');
        }

//        return $this->render('result', ['model' => $model]);
    }

    public function actionChangeStatus($id)
    {
        $model = $this->findModel($id);
        $model->status = 'finish';
        $model->save();
        $this->redirect('index');
    }

    public function actionCreateBase()
    {
        $resultBase = [];
        $model = new Dispatch;


        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->company_id = FunctionHelper::getCompanyId();
            Yii::warning($model, __METHOD__);

            //проходимся по всем файлам базы
            foreach ($model->selected_dispatches as $dispatch) {
                Yii::warning($dispatch, __METHOD__);
                //Перебираем статусы
                foreach ($model->selected_statuses as $status) {
                    Yii::warning($status, __METHOD__);
                    $tempBase = ArrayHelper::map(DispatchStatus::find()
                        ->where(['dispatch_id' => $dispatch])
                        ->andWhere(['status' => $status])->all(), 'id', 'account_id');
                    //Переводим в строку
                    $resultBase .= implode(',', $tempBase);
                }
            }
            $resultBase = str_replace('Array', '', $resultBase);
            $ids = explode(',', $resultBase);
            FunctionHelper::addRecipientFromArray($ids, $model);
            $data = '';
            foreach ($ids as $id) {
                $data = $data . 'http://vk.com/id' . $id . PHP_EOL;
            }

            FunctionHelper::saveDataRecipient($model, $data);

            Yii::warning($data, __METHOD__);

        }

        $this->redirect('result');
    }

    /**
     * @param $token
     * @param $proxy
     * @return mixed
     */
    private  function getGroup($group)
    {
        $token = Settings::find()->where(['key' => 'vk_access_token'])->one();
        $token = $token->value;

        $setingProxy = Settings::find()->where(['key' => 'proxy_server'])->one();
        $proxy = $setingProxy->value;

        //Парсим имя группы
        if (strpos($group, 'com/public') > 0) {
            //имя группы = int
            $groupId = mb_substr($group, mb_strpos($group, 'com/public') + 10);
        } else {
            //имя группы произвольная строка
            $groupId = mb_substr($group, mb_strrpos($group, '/') + 1);
        }
        $url = 'https://api.vk.com/method/groups.getById';
        $params = array(
            'group_ids' => $groupId,
            'fields' => 'domain,can_message',
            'name_case' => 'Nom', // Токен того кто отправляет
            'access_token' => $token, // Токен того кто отправляет
            'v' => '5.95',
        );

        // В $result вернется id отправленного сообщения

        $result = file_get_contents($url, false, stream_context_create(array(
            'http' => array(
                'proxy' => $proxy,
                'request_fulluri' => true,
                'method' => 'POST',
                'header' => 'Content-type: application/x-www-form-urlencoded',
                'content' => http_build_query($params)
            )
        )));

        return json_decode($result, true);

    }

}
