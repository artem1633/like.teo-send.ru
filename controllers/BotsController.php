<?php

namespace app\controllers;

use app\components\helpers\FunctionHelper;
use app\models\bots\BotSearch;
use app\models\bots\DialogSearch;
use app\models\BotsDialogs;
use app\models\Companies;
use PHPUnit\Framework\Error\Error;
use Yii;
use app\models\Bots;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * BotsController implements the CRUD actions for Bots model.
 */
class BotsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function actionIndex()
    {
        $company = null;
        if (!Yii::$app->user->identity->isSuperAdmin()) {
            $company = FunctionHelper::getCompanyId();
        }
        Yii::warning('Компания: ' . $company, __METHOD__);

        $request = Yii::$app->request;
        $session = Yii::$app->session;

        if ($request->get('clone')) {
            Yii::warning('CLONE', __METHOD__);

            $transaction = Yii::$app->db->beginTransaction();
            try {
                $original = $this->findModel($request->get('clone'));
                $clone = new Bots;
                $clone->attributes = $original->attributes;
                $clone->name = $clone->name . ' (Копия)';
                $clone->save();
                if ($originalDialogs = BotsDialogs::find()->where(['botId' => $original->id])->all()) {
                    foreach ($originalDialogs as $originalDialog) {
                        $cloneDialog = new BotsDialogs;
                        $cloneDialog->attributes = $originalDialog->attributes;
                        $cloneDialog->botId = $clone->id;
                        $cloneDialog->save();
                    }
                }
                $transaction->commit();

            } catch (\Exception $e) {
                $transaction->rollBack();
                throw $e;
            }
        }

        $searchModel = new BotSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $company);

        if ($session->get('searchText')) {

            $exactMatch = $session->get('exactMatch');
            $searchText = $session->get('searchText');

            Yii::warning('Текст для поиска: ' . $searchText, __METHOD__);
            Yii::warning('Точный поиск: ' . $exactMatch, __METHOD__);

        }
        Yii::warning(0);
        if ($searchText) {
            Yii::warning(1);
            if ($exactMatch == 'false') { //Не строгий поиск
                Yii::warning('1.1');
                $query = Bots::find()
                    ->leftJoin('bots_dialogs', 'bots_dialogs.botId = bots.id')
                    ->where([
                        'LIKE',
                        'bots_dialogs.result',
                        $searchText
                    ]);

                $dataProvider = new ActiveDataProvider([
                    'query' => $query,
                ]);
            } elseif ($exactMatch == 'true') {
                Yii::warning('1.2');
                $query = Bots::find()
                    ->leftJoin('bots_dialogs', 'bots_dialogs.botId = bots.id')
                    ->where(['bots_dialogs.result' => $searchText]);

                $dataProvider = new ActiveDataProvider([
                    'query' => $query,
                ]);
            }

            $session->remove('exactMatch');
            $session->remove('searchText');
        }


        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Bots model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => 'Название бота : ' . $this->findModel($id)->name,
                'size' => 'large',
                'content' => $this->renderAjax('view', [
                    'model' => $this->findModel($id),
                ]),
                'footer' => Html::button('Отмена', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::a('Изменить', ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
            ];
        } else {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new Bots model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Bots();
        $maxBots = $model->getMaxCountBots();
        if (!Yii::$app->user->identity->isSuperAdmin()) {
            $model->companyId = FunctionHelper::getCompanyId();
        }
        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;

            if ($model->getCountBots() >= $maxBots) {
                return [
                    'title' => "Вы создали максимальное разрешенное количество ботов!",
                    'content' => 'Ваш тарифный план предусматривает создание ' . $maxBots . ' ботов',
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])

                ];
            }

            if ($request->isGet) {
                return [
                    'title' => "Создать",
                    'size' => 'large',
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else if ($model->load($request->post()) && $model->save()) {
                return [
                    'forceReload' => '#crud-datatable-pjax',
                    'title' => "Пользователи",
                    'size' => 'normal',
                    'content' => '<span class="text-success">Успешно выполнено</span>',
                    'footer' => Html::button('В конструктор', [
                            'class' => 'btn btn-default pull-left',
                            'data-dismiss' => "modal",
                            'onclick' => 'document.location.href="bots/constructor?id=' . $model->id . '"',
                        ])
                        . Html::a('Создать ещё', ['create'], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
                ];
            } else {
                return [
                    'title' => "Создать",
                    'size' => 'large',
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }

    }

    /**
     * Updates an existing Bots model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Изменить",
                    'size' => 'large',
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else if ($model->load($request->post()) && $model->save()) {
                return [
                    'forceReload' => '#crud-datatable-pjax',
                    'size' => 'large',
                    'title' => 'Название бота : ' . $model->name,
                    'content' => $this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::a('Изменить', ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
                ];
            } else {
                return [
                    'title' => "Изменить",
                    'size' => 'large',
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Deletes an existing Bots model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionConstructor($id)
    {

        if (!Yii::$app->user->identity->isSuperAdmin()) {
            $userModel = Yii::$app->user->identity;
            $bot = $this->findModel($id);
            if ($userModel->company_id != $bot->companyId) {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }

        $model = new BotsDialogs;
        $model->botId = $id;
        $model->stepNumber = $model->getNextStepNumber();

        $searchModel = new DialogSearch;
        $dataProvider = $searchModel->search($id);

        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->session->setFlash('successSave');
            }
        }

        return $this->render('dialogs/constructor', [
            'model' => $model,
            'dataProvider' => $dataProvider,
            'steps' => BotsDialogs::getStepsByBot($id),
            'greatestStep' => BotsDialogs::getGreatestStep($id),
        ]);

    }

    /**
     * @param $id
     * @return array|string|Response
     */
    public function actionDialogCreate($id)
    {
        $request = Yii::$app->request;
        $model = new BotsDialogs;
        $model->botId = $id;

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Новый ответ",
                    'size' => 'large',
                    'content' => $this->renderAjax('dialogs/create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else if ($model->load($request->post()) && $model->save()) {
                return [
                    'title' => "Новый ответ",
                    'size' => 'normal',
                    'content' => '<span class="text-success">Успешно выполнено</span>',
                    'footer' => Html::button('Ок', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal", 'onclick' => 'document.location.reload(true);'])
                ];
            } else {
                return [
                    'title' => "Новый ответ",
                    'size' => 'large',
                    'content' => $this->renderAjax('dialogs/create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['', 'id' => $model->id]);
            } else {
                return $this->render('dialogs/create', [
                    'model' => $model,
                ]);
            }
        }


    }

    /**
     * @param $id
     * @return array|string|Response
     */
    public function actionDialogUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findDialog($id);

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Изменить",
                    'size' => 'large',
                    'content' => $this->renderAjax('dialogs/create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else if ($model->load($request->post()) && $model->save()) {
                return [

                    'size' => 'large',
                    'title' => 'Диалог',
                    'content' => '<span class="text-success">Успешно выполнено</span>',
                    'footer' => Html::button('Ок', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal", 'onclick' => 'document.location.reload(true);'])
                ];
            } else {
                return [
                    'title' => "Изменить",
                    'size' => 'large',
                    'content' => $this->renderAjax('dialogs/create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['', 'id' => $model->id]);
            } else {
                return $this->render('dialogs/create', [
                    'model' => $model,
                ]);
            }
        }

    }

    /**
     * @param $id
     * @return Response
     */
    public function actionDialogDelete($id)
    {
        $model = $this->findDialog($id);
        $botId = $model->botId;
        $model->delete();

        return $this->redirect(['/bots/constructor?id=' . $botId]);
    }

    /**
     * Finds the Bots model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Bots the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Bots::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * @param $id
     * @return null|static
     * @throws NotFoundHttpException
     */
    protected function findDialog($id)
    {
        if (($model = BotsDialogs::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionSearchbottext()
    {
        $request = Yii::$app->request;
        $exactMatch = $request->post('exactMatch');
        $searchText = $request->post('searchText');

        Yii::debug('Точный поиск: ' . $exactMatch);
        Yii::debug('Поисковый запрос: ' . $searchText);

        Yii::$app->session->set('exactMatch',$exactMatch);
        Yii::$app->session->set('searchText',$searchText);
        return $this->redirect(Url::to(['index']));
    }

}
