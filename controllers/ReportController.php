<?php

namespace app\controllers;

use app\filters\PermissionsFilter;
use app\models\DailyReport;
use app\models\RegistrationReportFilter;
use app\models\Users;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\Controller;

/**
 * GlobalMessagesController implements the CRUD actions for GlobalMessages model.
 */
class ReportController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'permissions' => [
                'class' => PermissionsFilter::class,
                'rules' => [
                    'actions' => '*',
                    'roles' => [Users::USER_TYPE_SUPER_ADMIN],
                ],
            ],
        ];
    }

    /**
     * Lists all GlobalMessages models.
     * @return mixed
     */
    public function actionRegistration()
    {
        $request = Yii::$app->request;
        $model = new RegistrationReportFilter();

        $model->load($request->get());

        $report = $model->search();

        return $this->render('registration', [
            'report' => $report,
            'model' => $model
        ]);
    }
    /**
     * Lists all GlobalMessages models.
     * @return mixed
     */
    public function actionVisit()
    {
//        $model = DailyReport::find()->where(['type' => 5])->select(['comments'])->distinct()->all();
        $query = DailyReport::find()->where(['type' => 5])->select(['comments'])->distinct();
        $model = new ActiveDataProvider([
            'query' => $query,
        ]);

        $query->andFilterWhere(['!=', 'comments', 'Пользователь первый раз зашол']);

        return $this->render('visit', [
            'model' => $model
        ]);
    }
}
