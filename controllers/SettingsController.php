<?php

namespace app\controllers;

use app\components\helpers\FunctionHelper;
use app\models\AnswerTemplate;
use app\models\AnswerTemplateSearch;
use app\models\autoregist\AutoRegistrSearch;
use app\models\AutoRegistsTemplatesSearch;
use app\models\Companies;
use app\models\CompanySettings;
//use Faker\Provider\Company;
use app\models\DataRecipient;
use app\models\proxy\ProxySearch;
use app\models\SliderSearch;
use app\models\Statuses;
use app\models\statuses\StatusesSearch;
use app\models\TemplateMessagesSearch;
use app\models\UsersSearch;
use app\models\WordsBlackListSearch;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\web\Controller;
use app\models\Settings;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;

//use yii\web\ForbiddenHttpException;

/**
 * SettingsController implements the CRUD actions for Users model.
 */
class SettingsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Отображет главную страницу настроек
     * @return mixed
     */
    public function actionIndex()
    {
        $request = Yii::$app->request;

        $company = Companies::findOne(FunctionHelper::getCompanyId());
        $company_settings = CompanySettings::findOne(['company_id' => $company->id]);

        $searchModelSlider = new SliderSearch();
        $dataProviderSlider = $searchModelSlider->search(Yii::$app->request->queryParams);

        if ($request->isPost) {

            $data = $request->post();

            $company->load($request->post());
            $company->save();

            if ($company_settings) {
                $company_settings->load($request->post());
                $company_settings->save();
            } else {
                $company_settings = new CompanySettings();
                $company_settings->company_id = $company->id;
                if ($company_settings->load($request->post()) && $company_settings->save()) {
                    Yii::$app->session->setFlash('success', 'Настройки успешно сохранены');
                } else {
                    Yii::$app->session->setFlash('danger', 'Ошибка сохранения настроек');

                }
            }

            $checked = [];

            foreach ($data['Settings'] as $key => $value) {
                $setting = Settings::findByKey($key);

                if($setting->type == Settings::TYPE_CHECKBOX){
                    $checked[] = $setting->id;
                }

                if ($setting != null) {
                    $setting->value = $value;
                    $setting->save();
                    Yii::$app->session->setFlash('success', 'Настройки успешно сохранены');
                }
            }


                $notChecked = Settings::find()->where(['type' => Settings::TYPE_CHECKBOX]);

                foreach ($checked as $id){
                    $notChecked->andWhere(['!=', 'id', $id]);
                }

                $notChecked = $notChecked->all();

                foreach ($notChecked as $checkboxSetting){
                    $checkboxSetting->value = '0';
                    $checkboxSetting->save(false);
                }
        }
        $settings = Settings::find()->orderBy('type desc')->all();

        if (!$company_settings) {
            $company_settings = new CompanySettings;
        }

        $searchModel = new StatusesSearch;

        $searchModelUser = new UsersSearch;
        $dataProviderUser = $searchModelUser->search(Yii::$app->request->queryParams);
        $dataProviderUser->sort->defaultOrder = ['id' => SORT_DESC];

        $searchModelTemplates = new AnswerTemplateSearch;
        $dataProviderTemplates = Yii::$app->user->identity->isSuperAdmin()
            ? new ActiveDataProvider([
                'query' => AnswerTemplate::find(),
            ])
            : new ActiveDataProvider([
                'query' => AnswerTemplate::find()->where(['company_id' => $company->id]),
            ]);
        $dataProviderTemplates->sort->defaultOrder = ['id' => SORT_DESC];

//        $searchModelProxy = new ProxySearch;
//        $dataProviderProxy = $searchModelProxy->search(Yii::$app->request->queryParams, $company->id);
//        $dataProviderProxy->sort->defaultOrder = ['id' => SORT_DESC];

        $dataProviderDataBases = Yii::$app->user->identity->isSuperAdmin()
            ? new ActiveDataProvider([
                'query' => DataRecipient::find(),
            ])
            : new ActiveDataProvider([
                'query' => DataRecipient::find()->where(['company_id' => $company->id]),
            ]);
        $dataProviderDataBases->sort->defaultOrder = ['id' => SORT_DESC];


        $searchModelAutoReg = new AutoRegistsTemplatesSearch();
        $dataProviderAutoReg = $searchModelAutoReg->search(Yii::$app->request->queryParams);

        $searchModelWordsBlackList = new WordsBlackListSearch();
        $dataProviderWordsBlackList = $searchModelWordsBlackList->search(Yii::$app->request->queryParams);

        $searchTemplateMessages = new TemplateMessagesSearch();
        $dataProviderTemplateMessages = $searchTemplateMessages->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'settings' => $settings,
            'company' => $company,
            'company_settings' => $company_settings,
            'dataProviderClientStatuses' => $searchModel->search(Yii::$app->request->queryParams, Statuses::TYPE_CLIENT),
            'searchModelUser' => $searchModelUser,
//            'searchModelProxy' => $searchModelProxy,
            'searchModelTemplates' => $searchModelTemplates,
            'dataProviderUser' => $dataProviderUser,
            'dataProviderTemplates' => $dataProviderTemplates,
//            'dataProviderProxy' => $dataProviderProxy,
            'dataProviderDataBases' => $dataProviderDataBases,
            'searchModelAutoReg' => $searchModelAutoReg,
            'dataProviderAutoReg' => $dataProviderAutoReg,
            'searchModelWordsBlackList' => $searchModelWordsBlackList,
            'dataProviderWordsBlackList' => $dataProviderWordsBlackList,
            'searchTemplateMessages' => $searchTemplateMessages,
            'dataProviderTemplateMessages' => $dataProviderTemplateMessages,
            'seachModelSlider' => $searchModelSlider,
            'dataProviderSlider' => $dataProviderSlider,
        ]);

    }

    /**
     * @param $id
     * @return string
     */
    public function actionCreateStatus($id)
    {
        $model = new Statuses;
        $model->type = $id;
        $model->sort = $model->getNewSort($id);


        if (Yii::$app->request->post() && $model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->session->setFlash('successSave');
            }
        }

        $this->layout = 'iframe';
        return $this->render('_create-status', [
            'model' => $model,

        ]);
    }

    /**
     * @param $id
     * @return string
     */
    public function actionUpdateStatus($id)
    {
        $model = $this->findStatus($id);


        if (Yii::$app->request->post() && $model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->session->setFlash('successSave');
            }
        }

        $this->layout = 'iframe';
        return $this->render('_create-status', [
            'model' => $model,

        ]);
    }

    /**
     * @param $id
     * @return array|Response
     */
    public function actionDeleteDatabase($id)
    {
        $request = Yii::$app->request;
        $this->findDatabase($id)->delete();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-database-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
    }

    /**
     * Creates a new Proxy model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateDatabase()
    {
        $request = Yii::$app->request;
        $model = new DataRecipient;
        $model->company_id = FunctionHelper::getCompanyId();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;

            if ($request->isGet) {
                return [
                    'title' => "Создать новую Базу Данных",
                    'content' => $this->renderAjax('/settings/database/_create-database', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            } else if ($model->load($request->post())) {

                $model->file_upload = UploadedFile::getInstance($model, 'file_upload');
                if ($model->file_upload) {

                    if ($model->upload()) {
                        if ($file = fopen($model->file, "r")) {
                            $i = 0;
                            while (!feof($file)) {
                                $line = fgets($file);
                                if ($line != null) {
                                    $i++;
                                }
                            }
                            $model->count = $i;
                            fclose($file);
                            unlink($file);
                        }
                        $model->type = DataRecipient::TYPE_UPLOAD;
                    }
                }

                if ($model->text_data) {
                    $lines = explode("\n", $model->text_data);
                    $i = 0;
                    foreach ($lines as $line) {
                        if ($line != null) {
                            $i++;
                        }
                    }
                    $model->count = $i;
                    $filename = Yii::getAlias('@webroot').'/data/data-' . $model->company_id . '-' . time();
                    if ($file = fopen($filename, "w")) {
                        fwrite($file, $model->text_data);
                        fclose($file);
                        $model->file = $filename;
                    }
                    $model->type = DataRecipient::TYPE_UPLOAD;
                }

                $model->description = 'Загружено ' . date('d.m.Y') . ' в' . date('H:i');
                $model->save();

                return [
                    'forceReload' => '#crud-datatable-database-pjax',
                    'title' => "Создать новую Базу данных",
                    'content' => '<span class="text-success">База данных загружена</span>',
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::a('Создать еще', ['create'], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])

                ];
            } else {
                return [
                    'title' => "Создать новую Базу Данных",
                    'content' => $this->renderAjax('/settings/database/_create-database', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('/settings/database/_create-database', [
                    'model' => $model,
                ]);
            }
        }

    }

    /**
     * @param $id
     * @return null|static
     * @throws NotFoundHttpException
     */
    protected function findDatabase($id)
    {
        if (($model = DataRecipient::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Статус (#' . $id . ') не обнаружен.');
        }
    }

    /**
     * @param $id
     * @return null|static
     * @throws NotFoundHttpException
     */
    protected function findStatus($id)
    {
        if (($model = Statuses::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Статус (#' . $id . ') не обнаружен.');
        }
    }

}
