<?php

namespace app\controllers;

use app\models\Bots;
use app\models\DataRecipient;
use app\models\DispatchRegist;
use app\models\Proxy;
use app\models\uploads\UploadItem;
use Yii;
use yii\helpers\Html;
use app\models\Shop;
use app\models\shop\ShopSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * StoreController implements the CRUD actions for Shop model.
 */
class StoreController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Shop models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (!Yii::$app->user->identity->isSuperAdmin()) {
            return $this->redirect(['/shop/']);
        }
        $searchModel = new ShopSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Shop model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title'=> "Товар : " . $model->name,
                'content'=>$this->renderAjax('view', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::a('Изменить',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
            ];
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new Shop model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {

        $request = Yii::$app->request;
        $model = new Shop();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;

            if($request->isGet){
                return [
                    'title'=> "Создать новый товар",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post()) && $model->save()){

                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Создать новый товар",
                    'content'=>'<span class="text-success">Создать новый товар</span>',
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::a('Создать еще',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])

                ];
            }else{
                return [
                    'title'=> "Создать новый товар",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Закрыть',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Updates an existing Shop model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Изменить",
                    'size' => 'large',
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else if ($model->load($request->post()) && $model->save()) {
                return [
                    'forceReload' => '#crud-datatable-pjax',
                    'size' => 'large',
                    'title' => 'Товар : ' . $model->name,
                    'content' => $this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::a('Изменить', ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
                ];
            } else {
                return [
                    'title' => "Изменить",
                    'size' => 'large',
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionImage($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Изменить",
                    'size' => 'large',
                    'content' => $this->renderAjax('_image', [
                        'model' => $model,
                    ]),
                    'footer' => false,
                ];
            } else if ($model->load($request->post()) && $model->save()) {
                return [
                    'size' => 'large',
                    'title' => 'Изображение товара ' . $model->name,
                    'content' => $this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer' => false,
                ];
            } else {
                return [
                    'title' => "Изменить",
                    'size' => 'large',
                    'content' => $this->renderAjax('_image', [
                        'model' => $model,
                    ]),
                    'footer' => false,
                ];
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['', 'id' => $model->id]);
            } else {
                return $this->render('_image', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionUpload()
    {
        if (Yii::$app->request->isPost) {
            $modelUpload = new UploadItem;
            $modelUpload->fileItem = UploadedFile::getInstance($modelUpload, 'fileItem');
            $modelUpload->upload(Yii::$app->request->post('id'));
            return 1;
        }
    }


    /**
     * Deletes an existing Shop model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        if($bots = Bots::find()->where(['shop_id' => $id])->all()) {
            foreach ($bots as $bot) {
                $bot->shop_id = null;
                $bot->save();
            }
        }

        if($accounts = DispatchRegist::find()->where(['shop_id' => $id])->all()) {
            foreach ($accounts as $account) {
                $account->shop_id = null;
                $account->busy = 1;
                $account->save();
            }
        }

        if($databases = DataRecipient::find()->where(['shop_id' => $id])->all()) {
            foreach ($databases as $database) {
                $database->shop_id = null;
                $database->save();
            }
        }

        if($proxys = Proxy::find()->where(['shop_id' => $id])->all()) {
            foreach ($proxys as $proxy) {
                $proxy->shop_id = null;
                $proxy->busy = 1;
                $proxy->save();
            }
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the Shop model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Shop the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Shop::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Копирование товара.
     *
     * @param $id
     * @throws NotFoundHttpException
     */
    public function actionCopy($id){
        $model = $this->findModel($id);
        $new_model = new Shop();

        $new_model->name = $model->name . ' - копия';
        $new_model->description_short = $model->description_short;
        $new_model->description_long = $model->description_long;
        $new_model->additional_info = $model->additional_info;
        $new_model->type = $model->type;
        $new_model->image = $model->image;
        $new_model->access = $model->access;
        $new_model->price = $model->price;
        $new_model->remains = $model->remains;

        if ($new_model->save()) {
            self::actionIndex();
        } else {
            new \Exception('Ошибка создания записи');
        }
    }
}
