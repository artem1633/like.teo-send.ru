<?php

namespace app\controllers;

use app\models\accounting\AccountingReportSearch;
use app\models\CompanyChat;
use app\models\CompanySettings;
//use app\models\Users;
use app\models\DailyReport;
use app\models\DispatchChats;
use app\models\RegistrationReportFilter;
use app\models\Telegram;
use app\modules\api\controllers\ClientController;
use Yii;
use app\models\Companies;
use app\models\companies\CompaniesSearch;
//use yii\db\Exception;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

//use app\models\Logs;
//use app\models\Drivers;
//use app\models\Auto;
//use app\models\Routes;
//use app\models\Clients;
//use app\models\Objects;

/**
 * CompaniesController implements the CRUD actions for Companies model.
 */
class CompaniesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'companies' => [
                'class' => \app\filters\CompaniesFilter::className(),
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Companies models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (!Yii::$app->user->identity->isSuperAdmin()) {
            return $this->goHome();
        }
        $searchModel = new CompaniesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Почтовое уведомлении о завершении действия лицензии
     * @return mixed
     */
    public function actionEndAccessEmailNotify()
    {
        $date = date('Y-m-d H:i:s');
        /** @var \app\models\Companies[] $companies */
        $companies = Companies::find()->with('admin')->where(['<', 'access_end_datetime', $date])->all();
        foreach ($companies as $company) {
//            try {
//
//            } catch (\Exception $e)
//            {}
            $company->admin->login;
        }

        Yii::$app->session->setFlash('success', 'Уведомления отправлены');
        return $this->redirect(['index']);
    }

    /**
     * Displays a single Companies model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;

        $searchModel = new AccountingReportSearch;
        $dataProviderDebit = $searchModel->searchDebit(Yii::$app->request->queryParams);
        $dataProviderDebit->sort->defaultOrder = ['id' => SORT_DESC];
        $dataProviderShop = $searchModel->searchShop(Yii::$app->request->queryParams);
        $dataProviderShop->sort->defaultOrder = ['id' => SORT_DESC];
        $dataProviderShop->query->andFilterWhere(['company_id' => $id]);
        $dataProviderDebit->query->andFilterWhere(['company_id' => $id]);

        $query2 = DailyReport::find()->where(['company_id' => $id, 'type' => DailyReport::TYPE_VISIT]);

        $visit = new ActiveDataProvider([
            'query' => $query2,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);


        //$refList = Companies::find()->where(['referal' => $id])->all();
        $query = Companies::find()->where(['referal' => $id]);

        $refList = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        $modelRep = new RegistrationReportFilter();
        $modelRep->company = $id;

        $modelRep->load($request->get());

        $report = $modelRep->search();

        if ( $request->post() ) {
            if($request->post()['text'] != ''){
                $chat = new CompanyChat();
                $chat->company_id = $id;
                $chat->telegram_name = 1;
                $chat->text = $request->post()['text'];
                $chat->save(false);
                $company = Companies::findOne($id);
                $company->new_message = 0;
                $company->save(false);
                $userAll = ArrayHelper::getColumn(Telegram::find()->where(['company_id' => $id])->all(), 'telegram_id');
                ClientController::sendTelMessage($userAll, $request->post()['text']);
            }
        }

        $chatText = CompanyChat::find()->where(['company_id' => $id ])->all();
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "компания #" . $id,
                'content' => $this->renderAjax('view', [
                    'model' => $this->findModel($id),
                ]),
                'footer' => Html::button('Отмена', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::a('Изменить', ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
            ];
        } else {
            return $this->render('view', [
                'model' => $this->findModel($id),
                'dataProviderDebit' => $dataProviderDebit,
                'dataProviderShop' => $dataProviderShop,
                'chatText' => $chatText,
                'refList' => $refList,
                'report' => $report,
                'modelRep' => $modelRep,
                'visit' => $visit,

            ]);
        }
    }

    /**
     * Creates a new Companies model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Companies();
        $companySettings = new CompanySettings();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Добавить компании",
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Создать', ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            } else if ($model->load($request->post()) && $model->save()) {
                //Добавляем запись в таблицу с настройками создаваакомпании
                $companySettings->company_id = $model->id;
                $companySettings->save();
                return [
                    'forceReload' => '#crud-datatable-pjax',
                    'title' => "Добавить компании",
                    'content' => '<span class="text-success">Создание компании успешно завершено</span>',
                    'footer' => Html::button('ОК', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::a('Создать еще', ['create'], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])

                ];
            } else {
                return [
                    'title' => "Добавить компании",
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Создать', ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }

    }

    /**
     * Updates an existing Companies model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        $settingsModel = CompanySettings::findOne(['company_id' => $model->id]);

        $settingsModel = $settingsModel ? $settingsModel : new CompanySettings();

        $settingsModel->company_id = $model->id;


        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Изменить компании #" . $id,
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                        'settingsModel' => $settingsModel,
                    ]),
                    'footer' => Html::button('Отмена', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else if (($model->load($request->post()) && $model->save()) && ($settingsModel->load($request->post()) && $settingsModel->save())) {
                return [
                    'forceReload' => '#crud-datatable-pjax',
                    'title' => "компания #" . $id,
                    'content' => $this->renderAjax('view', [
                        'model' => $model,
                        'settingsModel' => $settingsModel,
                    ]),
                    'footer' => Html::button('Отмена', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::a('Изменить', ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
                ];
            } else {
                return [
                    'title' => "Изменить компании #" . $id,
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                        'settingsModel' => $settingsModel,
                    ]),
                    'footer' => Html::button('Отмена', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if (($model->load($request->post()) && $model->save()) && ($settingsModel->load($request->post()) && $settingsModel->save())) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                    'settingsModel' => $settingsModel,
                ]);
            }
        }
    }

    /**
     * @param $id
     * @return array|Response
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;

        $company = $this->findModel($id);
        $company->delete();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    /**
     * Delete multiple existing Companies model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionBulkDelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post('pks')); // Array or selected records primary keys
        foreach ($pks as $pk) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    /**
     * Finds the Companies model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Companies the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Companies::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемой страницы не существует.');
        }
    }

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        if (in_array($action->id, ['create'])) {
            //Добавить проверку на уникальность компании
//            throw new ForbiddenHttpException('Доступ запрещен');
        }

        return parent::beforeAction($action);
    }


}
