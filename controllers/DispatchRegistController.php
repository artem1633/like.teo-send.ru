<?php

namespace app\controllers;

use app\components\helpers\FunctionHelper;
use app\models\AutoRegistr;
use app\models\Companies;
use app\models\Dispatch\DispatchRegistSearch;
use app\models\DispatchStatus;
use app\models\Proxy;
use app\models\RegisterForm;
use app\models\Settings;
use app\models\User;
use app\models\Users;
use app\modules\api\Api;
use app\modules\api\controllers\ClientController;
use app\modules\api\controllers\VkController;
use Yii;
use app\models\DispatchRegist;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * DispatchRegistController implements the CRUD actions for DispatchRegist model.
 */
class DispatchRegistController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all DispatchRegist models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DispatchRegistSearch();
        if (Yii::$app->user->identity->isSuperAdmin()) {
            $dataProvider = $searchModel->search(Yii::$app->request->get());
            $dataProvider->sort->defaultOrder = ['id' => SORT_DESC];
            $dataProvider->pagination = ['pageSize' => 50];
            return $this->render('index', [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
            ]);
        } else {
            $dataProvider = $searchModel->search(Yii::$app->request->get());
            $dataProvider->sort->defaultOrder = ['id' => SORT_DESC];
            $dataProvider->pagination = ['pageSize' => 50];
            $dataProvider->query->andFilterWhere(['company_id' => FunctionHelper::getCompanyId()]);
            return $this->render('index', [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
            ]);
        }

    }

    public function actionShowStatus($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        $proxy = Proxy::find()->one();

        $url = 'https://api.vk.com/method/account.setOnline';
        $params = array(
            'access_token' => $model->token,
            'v' => '5.80'
        );

        // В $result вернется id отправленного сообщения
        $result = file_get_contents($url, false, stream_context_create(array(
            'http' => array(
                'proxy' => "tcp://{$proxy->ip_adress}:{$proxy->port}",
                'request_fulluri' => true,
                'method' => 'POST',
                'header' => 'Content-type: application/x-www-form-urlencoded',
                'content' => http_build_query($params)
            )
        )));


        $result = json_decode($result, true);

        if(!$result['error'] && $model->proxy == null)
        {
            $proxyInstance = $model->getOneAvailableProxy();
            $model->proxy = $proxyInstance->id;
            $model->status = 'ok';
            $model->save(false);
        }

        if($model->username == null && $model->account_url == null)
        {
            DispatchRegist::setVkInfo($model);
        }


        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'forceReload' => '#crud-datatable-pjax',
            ];
        } else {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    public function actionShowStatusAll()
    {
        $request = Yii::$app->request;
        $models = DispatchRegist::find()->all();

        foreach($models as $model)
        {
            $proxy = Proxy::find()->one();

            $url = 'https://api.vk.com/method/account.setOnline';
            $params = array(
                'access_token' => $model->token,
                'v' => '5.80'
            );

            // В $result вернется id отправленного сообщения
            $result = file_get_contents($url, false, stream_context_create(array(
                'http' => array(
                    'proxy' => "tcp://{$proxy->ip_adress}:{$proxy->port}",
                    'request_fulluri' => true,
                    'method' => 'POST',
                    'header' => 'Content-type: application/x-www-form-urlencoded',
                    'content' => http_build_query($params)
                )
            )));


            $result = json_decode($result, true);

            if(!$result['error'] && $model->proxy == null)
            {
                $proxyInstance = $model->getOneAvailableProxy();
                $model->proxy = $proxyInstance->id;
                $model->status = 'ok';
                $model->save(false);
            }

            if($model->username == null && $model->account_url == null)
            {
                DispatchRegist::setVkInfo($model);
            }
        }


        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'forceReload' => '#crud-datatable-pjax',
            ];
        }
    }


    /**
     * Displays a single DispatchRegist model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "Акаунт #" . $id,
                'content' => $this->renderAjax('view', [
                    'model' => $this->findModel($id),
                ]),
                'footer' => Html::button('Закрыть', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::a('Изменить', ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
            ];
        } else {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    public function actionShow()
    {

        if (isset($_GET['unread'])) {
            $id = $_GET['unread'];
            $dataProvider = new ActiveDataProvider([

                'query' => DispatchStatus::find()->where(['new_message' => true, 'send_account_id' => $id]),
            ]);

            return $this->render('/dispatch-status/show', [
                'dataProvider' => $dataProvider,
            ]);
        }

        if (isset($_GET['send'])) {
            $id = $_GET['send'];
            $dataProvider = new ActiveDataProvider([

                'query' => DispatchStatus::find()->where(['send' => true, 'send_account_id' => $id]),
            ]);

            return $this->render('/dispatch-status/show', [
                'dataProvider' => $dataProvider,
            ]);
        }
        if (isset($_GET['conv'])) {
            $id = $_GET['conv'];
            $dataProvider = new ActiveDataProvider([

                'query' => DispatchStatus::find()
                    ->where('status != :status and send_account_id = :send_account_id',
                    ['status' => 4, 'send_account_id' => $id]),
            ]);

            return $this->render('/dispatch-status/show', [
                'dataProvider' => $dataProvider,
            ]);
        }
        if (isset($_GET['ref'])) {
            $id = $_GET['ref'];
            $dataProvider = new ActiveDataProvider([

                'query' => DispatchStatus::find()->where(['status' => 4, 'send_account_id' => $id]),
            ]);

            return $this->render('/dispatch-status/show', [
                'dataProvider' => $dataProvider,
            ]);
        }
        if (isset($_GET['view'])) {
            $id = $_GET['view'];
            $dataProvider = new ActiveDataProvider([

                'query' => DispatchStatus::find()->where(['read' => 'yes', 'send_account_id' => $id]),
            ]);

            return $this->render('/dispatch-status/show', [
                'dataProvider' => $dataProvider,
                'id' => $dataProvider,
            ]);
        }


    }

    public function actionShowAkk()
    {
        $query = DispatchRegist::find()
            ->alias('dr')
            ->joinWith('dispatchs d');
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (isset($_GET['ok'])) {
            $id = $_GET['ok'];

            $query->andFilterWhere([
                '!=','dr.status','account_blocked'
            ]);
            $query->andFilterWhere([
                'd.dispatch_id' => $id,
            ]);


            $dataProvider->pagination = ['pageSize' => 100];
            return $this->render('index', [
                'dataProvider' => $dataProvider,
            ]);
        }

        if (isset($_GET['all'])) {
            $id = $_GET['all'];

            $query->andFilterWhere([
                'd.dispatch_id' => $id,
            ]);

            $dataProvider->pagination = ['pageSize' => 100];
            return $this->render('index', [
                'dataProvider' => $dataProvider,
            ]);
        }

        if (isset($_GET['block'])) {
            $id = $_GET['block'];

            $query->andFilterWhere([
                'dr.status' => 'account_blocked'
            ]);
            $query->andFilterWhere([
                'd.dispatch_id' => $id,
            ]);

            $dataProvider->pagination = ['pageSize' => 100];
            return $this->render('index', [
                'dataProvider' => $dataProvider,
            ]);
        }

    }

    /**
     * @return string
     */
    public function actionShowAll()
    {

        if (isset($_GET['unread'])) {
            $id = $_GET['unread'];
            $dataProvider = new ActiveDataProvider([

                'query' => DispatchStatus::find()->where(['new_message' => true, 'dispatch_id' => $id]),
            ]);

            return $this->render('/dispatch-status/show', [
                'dataProvider' => $dataProvider,
            ]);
        }

        if (isset($_GET['send'])) {
            $id = $_GET['send'];
            $dataProvider = new ActiveDataProvider([

                'query' => DispatchStatus::find()->where(['send' => true, 'dispatch_id' => $id]),
            ]);

            return $this->render('/dispatch-status/show', [
                'dataProvider' => $dataProvider,
            ]);
        }
        if (isset($_GET['conv'])) {
            $id = $_GET['conv'];
            $dataProvider = new ActiveDataProvider([

                'query' => DispatchStatus::find()
                    ->where('status != :status and status IS NOT NULL and dispatch_id = :dispatch_id',
                        ['status' => 4, 'dispatch_id' => $id]),
            ]);

            return $this->render('/dispatch-status/show', [
                'dataProvider' => $dataProvider,
            ]);
        }
        if (isset($_GET['ref'])) {
            $id = $_GET['ref'];
            $dataProvider = new ActiveDataProvider([

                'query' => DispatchStatus::find()->where(['status' => 4, 'dispatch_id' => $id]),
            ]);

            return $this->render('/dispatch-status/show', [
                'dataProvider' => $dataProvider,
            ]);
        }
        if (isset($_GET['view'])) {
            $id = $_GET['view'];
            $dataProvider = new ActiveDataProvider([

                'query' => DispatchStatus::find()->where(['read' => 'yes', 'dispatch_id' => $id]),
            ]);

            return $this->render('/dispatch-status/show', [
                'dataProvider' => $dataProvider,
            ]);
        }


    }

    /**
     * Creates a new DispatchRegist model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new DispatchRegist();

        $maxAccounts = $model->getMaxCountAccounts();
        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;

            if ($model->getCountAccounts() >= $maxAccounts) {
                return [
                    'title' => "Вы создали максимальное разрешенное количество аккаунтов!",
                    'content' => 'Ваш тарифный план предусматривает создание ' . $maxAccounts . ' аккаунтов',
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
                ];
            }

            if ($request->isGet) {
                return [
                    'title' => "Создать новый",
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            } else if ($model->load($request->post()) && $model->save()) {

                DispatchRegist::setVkInfo($model);
                $model->data = date("Y-m-d H:i:s");
                $model->company_id = FunctionHelper::getCompanyId();
                $model->save();

                return [
                    'forceReload' => '#crud-datatable-pjax',
                    'title' => "Создать новый",
                    'content' => '<span class="text-success">Create DispatchRegist success</span>',
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::a('Создать еще', ['create'], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])

                ];
            } else {
                return [
                    'title' => "Создать новый",
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }


    }

    /**
     *перенаправление на странцицу получения токена
     */
    public function actionGetToken()
    {
        $client_id = Settings::find()->where(['key' => 'app_id'])->one();
        $scope = Settings::find()->where(['key' => 'scope'])->one();
        $url = "https://oauth.vk.com/authorize?client_id=" . $client_id->value . "&display=page&redirect_uri=https://oauth.vk.com/blank.html&scope=" . $scope->value . "&response_type=token&v=5.37";
        $this->redirect($url);
    }

    /**
     * @return array|string|Response
     */
    public function actionToken()
    {

        $request = Yii::$app->request;
        $model = new DispatchRegist();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Создать новый",
                    'content' => $this->renderAjax('token', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Получить', ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            } else if ($model->load($request->post()) && $model->save()) {
                $model->data = date("Y-m-d H:i:s");
                $model->save();
                return [
                    'forceReload' => '#crud-datatable-pjax',
                    'title' => "Создать новый",
                    'content' => '<span class="text-success">Create DispatchRegist success</span>',
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::a('Создать еще', ['create'], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])

                ];
            } else {
                return [
                    'title' => "Создать новый",
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }

    }

    /**
     * Updates an existing DispatchRegist model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Изменить  #" . $id,
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else if ($model->load($request->post()) && $model->save()) {
                if($model->username == null || $model->account_url == null)
                {
                    DispatchRegist::setVkInfo($model);
                }
                return [
                    'forceReload' => '#crud-datatable-pjax',
                    'title' => "Изменит #" . $id,
                    'content' => $this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::a('Изменить', ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
                ];
            } else {
                return [
                    'title' => "Изменит #" . $id,
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    public function actionVkCities($q)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $params = [
            'country_id' => 1,
            'q' => $q,
            'access_token' => Settings::findByKey('vk_access_token')->value,
            'v' => '5.37',
        ];

//        $proxy = Settings::findByKey('proxy_server')->value;

        $result = json_decode(file_get_contents('https://api.vk.com/method/database.getCities?'.http_build_query($params)));
        $output = [];
        foreach ($result->response->items as $item)
        {
            $output['results'][] = [
                'id' => $item->id,
                'title' => $item->title,
                'region' => $item->region,
                'area' => $item->area,
            ];
        }

        return $output;
    }

    /**
     * Delete an existing DispatchRegist model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionAuto($id)
    {
        /** @var DispatchRegist $model */
        /** @var AutoRegistr $reg */
        $model = $this->findModel($id);
        $report = $model->makeAutoPage();


        Yii::$app->session->setFlash('status_info', $report);
        return $this->redirect(['index']);
    }

    /**
     * Delete an existing DispatchRegist model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    /**
     * Delete multiple existing DispatchRegist model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post('pks')); // Array or selected records primary keys
        foreach ($pks as $pk) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if ($request->isAjax) {
            /*
            *   Process  for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    /**
     * Finds the DispatchRegist model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DispatchRegist the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DispatchRegist::findOne($id)) !== null && ($model->company_id == FunctionHelper::getCompanyId() || Yii::$app->user->identity->isSuperAdmin())) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
