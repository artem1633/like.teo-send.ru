<?php

namespace app\controllers;

use app\components\helpers\FunctionHelper;
use app\components\helpers\TagHelper;
use app\models\Companies;
use app\models\DispatchRegist;
use app\models\DispatchStatus;
use app\models\proxy\ProxySearch;
use app\models\Settings;
use app\models\Shop;
use app\models\TemplateMessages;
use app\models\User;
use app\models\WordsBlackList;
use app\modules\api\controllers\ClientController;
use Yii;
use app\models\Proxy;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * ProxyController implements the CRUD actions for Proxy model.
 */
class ProxyController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Proxy models.
     * @return mixed
     */
    public function actionIndex()
    {
        $company = !Yii::$app->user->identity->isSuperAdmin() ? FunctionHelper::getCompanyId() : null;

        $searchModel = new ProxySearch;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $company);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @test
     */
    public function actionSys()
    {
//        $tags = ArrayHelper::map(TemplateMessages::find()->where(['or', ['company_id' => FunctionHelper::getCompanyId()], ['company_id' => 1]])->all(), 'tag', 'title');
//
//        var_dump($tags);
//
//        exit;

        $msg = '{greeting}, {dispatchStatus.name}, меня зовут {dispatchRegist.username}. Содержимое: {content}';

        $user = DispatchStatus::find()->one();
        $account = DispatchRegist::find()->one();

        $tags = TemplateMessages::find()->where(['or', ['company_id' => FunctionHelper::getCompanyId()], ['company_id' => 1]])->all();
        $msg = TagHelper::handle($msg, [$account, $user], $tags);

        return $msg;
    }

    /**
     * Displays a single Proxy model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "Прокси #" . $id,
                'content' => $this->renderAjax('view', [
                    'model' => $this->findModel($id),
                ]),
                'footer' => Html::button('Закрыть', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::a('Изменить', ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
            ];
        } else {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new Proxy model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Proxy();
        $maxProxy = $model->getMaxCountProxy();
        if (!Yii::$app->user->identity->isSuperAdmin()) {
            $userModel = Yii::$app->user->identity;
            $model->companyId = $userModel->company_id;
        }

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;

            if ($model->getCountProxy() >= $maxProxy) {
                return [
                    'title' => "Вы создали максимальное разрешенное количество прокси!",
                    'content' => 'Ваш тарифный план предусматривает создание ' . $maxProxy . ' прокси',
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])

                ];
            }

            if ($request->isGet) {
                return [
                    'title' => "Создать новый Прокси",
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            } else if ($model->load($request->post()) && $model->save()) {

                foreach ($request->post()['Proxy']['accounts'] as $value) {

                    $account = DispatchRegist::findOne(['id' => $value]);
                    $account->proxy = $model->id;
                    $account->save();

                }
                return [
                    'forceReload' => '#crud-datatable-proxy-pjax',
                    'title' => "Создать новый Прокси",
                    'content' => '<span class="text-success">Create Proxy success</span>',
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::a('Создать еще', ['create'], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])

                ];
            } else {
                return [
                    'title' => "Создать новый Прокси",
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }

    }

    /**
     * Updates an existing Proxy model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                $accounts = $model->DispatchRegistList($model->id);
                //Сохраняем в сессию список аккаунтов, в которых указан текущий прокси
                Yii::$app->session->set('ProxyAccountsOld', Json::encode($accounts));
                return [
                    'title' => "Изменить Прокси #" . $id,
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else if ($model->load($request->post()) && $model->save()) {
                $accountsProxy = $request->post()['Proxy']['accounts'];
                $oldAccountsProxy = Json::decode(Yii::$app->session->get('ProxyAccountsOld'));
                Yii::$app->session->remove('ProxyAccountsOld');
                //Затираем  прокси у аккаунтов, выбранных ранее
                foreach ($oldAccountsProxy as $value) {
                    $account = DispatchRegist::findOne(['id' => $value]);

                    $account->proxy = 0;

                    Yii::warning('Записано: ' . $account->save(), __METHOD__);
                }

                Yii::warning($accountsProxy, __METHOD__);

                //Записываем прокси указанным аккаунтам

                foreach ($accountsProxy as $value) {
                    $account = DispatchRegist::findOne(['id' => $value]);
                    $account->proxy = $model->id;
                    $account->save();

                }

                return [
                    'forceReload' => '#crud-datatable-proxy-pjax',
                    'title' => "Прокси #" . $id,
                    'content' => $this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::a('Изменить', ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
                ];
            } else {
                return [
                    'title' => "Update Proxy #" . $id,
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing Proxy model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-proxy-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    /**
     * Delete multiple existing Proxy model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post('pks')); // Array or selected records primary keys
        foreach ($pks as $pk) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-proxy-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    /**
     * Finds the Proxy model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Proxy the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Proxy::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
