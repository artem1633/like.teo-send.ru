<?php

namespace app\controllers;

use app\components\AccountsDataPurchase;
use app\components\helpers\FunctionHelper;
use app\models\AccountingReport;
use app\models\Bots;
use app\models\BotsDialogs;
use app\models\Companies;
use app\models\DataRecipient;
use app\models\DispatchRegist;
use app\models\Proxy;
use app\models\Settings;
use app\models\Shop;
use app\modules\api\controllers\ClientController;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * BotsController implements the CRUD actions for Bots model.
 */
class TradeController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function actionIndex()
    {
        $items = Shop::find()->where(['access' => 1])->all();
        return $this->render('index', [
            'items' => $items,
        ]);
    }

    public function actionItem($id)
    {
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    public function actionBuy()
    {
        $transCommited = false;

        Yii::$app->response->format = Response::FORMAT_JSON;
        $statusCode = 200;
        $request = Yii::$app->request;
        if (Yii::$app->request->isAjax) {

            if ($request->post('count') < 1) {
                return false;
            }

            $transaction = Yii::$app->db->beginTransaction();
            $item = $this->findModel($request->post('item'));
            $company = Companies::findOne(FunctionHelper::getCompanyId());
            $allPrice = $item->price * $request->post('count');
            if ($item->getRemains() < $request->post('count')) {
                $buy = false;
                $reason = 'Товара нет в наличии';
                $statusCode = 400;
                $count = $item->getRemains();
            } else {
                if ($company->general_balance < $allPrice) {
                    $buy = false;
                    $reason = 'Недостаточно средств';
                    $statusCode = 402;
                    $count = $item->getRemains();
                } else {

                    $buy = true;

                    if($item->type != Shop::TYPE_ACCOUNT_DATA) {
                        $company->general_balance -= $allPrice;
                        if ($company->save()) {
                            $accountReport = new AccountingReport([
                                'company_id' => $company->id,
                                'operation_type' => AccountingReport::TYPE_OUTPUT_SHOP,
                                'amount' => $allPrice,
                                'description' => 'Покупка товара  "' . $item->name . '" (' . $request->post('count') . ' eд.).',
                            ]);
                            $accountReport->save();
                        }
                    }

                    switch ($item->type) {
                        case Shop::TYPE_ACCOUNT:
                            if ($accounts = DispatchRegist::find()->where(['shop_id' => $item->id])->all()) {
                                $i = 0;
                                foreach ($accounts as $account) {
                                    if ($i >= $request->post('count')) {
                                        break;
                                    }
                                    $account->company_id = $company->id;
                                    $account->shop_id = null;
                                    $account->save();
                                    $i++;
                                }
                            }
                            break;
                        case Shop::TYPE_PROXY:
                            if ($proxys = Proxy::find()->where(['shop_id' => $item->id])->all()) {
                                $i = 0;
                                foreach ($proxys as $proxy) {
                                    if ($i >= $request->post('count')) {
                                        break;
                                    }
                                    $proxy->companyId = $company->id;
                                    $proxy->shop_id = null;
                                    $proxy->save();
                                    $i++;
                                }
                            }
                            break;
                        case Shop::TYPE_BOT:
                            if ($bots = Bots::find()->where(['shop_id' => $item->id])->all()) {
                                $i = 0;
                                foreach ($bots as $bot) {
                                    if ($i >= $request->post('count')) {
                                        break;
                                    }
                                    $cloneBot = new Bots([
                                        'name' => $bot->name . ' (' . date('Y-m-d H:i:s') . ')',
                                        'companyId' => $company->id,
                                    ]);
                                    $cloneBot->save();
                                    if ($dialogModel = BotsDialogs::find()->where(['botId' => $bot->id])->all()) {
                                        foreach ($dialogModel as $dialog) {
                                            $cloneDialog = new BotsDialogs([
                                                'botId' => $cloneBot->id,
                                                'stepNumber' => $dialog->stepNumber,
                                                'result' => $dialog->result,
                                                'notify' => $dialog->notify,
                                                'anyAnswer' => $dialog->anyAnswer,
                                            ]);
                                            $cloneDialog->save();
                                        }
                                    }
                                    $i++;
                                }
                            }
                            break;
                        case Shop::TYPE_DATABASE:
                            if ($dataBases = DataRecipient::find()->where(['shop_id' => $item->id])->all()) {
                                $i = 0;
                                foreach ($dataBases as $dataBase) {
                                    if ($i >= $request->post('count')) {
                                        break;
                                    }

                                    $cloneDataBases = new DataRecipient([
                                        'name' => $dataBase->name . ' (' . date('Y-m-d H:i:s') . ')',
                                        'company_id' => $company->id,
                                        'file' => $dataBase->file,
                                        'type' => DataRecipient::TYPE_PAYD,
                                        'description' => 'Куплена ' . date('d.m.Y H:i:s'),
                                        'count' => $dataBase->count,
                                    ]);
                                    $cloneDataBases->save();
                                    $i++;
                                }
                            }
                            break;
                        case Shop::TYPE_ACCOUNT_DATA:
                            if($statusCode != 200){
                                throw new HttpException($statusCode);
                            }

                            $count = $request->post('count');
                            $purchase = AccountsDataPurchase::makePurchase($item, $company->id, $count);
                            $purchasedAccounts = $purchase->purchasedAccounts;

                            if(is_dir('tmp') == false)
                            {
                                mkdir('tmp');
                            }


                            $accounts = $purchasedAccounts;

                            $result = [
                                'errorText' => '',
                            ];
                            try{
                                foreach ($accounts as $account)
                                {
                                    $account = explode(":", $account);

                                    $dispatchRegist = new DispatchRegist();
                                    $dispatchRegist->login = trim($account[0]);
                                    $dispatchRegist->password = trim($account[1]);
                                    $dispatchRegist->company_id = $company->id;
                                    $dispatchRegist->data = date('Y-m-d H:i:s');
                                    $dispatchRegist->save();
//                                    if($dispatchRegist->token == null){
//                                        $clientIdSetting = Settings::findByKey('app_id');
//                                        $scopeSetting = Settings::findByKey('scope');
//                                        if($clientIdSetting == null || $scopeSetting == null){
//                                            throw new HttpException(500, 'Ошибка параментров настроек администратора');
//                                        }
//                                        if($dispatchRegist->proxy == null){
//                                            throw new HttpException(500, 'Прокси не был установлен у аккаунта');
//                                        }
//                                        $proxy = Proxy::findOne($dispatchRegist->proxy);
//                                        if($proxy == null){
//                                            throw new HttpException(500, 'Прокси не был найден в БД');
//                                        }
//                                        $proxyPks = array_values(ArrayHelper::map(Proxy::find()->all(), 'id', 'id'));
//                                        $randProxyNumber = rand(0, count($proxyPks)-1);
//                                        $proxy = Proxy::findOne($proxyPks[$randProxyNumber]);
//                                        $dispatchRegist->token = get_token($dispatchRegist->login, $dispatchRegist->password, $clientIdSetting->value, $scopeSetting->value, $proxy->fullIp);
//                                        $dispatchRegist->save(false);
//                                    }
                                    DispatchRegist::setVkInfo($dispatchRegist);
                                }
                            } catch (\Exception $e)
                            {
                                $result['errorText'] .= $e->getMessage();
                                ClientController::sendTelMessage('247187885',
                                    "Пользователь \"{$company->company_name}\" пытался совершить покупку \"{$item->name}\" в количестве {$request->post('count')} ед. Но получил ошибку: \"{$e->getMessage()}\"");
                                $transaction->rollBack();
                                return $result;
                            }

//                            $f = fopen('tmp/data_accounts.txt', 'w');
//                            fwrite($f, $purchasedAccounts);
//                            fclose($f);


                            $transCommited = true;
                            $transaction->commit();

                            return $result;
//                            return Yii::$app->response->sendFile('tmp/data_accounts.txt', 'Аккаунты.txt');
                            break;

                    }
                    $reason = 'Вы купили этот товар';
                    $count = $item->getRemains();


                }
            }

            if($statusCode != 200){
                $transaction->rollBack();
                throw new HttpException($statusCode);
            }

            if(!$transCommited)
            {
                $transaction->commit();
            }

            if ($reason == 'Вы купили этот товар') {
                //Отправка уведомления

                $company = FunctionHelper::getCompanyModel();
                ClientController::sendTelMessage('247187885', 'Пользователь ' . $company->company_name . ' совершил покупку "' . $item->name . '"" в количестве ' . $request->post('count') . ' штук');

            }

            Yii::warning($item, __METHOD__);


            return [
                'buy' => $buy,
                'reason' => $reason,
                'count' => $count,
            ];

        }

    }

    protected function findModel($id)
    {
        if (($model = Shop::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
