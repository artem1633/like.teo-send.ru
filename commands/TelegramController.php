<?php

namespace app\commands;

use app\models\Companies;
use app\models\RegisterForm;
use yii\console\Controller;

/**
 * Class TelegramController
 * @package app\commands
 */
class TelegramController extends Controller
{
    public function actionGenerate()
    {
        $companies = Companies::find()->where(['unique_code' => null])->all();

        foreach ($companies as $company)
        {
            $form = new RegisterForm();
            $company->unique_code = $form->generateUniqueCode();
            $company->save(false);
        }
    }
}
