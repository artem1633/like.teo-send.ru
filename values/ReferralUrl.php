<?php

namespace app\values;

use yii\base\Component;
use app\models\Settings;

/**
 * Class ReferralHref
 * @package app\values
 */
class ReferralUrl extends Component
{
    /**
     * Возвращает партерскую ссылку для конкретного ID пользователя
     * @param int $id
     * @return string
     */
    public static function getUrl($id)
    {

        return Settings::findByKey('partner_params_url')->value;
    }
}